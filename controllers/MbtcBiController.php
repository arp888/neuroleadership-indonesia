<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\MbtcAssessment;
use app\modules\administrator\models\HbdiItem;
use app\modules\administrator\models\HbdiAssessmentDetail;
use app\models\Model;

class MbtcBiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessment()
    {
    	$this->layout = 'mbtc-assessment';
        $model = new MbtcAssessment();
                
        if ($model->load(Yii::$app->request->post())) {
            $model->date = time(); 
            $model->save();
            return $this->redirect(['mbtc-assessment-step-one', 'id' => $model->id]);
        } 
        return $this->render('mbtc-assessment', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepOne($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = HbdiItem::find()->orderBy(new \yii\db\Expression('rand()'))->all(); 
        foreach ($items as $i => $item) {
            $details[] = new HbdiAssessmentDetail([
                'hbdi_assessment_id' => $model->id,
                'hbdi_item_id' => $item->id,
            ]);
        }  
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $stepOneTotalScore = 0;
            foreach ($details as $key => $detail) {
                $stepOneTotalScore = $stepOneTotalScore + $detail->score;
            }
            if ($stepOneTotalScore != 160) {                    
                if ($stepOneTotalScore < 160) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-one', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-two', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-one', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepTwo($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = HbdiItem::find()->all(); 
        $details = $model->hbdiAssessmentDetails; 
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }

            // var_dump($totalScore); die(); 
            if ($totalScore != 256) {                    
                if ($totalScore < 256) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-two', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-three', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-two', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepThree($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = HbdiItem::find()->all(); 
        $details = $model->hbdiAssessmentDetails;
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }
            if ($totalScore != 304) {                    
                if ($totalScore < 304) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-three', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-four', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-three', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepFour($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = HbdiItem::find()->all(); 
        $details = $model->hbdiAssessmentDetails;
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }
            if ($totalScore != 304) {                    
                if ($totalScore < 304) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-four', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            } 
            
            $model->is_completed = 1; 
            
            if ($valid) {           	
            	if ($model->save()) {
	    //         	$connection = new \yii\db\Connection([
					//     'dsn' => 'mysql:host=128.199.204.85;dbname=neuroleadership_ind',
					//     'username' => 'aqila',
					//     'password' => '@q1laSab1r',
					//     'emulatePrepare' => true,
					//     // 'enableParamLogging' => true,
					//     'enableProfiling' => true,
					//     'charset' => 'utf8',
					// ]);
					// $connection->open();

					// $connection->createCommand()->insert('hbdi_assessment', [
					//     'id' => $model->id,
					//     'name' => $model->name,
					//     // 'phone' => $model->phone,
					//     // 'email' => $model->email,
					//     'position' => $model->position,
					//     'organization' => $model->organization,
					//     'location' => $model->location,
					//     'date' => $model->date,
					//     'is_completed' => $model->is_completed,
					// ])->execute();

	                foreach ($details as $key => $detail) {
	                    $detail->save(); 
	     //                $connection->createCommand()->insert('hbdi_assessment_detail', [
						//     'id' => $detail->id,
						//     'hbdi_assessment_id' => $detail->hbdi_assessment_id,
						//     'hbdi_item_id' => $detail->hbdi_item_id,
						//     'score' => $detail->score,
						// ])->execute();
	                }

	                return $this->redirect(['mbtc-assessment-result', 'id' => $model->id]);
	            }
            }
        }

        return $this->render('_form-step-four', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentResult($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        if (!$model) {
            return $this->redirect(['mbtc-assessment']);
        }

        return $this->render('mbtc-assessment-result', [
            'model' => $model,            
        ]);
    }    
}
