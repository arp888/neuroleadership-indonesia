<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;
use yii\filters\VerbFilter;

use app\models\Model;
use yii\helpers\Inflector;
use yii\httpclient\Client;
use app\models\LoginForm;
use app\models\ChangePassword;
use app\models\PasswordResetRequest;
use app\models\ResetPassword;

use app\modules\coaching_implementation\models\CoachingParticipantJournal;
use app\modules\coaching_implementation\models\CoachingParticipantJournalSearch;
use app\modules\coaching_implementation\models\CoachingParticipant;


class BsjCoachingController extends Controller
{
    public $layout = 'bsj-coaching-layout';
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'user' => 'externalUser',
                // 'only' => ['logout', 'customer-account', 'subscribe'],
                'except' => ['login', 'request-password-reset', 'reset-password'],
                'rules' => [
                    [
                        // 'actions' => ['logout', 'customer-account', 'subscribe'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $participant = CoachingParticipant::find()->where(['user_id' => Yii::$app->externalUser->identity->id])->one();
        $journals = CoachingParticipantJournal::find()->where(['coaching_participant_id' => $participant->id])->all(); 

        $total = 0;
        $highly_destructive = 0; 
        $value_highly_destructive = 0; 
        $destructive = 0; 
        $value_destructive = 0; 
        $constructive = 0; 
        $value_constructive = 0; 
        $highly_constructive = 0;  
        $value_highly_constructive = 0;  

        $knob_val = []; 

        if ($journals) {
            foreach ($journals as $key => $val) {  
                if ($val->problem_and_situation_scale == 30) {
                    $label = 'Highly Constructive'; 
                    $fgColor = '#1abc9c';
                    $bgColor = '#d1f2eb'; 
                    $highly_constructive = $highly_constructive + 1;
                    $value_highly_constructive = $highly_constructive / count($journals) * 100;                      
                } elseif ($val->problem_and_situation_scale == 20) {
                    $label = 'Constructive';
                    $fgColor = '#1abc9c';
                    $bgColor = '#d8eff8'; 
                    $constructive = $constructive + 1;
                    $value_constructive = $constructive / count($journals) * 100;   
                } elseif ($val->problem_and_situation_scale == 10) {
                    $label = 'Destructive';
                    $fgColor = '#ffbd4a';
                    $bgColor = '#FFE6BA'; 
                    $destructive = $destructive + 1;
                    $value_destructive = $destructive / count($journals) * 100;                 
                } elseif ($val->problem_and_situation_scale == 0) {
                    $label = 'Highly Destructive';
                    $fgColor = '#f05050';
                    $bgColor = '#F9B9B9'; 
                    $highly_destructive = $highly_destructive + 1;
                    $value_highly_destructive = $highly_destructive / count($journals) * 100;
                } else {
                    $label = '';
                }
            }             

        }

        $knob_val = [
            'highly_destructive' => [
                'total' => $highly_destructive,
                'value' => $value_highly_destructive
            ], 
            'destructive' => [
                'total' => $destructive,
                'value' => $value_destructive
            ], 
            'constructive' => [
                'total' => $constructive,
                'value' => $value_constructive
            ],
            'highly_constructive' => [
                'total' => $highly_constructive,
                'value' => $value_highly_constructive
            ]
        ];   


        $start = new \DateTime($participant->coachingGroup->start_date);
        $end = new \DateTime($participant->coachingGroup->end_date);
        $oneday = new \DateInterval("P1D");

        $days = array();
        $dates = [];

        /* Iterate from $start up to $end+1 day, one day in each iteration.
           We add one day to the $end date, because the DatePeriod only iterates up to,
           not including, the end date. */
        foreach(new \DatePeriod($start, $oneday, $end->add($oneday)) as $day) {
            $day_num = $day->format("N"); /* 'N' number days 1 (mon) to 7 (sun) */
            if($day_num < 6) { /* weekday */
                $dates[] = $day->format("Y-m-d");
            } 
        } 

        $progressBarWidth = 100/count($dates); 

        $caseClosed = [];

        $totalCase = 0; 
        $totalCaseClosed = 0; 

        foreach ($dates as $key => $date) {
            $caseClosed[$date] = ['status' => 0];

            foreach ($journals as $journal) {
                if ($date == $journal->date) {
                    $caseClosed[$date] = ['status' => 10, 'journalId' => $journal->id];
                    $totalCase = $totalCase + 1;
                    if ($journal->reappraise != null && $journal->label_bias != 'null' && $journal->decide_and_action != null) {
                        $caseClosed[$date] = ['status' => 20, 'journalId' => $journal->id];
                        $totalCaseClosed = $totalCaseClosed + 1;
                    }
                }
            }
        }

    	return $this->render('index', [
            'knob_val' => $knob_val,
            'journals' => $journals,
            'dates' => $dates,
            'progressBarWidth' => $progressBarWidth,
            'caseClosed' => $caseClosed,
            'totalCase' => $totalCase,
            'totalCaseClosed' => $totalCaseClosed
        ]);
    }


    public function actionCoachingJournal()
    {
        $model = CoachingParticipant::find()->where(['user_id' => Yii::$app->externalUser->identity->id])->one();
        
        $searchModelCoachingJournal = new CoachingParticipantJournalSearch();
        $dataProviderCoachingJournal = $searchModelCoachingJournal->search(Yii::$app->request->queryParams);
        $dataProviderCoachingJournal->query->andWhere(['coaching_participant_id' => $model->id]);

        return $this->render('coaching-journal/index', [
            'model' => $model,
            'searchModelCoachingJournal' => $searchModelCoachingJournal,
            'dataProviderCoachingJournal' => $dataProviderCoachingJournal,
        ]);
    }

    /**
     * Displays a single CoachingParticipantJournal model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewCoachingJournal($id)
    {
        $model = CoachingParticipantJournal::findOne($id);
        return $this->render('coaching-journal/view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CoachingParticipantJournal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateCoachingJournal()
    {
        $model = new CoachingParticipantJournal();        
        $profile = CoachingParticipant::find()->where(['user_id' => Yii::$app->externalUser->identity->id])->one();

        $model->coaching_participant_id = $profile->id;
        $model->date = date('Y-m-d');
     
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (!empty($model->label_bias)) {
                    $model->label_bias = json_encode($model->label_bias);
                }
                $model->save(); 
                \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data dibuat.'), 'message' => Yii::t('app', 'Data berhasil dibuat.')]);
                return $this->redirect(['view-coaching-journal', 'id' => $model->id]);
            }
        } 
        return $this->render('coaching-journal/create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Updates an existing CoachingParticipantJournal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCoachingJournal($id)
    {
        $model = CoachingParticipantJournal::findOne($id);
        $model->label_bias = json_decode($model->label_bias);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (!empty($model->label_bias)) {
                    $model->label_bias = json_encode($model->label_bias);
                }
                $model->save(); 
                \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data diperbarui'), 'message' => Yii::t('app', 'Data berhasil diperbarui.')]);
                return $this->redirect(['view-coaching-journal', 'id' => $model->id]);
            }           
        } 
        return $this->render('coaching-journal/update', [
            'model' => $model,
        ]);
        
    }

    /**
     * Deletes an existing CoachingParticipantJournal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteCoachingJournal($id)
    {
        $model = CoachingParticipantJournal::findOne($id);
        try {
            if ($model->delete()) {
               \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data berhasil dihapus.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Data ini tidak dapat dihapus karena terkait dengan data lain.')]);
        }
        return $this->redirect(['coaching-journal']);
    }


    
    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login-layout';
        if (!Yii::$app->externalUser->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            // return $this->goBack();

            // return $this->goBack();
            return $this->redirect('index');


        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }



    public function actionUserProfile($id)
    {
        $model =  \app\models\UserProfile::findOne($id);

        return $this->render('user-profile', [
            'model' => $model,
        ]);
    }

    public function actionUserProfileUpdate($id)
    {
        $model = \app\models\UserProfile::findOne($id);
        $model->scenario = 'update';
        $oldFile = $model->getImageFile();
        $oldPhoto = $model->avatar;
        
        if ($model->load(Yii::$app->request->post())) {
            $image = $model->uploadImage();

            // var_dump($image); die(); 

            if ($image === false) {
                $model->avatar = $oldPhoto;
            }

            if ($model->save(false)) {
                // var_dump($model->getErrors()); die(); 
                if ($image !== false) { 
                    $path = $model->getImageFile();
                    // var_dump($path); die(); 
                    $image->saveAs($path);
                }
                return $this->redirect(['user-profile', 'id' => $model->id]);
            } 
        } 
        return $this->render('user-profile-update', [
            'model' => $model,
        ]);
        
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->externalUser->logout();

        return $this->redirect('login');
    }

    /**
     * Request reset password
     * @return string
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'login-layout';
        $model = new PasswordResetRequest();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Success'), 'message' => Yii::t('app', 'Check your email for further instructions.')]);
                return $this->redirect(['login']);
            } else {
                \Yii::$app->getSession()->setFlash('error', ['type' => 'warning', 'title' => Yii::t('app', 'Error'), 'message' => Yii::t('app', 'Sorry, we are unable to reset password for email provided.')]);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'login-layout';
        try {
            $model = new ResetPassword($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->resetPassword()) {
            // var_dump($model->getErrors()); die(); 
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Success'), 'message' => Yii::t('app', 'New password was saved.')]);

            return $this->redirect(['login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionChangePassword()
    {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {            
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Password Changed'), 'message' => Yii::t('app', 'Password has successfully changed.')]);
            return $this->redirect(['index']);
            // return $this->goHome();
        }

        return $this->render('change-password', [
            'model' => $model,
        ]);
    }
}
