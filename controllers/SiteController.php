<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;
use yii\filters\VerbFilter;

use app\modules\assessment\models\MbtcItem;
use app\modules\assessment\models\MbtcAssessment;
use app\modules\assessment\models\MbtcAssessmentDetail;
use app\models\Model;
use yii\web\UploadedFile;
use yii\helpers\Inflector;
use yii\httpclient\Client;
use mdm\admin\models\form\ChangePassword;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
    	return $this->render('index', [
        ]);
    }

    
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessment()
    {
    	$this->layout = 'mbtc-assessment';
        $model = new MbtcAssessment();
                
        if ($model->load(Yii::$app->request->post())) {
            $model->date = time(); 
            $model->save();
            return $this->redirect(['mbtc-assessment-step-one', 'id' => $model->id]);
        } 
        return $this->render('mbtc-assessment', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepOne($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->orderBy(new \yii\db\Expression('rand()'))->all(); 
        foreach ($items as $i => $item) {
            $details[] = new MbtcAssessmentDetail([
                'mbtc_assessment_id' => $model->id,
                'mbtc_item_id' => $item->id,
            ]);
        }  
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $stepOneTotalScore = 0;
            foreach ($details as $key => $detail) {
                $stepOneTotalScore = $stepOneTotalScore + $detail->score;
            }
            if ($stepOneTotalScore != 160) {                    
                if ($stepOneTotalScore < 160) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-one', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-two', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-one', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepTwo($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails; 
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }

            // var_dump($totalScore); die(); 
            if ($totalScore != 256) {                    
                if ($totalScore < 256) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-two', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-three', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-two', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepThree($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }
            if ($totalScore != 304) {                    
                if ($totalScore < 304) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-three', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-four', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-three', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepFour($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }
            if ($totalScore != 304) {                    
                if ($totalScore < 304) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-four', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {           	

            	$model->is_completed = 1; 
            	$model->save(); 

    //         	$connection = new \yii\db\Connection([
				//     'dsn' => 'mysql:host=128.199.204.85;dbname=neuroleadership_ind',
				//     'username' => 'aqila',
				//     'password' => '@q1laSab1r',
				//     'emulatePrepare' => true,
				//     // 'enableParamLogging' => true,
				//     'enableProfiling' => true,
				//     'charset' => 'utf8',
				// ]);
				// $connection->open();

				// $connection->createCommand()->insert('mbtc_assessment', [
				//     'id' => $model->id,
				//     'name' => $model->name,
				//     'phone' => $model->phone,
				//     'email' => $model->email,
				//     'position' => $model->position,
				//     'organization' => $model->organization,
				//     'location' => $model->location,
				//     'date' => $model->date,
				// ])->execute();

                foreach ($details as $key => $detail) {
                    $detail->save(); 
     //                $connection->createCommand()->insert('mbtc_assessment_detail', [
					//     'id' => $detail->id,
					//     'mbtc_assessment_id' => $detail->mbtc_assessment_id,
					//     'mbtc_item_id' => $detail->mbtc_item_id,
					//     'score' => $detail->score,
					// ])->execute();
                }

                return $this->redirect(['mbtc-assessment-result', 'id' => $model->id]);
            }
        }

        return $this->render('_form-step-four', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentResult($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        if (!$model) {
        	return $this->redirect(['mbtc-assessment']);
        }

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        } 

        // $score_categories = ['FACTS', 'FUTURE', 'FEELING', 'FORM'];
        $score_categories = ['D. FUTURE = ' . $total_relational, 'C. FEELING = ' . $total_practical, 'B. FORM = ' . $total_experimental, 'A. FACT = ' . $total_analytical];

        $score = [
            'analytical' => $total_analytical,
            'experimental' => $total_experimental,
            'practical' => $total_practical,
            'relational' => $total_relational,
        ];

        $series = [
            [
                'name' => 'Value',
                'data' => [$total_relational, $total_practical, $total_experimental, $total_analytical],
            ],            
        ];

        // echo '<pre>'; 
        // var_dump($score); die(); 

        return $this->render('mbtc-assessment-result', [
            'model' => $model,            
            'details' => $details,
            'score_categories' => $score_categories,
            'score' => $score,
            'series' => $series,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionDownloadAssessmentResult($id)
    {
    	// $this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        } 

        // $score_categories = ['FACTS', 'FUTURE', 'FEELING', 'FORM'];
        $score_categories = ['D. FUTURE = ' . $total_relational, 'C. FEELING = ' . $total_practical, 'B. FORM = ' . $total_experimental, 'A. FACT = ' . $total_analytical];

        $score = [
            'analytical' => $total_analytical,
            'experimental' => $total_experimental,
            'practical' => $total_practical,
            'relational' => $total_relational,
        ];

        $series = [
            [
                'name' => 'Value',
                'data' => [$total_relational, $total_practical, $total_experimental, $total_analytical],
            ],            
        ];

        // echo '<pre>'; 
        // var_dump($score); die(); 

        $response = Yii::$app->html2pdf->render('assessment-result', [
		    	'model' => $model,            
	            'details' => $details,
	            'score_categories' => $score_categories,
	            'score' => $score,
	            'series' => $series,
		    ])
		    ->saveAs('@webroot/files/output.pdf');

		return $response;

       
    }

    
    /**
     * Reset password
     * @return string
     */
    public function actionChangePassword()
    {
        // $this->layout = 'login-layout';
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }

        return $this->render('change-password', [
            'model' => $model,
        ]);
    }

    public function actionUserProfile($id)
    {
        $model =  \app\models\UserProfile::findOne($id);

        return $this->render('user-profile', [
            'model' => $model,
        ]);
    }

    public function actionUserProfileUpdate($id)
    {
        $model = \app\models\UserProfile::findOne($id);
        $model->scenario = 'update';
        $oldFile = $model->getImageFile();
        $oldPhoto = $model->avatar;
        
        if ($model->load(Yii::$app->request->post())) {
            $image = $model->uploadImage();

            // var_dump($image); die(); 

            if ($image === false) {
                $model->avatar = $oldPhoto;
            }

            if ($model->save(false)) {
                // var_dump($model->getErrors()); die(); 
                if ($image !== false) { 
                    $path = $model->getImageFile();
                    // var_dump($path); die(); 
                    $image->saveAs($path);
                }
                return $this->redirect(['user-profile', 'id' => $model->id]);
            } 
        } 
        return $this->render('user-profile-update', [
            'model' => $model,
        ]);
        
    }
}
