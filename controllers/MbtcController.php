<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\Model;
use app\modules\assessment\models\MbtcItem;
use app\modules\assessment\models\MbtcAssessment;
use app\modules\assessment\models\MbtcAssessmentDetail;
use app\modules\assessment\models\MbtcAssessmentGroup;

class MbtcController extends \yii\web\Controller
{
	public $layout = '@app/views/layouts/mbtc-assessment.php';

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        // Yii::$app->session->set('language', $_REQUEST['lang']);

        // $language = Yii::$app->session->get('language');

        $getLang = \Yii::$app->request->get('lang');
                            
        if (isset($getLang)) {
            Yii::$app->session->set('language', $getLang);
        }                           

        if (!Yii::$app->session->has('language')) {
            Yii::$app->session->set('language', 'id');
        }

        $language = Yii::$app->session->get('language');
        Yii::$app->language = $language; 

        return true; // or false to not run the action
    }

    public function actionAssessment($id)
    {
        $model = new MbtcAssessment();
        $model->mbtc_assessment_group_id = $id;
        $group = MbtcAssessmentGroup::findOne($id); 
        $model->organization = $group->group_name;
                
        if ($model->load(Yii::$app->request->post())) {
            $model->date = time(); 
            $model->save();
            return $this->redirect(['mbtc-assessment-step-one', 'id' => $model->id]);
        } 
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepOne($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);


        $details = $model->mbtcAssessmentDetails; 
        if ($details) {
            $items = MbtcItem::find()->all();    
        } else {
            $items = MbtcItem::find()->orderBy(new \yii\db\Expression('rand()'))->all();



            foreach ($items as $i => $item) {
                $details[] = new MbtcAssessmentDetail([
                    'mbtc_assessment_id' => $model->id,
                    'mbtc_item_id' => $item->id,
                ]);
            }    
        }

        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            // var_dump(count($details)); 

            $stepOneTotalScore = 0;
            foreach ($details as $key => $detail) {
                $stepOneTotalScore = $stepOneTotalScore + $detail->score;
            }
            if ($stepOneTotalScore != 160) {                    
                if ($stepOneTotalScore < 160) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-one', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-two', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-one', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepTwo($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails; 
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }

            // var_dump($totalScore); die(); 
            if ($totalScore != 256) {                    
                if ($totalScore < 256) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-two', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-three', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-two', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepThree($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }
            if ($totalScore != 304) {                    
                if ($totalScore < 304) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-three', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {
                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-step-four', 'id' => $model->id
                ]);

            }
        }

        return $this->render('_form-step-three', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentStepFour($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;
        
        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($details, Yii::$app->request->post());
            $valid = $model->validate();
            $valid = Model::validateMultiple($details);

            $totalScore = 0;
            foreach ($details as $key => $detail) {
                $totalScore = $totalScore + $detail->score;
            }
            if ($totalScore != 304) {                    
                if ($totalScore < 304) {
                    \Yii::$app->session->setFlash('error', "Anda memilih kurang dari 8 kata sifat."); 
                } else {
                    \Yii::$app->session->setFlash('error', "Anda memilih lebih dari 8 kata sifat."); 
                }

                return $this->render('_form-step-four', [
                    'model' => $model,
                    'items' => $items,
                    'details' => $details,
                ]);
            }

            if ($valid) {           	

            	$model->is_completed = 1; 
            	$model->save(); 

                foreach ($details as $key => $detail) {
                    $detail->save(); 
                }

                return $this->redirect(['mbtc-assessment-result', 'id' => $model->id]);
            }
        }

        return $this->render('_form-step-four', [
            'model' => $model,
            'items' => $items,
            'details' => $details,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionMbtcAssessmentResult($id)
    {
    	$this->layout = 'mbtc-assessment';
        $model = MbtcAssessment::findOne($id);

        if (!$model) {
        	return $this->redirect(['mbtc-assessment']);
        }

        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        } 

        // $score_categories = ['FACTS', 'FUTURE', 'FEELING', 'FORM'];
        $score_categories = ['D. FUTURE = ' . $total_relational, 'C. FEELING = ' . $total_practical, 'B. FORM = ' . $total_experimental, 'A. FACT = ' . $total_analytical];

        $score = [
            'analytical' => $total_analytical,
            'experimental' => $total_experimental,
            'practical' => $total_practical,
            'relational' => $total_relational,
        ];

        $series = [
            [
                'name' => 'Value',
                'data' => [$total_relational, $total_practical, $total_experimental, $total_analytical],
            ],            
        ];

        return $this->render('mbtc-assessment-result', [
            'model' => $model,            
            'details' => $details,
            'score_categories' => $score_categories,
            'score' => $score,
            'series' => $series,
        ]);
    }

}
