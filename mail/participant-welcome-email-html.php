<?php

// use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

?>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td style="vertical-align: top;" align="center" bgcolor="#F8F8F8">
                    <table id="module-container" class="main-table" style="min-width: 600px;" border="0" width="600" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr id="logo">
                                <td align="center" bgcolor="#FFFFFF">
                                    <table class="inner" border="0" width="535" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="centeralignedmargin no-float full-width text-center mobile-padding-top" border="0" width="200" cellspacing="0" cellpadding="0" align="left">
                                                        <tbody>
                                                            <tr>
                                                                <td class="centeraligned" style="vertical-align: middle;" bgcolor="#FFFFFF">
                                                                    <a href="#" target="_blank" rel="noopener"> 
                                                                        <img class="centeralignedmargin" style="border-style: none;" title="NeuroLeadership Indonesia" src="https://mbtc.leadership.id/images/logo-nlii@2x.png" alt="NeuroLeadership Indonesia" height="80" border="0" /> 
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mobile-height-15" height="15">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr id="header">
                                <td align="center" bgcolor="#FFFFFF">
                                    <table class="inner" border="0" width="535" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td class="text-center header-text-mobile">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" style="font-family: 'Roboto', Arial,Sans-serif; font-size: 14px; color: #333333; font-weight: 300;">
                                                    <span style="font-weight: 300; font-family: 'Roboto', Arial,Sans-serif; font-size: 16px; color: #333333;">
                                                    Hai </span><span style="font-weight: 500; font-family: 'Roboto', Arial,Sans-serif; font-size: 16px; color: #333333;"><?= $participant_name ?></span><span style="font-weight: 300; font-family: 'Roboto', Arial,Sans-serif; font-size: 16px; color: #333333;">, </span>                                                    
                                                    <p style="max-width: 600px; margin: 10px 0px 0px 0px !important; color: #333333;">
                                                        <span class="body-text">
                                                            Berikut adalah informasi login Anda: 
                                                        </span>                            
                                                    </p>

                                                    <p style="max-width: 600px; margin: 10px 0px 0px 15px !important; color: #333333;">
                                                        <span class="body-text">Username: </span><span style="font-weight: 500;"><?= $username ?></span>
                                                    </p>

                                                    <p style="max-width: 600px; margin: 10px 0px 0px 15px !important; color: #333333;">
                                                        <span class="body-text">Password: </span><span style="font-weight: 500;"><?= $password ?></span>
                                                    </p>
                                                    
                                                    <p style="max-width: 600px; margin: 15px 0px 0px 0px !important; color: #333333;">
                                                        <span class="body-text">
                                                            Silahkan masuk ke aplikasi kami menggunakan username dan password di atas.
                                                        </span>
                                                    </p>
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td class="mobile-height-15" height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center" width="250" height="40" bgcolor="#f59f2c" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff; display: block;">
                                                    <?= Html::a('Masuk ke Aplikasi', \yii\helpers\Url::toRoute(['/bsj-coaching'], 'https'), ['target' => 'blank', 'style' => 'padding: 8px 12px; border: 1px solid #f59f2c; border-radius: 5px; font-family:Roboto,Arial,Sans-serif;font-size: 14px; color: #ffffff; text-decoration:none; font-weight:bold;display:inline-block;']) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mobile-height-15" height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" style="font-family: 'Roboto', Arial,Sans-serif; font-size: 14px; color: #333333; font-weight: 300;">
                                                    <p style="max-width: 600px; margin: 10px 0px 0px 0px !important; color: #333333;"><span class="body-text">Jabat Erat,</span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mobile-height-15" height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" style="font-family: 'Roboto', Arial,Sans-serif; font-size: 14px; color: #333333; font-weight: 500;">
                                                    <p style="max-width: 600px; margin: 10px 0px 0px 0px !important; color: #333333;"><span class="body-text">NeuroLeadership Indonesia</span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mobile-height-15" height="35">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>                
                            
                        </tbody>
                    </table>
                    <table class="main-table" style="min-width: 600px;" border="0" width="600" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td width="100%" height="10">
                                    <div style="display: none; white-space: nowrap; line-height: 0; color: #f8f8f8;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!-- END EMAIL -->