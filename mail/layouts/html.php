<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html>
<head>
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"> 
    <meta content="width=device-width" name="viewport"> 
    
    <title>Gadar Medik Indonesia</title> 

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,200,400,300italic,300,400italic,500,500italic,700,700italic" rel="stylesheet" type="text/css">                               
    <!--[if gte mso 12]>
    <style>
        h1{ color:#ffffff;}
        h2 { color:#ffffff;}
        p { color:#ffffff;}
    </style>
    <![endif]-->
    
    <!--[if gte mso 13]>
    <style>
        h2 {padding-top:20px;}
    </style>
    <![endif]-->
    
    <!--[if gte mso 14]>
    <style>
        h1{ color:#ffffff;}
        h2 { color:#ffffff;}
        p { color:#ffffff;}
    </style>
    <![endif]-->

    <!--[if gte mso 15]>
    <style>
        h1{ color:#ffffff;}
        h2 { color:#ffffff;}
        p { color:#ffffff;}
    </style>
    <![endif]-->

    <!--[if mso]>
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic);
        body, table, td, h2, h3, h4, h6, h5, h1, p, span, a { font-family:'Roboto', Arial, sans-serif !important;}
    </style>
    <![endif]--> 
    
    <style type="text/css">
        div, p, a, li, td { -webkit-text-size-adjust:none; }
    </style> 
    
    <!--[if IEMobile]>
    <style type="text/css">
        table.main-table { width: 320px!important; min-width: 320px!important;}
        table.inner { width: 280px!important;}
        table.inner-inside { width: 240px!important;}
        img.inner { width: 280px!important;}
        td.alignleft { text-align: left!important;}
        td.centeraligned { text-align: center!important;}
        table.inline { display: inline-block!important;}
        td.desktop { width: 0px!important; height: 0px!important; visibility: hidden!important; display: none!important; line-height: 0px!important; font-size: 0px!important;}
        table.mobshow {display: inline-block !important; width:100% !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important;}
        td.mobshow {display: inline-block !important; width:100% !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important; max-height: 305px!important;}
        div.mobshow {display: inline-block !important; width:100% !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important; max-height: 305px!important;}
        table.mob-inner { width: 280px!important; display: inline-block !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important; max-height: 305px!important;}
        p.mobhide { font-size:0px!important; line-height:0px!important; display:none!important; margin:0px!important; padding:0px!important;}
    </style>
    <![endif]--> 

    <style type="text/css">
        span.preheader {display:none;height:0px;}
        body{ margin:0px; background:#f8f8f8;}
        /* a{ color:#ffffff; text-decoration:none;} */
        @media only screen and (max-width: 480px){
          table.main-table { width: 320px!important; min-width: 320px!important;}
          table.inner { width: 240px!important;}
          table.leftmargin { width: 280px!important; margin-left: 20px!important;}
          table.inner-inside { width: 240px!important;}
          img.inner { width: 255px!important;}
          td.alignleft { text-align: left!important;}
          td.centeraligned { text-align: center!important;}
          td.centeralignedmargin {margin:0 auto !important;}
          table.centeralignedmargin {margin:0 auto !important;}
          img.centeralignedmargin {margin:0 auto !important;}
          .full-width { width:100% !important;}
          .full-width { width:100% !important;}
          .no-float { float:none !important;}
          table.inline { display: inline-block!important; margin-left: 10px!important;}
          td.desktop { width: 0px!important; height: 0px!important; visibility: hidden!important; display: none!important; line-height: 0px!important; font-size: 0px!important;}
          td.full { display:block !important; width:100% !important;}
          td.mobile-height-15 { height:15px;} 
          img.desktop { width: 0px!important; height: 0px!important; visibility: hidden!important; display: none!important; line-height: 0px!important; font-size: 0px!important;}
          table.mobshow { display: inline-block !important; width:100% !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important;}
          td.mobshow { display: inline-block !important; width:100% !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important; max-height: 305px!important;}
          div.mobshow { display: inline-block !important; width:100% !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important; max-height: 305px!important;}
          table.mob-inner { width: 280px!important; display: inline-block !important; height:305px !important; overflow:visible !important; float:none !important; visibility:visible !important; border:none !important;font-size:15px !important; max-height: 305px!important;}
          p.mobhide { font-size:0px!important; line-height:0px!important; display:none!important; margin:0px!important; padding:0px!important;}
          img.full { width: 320px!important;}
          .mobile-padding-top { padding-top:15px !important;}
          .mobile-padding-lr { padding-right:15px !important; padding-left:15px !important;}            
          .text-center { text-align:center !important; font-size:16px !important;}
          .text-left { text-align:left !important; font-size:16px !important;}
          .data-cards-intro-text { font-size:13px !important; }
          .header-text { font-size:28px !important; padding-bottom:25px !important;}
          .header-text-mobile { font-size:22px !important; padding-bottom:25px !important;}
          .mobile-padding-bottom {padding-bottom:30px !important;}
          .mobile-spacing{ height:5px !important;}
          .text-size-16 { font-size:16px !important;line-height:22px !important;padding:0 20px;}
          .mobile-no-float { float:none !important;margin:0 auto !important;}
        }
        @media only screen and (min-width: 481px){
         td.desktop-padding { vertical-align:top; height:40px;} 
       }

    </style> 

    <!--[if mso]>
    <style type="text/css">
        table[module-table-wrapper]{width: 560px!important;}
        .body-text, body, td, span, div, a, p  {
            font-family: Arial, sans-serif !important;
        }
    </style>
    <![endif]-->
    
    <!--[if IEMobile]>
    <style type="text/css">
        td, span, p, a, h5, h6, div{font-family: Roboto,Arial,Helvetica,sans-serif !important;}
    </style>
    <![endif]-->      

</head>

<body>
    <?php $this->beginBody() ?>

    <?= $content ?>   

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
