<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user mdm\admin\models\User */

?>
<div class="password-reset">
    <p>Hello <?= Html::encode($respondentName) ?>,</p>
    <p>Thank you for completing the MBTC assessment on <?= $dateSubmitted ?>.</p>
    <p>Your assessment report is attached to this email.</p>
    <p>&nbsp;</p>
    <p>- NeuroLeadership Indonesia</p>
</div>
