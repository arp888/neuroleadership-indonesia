<?php
 
namespace app\components\helpers;
 
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use app\modules\assessment\models\MbtcAssessmentGroup;
use app\modules\assessment\models\MbtcAssessment;
use app\modules\assessment\models\MbtcItem;
 
class GenerateReport extends BaseObject implements \yii\queue\JobInterface
{
    public $respondentId;
    public $userId; 
    public $lang;

    public function execute($queue)
    {
        $model = MbtcAssessment::findOne($this->respondentId);  
        
        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        }

        $cerebral = $total_analytical + $total_relational; 
        $limbic = $total_experimental + $total_practical; 
        $left_mode = $total_analytical + $total_experimental; 
        $right_mode = $total_practical + $total_relational;

        $gapCerebralLimbic = abs($cerebral - $limbic);

        $arr = array();
        $pattern = '/([;:,-.\/ X])/';
        $responden_name = '';
        $array = preg_split($pattern, $model->name, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        foreach($array as $k => $v) {
            $responden_name .= ucwords(strtolower($v));
        }

        $path = \Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png';
        $url = \yii\helpers\Url::toRoute(['/assessment/mbtc-assessment/view', 'id' => $model->id], true);

        $x = 288;
        // $y = 672;
        // $y = 665;
        $y = 651.5;
        $width = 919.33;
        $height = 480;
       
        // $executablePath = '/usr/bin/google-chrome';
        $executablePath = '/usr/bin/chromium-browser';

        $puppeteer = new \Nesk\Puphpeteer\Puppeteer([
            'read_timeout' => 240,
            'idle_timeout' => 240
        ]);
        $browser = $puppeteer->launch([
            'executablePath' => $executablePath,
            // 'executable_path' => '/usr/bin/node',
            'headless' => true,
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
            // 'args' => ['--disable-gpu', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'],
        ]);

        $page = $browser->newPage();        
        $page->setViewport(['width' => 1263, 'height' => 1200, 'deviceScaleFactor' => 2]);
        $page->goto($url, ['waitUntil' => 'networkidle2', 'timeout' => 6000]);
        $page->screenshot(['path' => $path, 'clip' => [
            'x' => $x, 'y' => $y, 'width' => $width, 'height' => $height
        ]]);

        $browser->close();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4-L',
            'margin_left' => 16.5,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'fontDir' => array_merge($fontDirs, [
                \Yii::getAlias('@app/web/css/fonts')
            ]),
            'fontdata' => $fontData + [
                'roboto' => [
                    'R' => 'Roboto-Regular.ttf',
                    'I' => 'Roboto-Italic.ttf',
                    'B' => 'Roboto-Bold.ttf',
                ],
                'liberationsans' => [
                    'R' => 'LiberationSans-Regular.ttf',
                    'I' => 'LiberationSans-Italic.ttf',
                    'B' => 'LiberationSans-Bold.ttf',
                ],
                'poppins' => [
                    'R' => 'Poppins-Regular.ttf',
                    'I' => 'Poppins-Italic.ttf',
                    'B' => 'Poppins-Bold.ttf',
                ]
            ],
            'default_font' => 'poppins',
            'default_font_size' => 12,
        ]);

        $template = \Yii::getAlias('@app/web/files/templates/report-temp.pdf');
        if ($this->lang == 'en') {
            $template = \Yii::getAlias('@app/web/files/templates/report-temp-eng.pdf');
        }

        $pagecount = $mpdf->SetSourceFile($template);

        for ($i=1; $i<=$pagecount; $i++) {
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);

            if ($i == 1) {
                $name = $responden_name;
                $position = $model->position;
                $organization = $model->organization;
                $mpdf->text_input_as_HTML = true;
                $mpdf->SetFont('poppins', 'B', 25);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,112);
                $mpdf->Write(0, $name);

                $mpdf->SetFont('poppins', 'I', 13);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,124);
                $mpdf->Write(0, $position);

                $mpdf->SetFont('poppins', 'R', 18);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,130);
                $mpdf->Write(0, $organization);
            }

            if ($i == 2) {
                $img = \yii\helpers\Html::img(\Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png');
                $mpdf->SetXY(8.25,25.87);
                $mpdf->SetMargins(10, 10, 10, true);
                $mpdf->WriteHtml($img);

                $mpdf->SetFont('poppins', 'R', 26);
                $mpdf->SetTextColor(27,117,188);
                $mpdf->SetMargins(10, 10, 10);
                $mpdf->SetXY(10, 185);
                $mpdf->Cell(0, 0, "Cerebral - Limbic = " . $gapCerebralLimbic, 0, true, 'C');
            }

            if ($i < $pagecount)
                $mpdf->AddPage();
        }

        $filePath = \Yii::getAlias('@app/web/files/') . $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
        
        $mpdf->Output($filePath, 'F');
        $filename = $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
        $report_status = $model::REPORT_GENERATED;
        
        \Yii::$app->db->createCommand()->update(
        	'mbtc_assessment', 
        	[
				'filename' => $filename, 
				'report_status' => $report_status
			], 
			'id = :respondentId', [':respondentId' => $this->respondentId]
		)->execute();			
    }
}