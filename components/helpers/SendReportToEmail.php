<?php
 
namespace app\components\helpers;
 
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use app\modules\assessment\models\MbtcAssessmentGroup;
use app\modules\assessment\models\MbtcAssessment;
 
class SendReportToEmail extends BaseObject implements \yii\queue\JobInterface
{
    public $groupId;
    public $respondentId;
    public $userId;

    public function execute($queue)
    {
        // $mail_setting = \Yii::$app->appHelper->setting;
        \Yii::$app->mailer->setTransport([
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com', 
            'username' => 'admin@leadership.id',
            'password' => 'uqtztuytxfujmrmh',
            'port' => 587,
            'encryption' => 'tls',
            'streamOptions' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ],
        ]);

        $model = MbtcAssessment::findOne($this->respondentId);

        if ($model->report_status != $model::REPORT_SENT && $model->filename != null && is_file(\Yii::getAlias('@app/web/files/') . $model->filename)) {
            
            $message = \Yii::$app->mailer->compose(['html' => 'sendReport-html'], [
                'respondentName' => $model->name, 
                'dateSubmitted' => \Yii::$app->formatter->asDate($model->date, 'long'),                  
            ]); 

            $message->setTo($model->email)
                ->setFrom(['admin@leadership.id' => 'NeuroLeadership Indonesia'])
                ->setSubject('MBTC Assessment Report');

            $message->attach(\Yii::getAlias('@app/web/files/') . $model->filename);            

            if ($message->send()) {
                \Yii::$app->db->createCommand()->update(
    	        	'mbtc_assessment', 
    	        	[
    					'report_status' => $model::REPORT_SENT,     										
    				], 
    				'id = :id', [':id' => $model->id]
    			)->execute();                                          
            } else {
                \Yii::$app->db->createCommand()->update(
                    'mbtc_assessment', 
                    [
                        'report_status' => $model::REPORT_FAILED_TO_SEND,                                             
                    ], 
                    'id = :id', [':id' => $model->id]
                )->execute();
            }
        }		
    }
}