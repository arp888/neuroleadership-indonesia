<?php 

namespace app\components;

use Yii; 
use yii\base\Component;
use yii\base\InvalidConfigException;

class AppHelper extends Component
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10; 

	public static function extInfo($filename) 
    {
        if ($filename) {
    		$iconPath = ''; 
    		$file_parts = pathinfo($filename);
    		switch($file_parts['extension'])
    		{
    		    case "doc":
    		    $iconPath = '@web/themes/metronic/app/media/img/files/doc.svg';
    		    break;

    		    case "docx":
    		    $iconPath = '@web/themes/metronic/app/media/img/files/doc.svg';
    		    break;

    		    case "pdf":
    		    $iconPath = '@web/themes/metronic/app/media/img/files/pdf.svg';
    		    break;

                case "zip":
                $iconPath = '@web/themes/metronic/app/media/img/files/zip.svg';
                break;

    		    case "": // Handle file extension for files ending in '.'
    		    case NULL: // Handle no file extension
    		    break;
    		}

    		return $iconPath;
        }
        return false; 
	} 

    public function getTimestampInformation($param) 
    {
        return ($param != 0 ? Yii::$app->formatter->asDateTime($param, 'medium') : '-');   
    }

    public function getBlameableInformation($param) 
    {
        $user = \mdm\admin\models\User::findIdentity($param);        
        if ($user) {
        	$owner = \yii\helpers\Inflector::titleize($user->username, true); 
            // $employee = \app\modules\lms\models\Employee::findOne($user->id); 
            // if ($employee != null) {
            //     $owner = $employee->employee_name;
            // } else {
            //     $owner = \yii\helpers\Inflector::titleize($user->username, true); 
            // }
        } else {
            $owner = '-';
        }

        return $owner;
    }

    public static function getExchangeRate(){      
        $url = 'https://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx';  
        $data = curl_init();
        curl_setopt($data, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
        $results = curl_exec($data);
        curl_close($data);

        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($results);
        libxml_clear_errors();
        $xpath = new \DOMXpath($dom);

        $data = array();
        // get all table rows and rows which are not headers
        $table_rows = $xpath->query('//table[@id="ctl00_PlaceHolderMain_biWebKursTransaksiBI_GridView1"]/tr');
        foreach($table_rows as $row => $tr) {
            foreach($tr->childNodes as $td) {
                $data[$row][] = preg_replace('~[\r\n]+~', '', trim($td->nodeValue));
            }
            $data[$row] = array_values(array_filter($data[$row]));
        }

        $usd_rate = $data[24];
        $kurs_jual = str_replace(';', '.', str_replace(',', '', str_replace('.', ';', $usd_rate[2])));
        $kurs_beli = str_replace(';', '.', str_replace(',', '', str_replace('.', ';', $usd_rate[3])));
        $nilai_tengah = ($kurs_jual + $kurs_beli) /2;
        $result = number_format($nilai_tengah, 2); 
        // print_r($kurs_jual); echo '<br>';
        // print_r($kurs_beli); echo '<br>';
        // print_r($nilai_tengah); echo '<br>';
        return $nilai_tengah;
    }

    public static function getAutoNumber($template, $last_auto_number = 0) 
    {
        $label = '';              
        $label_number = '';

        // echo '<pre>'; 
        // var_dump(json_decode($template, true)); die(); 

        foreach (json_decode($template, true) as $val) {
            switch ($val['value']) {
                case 'YYYY':
                    $str = date('Y'); 
                    $label .= $str;
                    break;
                case 'YY':
                    $str = date('y'); 
                    $label .= $str;
                    break;
                case 'M':
                    $str = date('M'); 
                    $label .= $str;
                    break;
                case 'm':
                    $str = date('m'); 
                    $label .= $str;
                    break;

                default:
                    $next_number = 0; 
                    $running_number_count = 0;

                    foreach (str_split($val['value']) as $key => $str_1) {
                        if ($str_1 == 9) {
                            $running_number_count = $running_number_count + 1;                            
                        } 
                    }

                    if ($running_number_count > 0) {
                        $next_number = $last_auto_number + 1; 
                        $label_number = str_pad($next_number, $running_number_count, '0', STR_PAD_LEFT);                        
                    } else {
                        $label_number = $val['value']; 
                    }

                    $label .= $label_number; 
                    break;
            }
            
        }

        return $label; 
    }

    /**
     * @return array
     */
    public static function getStatusLists()
    {
        return [
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),                       
            self::STATUS_ACTIVE => Yii::t('app', 'Active'), 
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel($status)
    {
        if (isset($status)) {
            switch ($status) {
                case self::STATUS_INACTIVE:
                    $spanClass = 'danger';
                    break;

                case self::STATUS_ACTIVE:
                    $spanClass = 'success';
                    break;
                               
            }

            $val = self::getStatusLists();
            $label = '<span class="badge badge-' . $spanClass . '">' . strtoupper($val[$status]) . '</span>';           
            return $label; 
        }
        return false; 
    }

    public function getSetting() 
    {
        if (($setting = \app\models\GeneralSetting::find()->one()) !== null) {
            return $setting;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    function getConvertMonthToRoman($bln)
    {
        switch ($bln){
            case 1: 
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
    }
}

