<?php

namespace app\modules\scoreboard\controllers;

use Yii; 
use yii\web\Controller;
use app\modules\scoreboard\models\YearlyRevenueTargetPlan; 
use app\modules\scoreboard\models\Revenue; 
use app\modules\scoreboard\models\NetPromoterScore; 
use app\modules\scoreboard\models\NliProposal; 
use app\modules\scoreboard\models\LeadMeasure; 
use app\modules\scoreboard\models\LeadMeasureDetail; 
use app\modules\scoreboard\models\NliEvent; 
use yii\helpers\ArrayHelper; 


/**
 * Default controller for the `scoreboard` module
 */
class DefaultController extends Controller
{
    // public function beforeAction($action)
    // {
    //     if (parent::beforeAction($action)) {
    //         if($action->id=='scoreboard'){
    //             Yii::$app->user->returnUrl = Yii::$app->request->referrer;
    //         }
    //     }
    //     return true;
    // } 

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],            
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	$selected_year = date('Y');

        $model = new \yii\base\DynamicModel(['year']);
        $model->addRule(['year'], 'string', ['max' => 4]);

        $model->year = $selected_year; 

        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            $selected_year = $model->year;
        }



    	$target_plan = YearlyRevenueTargetPlan::find()->where(['year' => $selected_year])->one(); 
    	if ($target_plan) {
    		$target = $target_plan->target; 
    	} else {
    		$target = 2000000000;
    	}

    	$divider = $target / 3; 
    	$min = 0; 
    	$red = $min + $divider; 
    	$yellow = $red + $divider; 
    	$max = $target; 

    	$revenues = Revenue::find()->where(['YEAR(date)' => $selected_year])->all(); 
    	$current_revenue = 0; 
    	if ($revenues) {
    		foreach ($revenues as $rev) {
    			$current_revenue = $current_revenue + $rev->amount; 
    		}
    	} else {
    		$current_revenue = 0; 
    	}

    	$revenue_chart = [
    		'min' => $min,
    		'red' => $red,
    		'yellow' => $yellow,
    		'max' => $max,
    		'current_revenue' => $current_revenue
    	];

    	$net_promoter_scores = NetPromoterScore::find()->where(['YEAR(end_date)' => $selected_year])->all();
    	$current_nps = 0; 
    	$promoters = 0; 
    	$detractors = 0; 
    	$respondents = 0;       


    	if ($net_promoter_scores) {
    		foreach ($net_promoter_scores as $score) {    			
	    		$promoters = $promoters + $score->promoter; 
	    		$detractors = $detractors + $score->detractor; 
	    		$respondents = $respondents + $score->respondent;     		   
    		}

    		$promoter = ($promoters / $respondents) * 100;
    		$detractor = ($detractors / $respondents) * 100; 
    		$current_nps = $promoter - $detractor; 
    	} 

    	$min_nps = -50; 
    	$red_nps = 50;
    	$yellow_nps = 75; 
    	$max_nps = 100;  

    	$nps_chart = [
    		'min' => $min_nps,
    		'red' => $red_nps,
    		'yellow' => $yellow_nps,
    		'max' => $max_nps,
    		'current_nps' => $current_nps
    	];


        // $net_promoter_scores_by_event = NetPromoterScore::find()->where(['YEAR(end_date)' => $selected_year])->groupBy('nli_event_id')->all();

        // $nps_by_event = ArrayHelper::map($net_promoter_scores , 'nli_event_id', 'id', 'nli_event_id');
        $nps_by_event = ArrayHelper::index($net_promoter_scores, null, function ($element) {
            return $element['nli_event_id'];
        });

        // var_dump($nps_by_event); die();
        
        $events = [];
        $promoters = []; 
        $data = [];
        $nps_chart_by_event = [];
        $nps_event_series = [];

        if ($nps_by_event) {

	        foreach ($nps_by_event as $i => $nps) {
	            $event = NliEvent::findOne($i);
	            $events[] = $event->name;

	            $current_nps_event = 0; 
	            $promoters_event = 0; 
	            $detractors_event = 0; 
	            $respondents_event = 0;   

	            foreach ($nps as $j => $val) {
	                 $promoters_event = $promoters_event + $val->promoter;
	                 $detractors_event = $detractors_event + $val->detractor; 
	                 $respondents_event = $respondents_event + $val->respondent;
	            }

	            $promoter_event = ($promoters_event / $respondents_event) * 100;
	            $detractor_event = ($detractors_event / $respondents_event) * 100; 
	            $current_nps_event = $promoter_event - $detractor_event;

	            // var_dump($promoter_event); die(); 

	            $data['promoter'][] = $promoter_event;
	            $data['detractor'][] = $detractor_event;
	            $data['current'][] = $current_nps_event;
	            // $detractors[] = 2;
	            // $current[] = 3;

	            $nps_chart_by_event[] = [
	                'name' => 'Promoter',
	                'data' => [$promoter],
	                'promoter' => $promoter_event,
	                'detractor' => $detractor_event,
	                'current' => $current_nps_event,                
	            ];

	            // $nps_chart_by_event[] = [
	            //     'name' => 'Promoter',
	            //     'data' => [$promoter_event]              
	            // ];
	        }

	        // var_dump($data); die(); 

	        $nps_event_series = [
	            [
	                'name' => 'Promoter', 
	                'data' => $data['promoter'],
	            ],
	            [
	                'name' => 'Dectractor', 
	                'data' => $data['detractor'],
	            ],
	            [
	                'name' => 'Current', 
	                'data' => $data['current'],
	            ]
	        ];
	      }

        // $nps_by_event_series = [];




        // var_dump($nps_event_series); die(); 
        // var_dump($promoters); die(); 

        // [
        //     'name' => 'Jane', 
        //     'data' => [1, 0, 4]
        // ],
        // [
        //     'name' => 'John', 
        //     'data' => [5, 7, 3]
        // ]

        // var_dump($nps_chart_by_event); die();



    	$proposals = NliProposal::find()->where(['YEAR(proposed_at)' => $selected_year])->all();


    	$total_proposal = 0; 
    	$closed_proposal = 0; 
    	$rejected_proposal = 0; 
        $closing_rate = 0; 

    	if ($proposals) {
    		foreach ($proposals as $proposal) {
    			$total_proposal = $total_proposal + 1; 
    			if ($proposal->status == 10 && date('Y', strtotime($proposal->closing_date)) == $selected_year) {
    				$closed_proposal = $closed_proposal + 1; 
    			}
    			if ($proposal->status == 20) {
    				$rejected_proposal = $rejected_proposal + 1; 
    			}
    		}
    		$closing_rate = ($closed_proposal - $rejected_proposal) / $total_proposal * 100; 
    	}

    	$closing_rate_chart = [
    		'min' => ($closing_rate < 0 ? -100 : 0),
    		'red' => 50,
    		'yellow' => 75,
    		'max' => 100,
    		'current_rate' => $closing_rate
    	];

        $lead_charts = [];


        $proposals = NliProposal::find()->where(['YEAR(proposed_at)' => $selected_year])->all();

        $leads = LeadMeasure::find()->where(['is_active' => 10])->all(); 

        // $today = getdate();
        // $monday = strtotime("last monday");
        // $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;

        // $sunday = strtotime(date("Y-m-d",$monday)." +6 days");

        // $this_week_sd = date("Y-m-d",$monday);
        // $this_week_ed = date("Y-m-d",$sunday);

        $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last monday midnight", $previous_week);
        $end_week = strtotime("next sunday", $start_week);

        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        // echo $start_week.' '.$end_week ;

        // die(); 

        // echo "Current week range from $this_week_sd to $this_week_ed ";

        
        foreach ($leads as $key => $lead) {

            $details = LeadMeasureDetail::find()->where(['lead_measure_id' => $lead->id])->andWhere(['between', 'date', $start_week, $end_week])->all(); 
            // $details = $details->andWhere('date' => '2021-11-3'); 
            // $details = $details->nCondition(['date' => '2021-11-3']); 

            // var_dump($details); die(); 

            $stop = $lead->target / 4;             

            $lead_charts[$key] = [
                'title' => $lead->action_name,
                'date_from' => $start_week,
                'date_to' => $end_week,
                'min' => 0,
                'max' => $lead->target,
                'stop' => $stop, 
                'current' => count($details),
            ];
        }


    	// var_dump($closing_rate); 

    	// die(); 

        return $this->render('index', [
            'model' => $model, 
        	'revenue_chart' => $revenue_chart,
        	'nps_chart' => $nps_chart,
        	'closing_rate_chart' => $closing_rate_chart,
            'leads' => $leads,
            'lead_charts' => $lead_charts,
            'events' => $events, 
            'nps_chart_by_event' => $nps_chart_by_event,
            'nps_event_series' => $nps_event_series,
            'selected_year' => $selected_year,
        ]);
    }
}
