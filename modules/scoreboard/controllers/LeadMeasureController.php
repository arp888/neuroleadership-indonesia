<?php

namespace app\modules\scoreboard\controllers;

use Yii;
use app\modules\scoreboard\models\LeadMeasure;
use app\modules\scoreboard\models\LeadMeasureSearch;
use app\modules\scoreboard\models\LeadMeasureDetail;
use app\modules\scoreboard\models\LeadMeasureDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use cornernote\returnurl\ReturnUrl; 

/**
 * LeadMeasureController implements the CRUD actions for LeadMeasure model.
 */
class LeadMeasureController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LeadMeasure models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LeadMeasureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LeadMeasure model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new LeadMeasureDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['lead_measure_id' => $id]);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LeadMeasure model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LeadMeasure();        
        $model->is_active = Yii::$app->params['statusActive'];
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data dibuat.'), 'message' => Yii::t('app', 'Data berhasil dibuat.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new LeadMeasureDetailmodel.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateDetail($id)
    {
        $model = new LeadMeasureDetail();        
        $model->lead_measure_id = $id;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data dibuat.'), 'message' => Yii::t('app', 'Data berhasil dibuat.')]);
            return $this->redirect(['view', 'id' => $id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('create-detail', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LeadMeasure model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data diperbarui'), 'message' => Yii::t('app', 'Data berhasil diperbarui.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LeadMeasureDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateDetail($id)
    {
        $model = $this->findModelDetail($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data diperbarui'), 'message' => Yii::t('app', 'Data berhasil diperbarui.')]);
            return $this->redirect(['view', 'id' => $model->leadMeasure->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('update-detail', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LeadMeasure model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try {
            if ($model->delete()) {
               \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data berhasil dihapus.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Data ini tidak dapat dihapus karena terkait dengan data lain.')]);
        }
        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Deletes an existing LeadMeasure model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteDetail($id)
    {
        $model = $this->findModelDetail($id);
        $returnId = $model->leadMeasure->id; 
        try {
            if ($model->delete()) {
               \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data berhasil dihapus.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Data ini tidak dapat dihapus karena terkait dengan data lain.')]);
        }
        return $this->redirect(['view', 'id' => $model->leadMeasure->id, 'ru' => ReturnUrl::getRequestToken()]);
    }

    /**
     * Finds the LeadMeasure model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LeadMeasure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LeadMeasure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the LeadMeasureDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LeadMeasure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelDetail($id)
    {
        if (($model = LeadMeasureDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
