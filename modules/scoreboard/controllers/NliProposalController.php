<?php

namespace app\modules\scoreboard\controllers;

use Yii;
use app\modules\scoreboard\models\NliProposal;
use app\modules\scoreboard\models\NliProposalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use cornernote\returnurl\ReturnUrl; 

/**
 * NliProposalController implements the CRUD actions for NliProposal model.
 */
class NliProposalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NliProposal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NliProposalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['proposed_at' => SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NliProposal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new NliProposal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NliProposal();        
        $model->status = $model::STATUS_ONGOING;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data dibuat.'), 'message' => Yii::t('app', 'Data berhasil dibuat.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NliProposal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data diperbarui'), 'message' => Yii::t('app', 'Data berhasil diperbarui.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NliProposal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try {
            if ($model->delete()) {
               \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data berhasil dihapus.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Data ini tidak dapat dihapus karena terkait dengan data lain.')]);
        }
        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Finds the NliProposal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NliProposal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NliProposal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
