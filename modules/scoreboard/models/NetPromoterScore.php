<?php

namespace app\modules\scoreboard\models;

use Yii;

/**
 * This is the model class for table "net_promoter_score".
 *
 * @property int $id
 * @property int $nli_event_id
 * @property string $start_date
 * @property string $end_date
 * @property int $promoter
 * @property int $detractor
 * @property int $neutral
 * @property int $respondent
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property NliEvent $nliEvent
 */
class NetPromoterScore extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'net_promoter_score';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nli_event_id', 'start_date', 'end_date', 'promoter', 'detractor', 'neutral', 'respondent'], 'required'],
            [['nli_event_id', 'promoter', 'detractor', 'neutral', 'respondent', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['nli_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => NliEvent::className(), 'targetAttribute' => ['nli_event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nli_event_id' => Yii::t('app', 'Event Name'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'promoter' => Yii::t('app', 'Promoter'),
            'detractor' => Yii::t('app', 'Detractor'),
            'neutral' => Yii::t('app', 'Neutral'),
            'respondent' => Yii::t('app', 'Respondent'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNliEvent()
    {
        return $this->hasOne(NliEvent::className(), ['id' => 'nli_event_id']);
    }
}
