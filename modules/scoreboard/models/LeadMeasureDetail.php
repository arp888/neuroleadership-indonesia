<?php

namespace app\modules\scoreboard\models;

use Yii;

/**
 * This is the model class for table "lead_measure_detail".
 *
 * @property int $id
 * @property int $lead_measure_id
 * @property string $description
 * @property string $date
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property LeadMeasure $leadMeasure
 */
class LeadMeasureDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lead_measure_detail';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lead_measure_id', 'description', 'date'], 'required'],
            [['lead_measure_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['date'], 'safe'],
            [['lead_measure_id'], 'exist', 'skipOnError' => true, 'targetClass' => LeadMeasure::className(), 'targetAttribute' => ['lead_measure_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lead_measure_id' => Yii::t('app', 'Action Name'),
            'description' => Yii::t('app', 'Description'),
            'date' => Yii::t('app', 'Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadMeasure()
    {
        return $this->hasOne(LeadMeasure::className(), ['id' => 'lead_measure_id']);
    }
}
