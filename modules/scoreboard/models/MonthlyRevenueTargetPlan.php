<?php

namespace app\modules\scoreboard\models;

use Yii;

/**
 * This is the model class for table "monthly_revenue_target_plan".
 *
 * @property int $id
 * @property int $yearly_revenue_target_plan_id
 * @property int $month
 * @property double $target
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property YearlyRevenueTargetPlan $yearlyRevenueTargetPlan
 */
class MonthlyRevenueTargetPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monthly_revenue_target_plan';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearly_revenue_target_plan_id', 'month'], 'required'],
            [['yearly_revenue_target_plan_id', 'month', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['target'], 'match', 'pattern' => '/^0|[1-9][\.\d]*(,\d+)?$/'],
            [['yearly_revenue_target_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => YearlyRevenueTargetPlan::className(), 'targetAttribute' => ['yearly_revenue_target_plan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'yearly_revenue_target_plan_id' => Yii::t('app', 'Yearly Revenue Target Plan'),
            'month' => Yii::t('app', 'Month'),
            'target' => Yii::t('app', 'Target'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYearlyRevenueTargetPlan()
    {
        return $this->hasOne(YearlyRevenueTargetPlan::className(), ['id' => 'yearly_revenue_target_plan_id']);
    }
}
