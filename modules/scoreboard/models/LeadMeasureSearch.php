<?php

namespace app\modules\scoreboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\scoreboard\models\LeadMeasure;

/**
 * LeadMeasureSearch represents the model behind the search form about `app\modules\scoreboard\models\LeadMeasure`.
 */
class LeadMeasureSearch extends LeadMeasure
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'target', 'target_interval', 'is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['action_name', 'valid_from', 'valid_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LeadMeasure::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'valid_from' => $this->valid_from,
            'valid_to' => $this->valid_to,
            'target' => $this->target,
            'target_interval' => $this->target_interval,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'action_name', $this->action_name]);

        return $dataProvider;
    }
}
