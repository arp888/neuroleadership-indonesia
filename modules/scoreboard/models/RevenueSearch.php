<?php

namespace app\modules\scoreboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\scoreboard\models\Revenue;

/**
 * RevenueSearch represents the model behind the search form about `app\modules\scoreboard\models\Revenue`.
 */
class RevenueSearch extends Revenue
{

    public $createDateRange;
    public $createDateStart;
    public $createDateEnd;

    public function behaviors()
    {
        return [
            [
                'class' =>  \kartik\daterange\DateRangeBehavior::className(),
                'attribute' => 'createDateRange',
                'dateStartAttribute' => 'createDateStart',
                'dateEndAttribute' => 'createDateEnd',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description', 'date'], 'safe'],
            [['amount'], 'number'],
            [['createDateRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Revenue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        if ($this->createDateRange) {
            $query->andFilterWhere(['>=', 'date', date('Y-m-d', $this->createDateStart)])
                ->andFilterWhere(['<=', 'date', date('Y-m-d', $this->createDateEnd)]);
        }

        return $dataProvider;
    }
}
