<?php

namespace app\modules\scoreboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\scoreboard\models\NliProposal;

/**
 * NliProposalSearch represents the model behind the search form about `app\modules\scoreboard\models\NliProposal`.
 */
class NliProposalSearch extends NliProposal
{
    public $createDateRange;
    public $createDateStart;
    public $createDateEnd;

    public function behaviors()
    {
        return [
            [
                'class' =>  \kartik\daterange\DateRangeBehavior::className(),
                'attribute' => 'createDateRange',
                'dateStartAttribute' => 'createDateStart',
                'dateEndAttribute' => 'createDateEnd',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nli_event_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['proposed_to', 'proposed_at', 'closing_date', 'remarks'], 'safe'],
            [['proposed_price', 'closing_price', 'progress'], 'number'],
            [['createDateRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NliProposal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nli_event_id' => $this->nli_event_id,
            'proposed_at' => $this->proposed_at,
            'proposed_price' => $this->proposed_price,
            'closing_price' => $this->closing_price,
            'progress' => $this->progress,
            'status' => $this->status,
            'closing_date' => $this->closing_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'proposed_to', $this->proposed_to])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        if ($this->createDateRange) {
            $query->andFilterWhere(['>=', 'proposed_at', date('Y-m-d', $this->createDateStart)])
                ->andFilterWhere(['<=', 'proposed_at', date('Y-m-d', $this->createDateEnd)]);
        }

        return $dataProvider;
    }
}
