<?php

namespace app\modules\scoreboard\models;

use Yii;

/**
 * This is the model class for table "lead_measure".
 *
 * @property int $id
 * @property string $action_name
 * @property string $valid_from
 * @property string $valid_to
 * @property int $target
 * @property int $target_interval
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property LeadMeasureDetail[] $leadMeasureDetails
 */
class LeadMeasure extends \yii\db\ActiveRecord
{
	const TARGET_INTERVAL_WEEKLY = 0;
	const TARGET_INTERVAL_MONTHLY = 10; 
	const TARGET_INTERVAL_QUARTERLY = 20; 
	const TARGET_INTERVAL_SEMESTERLY = 30; 
	const TARGET_INTERVAL_YEARLY = 40; 

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lead_measure';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action_name', 'valid_from', 'valid_to', 'target', 'target_interval'], 'required'],
            [['valid_from', 'valid_to'], 'safe'],
            [['target', 'target_interval', 'is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['action_name'], 'string', 'max' => 255],
            [['is_active'], 'default', 'value' => Yii::$app->appHelper::STATUS_ACTIVE],
            [['is_active'], 'in', 'range' => [Yii::$app->appHelper::STATUS_INACTIVE, Yii::$app->appHelper::STATUS_ACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'action_name' => Yii::t('app', 'Action Name'),
            'valid_from' => Yii::t('app', 'Valid From'),
            'valid_to' => Yii::t('app', 'Valid To'),
            'target' => Yii::t('app', 'Target'),
            'target_interval' => Yii::t('app', 'Target Interval'),
            'is_active' => Yii::t('app', 'Is Active?'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadMeasureDetails()
    {
        return $this->hasMany(LeadMeasureDetail::className(), ['lead_measure_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getTargetIntervals()
    {
    	return [
    		self::TARGET_INTERVAL_WEEKLY => 'Weekly',
			self::TARGET_INTERVAL_MONTHLY => 'Montlhy', 
			self::TARGET_INTERVAL_QUARTERLY => 'Quarterly', 
			self::TARGET_INTERVAL_SEMESTERLY => 'Semesterly', 
			self::TARGET_INTERVAL_YEARLY => 'Yearly',
    	];
    }

    /**
     * @return string
     */
    public function getTargetIntervalLabel()
    {
        if (isset($this->target_interval)) {            
            $val = self::getTargetIntervals();
            $label = $val[$this->target_interval];           
            return $label; 
        }
        return false; 
    }
}
