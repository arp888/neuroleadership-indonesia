<?php

namespace app\modules\scoreboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\scoreboard\models\NetPromoterScore;

/**
 * NetPromoterScoreSearch represents the model behind the search form about `app\modules\scoreboard\models\NetPromoterScore`.
 */
class NetPromoterScoreSearch extends NetPromoterScore
{
    public $createDateRange;
    public $createDateStart;
    public $createDateEnd;

    public function behaviors()
    {
        return [
            [
                'class' =>  \kartik\daterange\DateRangeBehavior::className(),
                'attribute' => 'createDateRange',
                'dateStartAttribute' => 'createDateStart',
                'dateEndAttribute' => 'createDateEnd',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nli_event_id', 'promoter', 'detractor', 'neutral', 'respondent', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['createDateRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NetPromoterScore::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nli_event_id' => $this->nli_event_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'promoter' => $this->promoter,
            'detractor' => $this->detractor,
            'neutral' => $this->neutral,
            'respondent' => $this->respondent,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        if ($this->createDateRange) {
            $query->andFilterWhere(['>=', 'start_date', date('Y-m-d', $this->createDateStart)])
                ->andFilterWhere(['<=', 'start_date', date('Y-m-d', $this->createDateEnd)]);
        }

        return $dataProvider;
    }
}
