<?php

namespace app\modules\scoreboard\models;

use Yii;

/**
 * This is the model class for table "yearly_revenue_target_plan".
 *
 * @property int $id
 * @property string $year
 * @property double $target
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property MonthlyRevenueTargetPlan[] $monthlyRevenueTargetPlans
 */
class YearlyRevenueTargetPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yearly_revenue_target_plan';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year', 'target'], 'required'],
            [['year'], 'safe'],
            [['target'], 'match', 'pattern' => '/^0|[1-9][\.\d]*(,\d+)?$/'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'year' => Yii::t('app', 'Year'),
            'target' => Yii::t('app', 'Target'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonthlyRevenueTargetPlans()
    {
        return $this->hasMany(MonthlyRevenueTargetPlan::className(), ['yearly_revenue_target_plan_id' => 'id']);
    }
}
