<?php

namespace app\modules\scoreboard\models;

use Yii;

/**
 * This is the model class for table "nli_proposal".
 *
 * @property int $id
 * @property int $nli_event_id
 * @property string $proposed_to
 * @property string $proposed_at
 * @property double $proposed_price
 * @property double $closing_price
 * @property double $progress
 * @property int $status
 * @property string $closing_date
 * @property string $remarks
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property NliEvent $nliEvent
 */
class NliProposal extends \yii\db\ActiveRecord
{
    const STATUS_ONGOING = 0; 
    const STATUS_CLOSED = 10; 
    const STATUS_REJECTED = 20;  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nli_proposal';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nli_event_id', 'proposed_to', 'proposed_at'], 'required'],
            [['nli_event_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['proposed_at', 'closing_date'], 'safe'],
            [['progress', 'proposed_price', 'closing_price'], 'default', 'value' => 0],
            [['proposed_price', 'closing_price'], 'match', 'pattern' => '/^0|[1-9][\.\d]*(,\d+)?$/'],
            // [['progress'], 'match', 'pattern' => '/^0|[1-9][\.\d]*(,\d+)?$/'],
            [['progress'], 'number', 'numberPattern' => '/^0|[1-9][\.\d]*(,\d+)?$/', 'max' => 100, 'min' => 0],
            [['remarks'], 'string'],
            [['proposed_to'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::STATUS_ONGOING],
            [['status'], 'in', 'range' => [self::STATUS_ONGOING, self::STATUS_CLOSED, self::STATUS_REJECTED]],
            [['nli_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => NliEvent::className(), 'targetAttribute' => ['nli_event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nli_event_id' => Yii::t('app', 'Event Name'),
            'proposed_to' => Yii::t('app', 'Proposed To'),
            'proposed_at' => Yii::t('app', 'Proposed At'),
            'proposed_price' => Yii::t('app', 'Proposed Price'),
            'closing_price' => Yii::t('app', 'Closing Price'),
            'progress' => Yii::t('app', 'Progress'),
            'status' => Yii::t('app', 'Status'),
            'closing_date' => Yii::t('app', 'Closing Date'),
            'remarks' => Yii::t('app', 'Remarks'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNliEvent()
    {
        return $this->hasOne(NliEvent::className(), ['id' => 'nli_event_id']);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ONGOING => 'Ongoing',
            self::STATUS_CLOSED => 'Closed', 
            self::STATUS_REJECTED => 'Rejected', 
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        if (isset($this->status)) {            
            switch ($this->status) {
                case self::STATUS_ONGOING:
                    $spanClass = 'warning';
                    break;

                case self::STATUS_CLOSED:
                    $spanClass = 'success';
                    break;

                case self::STATUS_REJECTED:
                    $spanClass = 'danger';
                    break;
                               
            }

            $val = self::getStatuses();
            $label = '<span class="badge badge-' . $spanClass . '">' . strtoupper($val[$this->status]) . '</span>';           
            return $label;
        }
        return false; 
    }
}
