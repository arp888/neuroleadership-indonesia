<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\datecontrol\DateControl;
use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

// var_dump(\app\modules\scoreboard\models\LeadMeasure::getTargetIntervals()); die();

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasure */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="lead-measure-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
            <?= $form->field($model, 'action_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'valid_from', [
                'selectors' => ['input' => '#leadmeasure-valid_from-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'leadmeasure-valid_from'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('valid_from')) ?>

            <?= $form->field($model, 'valid_to', [
                'selectors' => ['input' => '#leadmeasure-valid_to-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'leadmeasure-valid_to'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('valid_to')) ?>

            <?= $form->field($model, 'target')->textInput() ?>

            <?= $form->field($model, 'target_interval')->widget(Select2::class, [
                'data' => $model::getTargetIntervals(),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('target_interval')) ?>


            <?= $form->field($model, 'is_active')->widget(SwitchInput::classname(), [
                'options' => ['uncheck' => Yii::$app->appHelper::STATUS_INACTIVE, 'value' => Yii::$app->appHelper::STATUS_ACTIVE],
                'inlineLabel' => true,
                'pluginOptions' => [
                   'onColor' => 'success',
                   'onText' => Yii::t('app', 'Active'),
                   'offText' => Yii::t('app', 'Inactive'),
                ],
             ])->hint(false) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
