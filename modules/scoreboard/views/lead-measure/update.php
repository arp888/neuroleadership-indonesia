<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasure */

$this->title = Yii::t('app', 'Perbarui Lead Measure');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lead Measure'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->action_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lead-measure-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
