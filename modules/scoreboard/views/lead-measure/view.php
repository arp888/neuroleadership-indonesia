<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\scoreboard\models\LeadMeasure;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasure */

$this->title = Yii::t('app', 'Detail Lead Measure');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lead Measure'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->action_name;
\yii\web\YiiAsset::register($this);
?>

<div class="lead-measure-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    

                    <?php if (Helper::checkRoute('create')): ?>
                    <?= Html::a(Yii::t('app', 'Create'), ['create', 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-primary ml-1'
                    ]) ?>
                    <?php endif; ?>
          
            
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <?= DetailView::widget([
                    'model' => $model,
                    'options' => ['tag' => false],
                    'template' => '<div class="col-md-4"><div class="mb-3"><h5>{label}</h5>{value}</div></div>',                
                    'attributes' => [
                        'action_name',

                        [
                            'attribute' => 'valid_from',                        
                            'format' => ['date', 'long'],
                        ],
                        [
                            'attribute' => 'valid_to',                        
                            'format' => ['date', 'long'],
                        ],
                        [
                            'attribute' => 'target',                        
                        ],
                        [
                            'attribute' => 'target_interval',                        
                            'format' => 'raw',
                             'value' => function ($model) {
                                return $model->getTargetIntervalLabel();
                            },
                        ],

                        [
                            'attribute' => 'is_active',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Yii::$app->appHelper->getStatusLabel($model->is_active);
                            },
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php if (Helper::checkRoute('create-detail')): ?>
                    <?= Html::a(Yii::t('app', 'Create Detail'), ['create-detail', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-primary ml-1'
                    ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'lead-measure-detail-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    // [
                    //     'attribute' => 'lead_measure_id',
                    //     'filter' => ArrayHelper::map(LeadMeasure::find()->all(), 'id', 'nama_lead_measure'),
                    //     'filterType' => GridView::FILTER_SELECT2,
                    //     'filterWidgetOptions' => [
                    //         'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                    //         'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    //         'pluginOptions' => ['allowClear' => true],
                    //     ],
                    //     'vAlign' => 'middle',
                    //     'value' => function ($model) {
                    //         return $model->leadMeasure->lead_measure_name;
                    //     },
                    // ],
                    [
                        'attribute' => 'description',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'date',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'format' => ['date', 'long'],
                    ],

                    [
                'attribute' => 'updated_at',
                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                'vAlign' => 'middle',
                'mergeHeader' => true,
                'value' => function ($model) {
                    return (new \app\components\AppHelper)->getTimestampInformation($model->updated_at);
                },
            ],
            [
                'attribute' => 'updated_by',
                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                'vAlign' => 'middle',
                'mergeHeader' => true,
                'value' => function ($model) {
                    return (new \app\components\AppHelper)->getBlameableInformation($model->updated_by);
                },
            ],



                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{update-detail}{delete-detail}') . '</div>',
                        'buttons' => [                            
                            'update-detail' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update-detail', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete-detail' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete-detail', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>        