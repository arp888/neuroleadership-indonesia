<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasure */

$this->title = Yii::t('app', 'Update Lead Measure Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lead Measure'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->leadMeasure->action_name, 'url' => ['view', 'id' => $model->leadMeasure->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lead-measure-detail-update">    
    <?= $this->render('_form-detail', [
        'model' => $model,        
    ]) ?>            
</div>
