<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 


/* @var $this yii\web\View */
/* @var $searchModel app\modules\scoreboard\models\LeadMeasureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lead Measure');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="lead-measure-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create Lead Measure'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'lead-measure-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    [
                        'attribute' => 'action_name',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'valid_from',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'valid_to',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'target',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                    ],
                    [
                        'attribute' => 'target_interval',
                        'filter' => $searchModel::getTargetIntervals(),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->getTargetIntervalLabel();
                        },
                    ],
                    [
                        'attribute' => 'is_active',
                        'filter' => Yii::$app->appHelper::getStatusLists(),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return Yii::$app->appHelper->getStatusLabel($model->is_active);
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>