<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasure */

$this->title = Yii::t('app', 'Create Lead Measure');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lead Measure'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-measure-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
