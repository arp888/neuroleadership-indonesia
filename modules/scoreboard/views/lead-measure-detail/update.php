<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasureDetail */

$this->title = Yii::t('app', 'Perbarui Lead Measure Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lead Measure Detail'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lead-measure-detail-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
