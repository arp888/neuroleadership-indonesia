<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasureDetailSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="lead-measure-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'lead_measure_id')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('lead_measure_id'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'description')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('description'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

