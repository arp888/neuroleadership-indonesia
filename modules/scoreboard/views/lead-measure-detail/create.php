<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\LeadMeasureDetail */

$this->title = Yii::t('app', 'Create Lead Measure Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lead Measure Detail'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-measure-detail-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
