<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NliEvent */

$this->title = Yii::t('app', 'Update Event Master');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Master'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="nli-event-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
