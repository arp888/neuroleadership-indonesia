<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NliEvent */

$this->title = Yii::t('app', 'Create Event Master');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Master'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="nli-event-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
