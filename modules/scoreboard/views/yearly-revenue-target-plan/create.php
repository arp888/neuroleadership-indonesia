<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\YearlyRevenueTargetPlan */

$this->title = Yii::t('app', 'Create Yearly Revenue Target Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Yearly Revenue Target Plan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yearly-revenue-target-plan-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
