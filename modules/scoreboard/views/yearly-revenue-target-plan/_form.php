<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\YearlyRevenueTargetPlan */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="yearly-revenue-target-plan-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
            <?= $form->field($model, 'year', [
                'selectors' => ['input' => '#yearlyrevenuetargetplan-year-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'yearlyrevenuetargetplan-year'],
                'displayFormat' => 'yyyy',
                'saveFormat' => 'yyyy',
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Year')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'startView'=>'year',
                        'minViewMode'=>'years',
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('year')) ?>

            <?= $form->field($model, 'target')->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' =>'form-control', 'placeholder' => $model->getAttributeLabel('target')],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'allowMinus' => false,
                    'radixPoint'=> ',',
                    'removeMaskOnSubmit' => true,
                    'rightAlign' => false,
                    'unmaskAsNumber' => true
                ],
            ])->hint(Yii::t('app', 'Enter') . ' ' . $model->getAttributeLabel('target')) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
