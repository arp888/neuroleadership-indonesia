<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\scoreboard\models\YearlyRevenueTargetPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Yearly Revenue Target Plan');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="yearly-revenue-target-plan-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create Yearly Revenue Target Plan'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'yearly-revenue-target-plan-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    [
                        'attribute' => 'year',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'target',
                        'mergeHeader' => true,
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->target, 0);
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>