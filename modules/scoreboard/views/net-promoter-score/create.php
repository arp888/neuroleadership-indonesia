<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NetPromoterScore */

$this->title = Yii::t('app', 'Create Net Promoter Score');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Net Promoter Score'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="net-promoter-score-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
