<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NetPromoterScoreSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="net-promoter-score-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'nli_event_id')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('nli_event_id'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'start_date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('start_date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'end_date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('end_date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'promoter')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('promoter'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'detractor')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('detractor'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'neutral')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('neutral'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'respondent')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('respondent'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

