<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NetPromoterScore */

$this->title = Yii::t('app', 'Perbarui Net Promoter Score');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Net Promoter Score'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="net-promoter-score-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
