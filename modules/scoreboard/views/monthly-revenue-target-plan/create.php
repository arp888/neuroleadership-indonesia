<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\MonthlyRevenueTargetPlan */

$this->title = Yii::t('app', 'Create Monthly Revenue Target Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monthly Revenue Target Plan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monthly-revenue-target-plan-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
