<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\MonthlyRevenueTargetPlan */

$this->title = Yii::t('app', 'Perbarui Monthly Revenue Target Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monthly Revenue Target Plan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="monthly-revenue-target-plan-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
