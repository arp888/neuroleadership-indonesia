<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\scoreboard\models\YearlyRevenueTargetPlan;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\MonthlyRevenueTargetPlan */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="monthly-revenue-target-plan-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
                    <?= $form->field($model, 'yearly_revenue_target_plan_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(YearlyRevenueTargetPlan::find()->all(), 'id', 'yearly_revenue_target_plan_name'),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('yearly_revenue_target_plan_id')) ?>

            <?= $form->field($model, 'month')->textInput() ?>

            <?= $form->field($model, 'target')->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' =>'form-control', 'placeholder' => $model->getAttributeLabel('target')],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'allowMinus' => false,
                    'radixPoint'=> ',',
                    'removeMaskOnSubmit' => true,
                    'rightAlign' => false,
                    'unmaskAsNumber' => true
                ],
            ])->hint(Yii::t('app', 'Enter') . ' ' . $model->getAttributeLabel('target')) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
