<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NliProposal */

$this->title = Yii::t('app', 'Create Nli Proposal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nli Proposal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nli-proposal-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
