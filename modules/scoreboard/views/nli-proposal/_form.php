<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use kartik\switchinput\SwitchInput;
use app\modules\scoreboard\models\NliEvent;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NliProposal */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="nli-proposal-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
            <?= $form->field($model, 'nli_event_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(NliEvent::find()->all(), 'id', 'name'),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('nli_event_id')) ?>

            <?= $form->field($model, 'proposed_to')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'proposed_at', [
                'selectors' => ['input' => '#nliproposal-proposed_at-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'nliproposal-proposed_at'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('proposed_at')) ?>

            <?= $form->field($model, 'proposed_price', [
                'template' => '{label}{beginWrapper}<div class="input-group"><div class="input-group-prepend"><span class="input-group-text">IDR</span></div>{input}</div>{hint}{error}{endWrapper}'
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' =>'form-control', 'placeholder' => $model->getAttributeLabel('proposed_price')],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'allowMinus' => false,
                    'radixPoint'=> ',',
                    'removeMaskOnSubmit' => true,
                    'rightAlign' => false,
                    'unmaskAsNumber' => true
                ],
            ])->hint(Yii::t('app', 'Enter') . ' ' . $model->getAttributeLabel('proposed_price')) ?>

            <?php if (!$model->isNewRecord): ?>

            <?= $form->field($model, 'progress', [
                'template' => '{label}{beginWrapper}<div class="input-group">{input}<div class="input-group-append"><span class="input-group-text">%</span></div></div>{hint}{error}{endWrapper}'
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' =>'form-control', 'placeholder' => $model->getAttributeLabel('progress')],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'allowMinus' => false,
                    'radixPoint'=> ',',
                    'removeMaskOnSubmit' => true,
                    'rightAlign' => false,
                    'unmaskAsNumber' => true
                ],
            ])->hint(Yii::t('app', 'Enter') . ' ' . $model->getAttributeLabel('progress')) ?>

            <?= $form->field($model, 'status')->widget(Select2::class, [
                'data' => $model::getStatuses(),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('status')) ?>           

            <?= $form->field($model, 'closing_date', [
                'selectors' => ['input' => '#nliproposal-closing_date-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'nliproposal-closing_date'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('closing_date')) ?>

            <?= $form->field($model, 'closing_price', [
                'template' => '{label}{beginWrapper}<div class="input-group"><div class="input-group-prepend"><span class="input-group-text">IDR</span></div>{input}</div>{hint}{error}{endWrapper}'
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' =>'form-control', 'placeholder' => $model->getAttributeLabel('closing_price')],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'autoGroup' => true,
                    'allowMinus' => false,
                    'radixPoint'=> ',',
                    'removeMaskOnSubmit' => true,
                    'rightAlign' => false,
                    'unmaskAsNumber' => true
                ],
            ])->hint(Yii::t('app', 'Enter') . ' ' . $model->getAttributeLabel('closing_price')) ?>

            <?php endif; ?>

            <?= $form->field($model, 'remarks')->widget(\dosamigos\tinymce\TinyMce::classname(), [
                'options' => ['rows' => 3],
                // 'language' => 'id',
                'clientOptions' => [
                'branding' => false,
                'menubar' => false,
                'relative_urls' => false,
                'remove_script_host' => false,
                'convert_urls' => true,
                'plugins' => [
                'advlist autolink lists link charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste image'
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
                ] 
            ])->hint(Yii::t('app', 'Pilih' . ' ' . $model->getAttributeLabel('remarks'))) ?>            

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
