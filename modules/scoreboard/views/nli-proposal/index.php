<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\ArrayHelper;
use app\modules\scoreboard\models\NliEvent;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\scoreboard\models\NliProposalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Proposal');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="nli-proposal-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">                    
                <div class="ml-auto">
                    <?php $form = \yii\bootstrap4\ActiveForm::begin([
                        'action' => ['index'],
                        'id' => 'nliproposal-search-form',
                        'method' => 'get',
                        'layout' => 'inline',        
                        'enableClientValidation'=>false,
                        'enableAjaxValidation'=>false,
                        'fieldConfig' => [
                            'template' => "{label}\n{input}\n{hint}\n{error}\n",
                        ],
                    ]); ?>

                    <?= $form->field($searchModel, 'createDateRange')->widget(\kartik\daterange\DateRangePicker::classname(), [
                            // 'useWithAddon'=>true,
                            // 'language' => 'id',
                            'options' => ['class' => 'form-control', 'placeholder' => 'Select Date Range...'],
                            'convertFormat' => true,
                            'autoUpdateOnInit' => true, 
                            'startAttribute' => 'createDateStart',
                            'endAttribute' => 'createDateEnd',
                            // 'startInputOptions' => ['value' => date('Y-01-01')],
                            // 'endInputOptions' => ['value' => date('Y-12-31')],
                            'presetDropdown' => true,
                            'hideInput'=>true,
                            'pluginOptions' => [
                                'timePicker'=>false,
                                'showDropdowns'=>true,
                                'todayHighlight' => true,
                                'locale'=>[
                                    'format' => 'd M Y',
                                    'lang' => 'id',
                                    'separator' => ' - ',
                                ],
                                'opens' => 'left',                                
                            ],
                            'pluginEvents' => [
                                "apply.daterangepicker" => "function(e) { $(e.target).closest('form').submit(); }",   
                            ],

                        ])->label(false)->error(false) 
                    ?>
                    
                    
                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create Proposal'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary ml-1']); 
                        }
                    ?>

                    <?php \yii\bootstrap4\ActiveForm::end(); ?>

                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'nli-proposal-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'showPageSummary' => true,
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    [
                        'attribute' => 'nli_event_id',
                        'filter' => ArrayHelper::map(NliEvent::find()->all(), 'id', 'name'),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->nliEvent->name;
                        },
                    ],
                    [
                        'attribute' => 'proposed_to',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                    ],
                    [
                        'attribute' => 'proposed_at',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    
                    [
                        'attribute' => 'proposed_price',
                        'mergeHeader' => true,
                        'vAlign' => 'middle',
                        'format' => ['decimal', 0],
                        'pageSummary'=>true,
                        'pageSummaryFunc' => GridView::F_SUM,
                        'pageSummaryOptions' => ['class' => 'kt-shape-bg-color-3 kt-font-light kt-font-bolder'],

                    ],

                    [
                        'attribute' => 'closing_price',
                        'mergeHeader' => true,
                        'vAlign' => 'middle',
                        'format' => ['decimal', 0],
                        'pageSummary'=>true,
                        'pageSummaryFunc' => GridView::F_SUM,
                        'pageSummaryOptions' => ['class' => 'kt-shape-bg-color-3 kt-font-light kt-font-bolder'],
                    ],

                    [
                        'attribute' => 'progress',
                        'mergeHeader' => true,
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->progress, 0) . '%';
                        },
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => $searchModel::getStatuses(),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->getStatusLabel();
                        },
                    ],
                    // [
                    //     'attribute' => 'closing_date',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                    //     'vAlign' => 'middle',
                    // ],
                    // [
                    //     'attribute' => 'remarks',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                    //     'vAlign' => 'middle',
                    // ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>

<?php 
$clearDateRangePicker = <<<JS
$('#nliproposalsearch-createdaterange').on('cancel.daterangepicker', function(ev, picker) {
    $('#nliproposalsearch-createdaterange, #nliproposalsearch-createdaterange-start, #nliproposalsearch-createdaterange-end').val(null);
    $('#nliproposal-search-form').yiiActiveForm('submitForm');
    // $.pjax.reload({container: '#nliproposal-index-pjax'});
});
JS;
$this->registerJs($clearDateRangePicker);
?>