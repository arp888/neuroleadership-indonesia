<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NliProposalSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="nli-proposal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'nli_event_id')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('nli_event_id'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'proposed_to')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('proposed_to'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'proposed_at')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('proposed_at'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'progress')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('progress'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'status')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('status'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'closing_date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('closing_date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'remarks')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('remarks'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

