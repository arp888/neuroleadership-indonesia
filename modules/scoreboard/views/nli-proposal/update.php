<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\NliProposal */

$this->title = Yii::t('app', 'Update Proposal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proposal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="nli-proposal-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
