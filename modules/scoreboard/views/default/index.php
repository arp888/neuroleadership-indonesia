<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\production\models\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = 'Dashboard';

// $periods = (new yii\db\Query())
//     ->select('YEAR(date) as year, MONTH(date) as month')
//     ->from('yearly_revenue_target_plan')
//     ->distinct()
//     ->orderBy('month, year')
//     ->all();

$years = ['2022' => '2022', '2021' => '2021', '2020' => '2020', '2019' => '2019'];

?>

<div class="d-flex align-items-between mb-3">
	<div class="ml-auto">
		<?php $form = \yii\bootstrap4\ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
	        // 'layout' => 'inline',        
	        'enableClientValidation'=>false,
	        'enableAjaxValidation'=>false,
	        // 'fieldConfig' => [
	        //     'template' => "{label}\n{input}\n{hint}\n{error}\n",
	        // ],
	    ]); ?>    

    <?= $form->field($model, 'year', [
            'options' => ['class' => 'form-inline'],       
        ])->widget(Select2::classname(), [
            'data' => $years,
            'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select Year'), 'value' => $selected_year],
            'theme' => Select2::THEME_DEFAULT,
            'pluginOptions' => [
                'allowClear' => true
            ],
            'pluginEvents' => [
                "change" => 'function() {                                         
                    this.form.submit();
                }',                
            ],
        ])->label(false)->hint(false);
    ?>

	<?php \yii\bootstrap4\ActiveForm::end(); ?>
	</div>
</div>

	

<div class="scoreboard-default-index">
	<div class="row">		

		<!-- NPS CHART -->

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h4 class="header-title">1. NPS</h4>
					<?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
		                            'type' => 'solidgauge',
		                            'type' => 'gauge',
		                            'height' => 275,
		                            'plotBackgroundColor' => null,
		                            'plotBackgroundImage' => null,
		                            'plotBorderWidth' => 0,
		                            'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'margin' => [0,0,0,0]
		                        ],    
		                        'title' => false, 
                                
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $nps_chart['min'],
                                    'max' => $nps_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        // 'formatter' => new JsExpression("
                                        //     function() {
                                        //         if (this.value >= 1000000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                        //         } else if (this.value > 1000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000, 0) + 'J'
                                        //         } else if (this.value > 1000) {
                                        //             return Highcharts.numberFormat(this.value / 1000, 0) + 'R';
                                        //         } else {
                                        //             return this.value
                                        //         }
                                        //     }
                                        // "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'POINT'
                                    ],
                                    'plotBands' => [[
                                        'from' => $nps_chart['min'],
                                        'to' => $nps_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $nps_chart['red'],
                                        'to' => $nps_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $nps_chart['yellow'],
                                        'to' => $nps_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'NPS',
                                    'data' => [$nps_chart['current_nps']],
                                    'tooltip' => [
                                        'valueSuffix' => ''
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
				</div>
			</div>
					
		</div>

		<!-- NCS CHART -->

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h4 class="header-title">2. NCS</h4>
					<?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
		                            'type' => 'solidgauge',
		                            'type' => 'gauge',
		                            'height' => 275,
		                            'plotBackgroundColor' => null,
		                            'plotBackgroundImage' => null,
		                            'plotBorderWidth' => 0,
		                            'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'margin' => [0,0,0,0]
		                        ],    
		                        'title' => false, 
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $closing_rate_chart['min'],
                                    'max' => $closing_rate_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        // 'formatter' => new JsExpression("
                                        //     function() {
                                        //         if (this.value >= 1000000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                        //         } else if (this.value > 1000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000, 0) + 'J'
                                        //         } else if (this.value > 1000) {
                                        //             return Highcharts.numberFormat(this.value / 1000, 0) + 'R';
                                        //         } else {
                                        //             return this.value
                                        //         }
                                        //     }
                                        // "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'PERCENT'
                                    ],
                                    'plotBands' => [[
                                        'from' => $closing_rate_chart['min'],
                                        'to' => $closing_rate_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $closing_rate_chart['red'],
                                        'to' => $closing_rate_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $closing_rate_chart['yellow'],
                                        'to' => $closing_rate_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'Rate',
                                    'data' => [$closing_rate_chart['current_rate']],
                                    'tooltip' => [
                                        'valueSuffix' => '%'
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
				</div>
			</div>
					
		</div>

		<!-- REVENUE CHART -->

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="d-flex justify-content-between align-items-center">
						<h4 class="header-title">3. Revenue</h4>
						<!-- <div class="ml-auto">
							dafafaf
						</div> -->
					</div>
					<?php
		                echo Highcharts::widget([
		                    'scripts' => [
		                        'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
		                        // 'modules/solid-gauge',
		                        'themes/grid-light',
		                    ],
		                    'options' => [
		                        'chart' => [
		                            'type' => 'solidgauge',
		                            'type' => 'gauge',
		                            'height' => 275,
		                            'plotBackgroundColor' => null,
		                            'plotBackgroundImage' => null,
		                            'plotBorderWidth' => 0,
		                            'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'margin' => [0,0,0,0]
		                        ],    
		                        'title' => false,     
		                        // 'title' => [
		                        //     'text' => 'Revenue',
		                        //     'margin' => 0,
		                        //     'style' => [
		                        //         "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
		                        //         'fontSize' => '14px',
		                        //         'fontWeight' => 'regular',
		                        //         'color' => "#414042",
		                        //         'borderWidth' => 0,
		                        //         'textShadow' => false,
		                        //         'textOutline' => false  
		                        //     ]
		                            
		                        // ],
		                        
		                        'credits' => [
		                            'enabled' => false
		                        ],

		                        'pane' => [
		                            'startAngle' => -150,
		                            'endAngle' => 150,
		                            'background' => [
		                                [
		                                    'backgroundColor' => [
		                                        'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
		                                        'stops' => [
		                                            [0, '#FFF'],
		                                            [1, '#333']
		                                        ]
		                                    ],
		                                    'borderWidth' => 0,
		                                    'outerRadius' => '109%'
		                                ], 
		                                [
		                                    'backgroundColor' => [
		                                        'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
		                                        'stops' => [
		                                            [0, '#333'],
		                                            [1, '#FFF']
		                                        ]
		                                    ],
		                                    'borderWidth' => 1,
		                                    'outerRadius' => '107%'
		                                ], 
		                                [
		                                    // default background
		                                ], 
		                                [
		                                    'backgroundColor' => '#DDD',
		                                    'borderWidth' => 0,
		                                    'outerRadius' => '105%',
		                                    'innerRadius' => '103%'
		                                ]
		                            ],
		                        ],                                

		                        // the value axis
		                        'yAxis' => [
		                            'min' => $revenue_chart['min'],
		                            'max' => $revenue_chart['max'],
		                            // 'minorTickInterval' => 'auto',
		                            // 'minorTickWidth' => 0,
		                            // 'minorTickLength' => 10,
		                            // 'minorTickPosition' => 'inside',
		                            // 'minorTickColor' => '#666',
		                            // 'tickPixelInterval' => 10,
		                            // 'tickWidth' => 0,
		                            // 'tickPosition' => 'inside',
		                            // 'tickLength' => 10,
		                            // 'tickColor' => '#666',
		                            'showFirstLabel' => true,
		                            'showLastLabel' => true,
		                            'labels' => [
		                                'step' => 1,
		                                'rotation' => 'auto',
		                                'formatter' => new JsExpression("
		                                    function() {
		                                        if (this.value >= 1000000000) {
		                                            return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
		                                        } else if (this.value > 1000000) {
		                                            return Highcharts.numberFormat(this.value / 1000000, 0) + 'Jt'
		                                        } else if (this.value > 1000) {
		                                            return Highcharts.numberFormat(this.value / 1000, 0) + 'Rb';
		                                        } else {
		                                            return this.value
		                                        }
		                                    }
		                                "),
		                                'style' => [
		                                    "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
		                                    'fontSize' => '10px',
		                                    'fontWeight' => 'regular',
		                                    'color' => "#414042",
		                                    'borderWidth' => 0,
		                                    'textShadow' => false,
		                                    'textOutline' => false  
		                                ]
		                            ],
		                            'title' => [
		                                'text' => 'IDR'
		                            ],
		                            'plotBands' => [[
		                                'from' => $revenue_chart['min'],
		                                'to' => $revenue_chart['red'],
		                                'color' => '#DF5353' // red                                        
		                            ], [
		                                'from' => $revenue_chart['red'],
		                                'to' => $revenue_chart['yellow'],
		                                'color' => '#DDDF0D' // yellow
		                            ], [
		                                'from' => $revenue_chart['yellow'],
		                                'to' => $revenue_chart['max'],
		                                'color' => '#55BF3B' // green
		                            ]]
		                        ],

		                        'series' => [[
		                            'name' => 'Revenue',
		                            'data' => [$revenue_chart['current_revenue']],
		                            'tooltip' => [
		                                'valuePrefix' => 'IDR '
		                            ],
		                            'dataLabels' => [
		                                'style' => [
		                                    "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
		                                    'fontSize' => '12px',
		                                    'fontWeight' => 'bold',
		                                    'color' => "#414042",
		                                    'borderWidth' => 0,
		                                    'textShadow' => false,
		                                    'textOutline' => false  
		                                ],
		                                'formatter' => new JsExpression("
		                                    function() {
		                                        return Highcharts.numberFormat(this.y, 0, ',', '.')
		                                    }
		                                "),
		                            ]
		                        ]]
		                    ]
		                ]);
		            ?>
				</div>
			</div>
					
		</div>

	</div>

	<?php if ($leads): ?>
	

	<div class="row">
		<?php foreach ($lead_charts as $lead): ?>
        <div class="col-xl-4 col-md-4">
            <div class="card">
                <div class="card-body" id="reflow">
                	<h4 class="header-title pb-1"><?= $lead['title'] ?></h4>
                	<h6 class="card-subtitle text-muted"><?= Yii::$app->formatter->asDate($lead['date_from'], 'long') ?> - <?= Yii::$app->formatter->asDate($lead['date_to'], 'long') ?></h6>

                	<?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
		                            'type' => 'solidgauge',
		                            // 'type' => 'gauge',
		                            'height' => 200,
		                            // 'plotBackgroundColor' => null,
		                            // 'plotBackgroundImage' => null,
		                            // 'plotBorderWidth' => 0,
		                            // 'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'marginTop' => 0,
							        'marginBottom' => -70,
							        'marginLeft' => 0,
							        'marginRight' => 0,
							        'padding' => 0,
							        // 'margin' => [0,0,0,0]
		                        ],    
		                        'title' => null, 
		                        'credits' => [
                                    'enabled' => false
                                ],
                                
                                'pane' => [
							        // 'center' => ['50%', '85%'],
							        // 'size' => '140%',
							        'startAngle' => -90,
							        'endAngle' => 90,
							        'background' => [							            
							            'innerRadius' => '60%',
							            'outerRadius' => '100%',
							            'shape' => 'arc'
							        ]
							    ],

							    'exporting' => [
							        'enabled' => false
							    ],

							    'tooltip' => [
							        'enabled' => false
							    ],

							    // the value axis
							    'yAxis' => [
							        'stops' => [
							            [$lead['min'], '#DF5353'], // red
							            [$lead['stop'] + $lead['stop'], '#DDDF0D'], // yellow
							            [$lead['max'], '#55BF3B'] // green
							        ],
							        // 'lineWidth' => 0,
							        // 'tickWidth' => 0,
							        'minorTickInterval' => null,
							        'tickAmount' => 1,
							        // 'title' => [
							        //     'y' => -70
							        // ],
							        'labels' => [
							            'y' => 16
							        ],
							        'min' => $lead['min'],
							        'max' => $lead['max'],
							        'tickPositions' => [$lead['min'], $lead['max']],
							        // 'tickPositions' => [0,6],
							        // 'title' => [
							        //     'text' => 'Speed'
							        // ]
							    ],

							    'plotOptions' => [
							        'solidgauge' => [
							            'dataLabels' => [
							                // 'y' => -15,
							                'borderWidth' => 0,
							                'useHTML' => true,
							                'style' => [
	                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
	                                            'fontSize' => '12px',
	                                            'fontWeight' => 'bold',
	                                            'color' => "#414042",
	                                            'borderWidth' => 0,
	                                            'textShadow' => false,
	                                            'textOutline' => false  
	                                        ],
							            ]
							        ]
							    ],

				
							    // 'credits' => [
							    //     'enabled' => false
							    // ],

							    'series' => [[
							        // 'name' => 'Speed',
							        'data' => [$lead['current']],
							        
							        'tooltip' => [
							            'valueSuffix' => ''
							        ]
							    ]]
                            ]
                        ]);
                    ?>

                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

	
	<?php endif; ?>

    <!-- <div class="card">
        <div class="card-body">
        	<h4 class="header-title pb-1">Net Promoter Score by Program</h4>

        	<?php 
	        	echo Highcharts::widget([
				   'options' => [
				   		'chart' => [
	                        'type' => 'column',
	                        
	                    ],
						'title' => null,
						'credits' => [
	                        'enabled' => false
	                    ],
						'xAxis' => [
						 	'categories' => $events,
						],
						'yAxis' => [
						 	'title' => ['text' => 'Skor']
						],
						'series' => $nps_event_series,
				   	]
				]);

        	?>
            
        </div>
    </div> -->


</div>

<?php 
$js = <<<JS
let wide = false;
document.getElementById('reflow-chart').addEventListener('click', () => {
  document.getElementById('reflow').style.width = wide ? window.innerWidth - 310 + 'px' : window.innerWidth - 90 + 'px';
  wide = !wide;
  refreshChart.reflow();
  console.log(document.documentElement.clientWidth);
});

JS;

?>