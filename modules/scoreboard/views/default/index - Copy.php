<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\production\models\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = 'Dashboard';
?>




<div class="scoreboard-default-index">
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h4 class="header-title">Revenue</h4>
					<?php
		                echo Highcharts::widget([
		                    'scripts' => [
		                        'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
		                        // 'modules/solid-gauge',
		                        'themes/grid-light',
		                    ],
		                    'options' => [
		                        'chart' => [
		                            'type' => 'solidgauge',
		                            'type' => 'gauge',
		                            'height' => 275,
		                            'plotBackgroundColor' => null,
		                            'plotBackgroundImage' => null,
		                            'plotBorderWidth' => 0,
		                            'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'margin' => [0,0,0,0]
		                        ],    
		                        'title' => false,     
		                        // 'title' => [
		                        //     'text' => 'Revenue',
		                        //     'margin' => 0,
		                        //     'style' => [
		                        //         "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
		                        //         'fontSize' => '14px',
		                        //         'fontWeight' => 'regular',
		                        //         'color' => "#414042",
		                        //         'borderWidth' => 0,
		                        //         'textShadow' => false,
		                        //         'textOutline' => false  
		                        //     ]
		                            
		                        // ],
		                        
		                        'credits' => [
		                            'enabled' => false
		                        ],

		                        'pane' => [
		                            'startAngle' => -150,
		                            'endAngle' => 150,
		                            'background' => [
		                                [
		                                    'backgroundColor' => [
		                                        'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
		                                        'stops' => [
		                                            [0, '#FFF'],
		                                            [1, '#333']
		                                        ]
		                                    ],
		                                    'borderWidth' => 0,
		                                    'outerRadius' => '109%'
		                                ], 
		                                [
		                                    'backgroundColor' => [
		                                        'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
		                                        'stops' => [
		                                            [0, '#333'],
		                                            [1, '#FFF']
		                                        ]
		                                    ],
		                                    'borderWidth' => 1,
		                                    'outerRadius' => '107%'
		                                ], 
		                                [
		                                    // default background
		                                ], 
		                                [
		                                    'backgroundColor' => '#DDD',
		                                    'borderWidth' => 0,
		                                    'outerRadius' => '105%',
		                                    'innerRadius' => '103%'
		                                ]
		                            ],
		                        ],                                

		                        // the value axis
		                        'yAxis' => [
		                            'min' => $revenue_chart['min'],
		                            'max' => $revenue_chart['max'],
		                            // 'minorTickInterval' => 'auto',
		                            // 'minorTickWidth' => 0,
		                            // 'minorTickLength' => 10,
		                            // 'minorTickPosition' => 'inside',
		                            // 'minorTickColor' => '#666',
		                            // 'tickPixelInterval' => 10,
		                            // 'tickWidth' => 0,
		                            // 'tickPosition' => 'inside',
		                            // 'tickLength' => 10,
		                            // 'tickColor' => '#666',
		                            'showFirstLabel' => true,
		                            'showLastLabel' => true,
		                            'labels' => [
		                                'step' => 1,
		                                'rotation' => 'auto',
		                                'formatter' => new JsExpression("
		                                    function() {
		                                        if (this.value >= 1000000000) {
		                                            return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
		                                        } else if (this.value > 1000000) {
		                                            return Highcharts.numberFormat(this.value / 1000000, 0) + 'Jt'
		                                        } else if (this.value > 1000) {
		                                            return Highcharts.numberFormat(this.value / 1000, 0) + 'Rb';
		                                        } else {
		                                            return this.value
		                                        }
		                                    }
		                                "),
		                                'style' => [
		                                    "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
		                                    'fontSize' => '10px',
		                                    'fontWeight' => 'regular',
		                                    'color' => "#414042",
		                                    'borderWidth' => 0,
		                                    'textShadow' => false,
		                                    'textOutline' => false  
		                                ]
		                            ],
		                            'title' => [
		                                'text' => 'IDR'
		                            ],
		                            'plotBands' => [[
		                                'from' => $revenue_chart['min'],
		                                'to' => $revenue_chart['red'],
		                                'color' => '#DF5353' // red                                        
		                            ], [
		                                'from' => $revenue_chart['red'],
		                                'to' => $revenue_chart['yellow'],
		                                'color' => '#DDDF0D' // yellow
		                            ], [
		                                'from' => $revenue_chart['yellow'],
		                                'to' => $revenue_chart['max'],
		                                'color' => '#55BF3B' // green
		                            ]]
		                        ],

		                        'series' => [[
		                            'name' => 'Revenue',
		                            'data' => [$revenue_chart['current_revenue']],
		                            'tooltip' => [
		                                'valuePrefix' => 'IDR '
		                            ],
		                            'dataLabels' => [
		                                'style' => [
		                                    "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
		                                    'fontSize' => '12px',
		                                    'fontWeight' => 'bold',
		                                    'color' => "#414042",
		                                    'borderWidth' => 0,
		                                    'textShadow' => false,
		                                    'textOutline' => false  
		                                ],
		                                'formatter' => new JsExpression("
		                                    function() {
		                                        return Highcharts.numberFormat(this.y, 0, ',', '.')
		                                    }
		                                "),
		                            ]
		                        ]]
		                    ]
		                ]);
		            ?>
				</div>
			</div>
					
		</div>

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h4 class="header-title">Net Promoter Score</h4>
					<?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
		                            'type' => 'solidgauge',
		                            'type' => 'gauge',
		                            'height' => 275,
		                            'plotBackgroundColor' => null,
		                            'plotBackgroundImage' => null,
		                            'plotBorderWidth' => 0,
		                            'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'margin' => [0,0,0,0]
		                        ],    
		                        'title' => false, 
                                
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $nps_chart['min'],
                                    'max' => $nps_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        // 'formatter' => new JsExpression("
                                        //     function() {
                                        //         if (this.value >= 1000000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                        //         } else if (this.value > 1000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000, 0) + 'J'
                                        //         } else if (this.value > 1000) {
                                        //             return Highcharts.numberFormat(this.value / 1000, 0) + 'R';
                                        //         } else {
                                        //             return this.value
                                        //         }
                                        //     }
                                        // "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'POINT'
                                    ],
                                    'plotBands' => [[
                                        'from' => $nps_chart['min'],
                                        'to' => $nps_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $nps_chart['red'],
                                        'to' => $nps_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $nps_chart['yellow'],
                                        'to' => $nps_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'NPS',
                                    'data' => [$nps_chart['current_nps']],
                                    'tooltip' => [
                                        'valueSuffix' => ''
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
				</div>
			</div>
					
		</div>

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h4 class="header-title">Closing Rate</h4>
					<?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
		                            'type' => 'solidgauge',
		                            'type' => 'gauge',
		                            'height' => 275,
		                            'plotBackgroundColor' => null,
		                            'plotBackgroundImage' => null,
		                            'plotBorderWidth' => 0,
		                            'plotShadow' => false,  		   
		                            'spacingTop' => 0,
							        'spacingRight' => 0,
							        'spacingBottom' => 0,
							        'spacingLeft' => 0,
							        'margin' => [0,0,0,0]
		                        ],    
		                        'title' => false, 
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $closing_rate_chart['min'],
                                    'max' => $closing_rate_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        // 'formatter' => new JsExpression("
                                        //     function() {
                                        //         if (this.value >= 1000000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                        //         } else if (this.value > 1000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000, 0) + 'J'
                                        //         } else if (this.value > 1000) {
                                        //             return Highcharts.numberFormat(this.value / 1000, 0) + 'R';
                                        //         } else {
                                        //             return this.value
                                        //         }
                                        //     }
                                        // "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'PERCENT'
                                    ],
                                    'plotBands' => [[
                                        'from' => $closing_rate_chart['min'],
                                        'to' => $closing_rate_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $closing_rate_chart['red'],
                                        'to' => $closing_rate_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $closing_rate_chart['yellow'],
                                        'to' => $closing_rate_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'Rate',
                                    'data' => [$closing_rate_chart['current_rate']],
                                    'tooltip' => [
                                        'valueSuffix' => '%'
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
				</div>
			</div>
					
		</div>

	</div>

    <div class="card">
        <div class="card-body">

            <div class="row">
                <!-- REVENUE CHART -->
                <div class="col-md-4">
                    <?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
                                    'type' => 'solidgauge',
                                    'type' => 'gauge',
                                    'plotBackgroundColor' => null,
                                    'plotBackgroundImage' => null,
                                    'plotBorderWidth' => 0,
                                    'plotShadow' => false,  
                                    // 'marginBottom' => 0,
                                    'margin' => 0,
                                    'padding' => 0,     
                                ],         
                                'title' => [
                                    'text' => 'Revenue',
                                    'margin' => 0,
                                    'style' => [
                                        "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                        'fontSize' => '14px',
                                        'fontWeight' => 'regular',
                                        'color' => "#414042",
                                        'borderWidth' => 0,
                                        'textShadow' => false,
                                        'textOutline' => false  
                                    ]
                                    
                                ],
                                
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $revenue_chart['min'],
                                    'max' => $revenue_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        'formatter' => new JsExpression("
                                            function() {
                                                if (this.value >= 1000000000) {
                                                    return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                                } else if (this.value > 1000000) {
                                                    return Highcharts.numberFormat(this.value / 1000000, 0) + 'Jt'
                                                } else if (this.value > 1000) {
                                                    return Highcharts.numberFormat(this.value / 1000, 0) + 'Rb';
                                                } else {
                                                    return this.value
                                                }
                                            }
                                        "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'IDR'
                                    ],
                                    'plotBands' => [[
                                        'from' => $revenue_chart['min'],
                                        'to' => $revenue_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $revenue_chart['red'],
                                        'to' => $revenue_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $revenue_chart['yellow'],
                                        'to' => $revenue_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'Revenue',
                                    'data' => [$revenue_chart['current_revenue']],
                                    'tooltip' => [
                                        'valuePrefix' => 'IDR '
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
                </div>
            

                <!-- NET PROMOTER SCORE CHART -->
                <div class="col-md-4">
                    <?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
                                    'type' => 'gauge',
                                    'plotBackgroundColor' => null,
                                    'plotBackgroundImage' => null,
                                    'plotBorderWidth' => 0,
                                    'plotShadow' => false,  
                                    // 'marginBottom' => 0,
                                    'margin' => 0,
                                    'padding' => 0,     
                                ],         
                                'title' => [
                                    'text' => 'Net Promoter Score',
                                    'margin' => 0,
                                    'style' => [
                                        "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                        'fontSize' => '14px',
                                        'fontWeight' => 'regular',
                                        'color' => "#414042",
                                        'borderWidth' => 0,
                                        'textShadow' => false,
                                        'textOutline' => false  
                                    ]
                                    
                                ],
                                
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $nps_chart['min'],
                                    'max' => $nps_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        // 'formatter' => new JsExpression("
                                        //     function() {
                                        //         if (this.value >= 1000000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                        //         } else if (this.value > 1000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000, 0) + 'J'
                                        //         } else if (this.value > 1000) {
                                        //             return Highcharts.numberFormat(this.value / 1000, 0) + 'R';
                                        //         } else {
                                        //             return this.value
                                        //         }
                                        //     }
                                        // "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'POINT'
                                    ],
                                    'plotBands' => [[
                                        'from' => $nps_chart['min'],
                                        'to' => $nps_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $nps_chart['red'],
                                        'to' => $nps_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $nps_chart['yellow'],
                                        'to' => $nps_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'NPS',
                                    'data' => [$nps_chart['current_nps']],
                                    'tooltip' => [
                                        'valueSuffix' => ''
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
                </div>

                <!-- CLOSING RATE CHART -->
                <div class="col-md-4">
                    <?php
                        echo Highcharts::widget([
                            'scripts' => [
                                'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                // 'modules/solid-gauge',
                                'themes/grid-light',
                            ],
                            'options' => [
                                'chart' => [
                                    'type' => 'gauge',
                                    'plotBackgroundColor' => null,
                                    'plotBackgroundImage' => null,
                                    'plotBorderWidth' => 0,
                                    'plotShadow' => false,  
                                    // 'marginBottom' => 0,
                                    'margin' => 0,
                                    'padding' => 0,     
                                ],         
                                'title' => [
                                    'text' => 'Closing Rate',
                                    'margin' => 0,
                                    'style' => [
                                        "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                        'fontSize' => '14px',
                                        'fontWeight' => 'regular',
                                        'color' => "#414042",
                                        'borderWidth' => 0,
                                        'textShadow' => false,
                                        'textOutline' => false  
                                    ]
                                    
                                ],
                                
                                'credits' => [
                                    'enabled' => false
                                ],

                                'pane' => [
                                    'startAngle' => -150,
                                    'endAngle' => 150,
                                    'background' => [
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1],
                                                'stops' => [
                                                    [0, '#FFF'],
                                                    [1, '#333']
                                                ]
                                            ],
                                            'borderWidth' => 0,
                                            'outerRadius' => '109%'
                                        ], 
                                        [
                                            'backgroundColor' => [
                                                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                                                'stops' => [
                                                    [0, '#333'],
                                                    [1, '#FFF']
                                                ]
                                            ],
                                            'borderWidth' => 1,
                                            'outerRadius' => '107%'
                                        ], 
                                        [
                                            // default background
                                        ], 
                                        [
                                            'backgroundColor' => '#DDD',
                                            'borderWidth' => 0,
                                            'outerRadius' => '105%',
                                            'innerRadius' => '103%'
                                        ]
                                    ],
                                ],                                

                                // the value axis
                                'yAxis' => [
                                    'min' => $closing_rate_chart['min'],
                                    'max' => $closing_rate_chart['max'],
                                    // 'minorTickInterval' => 'auto',
                                    // 'minorTickWidth' => 0,
                                    // 'minorTickLength' => 10,
                                    // 'minorTickPosition' => 'inside',
                                    // 'minorTickColor' => '#666',
                                    // 'tickPixelInterval' => 10,
                                    // 'tickWidth' => 0,
                                    // 'tickPosition' => 'inside',
                                    // 'tickLength' => 10,
                                    // 'tickColor' => '#666',
                                    'showFirstLabel' => true,
                                    'showLastLabel' => true,
                                    'labels' => [
                                        'step' => 1,
                                        'rotation' => 'auto',
                                        // 'formatter' => new JsExpression("
                                        //     function() {
                                        //         if (this.value >= 1000000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000000, 1) + 'M'
                                        //         } else if (this.value > 1000000) {
                                        //             return Highcharts.numberFormat(this.value / 1000000, 0) + 'J'
                                        //         } else if (this.value > 1000) {
                                        //             return Highcharts.numberFormat(this.value / 1000, 0) + 'R';
                                        //         } else {
                                        //             return this.value
                                        //         }
                                        //     }
                                        // "),
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '10px',
                                            'fontWeight' => 'regular',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],
                                    'title' => [
                                        'text' => 'PERCENT'
                                    ],
                                    'plotBands' => [[
                                        'from' => $closing_rate_chart['min'],
                                        'to' => $closing_rate_chart['red'],
                                        'color' => '#DF5353' // red                                        
                                    ], [
                                        'from' => $closing_rate_chart['red'],
                                        'to' => $closing_rate_chart['yellow'],
                                        'color' => '#DDDF0D' // yellow
                                    ], [
                                        'from' => $closing_rate_chart['yellow'],
                                        'to' => $closing_rate_chart['max'],
                                        'color' => '#55BF3B' // green
                                    ]]
                                ],

                                'series' => [[
                                    'name' => 'Rate',
                                    'data' => [$closing_rate_chart['current_rate']],
                                    'tooltip' => [
                                        'valueSuffix' => '%'
                                    ],
                                    'dataLabels' => [
                                        'style' => [
                                            "fontFamily" => "roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12px',
                                            'fontWeight' => 'bold',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ],
                                        'formatter' => new JsExpression("
                                            function() {
                                                return Highcharts.numberFormat(this.y, 0, ',', '.')
                                            }
                                        "),
                                    ]
                                ]]
                            ]
                        ]);
                    ?>
                </div>
            </div>    
        </div>
    </div>


    



    <div class="card">
        <div class="card-body">
            
        </div>
    </div>
</div>
