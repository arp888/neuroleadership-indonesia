<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\RevenueSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="revenue-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
<?= $form->field($model, 'createDateRange', [
            'options' => ['class' => 'kt-subheader__searchcustom kt-margin-l-10']
        ])->widget(DateRangePicker::classname(), [
            // 'useWithAddon'=>true,
            // 'language' => 'id',
            'options' => ['class' => 'form-control kt-form__control', 'placeholder' => 'Select Date Range...'],
            'convertFormat' => true,
            'autoUpdateOnInit' => true, 
            'startAttribute' => 'createDateStart',
            'endAttribute' => 'createDateEnd',
            // 'startInputOptions' => ['value' => date('Y-01-01')],
            // 'endInputOptions' => ['value' => date('Y-12-31')],
            'presetDropdown' => true,
            'hideInput'=>true,
            'pluginOptions' => [
                'timePicker'=>false,
                'showDropdowns'=>true,
                'todayHighlight' => true,
                'locale'=>[
                    'format' => 'd M Y',
                    'lang' => 'id',
                    'separator' => ' - ',
                ],
                'opens' => 'left',                                
            ],
            'pluginEvents' => [
                "apply.daterangepicker" => "function(e) { $(e.target).closest('form').submit(); }",   
            ],

        ])->label(false)->error(false) 
    ?>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

