<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\Revenue */

$this->title = Yii::t('app', 'Detail Revenue');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Revenue'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
\yii\web\YiiAsset::register($this);
?>

<div class="revenue-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    

                    <?php if (Helper::checkRoute('create')): ?>
                    <?= Html::a(Yii::t('app', 'Create'), ['create', 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-primary ml-1'
                    ]) ?>
                    <?php endif; ?>
          
            
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => false],
                'template' => '<div class="mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
                    'description',
                    [
                        'attribute' => 'amount',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->amount, 0);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->date, 'long');
                        },
                    ],                    
                ],
            ]) ?>
        </div>
    </div>
</div>        