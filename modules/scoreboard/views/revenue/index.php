<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use kartik\daterange\DateRangePicker;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\scoreboard\models\RevenueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Revenue');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="revenue-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">                 
                <div class="ml-auto">
                    <?php $form = ActiveForm::begin([
                        'action' => ['index'],
                        'id' => 'revenue-search-form',
                        'method' => 'get',
                        'layout' => 'inline',        
                        'enableClientValidation'=>false,
                        'enableAjaxValidation'=>false,
                        'fieldConfig' => [
                            'template' => "{label}\n{input}\n{hint}\n{error}\n",
                        ],
                    ]); ?>

                    <?= $form->field($searchModel, 'createDateRange')->widget(DateRangePicker::classname(), [
                            // 'useWithAddon'=>true,
                            // 'language' => 'id',
                            'options' => ['class' => 'form-control', 'placeholder' => 'Select Date Range...'],
                            'convertFormat' => true,
                            'autoUpdateOnInit' => true, 
                            'startAttribute' => 'createDateStart',
                            'endAttribute' => 'createDateEnd',
                            // 'startInputOptions' => ['value' => date('Y-01-01')],
                            // 'endInputOptions' => ['value' => date('Y-12-31')],
                            'presetDropdown' => true,
                            'hideInput'=>true,
                            'pluginOptions' => [
                                'timePicker'=>false,
                                'showDropdowns'=>true,
                                'todayHighlight' => true,
                                'locale'=>[
                                    'format' => 'd M Y',
                                    'lang' => 'id',
                                    'separator' => ' - ',
                                ],
                                'opens' => 'left',                                
                            ],
                            'pluginEvents' => [
                                "apply.daterangepicker" => "function(e) { $(e.target).closest('form').submit(); }",   
                            ],

                        ])->label(false)->error(false) 
                    ?>

                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create Revenue'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary ml-1']); 
                        }
                    ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'revenue-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    [
                        'attribute' => 'description',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'amount',
                        'mergeHeader' => true,
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->amount, 0);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'mergeHeader' => true,
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->date, 'long');
                        }
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>

<?php 
$clearDateRangePicker = <<<JS
$('#revenuesearch-createdaterange').on('cancel.daterangepicker', function(ev, picker) {
    $('#revenuesearch-createdaterange, #revenuesearch-createdaterange-start, #revenuesearch-createdaterange-end').val(null);
    $('#revenue-search-form').yiiActiveForm('submitForm');
    // $.pjax.reload({container: '#revenue-index-pjax'});
});
JS;
$this->registerJs($clearDateRangePicker);
?>