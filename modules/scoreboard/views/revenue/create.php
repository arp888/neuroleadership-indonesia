<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\scoreboard\models\Revenue */

$this->title = Yii::t('app', 'Create Revenue');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Revenue'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="revenue-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
