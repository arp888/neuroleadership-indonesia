<?php

namespace app\modules\scoreboard;

/**
 * scoreboard module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\scoreboard\controllers';
    public $layout = 'scoreboard-layout';
    public $defaultRoute = 'default/index';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        \Yii::configure($this, [
            'components' => [
                'errorHandler' => [
                    'class' => \yii\web\ErrorHandler::className(),
                    'errorAction' => '/scoreboard/default/error',
                ]
            ],
        ]);

        /** @var ErrorHandler $handler */
        $handler = $this->get('errorHandler');
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();
        
        // \Yii::$app->set('user', [
        //     'class' => 'yii\web\User',
        //     'identityClass' => 'mdm\admin\models\User',
        //     'enableAutoLogin' => false,
        //     'loginUrl' => ['/admin/user/login'],
        //     'identityCookie' => ['name' => 'backend_module', 'httpOnly' => true],
        //     // 'idParam' => 'backend_id', //this is important !
        // ]); 
    }
}
