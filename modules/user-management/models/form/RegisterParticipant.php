<?php
namespace mdm\admin\models\form;

use Yii;
use mdm\admin\models\UserParticipant;
use yii\base\Model;
use mdm\admin\models\Assignment;

/**
 * Signup form
 */
class RegisterParticipant extends Model
{
    public $fullname;
    public $username;
    public $email;
    public $password;
    public $roles;
    public $permissions;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['username'], 'required', 'message' => 'Isian tidak boleh kosong.'],
            ['username', 'unique', 'targetClass' => 'mdm\admin\models\User', 'message' => 'Username ini telah dipakai oleh user lain.'],
            [['username', 'fullname'], 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            // ['email', 'required', 'message' => 'Alamat email tidak boleh kosong.'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'mdm\admin\models\User', 'message' => 'Alamat email ini telah dipakai oleh user lain.'],

            // ['password', 'required', 'message' => 'Password tidak boleh kosong.'],
            ['password', 'string', 'min' => 6],

            // ['roles', 'required'],
            ['roles', 'each', 'rule' => ['string', 'max' => 64]],
            ['permissions', 'each', 'rule' => ['string', 'max' => 64]],
        ];
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function registerParticipant($email)
    {
        // var_dump('dafafafafaf'); die(); 
        if ($this->validate()) {
            $user = new UserParticipant();
            $user->username = $this->username;
            $user->email = $email;   
            $pass = self::randomPassword();         
            // $user->setPassword($pass);
            // $user->setPassword($this->password);
            // $user->generatePasswordResetToken();
            $user->generateAuthKey();
            // $roles = Yii::$app->getRequest()->post('Register["roles"]', []);

            $roles = $_POST['Register']['roles']; 
            $permissions = $_POST['Register']['permissions']; 

            // echo '<pre>'; 
            // var_dump($roles);  
            // var_dump($permissions); die(); 

            if ($user->save()) {
                $assignment = new Assignment($user->id);
                $assignment->assign($this->roles);                
                $assignment->assign($this->permissions);                

                // Yii::$app->set('mailer', [
                //     'class' => 'yii\swiftmailer\Mailer',
                //     'useFileTransport' => false,
                //     'transport' => [
                //         'class' => 'Swift_SmtpTransport',
                //         'host' => 'smtp.gmail.com', 
                //         'username' => 'hcmsid@gmail.com',
                //         'password' => 'mining13579',
                //         'port' => 587,
                //         'encryption' => 'tls',
                //         'streamOptions' => [
                //             'ssl' => [
                //                 'allow_self_signed' => true,
                //                 'verify_peer' => false,
                //                 'verify_peer_name' => false,
                //             ],
                //         ],
                //     ],
                // ]);

                // Yii::$app->mailer->compose(['html' => 'register-html'], ['nama' => $this->fullname, 'user' => $this->username, 'password' => $pass])
                //     ->setFrom([Yii::$app->params['noReplyEmail'] => 'Human Resource Information System'])
                //     ->setTo($this->email)
                //     ->setSubject('Informasi Login')
                //     ->send();

                return $user->id;
                
            }
        }

        return null;
    }
}
