<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Menu */

$this->title = Yii::t('rbac-admin', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">
    <div class="m-portlet m-portlet--responsive-mobile">        
        <div class="m-portlet__body">
            <div class="m-form m-form--label-align-right m--margin-top-0 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <?php //echo $this->render('_search', ['model' => $searchModel]); ?>                        
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">                        
                        <?= Html::a(Yii::t('rbac-admin', 'Create Menu'), ['create'], ['class' => 'btn btn-accent m-btn m-btn--icon m-btn--wide m-btn--md']) ?>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>

            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n<div class='custom__pager custom--paging-loaded clearfix'>{pager}<div class='custom__pager-info'><span class='custom__pager-detail'>{summary}</span></div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'custom__pager-nav'],
                    // 'activePageCssClass' => 'custom__pager-link--active',
                    'linkOptions' => ['class' => 'custom__pager-link custom__pager-link-number'],
                    // 'disabledPageCssClass' => 'custom__pager-link custom__pager-link--first custom__pager-link--disabled',
                    'linkContainerOptions' => ['class' => ''],
                    'prevPageCssClass' => 'prev',
                    'prevPageLabel' => '<i class="la la-angle-left"></i>',
                    'nextPageCssClass' => 'next',
                    'nextPageLabel' => '<i class="la la-angle-right"></i>',
                    'maxButtonCount' => 5,
                    // 'disableCurrentPageButton' => true,
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'custom__pager-link custom__pager-link--first m-datatable__pager-link--disabled', 'href' => '#'],
                    'firstPageCssClass' => 'first',
                    'firstPageLabel' => '<i class="la la-angle-double-left"></i>',
                    'lastPageCssClass' => 'last',
                    'lastPageLabel' => '<i class="la la-angle-double-right"></i>',
                ],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    'name',
                    [
                        'attribute' => 'menuParent.name',
                        'filter' => Html::activeTextInput($searchModel, 'parent_name', [
                            'class' => 'form-control', 'id' => null
                        ]),
                        'label' => Yii::t('rbac-admin', 'Parent'),
                    ],
                    'route',
                    'order',
                    ['class' => 'kartik\grid\ActionColumn'],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
