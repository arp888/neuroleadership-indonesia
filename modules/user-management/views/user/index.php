<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use cornernote\returnurl\ReturnUrl; 
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('rbac-admin', 'User List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="card">  
    <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php                        
                        echo Html::a(Yii::t('app', 'Register New User'), ['signup'], ['class' => 'btn btn-success']);                         
                    ?>
                </div>
            </div>
        </div>      
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'user-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],
                    
                    [
                        'attribute' => 'username',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Cari...')],
                        'vAlign' => 'middle',
                    ],

                    [
                        'attribute' => 'email',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Cari...')],
                        'format' => 'email',
                        'vAlign' => 'middle',
                    ],

                    // [
                    //     'attribute' => 'last_login_at',
                    //     'label' => 'Terakhir login',
                    //     'vAlign' => 'middle',
                    //     'mergeHeader' => true,
                    //     'value' => function($model) {
                    //         return Yii::$app->formatter->asDateTime($model->last_login_at, 'long');
                    //     }
                    // ],
          
                    [
                        'attribute' => 'status',
                        'filter' => [
                            0 => 'Inactive',
                            10 => 'Active'
                        ],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Semua Status')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'vAlign' => 'middle',
                        'value' => function($model) {
                            return $model->status == 0 ? 'Inaktif' : 'Aktif';
                        },                        
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group">' . Helper::filterActionColumn(['view', 'activate', 'delete'])  . '</div>',
                        'buttons' => [
                            'activate' => function($url, $model) {
                                if ($model->status == 10) {
                                    return '';
                                }
                                $options = [
                                    'title' => Yii::t('rbac-admin', 'Aktifkan'),
                                    'aria-label' => Yii::t('rbac-admin', 'Activate'),
                                    'data-confirm' => Yii::t('rbac-admin', 'Are you sure you want to activate this user?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ];
                                return Html::a('<i class="mdi mdi-account-convert-outline"></i>', $url, $options);
                            },
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', $url,
                                [
                                    'class' => 'action-icon',
                                ]);
                            },                                             
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', $url,
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Apakah Anda yakin ingin menghapus data ini?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
