<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;
use mdm\admin\AnimateAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\YiiAsset;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'User List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$controllerId = $this->context->uniqueId . '/';

AnimateAsset::register($this);
YiiAsset::register($this);
$opts = Json::htmlEncode([
    'items' => $modelAssignment->getItems(),
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
// $this->registerJsFile('@web/theme/assets/js/pages/components/extended/dual-listbox.js', ['depends' => [\app\assets\MetronicVerticalAsset::className()]]);
$animateIcon = '<i class="fas fa-spinner fa-pulse"></i>';

?>
<div class="user-view">
    <div class="card mb-3">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>   
                    <div class="btn-group" role="group" aria-label="...">
                        <?php
                        if ($model->status == 0 && Helper::checkRoute($controllerId . 'activate')) {
                            echo Html::a(Yii::t('rbac-admin', 'Activate'), ['activate', 'id' => $model->id], [
                                'class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => Yii::t('rbac-admin', 'Are you sure you want to activate this user?'),
                                    'method' => 'post',
                                ],
                            ]);
                        }
                        ?>
                        <?php
                        if (Helper::checkRoute($controllerId . 'delete')) {
                            echo Html::a(Yii::t('rbac-admin', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]);
                        }
                        ?>
                    </div> 
                </div>
            </div>
        </div>
        <div class="card-body">
            
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => 'div', 'class' => 'row'],
                'template' => '<div class="col-md-6 mb-3"><h5>{label}</h5>{value}</div>',
                'attributes' => [
                    'username',
                    'email:email',   
                    [
                        'attribute' => 'created_at',
                        'format' => ['datetime', 'long'],
                    ],
                    [
                        'attribute' => 'status',                                
                        'value' => function($model) {
                            return $model->status == 0 ? 'Inactive' : 'Active';
                        },                                
                    ],                         
                ],
            ]) ?>                
                          
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="lead"><?= Yii::t('app', 'Role & Permission') ?></h4> 
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <input class="form-control search mb-1" data-target="available"
                           placeholder="<?=Yii::t('rbac-admin', 'Search for available');?>">
                    <select multiple size="20" class="form-control list" data-target="available"></select>
                </div>
                <div class="col-md-1 text-center">
                    <br><br>
                    <?=Html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => (string) $model->id], [
                        'class' => 'btn btn-success btn-assign',
                        'data-target' => 'available',
                        'title' => Yii::t('rbac-admin', 'Assign'),
                    ]);?>
                    <br><br>
                    <?=Html::a('&lt;&lt;' . $animateIcon, ['revoke', 'id' => (string) $model->id], [
                        'class' => 'btn btn-danger btn-assign mb-5 mb-md-0',
                        'data-target' => 'assigned',
                        'title' => Yii::t('rbac-admin', 'Remove'),
                    ]);?>
                    <br><br>
                </div>
                <div class="col">
                    <input class="form-control search mb-1" data-target="assigned"
                           placeholder="<?=Yii::t('rbac-admin', 'Search for assigned');?>">
                    <select multiple size="20" class="form-control list" data-target="assigned"></select>
                </div>
            </div>
        </div>
    </div>
</div>
