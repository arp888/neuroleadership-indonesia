<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

$this->title = Yii::t('rbac-admin', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-form">
    <!-- Logo -->    

    <h4 class="mt-0">Sign In</h4>
    <p class="text-muted mb-4">Please enter your username and password to access admin panel.</p>
   
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => ''],
    ]); ?>   

    <?= $form->field($model, 'username')->textInput([
            'class' => 'form-control',
            'placeholder' => 'Username',
            'autocomplete' => 'off',
        ]) 
    ?>
    
    <?= $form->field($model, 'password', [
            'template' => 
                '{beginWrapper}
                    ' . Html::a(Yii::t('app', 'Forgot your password?'), ['/admin/user/request-password-reset'], ['class' => 'text-muted float-right']) . '
                    {label}
                    <div class="input-group input-group-merge">
                        {input}
                        <div class="input-group-append" data-password="false">
                            <div class="input-group-text">
                                <span class="password-eye"></span>
                            </div>
                        </div>
                    </div>
                    
                    {hint}
                    {error}
                {endWrapper}', 
        ])->passwordInput([
            'class' => 'form-control',
            'placeholder' => 'Password',
        ])
    ?>
    
    </div>    

    <div class="form-group mb-0 text-center">
        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>

<?php 
    $this->registerCss('
        .auth-fluid {
            position: relative;
            display: flex;
            align-items: center;
            min-height: 100vh;
            flex-direction: row;
            align-items: stretch;
            background: url(' . Yii::getAlias("@web/images/brain-cell-neuron-illustration.jpg") . ') center;
            background-size: cover;
        }
    ');
?>