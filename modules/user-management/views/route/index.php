<?php

use mdm\admin\AnimateAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\YiiAsset;

/* @var $this yii\web\View */
/* @var $routes [] */

$this->title = Yii::t('rbac-admin', 'Routes');
$this->params['breadcrumbs'][] = $this->title;

AnimateAsset::register($this);
YiiAsset::register($this);
$opts = Json::htmlEncode([
    'routes' => $routes,
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="fa fa-refresh glyphicon-refresh-animate"></i>';
?>

<div class="route-index">      
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        <input id="inp-route" type="text" class="form-control" placeholder="<?=Yii::t('rbac-admin', 'New route(s)');?>">
                        <div class="input-group-append">
                            <?=Html::a(Yii::t('rbac-admin', 'Add') . $animateIcon, ['create'], [
                                'class' => 'btn btn-success',
                                'id' => 'btn-new',
                            ]);?>
                        </div>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col">
                    <div class="input-group mb-2">
                        <input class="form-control search" data-target="available" placeholder="<?=Yii::t('rbac-admin', 'Search for available');?>">
                        <div class="input-group-append">
                            <?=Html::a('<i class="la la-refresh"></i>', ['refresh'], [
                                'class' => 'btn btn-success',
                                'id' => 'btn-refresh',
                            ]);?>
                        </div>
                    </div>
                    <select multiple size="30" class="form-control list" data-target="available"></select>
                </div>
                <div class="col-sm-1 text-center">
                    <br><br>
                    <?=Html::a('&gt;&gt;' . $animateIcon, ['assign'], [
                        'class' => 'btn btn-success btn-assign',
                        'data-target' => 'available',
                        'title' => Yii::t('rbac-admin', 'Assign'),
                    ]);?>
                    <br><br>
                    <?=Html::a('&lt;&lt;' . $animateIcon, ['remove'], [
                        'class' => 'btn btn-danger btn-assign mb-5 mb-md-0',
                        'data-target' => 'assigned',
                        'title' => Yii::t('rbac-admin', 'Remove'),
                    ]);?>
                </div>
                <div class="col">
                    <input class="form-control search mb-2" data-target="assigned"
                           placeholder="<?=Yii::t('rbac-admin', 'Search for assigned');?>">
                    <select multiple size="30" class="form-control list" data-target="assigned"></select>
                </div>
            </div>
        </div>
    </div>
            
</div>