<?php

use mdm\admin\AnimateAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\YiiAsset;
use yii\widgets\DetailView;
use cornernote\returnurl\ReturnUrl;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */
/* @var $context mdm\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', $labels['Items'] . ' Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $labels['Items']), 'url' => ['index']];
$this->params['breadcrumbs'][] =$model->name;

AnimateAsset::register($this);
YiiAsset::register($this);
$opts = Json::htmlEncode([
    'items' => $model->getItems(),
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="fa fa-refresh glyphicon-refresh-animate"></i>';
?>

<div class="auth-item-view">

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    

                    <div class="btn-group ml-1" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->name, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->name, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => 'div', 'class' => 'row'],
                'template' => '<div class="col-sm-6 mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
                    'name',
                    'description:ntext', 
                    'ruleName',
                    'data:ntext',
                ],
            ]) ?>

        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <input class="form-control search mb-2" data-target="available"
                           placeholder="<?=Yii::t('rbac-admin', 'Search for available');?>">
                    <select multiple size="20" class="form-control list" data-target="available"></select>
                </div>
                <div class="col-sm-1 text-center">
                    <br><br>
                    <?=Html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => $model->name], [
                        'class' => 'btn btn-success btn-assign',
                        'data-target' => 'available',
                        'title' => Yii::t('rbac-admin', 'Assign'),
                    ]);?>
                    <br><br>
                    <?=Html::a('&lt;&lt;' . $animateIcon, ['remove', 'id' => $model->name], [
                        'class' => 'btn btn-danger btn-assign mb-5 mb-md-0',
                        'data-target' => 'assigned',
                        'title' => Yii::t('rbac-admin', 'Remove'),
                    ]);?>
                </div>
                <div class="col">
                    <input class="form-control search mb-2" data-target="assigned"
                           placeholder="<?=Yii::t('rbac-admin', 'Search for assigned');?>">
                    <select multiple size="20" class="form-control list" data-target="assigned"></select>
                </div>
            </div>            
        </div>
    </div>
</div>
