<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\RouteRule;
use mdm\admin\components\Configs;
use cornernote\returnurl\ReturnUrl; 
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\AuthItem */
/* @var $context mdm\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', $labels['Items']);
$this->params['breadcrumbs'][] = $this->title;

$rules = array_keys(Configs::authManager()->getRules());
$rules = array_combine($rules, $rules);
unset($rules[RouteRule::RULE_NAME]);
?>


<div class="card">
    <div class="card-header">
        <div class="d-flex align-items-between">
            <div class="ml-auto">
                <?php
                    if (Helper::checkRoute('create')) {
                        echo Html::a(Yii::t('rbac-admin', 'Create ' . $labels['Item']), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                    }                    
                ?>
            </div>
        </div>
    </div>

    <div class="card-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,        
            'pjax' => true,          
            'pjaxSettings'=>[
                // 'neverTimeout' => false,
                'loadingCssClass' => false,
                'options' => [
                    'id' => 'nli-event-grid-pjax',                    
                ],            
            ],
            'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
            'striped' => false,
            'hover' => false,
            'bordered' => false,
            'responsiveWrap' => false,
            'perfectScrollbar' => false,
            'emptyText' => 'Data tidak ditemukan.',
            'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
            // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
            'pager' => [
                'options' => ['class' => 'pagination pagination-rounded'],
            ],
            'columns' => [           
                [
                    'class' => 'kartik\grid\SerialColumn'
                ],

                [
                'attribute' => 'name',
                'label' => Yii::t('rbac-admin', 'Name'),
                'vAlign' => 'middle',
                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],
            ],
            [
                'attribute' => 'ruleName',
                'label' => Yii::t('rbac-admin', 'Rule Name'),
                'vAlign' => 'middle',
                'filter' => $rules,
                'filterInputOptions' => ['class' => 'form-control custom-select', 'prompt' => 'All Rule'],
            ],
            [
                'attribute' => 'description',
                'vAlign' => 'middle',
                'label' => Yii::t('rbac-admin', 'Description'),
                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],
            ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    // 'width' => '110px',
                    'vAlign' => 'middle',
                    'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->name, 'ru' => ReturnUrl::getToken()],
                            [
                                'class' => 'action-icon',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->name, 'ru' => ReturnUrl::getToken()],
                            [
                                'class' => 'action-icon',                            
                            ]);
                        },                    
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->name, 'ru' => ReturnUrl::getToken()],
                            [
                                'class' => 'action-icon',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'method' => 'post'
                                ]
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>