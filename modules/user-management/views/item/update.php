<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */
/* @var $context mdm\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', $labels['Item']);
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $labels['Items']), 'url' => ['index'], 'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name], 'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'Update');
?>
<div class="auth-item-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'headIcon' => '<i class="flaticon-list"></i>',
        'headText' => Yii::t('app', 'Update ') . $this->title
    ]);
    ?>
</div>
