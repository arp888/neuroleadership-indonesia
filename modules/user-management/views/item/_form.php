<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use mdm\admin\components\RouteRule;
use mdm\admin\AutocompleteAsset;
use yii\helpers\Json;
use mdm\admin\components\Configs;
use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $context mdm\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$rules = Configs::authManager()->getRules();
unset($rules[RouteRule::RULE_NAME]);
$source = Json::htmlEncode(array_keys($rules));

$js = <<<JS
    $('#rule_name').autocomplete({
        source: $source,
    });
JS;
AutocompleteAsset::register($this);
$this->registerJs($js);
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin([
        'id' => 'item-form',
        'options' => ['class' => ''],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

            <?= $form->field($model, 'ruleName')->textInput(['id' => 'rule_name']) ?>

            <?= $form->field($model, 'data')->textarea(['rows' => 4]) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>  
    </div> 

    <?php ActiveForm::end(); ?>

</div>
