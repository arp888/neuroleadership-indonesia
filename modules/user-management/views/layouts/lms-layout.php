<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
// use yii\bootstrap4\Nav;
// use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;
use mdm\admin\components\Helper;
use yii\web\JsExpression;

AppAsset::register($this);

if (!Yii::$app->user->isGuest) $user = \mdm\admin\models\User::findIdentity(Yii::$app->user->identity->id);

// $route = $this->context->route;
$route = Yii::$app->controller->id;
$context = Yii::$app->controller;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Main Wrapper -->
<div class="main-wrapper">

    <!-- Header -->
    <div class="header">
    
        <!-- Logo -->
        <div class="header-left">
            <?= Html::a(Html::img('@web/img/gdmi-logo.png', ['height' => '50', 'alt' => 'GDMI']), Yii::$app->homeUrl, ['class' => 'logo']) ?>
            <?php // echo Html::a(Html::img('@web/img/logo-gdmi-justlogo-white', ['width' => '50', 'height' => '50', 'alt' => 'GDMI']), Yii::$app->homeUrl, ['class' => 'logo']) ?>
        </div>
        <!-- /Logo -->
        
        <a id="toggle_btn" href="javascript:void(0);">
            <span class="bar-icon">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </a>
        
        <!-- Header Title -->
        <div class="page-title-box">
            <h3><?= Yii::t('app', 'Sistem Informasi Administrasi Gadar Medik (SIAGA)') ?></h3>
        </div>
        <!-- /Header Title -->
        
        <a id="mobile_btn" class="mobile_btn" href="#sidebar"><i class="fa fa-bars"></i></a>
        
        <!-- Header Menu -->
        <ul class="nav user-menu">
        
            <!-- Search -->
            <!-- <li class="nav-item">
                <div class="top-nav-search">
                    <a href="javascript:void(0);" class="responsive-search">
                        <i class="fa fa-search"></i>
                   </a>
                    <form action="search.html">
                        <input class="form-control" type="text" placeholder="Search here">
                        <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </li> -->
            <!-- /Search -->
            
            <!-- Flag -->
            <li class="nav-item dropdown has-arrow flag-nav">
                <?php 
                    $getLang = \Yii::$app->request->get('lang');
                    
                    $cookies = Yii::$app->request->cookies;
                    $language = $cookies->getValue('language', 'en');

                    if ($cookies->has('language')) {
                        $language = $cookies['language']->value;
                    }


                    // if (isset($getLang)) {
                    //     $cookies = \Yii::$app->response->cookies;
                    //     $cookies->remove('language');

                    //     $languageCookie = new \yii\web\Cookie([
                    //         'name' => 'language',
                    //         'value' => $getLang,
                    //         'expire' => time() + 60 * 60 * 24 * 30, // 30 days
                    //     ]);
                    //     $cookies->add($languageCookie);
                    // } 

                    // $cookies = Yii::$app->request->cookies;
                    // $language = $cookies->getValue('language');

                    // var_dump($getLang);
                    // var_dump($language); 
                    
                    if ($getLang == "id" || $language == 'id') {
                        echo '<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">' .
                            Html::img('@web/img/flags/id.png', ['alt' => '', 'height' => '20']) . ' <span>Indonesia</span></a>';
                    } else {
                        echo '<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">' .
                            Html::img('@web/img/flags/us.png', ['alt' => '', 'height' => '20']) . ' <span>English</span></a>';
                    }                     
                ?>

                <div class="dropdown-menu dropdown-menu-right">
                    <?= Html::a(Html::img('@web/img/flags/us.png', ['alt' => '', 'height' => '16']) . ' English', \yii\helpers\Url::current(['lang' => 'en']), ['class' => 'dropdown-item']) ?>

                    <?= Html::a(Html::img('@web/img/flags/id.png', ['alt' => '', 'height' => '16']) . ' Indonesia', \yii\helpers\Url::current(['lang' => 'id']), ['class' => 'dropdown-item']) ?>
                </div>
            </li>
            <!-- /Flag -->

            <li class="nav-item dropdown has-arrow main-drop">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <span class="user-img"><?= Html::img('@web/img/user.jpg', ['alt' => '']) ?>
                    <span><?= \yii\helpers\Inflector::titleize($user->username, true) ?></span>
                </a>
                <div class="dropdown-menu">
                    <?= Html::a(Yii::t('app', 'My Profile'), ['/user/profile'], ['class' => 'dropdown-item']) ?>
                    <?= Html::a('Logout', ['/user/logout'], ['class' => 'dropdown-item', 'data-method' => 'post']) ?>

                </div>
            </li>
        </ul>
        <!-- /Header Menu -->
        
        <!-- Mobile Menu -->
        <div class="dropdown mobile-user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
            <div class="dropdown-menu dropdown-menu-right">
                <?= Html::a(Yii::t('app', 'My Profile'), ['/user/profile'], ['class' => 'dropdown-item']) ?>
                <!-- <a class="dropdown-item" href="settings.html">Settings</a> -->
                <?= Html::a('Logout', ['/user/logout'], ['class' => 'dropdown-item', 'data-method' => 'post']) ?>
            </div>
        </div>
        <!-- /Mobile Menu -->
        
    </div>
    <!-- /Header -->
    
    <!-- Sidebar -->
    <div class="sidebar" id="sidebar">
        <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <?php 
                    $items = [
                        [
                            'label' => '<span>Main</span>',
                            'options' => ['class' => 'menu-title'],
                            'url' => null,
                        ],
                        
                        [
                            'label' => '<i class="la la-dashboard"></i> <span>Dashboard</span>',
                            'url' => ['/site/index'],
                            // 'active' => $this->context->route === 'hr/default/index',  
                        ],                        

                        [
                            'label' => '<span>' . Yii::t('app', 'Company Profile') . '</span>',
                            'url' => null,
                            'options' => ['class' => 'menu-title'],
                        ],

                        [
                            'label' => '<i class="la la-industry"></i> <span>' . Yii::t('app', 'Company Profile') . '</span>',
                            'url' => ['/lms/company-profile'],
                            'active' => in_array($route, ['company-profile']),  
                        ],
                        
                        [
                            'label' => '<i class="la la-users"></i> <span>' . Yii::t('app', 'Employees') . '</span>',
                            'url' => ['/lms/employee'],
                            'active' => in_array($route, ['employee']),  
                        ],

                        [
                            'label' => '<i class="la la-cube"></i> <span>' . Yii::t('app', 'Departments') . '</span>',
                            'url' => ['/lms/department'],
                            'active' => in_array($route, ['department']),
                        ],

                        [
                            'label' => '<i class="la la-briefcase"></i> <span>' . Yii::t('app', 'Designations') . '</span>',
                            'url' => ['/lms/designation'],
                            'active' => in_array($route, ['designation']),  
                        ],

                        [
                            'label' => '<i class="la la-columns"></i> <span>' . Yii::t('app', 'Clients') . '</span>',
                            'url' => ['/lms/client'],
                            'active' => in_array($route, ['client']),
                        ],

                        [
                            'label' => '<i class="la la-database"></i> <span>' . Yii::t('app', 'Official Memo') . '</span>',
                            'url' => ['/lms/official-memo'],
                            'active' => in_array($route, ['official-memo']),
                        ],

                        
                        [
                            'label' => '<span>' . Yii::t('app', 'Training') . '</span>',
                            'url' => null,
                            'options' => ['class' => 'menu-title'],
                        ],

                        [
                            'label' => '<i class="la la-calendar"></i> <span>' . Yii::t('app', 'Training Submission') . '</span>',
                            'url' => ['/lms/training-submission'],
                            'active' => in_array($route, ['training-submission']),
                        ],                       
                        
                        [
                            'label' => '<i class="la la-book"></i> <span>' . Yii::t('app', 'Training Schedules') . '</span>',
                            'url' => ['/lms/training-schedule'],
                            'active' => in_array($route, ['training-schedule']), 
                        ],

                        [
                            'label' => '<i class="la la-files-o"></i> <span>' . Yii::t('app', 'Training Curriculum') . '</span>',
                            'url' => ['/lms/training-master'],
                            'active' => in_array($route, ['training-master']),
                        ],

                        [
                            'label' => '<i class="la la-database"></i> <span>' . Yii::t('app', 'Participant Data') . '</span>',
                            'url' => ['/lms/participant'],
                            'active' => in_array($route, ['participant']),
                        ],

                        [
                            'label' => '<i class="la la-edit"></i> <span>' . Yii::t('app', 'Evaluation Template') . '</span>',
                            'url' => ['/lms/training-evaluation-template'],
                            'active' => in_array($route, ['training-evaluation-template']), 
                        ],        

                        [
                            'label' => '<span>' . Yii::t('app', 'System Manager') . '</span>',
                            'url' => null,
                            'options' => ['class' => 'menu-title'],
                        ],               

                        // [
                        //     'label' => '<span>' . Yii::t('app', 'Report') . '</span>',
                        //     'url' => null,
                        //     'options' => ['class' => 'menu-title'],
                        // ],

                        // [
                        //     'label' => '<i class="la la-book"></i> <span>' . Yii::t('app', 'Training Report') . '</span>',
                        //     'url' => ['/lms/training-schedule'],
                        //     'active' => in_array($route, ['training-schedule']), 
                        // ],                       

                        [
                            'label' => '<i class="la la-user"></i> <span>' . Yii::t('app', 'User Manager') . '</span><span class="menu-arrow"></span>',
                            'url' => '#',
                            'options' => ['class' => 'submenu'],
                            'template' => '<a href="{url}" class=' . (in_array($route, ['user', 'assignment', 'role', 'permission', 'route']) ? 'active subdrop' : null) . '>{label}</a>',
                            'items' => [
                                [
                                    'label' => Yii::t('app', 'User List'),
                                    'url' => ['/admin/user/index'],
                                    'template' => '<a href="{url}" class=' . ($route == 'user' ? 'active' : null). '>{label}</a>',
                                    // 'active' => $route == 'user',
                                ],  

                                // [
                                //     'label' => Yii::t('app', 'Assignments'),
                                //     'url' => ['/admin/assignment'],
                                //     'template' => '<a href="{url}" class=' . ($route == 'assignment' ? 'active' : null). '>{label}</a>',
                                //     // 'active' => $route == 'admin',
                                // ],

                                [
                                    'label' => Yii::t('app', 'Roles'),
                                    'url' => ['/admin/role'],
                                    'template' => '<a href="{url}" class=' . ($route == 'role' ? 'active' : null). '>{label}</a>',
                                    // 'active' => $route == 'admin',
                                ], 

                                [
                                    'label' => Yii::t('app', 'Permissions'),
                                    'url' => ['/admin/permission'],
                                    'template' => '<a href="{url}" class=' . ($route == 'permission' ? 'active' : null). '>{label}</a>',
                                    // 'active' => $route == 'admin',
                                ], 

                                [
                                    'label' => Yii::t('app', 'Routes'),
                                    'url' => ['/admin/route'],
                                    'template' => '<a href="{url}" class=' . ($route == 'route' ? 'active' : null). '>{label}</a>',
                                    // 'active' => $route == 'admin',
                                ], 
                            ]
                        ],

                        [
                            'label' => '<i class="la la-gear"></i> <span>' . Yii::t('app', 'General Setting') . '</span>',
                            'url' => ['/lms/general-setting'],
                            'active' => in_array($route, ['general-setting']), 
                        ], 

                        // [
                        //     'label' => '<i class="la la-user"></i> <span>' . Yii::t('app', 'User Manager') . '</span><span class="menu-arrow"></span>',
                        //     'url' => 'javascript:;',
                        //     'options' => ['class' => 'submenu' . (in_array($route, []) ? ' kt-menu__item--expanded kt-menu__item--hover kt-menu__item--open' : null), 'kt-menu-submenu-toggle' => 'click', 'aria-haspopup' => 'true'],
                        //     'template' => '<a href="{url}">{label}</a>',
                        //     'items' => [
                        //         [
                        //             'label' => Yii::t('app', 'User List'),
                        //             'url' => ['/admin/user/index'],
                        //             'active' => $route == 'admin',
                        //         ],  

                        //         [
                        //             'label' => Yii::t('app', 'User Assignment'),
                        //             'url' => ['/admin/assignment'],
                        //             'active' => $route == 'admin',
                        //         ], 
                        //     ]
                        // ]

                                
                        
                    ];

                    // $items = Helper::filter($items);

                    echo Menu::widget([
                        'items' => $items,
                        // 'options' => ['class' => 'kt-menu__nav'],
                        // 'submenuTemplate' => '<div class="kt-menu__submenu"><span class="kt-menu__arrow kt-menu__arrow--adjust"></span><ul class="kt-menu__subnav">{items}</ul></div>',
                        'encodeLabels' => false,
                        'activateItems' => true,
                        // 'activeCssClass' => '',
                        // 'activateParents' => true,
                    ]);
                ?>
                <!-- <ul>
                    <li class="menu-title"> 
                        <span>Main</span>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="la la-dashboard"></i> <span> Dashboard</span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            <li><a class="active" href="index.html">Admin Dashboard</a></li>
                            <li><a href="employee-dashboard.html">Employee Dashboard</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="la la-cube"></i> <span> Apps</span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            <li><a href="chat.html">Chat</a></li>
                            <li class="submenu">
                                <a href="#"><span> Calls</span> <span class="menu-arrow"></span></a>
                                <ul style="display: none;">
                                    <li><a href="voice-call.html">Voice Call</a></li>
                                    <li><a href="video-call.html">Video Call</a></li>
                                    <li><a href="outgoing-call.html">Outgoing Call</a></li>
                                    <li><a href="incoming-call.html">Incoming Call</a></li>
                                </ul>
                            </li>
                            <li><a href="events.html">Calendar</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="inbox.html">Email</a></li>
                            <li><a href="file-manager.html">File Manager</a></li>
                        </ul>
                    </li>
                    <li class="menu-title"> 
                        <span>Employees</span>
                    </li>
                    <li class="submenu">
                        <a href="#" class="noti-dot"><i class="la la-user"></i> <span> Employees</span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            <li><a href="employees.html">All Employees</a></li>
                            <li><a href="holidays.html">Holidays</a></li>
                            <li><a href="leaves.html">Leaves (Admin) <span class="badge badge-pill bg-primary float-right">1</span></a></li>
                            <li><a href="leaves-employee.html">Leaves (Employee)</a></li>
                            <li><a href="leave-settings.html">Leave Settings</a></li>
                            <li><a href="attendance.html">Attendance (Admin)</a></li>
                            <li><a href="attendance-employee.html">Attendance (Employee)</a></li>
                            <li><a href="departments.html">Departments</a></li>
                            <li><a href="designations.html">Designations</a></li>
                            <li><a href="timesheet.html">Timesheet</a></li>
                            <li><a href="overtime.html">Overtime</a></li>
                        </ul>
                    </li>
                    <li> 
                        <a href="clients.html"><i class="la la-users"></i> <span>Clients</span></a>
                    </li>                    
                </ul> -->
            </div>
        </div>
    </div>
    <!-- /Sidebar -->
    
    <!-- Page Wrapper -->
    <div class="page-wrapper">
    
        <!-- Page Content -->
        <div class="content container-fluid">
        
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
                        <?= Breadcrumbs::widget([
                            'options' => ['class' => 'breadcrumb'],
                            'tag' => 'ul',
                            'itemTemplate' => "<li class=\"breadcrumb-item\">{link}</li>\n",
                            'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                            'homeLink' => [
                                'label' => 'Dashboard', 
                                'url' => Yii::$app->homeUrl, 
                            ],
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            'encodeLabels' => false,
                        ]) ?>
                    </div>
                    
                    <?php if (isset($this->blocks['subheader'])): ?>
                    <?= $this->blocks['subheader'] ?>                
                    <?php endif; ?>
                        
                </div>
            </div>

            <!-- /Page Header -->
        
            <?= $content ?>
        
        </div>
        <!-- /Page Content -->

    </div>
    <!-- /Page Wrapper -->
    
</div>
<!-- /Main Wrapper -->

<?php 
foreach (Yii::$app->session->getAllFlashes() as $message) {
    if (Yii::$app->session->hasFlash('success')) {        
        $flash = Yii::$app->session->getFlash('success');     
        $type = \yii\helpers\Json::htmlEncode($flash['type']);
        $title = \yii\helpers\Json::htmlEncode('<h4>' . $flash['title'] . '</h4>');
        $message = \yii\helpers\Json::htmlEncode($flash['message']);
        $this->registerJs("
            jQuery(document).ready(function() {    
                var content = {};
                content.title = $title;
                content.message = $message;
                var notify = $.notify(content, { 
                    type: $type,                
                    spacing: 10,                    
                    timer: 2000,
                    placement: {
                        from: 'bottom', 
                        align: 'left'
                    },
                    offset: {
                        x: 30, 
                        y: 30
                    },
                    delay: 1000,
                    z_index: 10000,
                    animate: {
                        enter: 'animated slideInUp',
                        exit: 'animated slideOutDown'
                    }
                });
            });        
        ");
    }

    if (Yii::$app->session->hasFlash('error')) {        
        $flash = Yii::$app->session->getFlash('error');     
        $type = \yii\helpers\Json::htmlEncode($flash['type']);
        $title = \yii\helpers\Json::htmlEncode($flash['title']);
        $message = \yii\helpers\Json::htmlEncode($flash['message']); 
        $this->registerJs("
            Swal.fire({
              icon: $type,
              title: $title,
              text: $message,
            })
        ");
    } 
}
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
