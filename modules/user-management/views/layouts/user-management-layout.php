<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\assets\MintonAsset;

MintonAsset::register($this);

$route = Yii::$app->controller->id;
$context = Yii::$app->controller;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="loading">
<?php $this->beginBody() ?>

<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <!-- LOGO -->
        <div class="logo-box">
            <?= Html::a('
                <span class="logo-sm">' . Html::img('@web/images/logo-nli-img-white.png', ['height' => 70, 'alt' => '']) . '</span>
                <span class="logo-lg">' .  Html::img('@web/images/logo-dark-nli.png', ['height' => 70, 'alt' => '']) . '</span>',
                Yii::$app->homeUrl,
                [
                    'class' => 'logo logo-dark text-center'
                ]
            ) ?>

            <?= Html::a('
                <span class="logo-sm">' . Html::img('@web/images/logo-nli-img-white.png', ['height' => 70, 'alt' => '']) . '</span>
                <span class="logo-lg">' .  Html::img('@web/images/logo-dark-nli.png', ['height' => 70, 'alt' => '']) . '</span>',
                Yii::$app->homeUrl,
                [
                    'class' => 'logo logo-light text-center'
                ]
            ) ?>
        </div>

        <div class="h-100" data-simplebar>

            <?php if (!Yii::$app->user->isGuest): ?>

            <?php 
                $user = \app\models\UserProfile::find()->where(['id' => Yii::$app->user->identity->id])->one();
                if ($user->avatar != null) {
                    $avatar = $user->getImageUrl();
                } else {
                    $avatar = Yii::getAlias('@web/images/user.jpg');
                }
                $name = $user->fullname;                         
            ?>

            <!-- User box -->
            <div class="user-box text-center">
                <?= Html::img($avatar, ['class' => 'rounded-circle avatar-md', 'alt' => $name]) ?>   
                <div class="dropdown">
                    <a href="javascript: void(0);" class="text-reset dropdown-toggle h5 mt-2 mb-1 d-block"
                        data-toggle="dropdown"><?= $name ?></a>
                    <div class="dropdown-menu user-pro-dropdown">
                        <!-- item-->
                        <?php // echo Html::a('<i class="ri-account-circle-line"></i> <span> Profil Pengguna</span>', ['/user-management/user/user-profile', 'id' => Yii::$app->user->identity->id], ['class' => 'dropdown-item notify-item', 'data' => ['method' => 'post']]) ?>
                        
                        <?= Html::a('<i class="ri-logout-box-line"></i> <span> Logout</span>', ['/user-management/user/logout'], ['class' => 'dropdown-item notify-item', 'data' => ['method' => 'post']]) ?> 
                        

                    </div>
                </div>
                <p class="text-reset">Admin Head</p>
            </div>

        <?php endif; ?>

            <!--- Sidemenu -->
            <div id="sidebar-menu">                
                <?php 
                    $nav_items = [
                        // [
                        //     'label' => '<i class="ri-dashboard-line"></i><span>Dashboard</span>', 
                        //     'url' => ['/scoreboard/'], 
                        //     'active' => ($this->context->route === 'scoreboard/default/index'),
                        //     'template' => '<a class="waves-effect'. ($this->context->route == 'scoreboard/default/index' ? ' active' : null) . '" href="{url}">{label}</a>',
                        // ],
                        // [
                        //     'label' => '<hr class="my-0">', 
                        //     'url' => null, 
                        //     'options' => ['class' => 'menu-title'],
                        // ],

                        [
                            'label' => '<i class="ri-group-line"></i><span>User List</span>', 
                            'url' => ['/user-management/user/index'], 
                            'active' => ($this->context->id === 'user'),
                            'template' => '<a class="waves-effect'. ($this->context->id == 'user' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],                      
                                          
                        [
                            'label' => '<i class="ri-shield-user-line"></i><span>Role</span>', 
                            'url' => ['/user-management/role'],
                            'active' => ($this->context->id === 'role'),
                            'template' => '<a class="waves-effect'. ($this->context->id == 'role' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],

                        [
                            'label' => '<i class="ri-user-shared-2-line"></i><span>Permission</span>', 
                            'url' => ['/user-management/permission'],
                            'active' => ($this->context->id === 'permission'),
                            'template' => '<a class="waves-effect'. ($this->context->id == 'permission' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],

                        [
                            'label' => '<i class="ri-user-4-line"></i><span>Route</span>', 
                            'url' => ['/user-management/route'],
                            'active' => ($this->context->id === 'route'),
                            'template' => '<a class="waves-effect'. ($this->context->id == 'route' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],

                    ];

                    echo Menu::widget([
                        'items' => $nav_items,
                        // 'options' => ['class' => 'navigation-menu'],
                        'submenuTemplate' => "<ul class='side-menu'>{items}</ul>",
                        'encodeLabels' => false,
                        'activeCssClass' => 'menuitem-active',
                        'activateParents' => true,
                    ]);                    
                ?>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">

                    <ul class="list-unstyled topnav-menu float-right mb-0">
                        <li class="dropdown d-none d-lg-inline-block">
                            <a class="nav-link arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                                <i class="fe-maximize noti-icon"></i>
                            </a>
                        </li>    

                        <?php if (!Yii::$app->user->isGuest): ?>

                        <?php 
                            $user = \app\models\UserProfile::find()->where(['id' => Yii::$app->user->identity->id])->one();
                            if ($user->avatar != null) {
                                $avatar = $user->getImageUrl();
                            } else {
                                $avatar = Yii::getAlias('@web/images/user.jpg');
                            }
                            $name = $user->fullname;                         
                        ?>

                        <li class="dropdown notification-list topbar-dropdown">
                            <a class="nav-link nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <?= Html::img($avatar, ['class' => 'rounded-circle', 'alt' => $name]) ?>   
                                
                                <!-- <img src="../assets/images/users/avatar-1.jpg" alt="user-image" class="rounded-circle"> -->
                                
                                <span class="pro-user-name ml-1">
                                    <?= $name ?> <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Welcome !</h6>
                                </div>

                                <?php // echo Html::a('<i class="ri-account-circle-line"></i> <span> Profil Pengguna</span>', ['/user-management/user/user-profile', 'id' => Yii::$app->user->identity->id], ['class' => 'dropdown-item notify-item', 'data' => ['method' => 'post']]) ?>
                                
                                <?= Html::a('<i class="ri-logout-box-line"></i> <span> Logout</span>', ['/user-management/user/logout'], ['class' => 'dropdown-item notify-item', 'data' => ['method' => 'post']]) ?>                               

                            </div>
                        </li>

                        <?php endif; ?>


                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="index.html" class="logo logo-dark text-center">
                            <span class="logo-sm">
                                <?= Html::img('@web/images/logo-nli-img-white.png', ['height' => 70, 'alt' => '']) ?>
                            </span>
                            <span class="logo-lg">
                                <?= Html::img('@web/images/logo-dark-nli.png', ['height' => 70, 'alt' => '']) ?>
                            </span>
                        </a>

                        <a href="index.html" class="logo logo-light text-center">
                            <span class="logo-sm">
                                <?= Html::img('@web/images/logo-nli-img-white.png', ['height' => 70, 'alt' => '']) ?>
                            </span>
                            <span class="logo-lg">
                                <?= Html::img('@web/images/logo-dark-nli.png', ['height' => 70, 'alt' => '']) ?>
                            </span>
                        </a>
                    </div>

                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                        <li>
                            <button class="button-menu-mobile waves-effect waves-light">
                                <i class="fe-menu"></i>
                            </button>
                        </li>

                        <li>
                            <!-- Mobile menu toggle (Horizontal Layout)-->
                            <a class="navbar-toggle nav-link" data-toggle="collapse" data-target="#topnav-menu-content">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>   
    

                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->

            <!-- Start Content-->
            <div class="container-fluid">
                
                <!-- start page title -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title"><?= $this->title ?></h4> 
                            <div class="page-title-right">     
                                <?= Breadcrumbs::widget([
                                    'options' => ['class' => 'breadcrumb m-0'],
                                    'tag' => 'ol',
                                    'itemTemplate' => "<li class=\"breadcrumb-item\">{link}</li>\n",
                                    'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                                    'homeLink' => ['label' => 'Home', 'url' => ['/user-management/user']],
                                    'encodeLabels' => false,
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ]) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title --> 

                <div class="row">
                    <div class="col-12">                        
                        <?= $content ?>                            
                    </div><!-- end col --> 
                </div>
                <!-- end row -->
                
            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <script>document.write(new Date().getFullYear())</script> &copy; NeuroLeadership Indonesia 
                    </div>
                   <!--  <div class="col-md-6">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="javascript:void(0);">About Us</a>
                            <a href="javascript:void(0);">Help</a>
                            <a href="javascript:void(0);">Contact Us</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->
<script>
    var resizefunc = [];
</script>

<?php 
$this->registerJsFile('@web/admin-assets/js/modernizr.min.js', ['depends'=> [], 'position' => \yii\web\View::POS_HEAD]);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
