<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this  yii\web\View */
/* @var $model mdm\admin\models\BizRule */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\BizRule */

$this->title = Yii::t('rbac-admin', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-index">
    <div class="kt-portlet">  
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon"><i class="flaticon-layers"></i></span>
                <h3 class="kt-portlet__head-title"><?= Html::encode($this->title . ' List') ?></h3>
            </div>
            <div class="kt-portlet__head-toolbar">    
                <?= Html::a('<i class="la la-plus pr-md-2 pr-0"></i><span class="kt-hidden-mobile">' . Yii::t('rbac-admin', 'New Rule'), ['create'], ['class' => 'btn btn-brand btn-icon-sm']) ?>                       
            </div>
        </div>

        <div class="kt-portlet__body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n<div class='custom__pager custom--paging-loaded clearfix'>{pager}<div class='custom__pager-info'><span class='custom__pager-detail'>{summary}</span></div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'custom__pager-nav'],
                    // 'activePageCssClass' => 'custom__pager-link--active',
                    'linkOptions' => ['class' => 'custom__pager-link custom__pager-link-number'],
                    // 'disabledPageCssClass' => 'custom__pager-link custom__pager-link--first custom__pager-link--disabled',
                    'linkContainerOptions' => ['class' => ''],
                    'prevPageCssClass' => 'prev',
                    'prevPageLabel' => '<i class="la la-angle-left"></i>',
                    'nextPageCssClass' => 'next',
                    'nextPageLabel' => '<i class="la la-angle-right"></i>',
                    'maxButtonCount' => 5,
                    // 'disableCurrentPageButton' => true,
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'custom__pager-link custom__pager-link--first m-datatable__pager-link--disabled', 'href' => '#'],
                    'firstPageCssClass' => 'first',
                    'firstPageLabel' => '<i class="la la-angle-double-left"></i>',
                    'lastPageCssClass' => 'last',
                    'lastPageLabel' => '<i class="la la-angle-double-right"></i>',
                ],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('rbac-admin', 'Name'),
                        'vAlign' => 'middle',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'width' => '125px',
                        'template' => '{view}{update}{delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="la la-list"></i>', $url,
                                [
                                    'class' => 'btn btn-sm btn-clean btn-icon btn-icon-md',
                                    'data-toggle' => 'kt-tooltip',
                                    'title' => "", 
                                    'data-original-title' => "Detail",
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="la la-edit"></i>', $url,
                                [
                                    'class' => 'btn btn-sm btn-clean btn-icon btn-icon-md',
                                    'data-toggle' => 'kt-tooltip',
                                    'title' => "", 
                                    'data-original-title' => "Perbarui",
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="la la-trash"></i>', $url,
                                [
                                    'class' => 'btn btn-sm btn-clean btn-icon btn-icon-md',
                                    'data-toggle' => 'kt-tooltip',
                                    'title' => "", 
                                    'data-original-title' => "Hapus",
                                    'data' => [
                                        'confirm' => Yii::t('rbac-admin', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>

        </div>
    </div>
</div>
