<?php

use yii\helpers\Html;

/* @var $this  yii\web\View */
/* @var $model mdm\admin\models\BizRule */

$this->title = Yii::t('rbac-admin', 'New Rule');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Rules'), 'url' => ['index'], 'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'headIcon' => '<i class="flaticon-file-1"></i>',
        'headText' => Yii::t('rbac-admin', 'New ') . $this->title
    ]);
    ?>

</div>
