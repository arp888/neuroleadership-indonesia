<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this  yii\web\View */
/* @var $model mdm\admin\models\BizRule */
/* @var $form ActiveForm */
?>

<div class="auth-item-form">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'kt-form kt-form--label-right'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group kt-form__group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="main_portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon"><?= $headIcon ?></span>
                <h3 class="kt-portlet__head-title"><?= Html::encode($headText) ?></h3>
            </div>
            <div class="kt-portlet__head-toolbar">    
                <?= Html::a('<i class="la la-arrow-left pr-md-2 pr-0"></i><span class="kt-hidden-mobile">' . Yii::t('app', 'Back') . '</span>', Yii::$app->request->referrer, ['class' => 'btn btn-clean']) ?>                       
            </div>
        </div>

        <div class="kt-portlet__body">

            <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

            <?= $form->field($model, 'className')->textInput() ?>

        </div>
        <div class="kt-portlet__foot">            
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-md-7 offset-md-3">
                        <?= Html::submitButton('<i class="la la-check"></i><span>' . Yii::t('rbac-admin', 'Save') . '</span>', ['class' => 'btn btn-brand', 'name' => 'submit-button']) ?>
                        <?= Html::a('<i class="la la-remove"></i>' . Yii::t('rbac-admin', 'Cancel') . '</span>', Yii::$app->request->referrer, ['class' => 'btn btn-clean kt-margin-r-10']) ?>
                    </div>
                </div>                
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
