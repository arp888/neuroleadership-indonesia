<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('rbac-admin', 'Assignments');
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'attribute' => $usernameField,
        'vAlign' => 'middle',
        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],
    ],
];
if (!empty($extraColumns)) {
    $columns = array_merge($columns, $extraColumns);
}
$columns[] = [
    'class' => 'kartik\grid\ActionColumn',
    'template' => '{view}',
    'width' => '125px',
    'viewOptions' => [
        'label' => '<i class="la la-list"></i>', 
        'class' => 'btn btn-sm btn-clean btn-icon btn-icon-md',
        'data-toggle' => 'kt-tooltip',
        'title' => "", 
        'data-original-title' => "Detail",
    ],
    // 'updateOptions' => [
    //     'label' => '<i class="la la-edit"></i>', 
    //     'class' => 'm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill', 
    //     'data-toggle' => 'm-tooltip',
    //     'title' => "", 
    //     'data-original-title' => "Perbarui",
    // ],
    // 'deleteOptions' => [
    //     'label' => '<i class="la la-trash"></i>',
    //     'class' => 'm-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill',
    //     'data-toggle' => 'm-tooltip',
    //     'title' => "", 
    //     'data-original-title' => "Hapus",
    //     'data' => [
    //         'confirm' => 'Anda yakin ingin menghapus item ini?',
    //         'method' => 'post'
    //     ]
    // ],
];
?>
<div class="assignment-index">
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile">    
        <div class="kt-portlet__head kt-portlet__head--lg" style="">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon"><i class="flaticon-layers"></i></span>
                <h3 class="kt-portlet__head-title"><?= Html::encode($this->title . ' List') ?></h3>
            </div>            
        </div>   
        <div class="kt-portlet__body">

            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n<div class='custom__pager custom--paging-loaded clearfix'>{pager}<div class='custom__pager-info'><span class='custom__pager-detail'>{summary}</span></div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                // 'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
                'pager' => [
                    'options' => ['class' => 'custom__pager-nav'],
                    // 'activePageCssClass' => 'custom__pager-link--active',
                    'linkOptions' => ['class' => 'custom__pager-link custom__pager-link-number'],
                    // 'disabledPageCssClass' => 'custom__pager-link custom__pager-link--first custom__pager-link--disabled',
                    'linkContainerOptions' => ['class' => ''],
                    'prevPageCssClass' => 'prev',
                    'prevPageLabel' => '<i class="la la-angle-left"></i>',
                    'nextPageCssClass' => 'next',
                    'nextPageLabel' => '<i class="la la-angle-right"></i>',
                    'maxButtonCount' => 5,
                    // 'disableCurrentPageButton' => true,
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'custom__pager-link custom__pager-link--first m-datatable__pager-link--disabled', 'href' => '#'],
                    'firstPageCssClass' => 'first',
                    'firstPageLabel' => '<i class="la la-angle-double-left"></i>',
                    'lastPageCssClass' => 'last',
                    'lastPageLabel' => '<i class="la la-angle-double-right"></i>',
                ],
                'columns' => $columns,
            ]);
            ?>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
