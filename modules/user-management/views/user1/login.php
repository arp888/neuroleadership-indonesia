<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

$this->title = Yii::t('rbac-admin', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="kt-login__signin">
   
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'kt-form mt-3'],
    ]); ?>   

    <?= $form->field($model, 'username', ['options' => ['class' => 'form-group input-group']])->textInput([
            'class' => 'form-control',
            'placeholder' => 'Username',
            'autocomplete' => 'off',
        ])->label(false) 
    ?>

    <?= $form->field($model, 'password', ['options' => ['class' => 'form-group input-group']])->passwordInput([
            'class' => 'form-control',
            'placeholder' => 'Password',
        ])->label(false) 
    ?>
    
    <div class="row kt-login__extra mb-0">
        <div class="col pb-1">
            <?= $form->field($model, 'rememberMe', [
                    'checkEnclosedTemplate' => "<div class=\"kt-checkbox-list\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",                    
                ])->checkbox(['value' => false], true)->label(Yii::t('app', 'Remember Me') . '<span></span>', ['class' => 'kt-checkbox kt-checkbox--light'])
            ?>
        </div>
        <div class="col kt-align-right pb-1">
            <?= Html::a(Yii::t('app', 'Forget password?'), ['/user/request-password-reset'], ['class' => 'kt-link kt-login__link']) ?>
        </div>
    </div>

    <div class="kt-login__actions mt-0">
        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-pill kt-login__btn-primary', 'name' => 'login-button']) ?>
       
    </div>

    <?php ActiveForm::end(); ?>

</div>