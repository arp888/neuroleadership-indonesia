<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;
use mdm\admin\AnimateAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\YiiAsset;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$controllerId = $this->context->uniqueId . '/';

AnimateAsset::register($this);
YiiAsset::register($this);
$opts = Json::htmlEncode([
    'items' => $modelAssignment->getItems(),
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
// $this->registerJsFile('@web/theme/assets/js/pages/components/extended/dual-listbox.js', ['depends' => [\app\assets\MetronicVerticalAsset::className()]]);
$animateIcon = ' <i class="fa fa-refresh glyphicon-refresh-animate"></i>';

?>

<?php $this->beginBlock('subheader') ?>
<div class="col-auto float-right ml-auto">
    <?= Html::a('<i class="la la-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>              
            
    <div class="btn-group" role="group" aria-label="...">
        <?php 
        if ($model->status == 0 && Helper::checkRoute($controllerId . 'activate')) {
            echo Html::a(Yii::t('app', 'Activate'), ['activate', 'id' => $model->id], [
                'class' => 'btn btn-warning',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to activate this user?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        
        <?php if (Helper::checkRoute('delete')): ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to inactivated this user?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
    </div> 
</div>
<?php  $this->endBlock() ?>

<div class="user-view">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-view">           
                <?= DetailView::widget([
                    'model' => $model,
                    'options' => ['class' => 'ul'],
                    'template' => '<li class="d-flex align-items-center"><div class="title" style="width:35%">{label}</div><div class="text">{value}</div></li>',
                    'attributes' => [
                        'username',
                        'email:email',                                                 
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="detail-view">           
                <?= DetailView::widget([
                    'model' => $model,
                    'options' => ['class' => 'ul'],
                    'template' => '<li class="d-flex align-items-center"><div class="title" style="width:35%">{label}</div><div class="text">{value}</div></li>',
                    'attributes' => [                        
                        'created_at:date',
                        [
                            'attribute' => 'status',                                
                            'value' => function($model) {
                                return $model->status == 0 ? 'Inactive' : 'Active';
                            },                                
                        ],                         
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    

    <hr>

    <p class="lead"><?= Yii::t('app', 'Role & Permission') ?></p>                                    

    <div class="row">
        <div class="col">
            <input class="form-control search mb-2" data-target="available"
                   placeholder="<?=Yii::t('rbac-admin', 'Search for available');?>">
            <select multiple size="20" class="form-control list" data-target="available"></select>
        </div>
        <div class="col-sm-1 text-center">
            <br><br>
            <?=Html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => (string) $model->id], [
                'class' => 'btn btn-success btn-assign',
                'data-target' => 'available',
                'title' => Yii::t('rbac-admin', 'Assign'),
            ]);?>
            <br><br>
            <?=Html::a('&lt;&lt;' . $animateIcon, ['revoke', 'id' => (string) $model->id], [
                'class' => 'btn btn-danger btn-assign mb-5 mb-md-0',
                'data-target' => 'assigned',
                'title' => Yii::t('rbac-admin', 'Remove'),
            ]);?>
        </div>
        <div class="col">
            <input class="form-control search mb-2" data-target="assigned"
                   placeholder="<?=Yii::t('rbac-admin', 'Search for assigned');?>">
            <select multiple size="20" class="form-control list" data-target="assigned"></select>
        </div>
    </div>
               
</div>
