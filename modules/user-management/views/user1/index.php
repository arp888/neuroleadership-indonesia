<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User List');
$this->params['breadcrumbs'][] = $this->title;

// echo '<pre>';
// var_dump($this->params['breadcrumbs']);
// echo '</pre>';

?>

<?php $this->beginBlock('subheader') ?>
<div class="col-auto float-right ml-auto">
    <?php
        if (Helper::checkRoute('create')) {
            echo Html::a('<i class="la la-user"></i> ' . Yii::t('app', 'Register User'), ['signup', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
        }
    ?>
</div>
<?php $this->endBlock() ?>

<div class="user-index">
    <?php echo $this->render('_search', ['model' => $searchModel]) ?> 
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
        'striped' => true,
        'hover' => false,
        'bordered' => false,
        'responsiveWrap' => false,
        'perfectScrollbar' => false,
        // 'emptyText' => 'Belum ada data',
        'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
        // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
        'pager' => [
            'options' => ['class' => 'pagination mb-0'],
            // 'linkOptions' => ['class' => 'custom__pager-link custom__pager-link-number'],
            // 'linkContainerOptions' => ['class' => ''],
            'prevPageCssClass' => 'prev',
            'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
            'nextPageCssClass' => 'next',
            'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
            'maxButtonCount' => 5,
            // // 'disableCurrentPageButton' => true,
            // 'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'custom__pager-link custom__pager-link--first m-datatable__pager-link--disabled', 'href' => '#'],
            'firstPageCssClass' => 'first',
            'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'lastPageCssClass' => 'last',
            'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'username',
                'vAlign' => 'middle',
                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->username, ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()]);
                }
            ],

            [
                'attribute' => 'email',
                'format' => 'email',
                'vAlign' => 'middle',
                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],
            ],

            [
                'attribute' => 'last_login_at',
                'vAlign' => 'middle',
                'mergeHeader' => true,
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->last_login_at, 'long');
                }
            ],

            [
                'attribute' => 'status',
                'vAlign' => 'middle',
                'value' => function($model) {
                    return $model->status == 0 ? 'Inactive' : 'Active';
                },
                'filter' => [
                    0 => 'Inactive',
                    10 => 'Active'
                ],
                'filterInputOptions' => ['class' => 'form-control custom-select', 'prompt' => 'All Status'],
            ],

            [
                'class' => 'app\widgets\ActionColumn',
                'vAlign' => 'middle',
                'dropdown' => true,
                'dropdownOptions' => ['class' => 'dropdown dropdown-action'],
                'dropdownButton' => ['label' => '<i class="material-icons">more_vert</i>', 'class' => 'action-icon dropdown-toggle', 'data-toggle' => 'dropdown'],
                'dropdownMenu' => ['class' => 'dropdown-menu dropdown-menu-right'],
                'template' => Helper::filterActionColumn(['view', 'activate', 'delete']),
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="far fa-eye m-r-5"></i>' . Yii::t('app', 'View'), ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                        [
                            'class' => 'dropdown-item',
                        ]);
                    },
                    // 'activate' => function ($url, $model) {
                    //     return Html::a('<i class="fa fa-refresh m-r-5"></i>' . Yii::t('app', 'Activate'), ['activate', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                    //     [
                    //         'class' => 'dropdown-item',
                    //         'data' => [
                    //             'confirm' => Yii::t('app', 'Are you sure you want to activate this user?'),
                    //             'method' => 'post',
                    //             'pjax' => 0,
                    //         ]     

                    //     ]);
                    // },                    
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="far fa-trash-alt m-r-5"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                        [
                            'class' => 'dropdown-item',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to inactivated this user?'),
                                'method' => 'post',                                
                            ]
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    
</div>
