<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$this->title = Yii::t('rbac-admin', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('subheader') ?>
<div class="col-auto float-right ml-auto">          
    <?= Html::a('<i class="la la-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>              
</div>
<?php  $this->endBlock() ?>

<div class="site-signup">    
    <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'options' => ['class' => ''],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <?= Html::errorSummary($model)?>

    <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
            
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'retypePassword')->passwordInput() ?>

    <hr>

    <div class="row">
        <div class="col-md-9 offset-md-3">
            <?= Html::submitButton(Yii::t('app', 'Register'), ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
            <?= Html::a('<i class="la la-remove"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
        </div>  
    </div>

    <?php ActiveForm::end(); ?>
</div>
