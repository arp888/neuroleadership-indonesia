<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\lms\models\EmployeeSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="employee-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-4">  
            <?= $form->field($model, 'username')->textInput([
                    'class' => 'form-control floating'
                ])->label($model->getAttributeLabel('username'), ['class' => 'focus-label']) 
            ?>            
        </div>

       
        <div class="col-sm-4 col-md-4"> 
            <?= $form->field($model, 'status', [
                    'options' => ['class' => 'form-group form-focus select-focus']
                ])->widget(Select2::classname(), [
                    'data' => [0 => Yii::t('app', 'Inactive'), 10 => Yii::t('app', 'Active')],
                    'options' => ['class' => 'select floating', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select Status')],
                    'theme' => Select2::THEME_DEFAULT,                                
                    'pluginOptions' => [
                        'allowClear' => true,
                        // 'disabled' => true, 
                    ],
 
                ])->label($model->getAttributeLabel('status'), ['class' => 'focus-label']) 
            ?>
        </div>
        <div class="col-sm-4 col-md-4">  
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>     
    </div>        
        
    <?php ActiveForm::end(); ?>

</div>
