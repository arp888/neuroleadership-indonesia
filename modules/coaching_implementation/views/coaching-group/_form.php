<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\datecontrol\DateControl;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroup */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-group-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
                    <?= $form->field($model, 'group_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(\dosamigos\tinymce\TinyMce::classname(), [
                'options' => ['rows' => 3],
                // 'language' => 'id',
                'clientOptions' => [
                'branding' => false,
                'menubar' => false,
                'relative_urls' => false,
                'remove_script_host' => false,
                'convert_urls' => true,
                'plugins' => [
                'advlist autolink lists link charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste image'
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
                ] 
            ])->hint(Yii::t('app', 'Pilih' . ' ' . $model->getAttributeLabel('description'))) ?>

            <?= $form->field($model, 'start_date', [
                'selectors' => ['input' => '#coachinggroup-start_date-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'coachinggroup-start_date'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('start_date')) ?>

            <?= $form->field($model, 'end_date', [
                'selectors' => ['input' => '#coachinggroup-end_date-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'coachinggroup-end_date'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('end_date')) ?>

            <?= $form->field($model, 'is_active')->widget(SwitchInput::classname(), [
                'options' => ['uncheck' => Yii::$app->appHelper::STATUS_INACTIVE, 'value' => Yii::$app->appHelper::STATUS_ACTIVE],
                'inlineLabel' => true,
                'pluginOptions' => [
                   'onColor' => 'success',
                   'onText' => Yii::t('app', 'Active'),
                   'offText' => Yii::t('app', 'Inactive'),
                ],
             ])->hint(false) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
