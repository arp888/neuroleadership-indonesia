<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\coaching_implementation\models\CoachingGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Coaching Group');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="coaching-group-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create Coaching Group'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'coaching-group-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    [
                        'attribute' => 'group_name',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'description',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'mergeHeader' => true, 
                    ],
                    [
                        'attribute' => 'start_date',
                        'format' => ['date', 'long'],
                        'mergeHeader' => true, 
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'end_date',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'format' => ['date', 'long'],
                        'mergeHeader' => true, 
                    ],
                    [
                        'attribute' => 'is_active',
                        'width' => '130px',
                        'filter' => Yii::$app->appHelper::getStatusLists(),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return Yii::$app->appHelper->getStatusLabel($model->is_active);
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>