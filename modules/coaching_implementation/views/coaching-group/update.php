<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroup */

$this->title = Yii::t('app', 'Perbarui Coaching Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coaching-group-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
