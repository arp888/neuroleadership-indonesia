<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroup */

$this->title = Yii::t('app', 'Update Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coachingGroup->group_name, 'url' => ['view', 'id' => $model->coachingGroup->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coaching-participant-update">    
    <?= $this->render('_form-participant', [
        'model' => $model,        
    ]) ?>            
</div>
