<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\coaching_implementation\models\CoachingGroup;
use app\models\ExternalUser;
use kartik\export\ExportMenu;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Fill;


/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroup */

$this->title = Yii::t('app', 'Detail Coaching Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->group_name;
\yii\web\YiiAsset::register($this);

?>

<div class="coaching-group-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    
                                         
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('group_name') ?></h5><?= ($model->group_name ? $model->group_name : '-') ; ?>
                </div>

                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('start_date') ?></h5><?= ($model->start_date ? Yii::$app->formatter->asDate($model->start_date, 'long') : '-'); ?>
                </div>

                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('end_date') ?></h5><?= ($model->end_date ? Yii::$app->formatter->asDate($model->end_date, 'long') : '-'); ?>
                </div>               
            </div>

            <div class="row">
                 <div class="col-md-8 mb-3">
                    <h5><?= $model->getAttributeLabel('description') ?></h5><?= ($model->description ? $model->description : '-'); ?>
                </div>
                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('is_active') ?></h5><?= Yii::$app->appHelper->getStatusLabel($model->is_active) ?>
                </div>
            </div>


            <!-- <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => false],
                'template' => '<div class="mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
                    'group_name',
                    [
                        'attribute' => 'description',
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'start_date',
                        'format' => ['date', 'long'],
                    ],

                    [
                        'attribute' => 'end_date',
                        'format' => ['date', 'long'],
                    ],

                    [
                        'attribute' => 'is_active',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Yii::$app->appHelper->getStatusLabel($model->is_active);
                        },
                    ],
                ],
            ]) ?> -->
        </div>
    </div>

    <div class="card">
        <div class="card-header">            
            <div class="d-flex row justify-content-between">
                <!-- <div class="card-title"></div> -->
                <h4><?= Yii::t('app', 'Coaching Participant') ?></h4>

                 <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('create-participant')) {
                            echo Html::a(Yii::t('app', 'Create Coaching Participant'), ['create-participant', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div>

                <!-- <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('export')) {
                            echo Html::a(Yii::t('app', 'Export'), ['export', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div> -->
                
            </div>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'coaching-participant-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    // [
                    //     'class' => '\kartik\grid\CheckboxColumn',
                    //     'vAlign'=>'middle',
                    //     'hiddenFromExport'=>true,
                    //     'mergeHeader'=>true,
                    //     'width' => '15px',
                    // ],
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],
                    
                    [
                        'attribute' => 'name',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->name, ['coaching-participant/view', 'id' => $model->id]); 
                        }
                    ],
                    [
                        'attribute' => 'email',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                    ],
                    [
                        'attribute' => 'phone_number',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true,
                        'value' => function ($model) {
                            return ($model->phone_number ? $model->phone_number : null); 
                        } 
                    ],

                    [
                        'label' => 'Address',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true,
                        'value' => function ($model) {
                            return $model->address; 
                        } 
                    ],
                    [
                        'attribute' => 'user_status',
                        'width' => '120px',
                        // 'mergeHeader' => true, 
                        'filter' => [0 => Yii::t('app', 'Inactive'), 10 => Yii::t('app', 'Active')],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            $status = Yii::$app->appHelper->getStatusLabel($model->user->status);                            
                            return $status; 
                        },
                    ],

                    [
                        'label' => '',
                        // 'width' => '180px',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            if ($model->user->status == 10) {
                                $url = Html::a('Deactivate', ['deactivate', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                    [
                                        'class' => 'btn btn-warning btn-xs rounded pill',
                                    ]);
                            } elseif ($model->user->status == 0) {
                                $url = Html::a('Activate', ['activate', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'btn btn-success btn-xs rounded pill',
                                ]);
                            } else {

                            }

                            return $url; 
                            // return Yii::$app->appHelper->getStatusLabel($model->user->status);
                        },
                    ],

                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update-participant}{delete-participant}') . '</div>',
                        'buttons' => [
                            'activate' => function ($url, $model) {
                                if ($model->user->status == 0) {
                                    return Html::a('Activate', ['activate', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                    [
                                        'class' => 'btn btn-success btn-sm rounded pill',
                                    ]);
                                }
                            },

                            'deactivate' => function ($url, $model) {
                                if ($model->user->status == 10) {
                                    return Html::a('Deactivate', ['deactivate', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                    [
                                        'class' => 'btn btn-warning btn-sm rounded pill',
                                    ]);
                                }
                            },

                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['coaching-participant/view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update-participant' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update-participant', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete-participant' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete-participant', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>        

