<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroup */

$this->title = Yii::t('app', 'Create Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $group->group_name, 'url' => ['view', 'id' => $group->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="coaching-participant-create">    
    <?= $this->render('_form-participant', [
        'model' => $model,        
    ]) ?>            
</div>
