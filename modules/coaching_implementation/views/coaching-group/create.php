<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroup */

$this->title = Yii::t('app', 'Create Coaching Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coaching-group-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
