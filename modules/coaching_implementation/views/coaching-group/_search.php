<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingGroupSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-group-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'group_name')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('group_name'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'description')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('description'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'start_date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('start_date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'end_date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('end_date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'is_active', ['options' => ['class' => 'form-group form-focus select-focus']])->widget(Select2::class, [
                'data' => [Yii::$app->appHelper::STATUS_INACTIVE => Yii::t('app', 'Inactive'), Yii::$app->appHelper::STATUS_ACTIVE => Yii::t('app', 'Active')],
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'All Status')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label($model->getAttributeLabel('is_active'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

