<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-participant-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'user_id')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('user_id'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'coaching_group_id')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('coaching_group_id'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'name')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('name'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'email')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('email'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'phone_number')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('phone_number'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'street')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('street'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'province')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('province'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'city')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('city'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'district')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('district'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'subdistrict')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('subdistrict'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'postal_code')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('postal_code'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

