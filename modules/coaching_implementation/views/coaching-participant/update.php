<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipant */

$this->title = Yii::t('app', 'Perbarui Coaching Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Participant'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coaching-participant-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
