<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\coaching_implementation\models\CoachingGroup;
use borales\extensions\phoneInput\PhoneInput;
use kartik\depdrop\DepDrop;
use app\models\Province; 
use app\models\City; 
use app\models\District; 
use app\models\Subdistrict;
use app\models\Postalcode;


/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipant */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-participant-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
            <?= $form->field($model, 'coaching_group_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(CoachingGroup::find()->all(), 'id', 'group_name'),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('coaching_group_id')) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(), [
                    'jsOptions' => [
                        'allowDropdown' => false,
                        'preferredCountries' => ['id'],
                        'initialCountry' => 'id',
                        'onlyCountries' => ['id'],
                        'nationalMode' => true,
                    ]
                ]) 
            ?>

            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'province')->widget(Select2::class, [
                 'data' => ArrayHelper::map(Province::find()->all(), 'prov_id', 'prov_name'),
                 'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                 'theme' => Select2::THEME_DEFAULT,
                 'pluginOptions' => [
                     'allowClear' => true,
                 ],
            ]) ?>

            <?= $form->field($model, 'city')->widget(DepDrop::classname(), [
                'data' => (!$model->isNewRecord && !empty($model->city) ? ArrayHelper::map(City::find()->where(['prov_id' => $model->province])->asArray()->all(), 'city_id', 'city_name') : null),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => 'Select ...',],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                    'theme' => Select2::THEME_DEFAULT,
                    'pluginOptions' => ['allowClear' => true]
                ],
                'pluginOptions' => [
                    'depends' => ['coachingparticipant-province'],
                    'url' => \yii\helpers\Url::to(['city-list']),
                    'loadingText' => 'Loading ...',
                ]
            ]); ?>

            <?= $form->field($model, 'district')->widget(DepDrop::classname(), [
                'data' => (!$model->isNewRecord && !empty($model->district) ? ArrayHelper::map(District::find()->where(['city_id' => $model->city])->asArray()->all(), 'dis_id', 'dis_name') : null),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => 'Select ...',],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                    'theme' => Select2::THEME_DEFAULT,
                    'pluginOptions' => ['allowClear' => true]
                ],
                'pluginOptions' => [
                    'depends' => ['coachingparticipant-city'],
                    'url' => \yii\helpers\Url::to(['district-list']),
                    'loadingText' => 'Loading ...',
                ]
            ]); ?>

            <?= $form->field($model, 'subdistrict')->widget(DepDrop::classname(), [
                'data' => (!$model->isNewRecord && !empty($model->subdistrict) ? ArrayHelper::map(Subdistrict::find()->where(['dis_id' => $model->district])->asArray()->all(), 'subdis_id', 'subdis_name') : null),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => 'Select ...',],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                    'theme' => Select2::THEME_DEFAULT,
                    'pluginOptions' => ['allowClear' => true]
                ],
                'pluginOptions' => [
                    'depends' => ['coachingparticipant-district'],
                    'url' => \yii\helpers\Url::to(['subdistrict-list']),
                    'loadingText' => 'Loading ...',
                ]
            ]); ?>

            <?= $form->field($model, 'postal_code')->widget(DepDrop::classname(), [
                'data' => (!$model->isNewRecord && !empty($model->postal_code) ? ArrayHelper::map(Postalcode::find()->where(['subdis_id' => $model->subdistrict])->asArray()->all(), 'postal_code', 'postal_code') : null),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => 'Select ...',],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                    'theme' => Select2::THEME_DEFAULT,
                    'pluginOptions' => ['allowClear' => true]
                ],
                'pluginOptions' => [
                    'depends' => ['coachingparticipant-subdistrict'],
                    'url' => \yii\helpers\Url::to(['postal-code-list']),
                    'loadingText' => 'Loading ...',
                ]
            ]); ?>            

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>

<?php 
    $this->registerCss("
        .iti {
            position: relative;
            display: block;
        }
    ");
?>