<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\coaching_implementation\models\CoachingParticipant;
use kartik\export\ExportMenu;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Fill;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipant */

$this->title = Yii::t('app', 'Detail Coaching Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Participant'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
\yii\web\YiiAsset::register($this);
?>

<div class="coaching-participant-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <h4 class="lead"><?= Yii::t('app', 'General Information') ?></h4>
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    
           
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 

                    <?php if ($model->user->status == 0): ?>
                    <?php if (Helper::checkRoute('activate-user')): ?>
                    <?= Html::a(Yii::t('app', 'Activate User'), ['activate-user', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-info'
                    ]) ?>
                    <?php endif; ?>
                    <?php endif; ?>

                    <?php if ($model->user->status == 10): ?>
                    <?php if (Helper::checkRoute('deactivate-user')): ?>
                    <?= Html::a(Yii::t('app', 'Deactivate User'), ['deactivate-user', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-warning'
                    ]) ?>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('coaching_group_id') ?></h5><?= ($model->coachingGroup->group_name ? $model->coachingGroup->group_name : '-') ; ?>
                </div>

                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('name') ?></h5><?= ($model->name ? $model->name . ' ' . ($model->user->status == 10 ? '<span class="badge badge-success">ACTIVE</span>' : '<span class="badge badge-danger">INACTIVE</span>') : '-'); ?>
                </div>

                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('email') ?></h5><?= ($model->email ? $model->email : '-'); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <h5><?= $model->getAttributeLabel('phone_number') ?></h5><?= ($model->phone_number ? $model->phone_number : '-'); ?>
                </div>
                <div class="col-md-8">
                    <h5><?= Yii::t('app', 'Address') ?></h5><?= ($model->address ? $model->address : '-'); ?>
                </div>
            </div>


            <!-- <div class="row">               
                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('user_status') ?></h5><?= ($model->user->status == 10 ? '<span class="badge badge-success">ACTIVE</span>' : '<span class="badge badge-danger">INACTIVE</span>'); ?>
                </div>

                <?php if ($model->temporary_password == null): ?>

                <div class="col-md-4 mb-3">
                    <h5><?= $model->getAttributeLabel('temporary_password') ?></h5><?= ($model->temporary_password ? $model->temporary_password : '-'); ?>
                </div>

                <?php endif; ?>

            </div> -->
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <h4 class="lead"><?= Yii::t('app', 'Participant Dashboard') ?></h4>                
            </div>
        </div>

        <div class="card-body">
            <h5 class="card-title">Coaching Case Progress</h5>
            <div class="progress mb-0" style="height: 50px;">    
                <?php 
                $i = 0; 

                foreach ($caseClosed as $key => $case) {
                    if ($case['status'] == 20) {
                        echo '<div class="progress-bar bg-success small-font" role="progressbar" style="width: ' . $progressBarWidth . '%" aria-valuenow="' . $progressBarWidth .'" aria-valuemin="0" aria-valuemax="100">' . Html::a(Yii::$app->formatter->asDate($key, 'short'), ['coaching-participant-journal/view', 'id' => $case['journalId'], 'ru' => ReturnUrl::getToken()], ['class' => 'btn-link text-white', 'target' => 'blank']) . '</div>'; 
                    } elseif ($case['status'] == 10) {
                        echo '<div class="progress-bar bg-warning small-font" role="progressbar" style="width: ' . $progressBarWidth . '%" aria-valuenow="' . $progressBarWidth .'" aria-valuemin="0" aria-valuemax="100">' . Html::a(Yii::$app->formatter->asDate($key, 'short'), ['coaching-participant-journal/view', 'id' => $case['journalId'], 'ru' => ReturnUrl::getToken()], ['class' => 'btn-link text-white', 'target' => 'blank']) . '</div>'; 
                    } else {
                        echo '<div class="progress-bar bg-secondary small-font" role="progressbar" style="width: ' . $progressBarWidth . '%" aria-valuenow="' . $progressBarWidth .'" aria-valuemin="0" aria-valuemax="100">' . Yii::$app->formatter->asDate($key, 'short') . '</div>'; 
                    }
                }

                ?>
            </div>     

            <div class="mt-2">
                <strong><span class="mr-3">Total Case: <?= $totalCase ?></span><span class="bg-success px-1 py-0 mr-1">&nbsp;</span>Total Closed Case: <?= $totalCaseClosed ?><span class="bg-warning px-1 py-0 ml-3 mr-1">&nbsp;</span>Total Unclosed Case: <?= ($totalCase - $totalCaseClosed) ?></strong>
            </div>

            <div class="row mb-1 mt-3">
                <div class="col-lg-3 col-md-6 d-flex">
                    <div class="card mb-2 flex-fill">
                        <div class="card-body py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#1abc9c"
                                        data-bgColor="#d1f2eb" value="<?= Yii::$app->formatter->asDecimal($knob_val['highly_constructive']['value'], 0) ?>"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-right">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['highly_constructive']['total'] ?></span> </h3>
                                    <p class="text-muted mb-0">Highly Constructive</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-3 col-md-6 d-flex">
                    <div class="card mb-2 flex-fill">
                        <div class="card-body py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#3bafda"
                                        data-bgColor="#d8eff8" value="<?= Yii::$app->formatter->asDecimal($knob_val['constructive']['value'], 0) ?>"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-right">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['constructive']['total'] ?></span> </h3>
                                    <p class="text-muted mb-0">Constructive</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex">
                    <div class="card mb-2 flex-fill">
                        <div class="card-body py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#ffbd4a"
                                        data-bgColor="#FFE6BA" value="<?= Yii::$app->formatter->asDecimal($knob_val['destructive']['value'], 0) ?>"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-right">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['destructive']['total'] ?></span> </h3>
                                    <p class="text-muted mb-0">Destructive</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-3 col-md-6 d-flex">
                    <div class="card mb-2 flex-fill">
                        <div class="card-body py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="knob-chart" dir="ltr">
                                    <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#f05050"
                                        data-bgColor="#F9B9B9" value="<?= Yii::$app->formatter->asDecimal($knob_val['highly_destructive']['value'], 0) ?>"
                                        data-skin="tron" data-angleOffset="0" data-readOnly=true
                                        data-thickness=".15"/>
                                </div>
                                <div class="text-right">
                                    <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['highly_destructive']['total'] ?></span> </h3>
                                    <p class="text-muted mb-0">Highly Destructive</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div>

        </div>
    </div>


    <?php 
        $exportColumns = [
            [
                'class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
                // 'vAlign' => 'top',
            ],
            [
                'attribute' => 'date',
                // 'vAlign' => 'top',
                'mergeHeader' => true, 
                'format' => ['date', 'long'],
            ],
            [
                'attribute' => 'problem_and_situation',
                // 'vAlign' => 'top',
                // 'mergeHeader' => true, 
                'format' => 'raw',
                // 'width' => 75,
            ],
            [
                'attribute' => 'problem_and_situation_scale',
                // 'vAlign' => 'top',
                'mergeHeader' => true, 
                'value' => function ($model) {
                    if ($model->problem_and_situation_scale == 30) {
                        $label = 'Highly Constructive'; 
                    } elseif ($model->problem_and_situation_scale == 20) {
                        $label = 'Constructive';
                    } elseif ($model->problem_and_situation_scale == 10) {
                        $label = 'Destructive';
                    } elseif ($model->problem_and_situation_scale == 0) {
                        $label = 'Highly Destructive';
                    } else {
                        $label = '';
                    }
                    return $label;
                }
            ],
            [
                'attribute' => 'reappraise',
                // 'vAlign' => 'top',
                'format' => 'raw',
            ],
            [
                'attribute' => 'visioning',
                // 'vAlign' => 'top',
                'format' => 'raw',
            ],
            [
                'attribute' => 'label_bias',
                // 'vAlign' => 'top',
                'format' => 'html',
                'value' => function ($model) {
                    $label = '';
                    if ($model->label_bias != 'null') {            
                        if (!empty($model->label_bias)) {
                            $values = json_decode($model->label_bias);                             
                            foreach ($values as $key => $val) {
                                $label .= '<p>' . $val . '</p>';
                            }
                            // $label .= '</ul>'; 
                        }
                    }
                    return $label; 
                }
            ],
            [
                'attribute' => 'decide_and_action',
                'vAlign' => 'top',
                'format' => 'raw',
            ],   
        ];

        $headerRow = [
            [
                'value' => 'Jurnal Coaching ' . $model->coachingGroup->group_name,                              
                'styleOptions' => [
                    ExportMenu::FORMAT_EXCEL => ['font' => ['bold' => true, 'size' => 14]],
                    ExportMenu::FORMAT_EXCEL_X => ['font' => ['name' => 'Roboto', 'bold' => true, 'size' => 14]],
                ],
            ],
            [
                'value' => 'Nama: ' . $model->name,                              
                'styleOptions' => [
                    ExportMenu::FORMAT_EXCEL => ['font' => ['bold' => true, 'size' => 11]],
                    ExportMenu::FORMAT_EXCEL_X => ['font' => ['name' => 'Roboto', 'bold' => true, 'size' => 11]],
                ],
            ],
            [
                'value' => 'Periode: ' . Yii::$app->formatter->asDate($model->coachingGroup->start_date, 'long') . ' - ' . Yii::$app->formatter->asDate($model->coachingGroup->end_date, 'long'),                              
                'styleOptions' => [
                    ExportMenu::FORMAT_EXCEL => ['font' => ['bold' => true, 'size' => 11]],
                    ExportMenu::FORMAT_EXCEL_X => ['font' => ['name' => 'Roboto', 'bold' => true, 'size' => 11]],
                ],
            ], 
            ['value' => ''],
        ];

        $headerStyle = [
            'font' => ['bold' => true, 'size' => 14],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => [
                    'argb' => 'FFE5E5E5',
                ],
            ],
        ];

        $fileName = Yii::t('app', 'Participant Coaching Journal') . ' ' . $model->name;
    ?>

    <div class="card">
        <div class="card-header">            
            <div class="d-flex row justify-content-between">
                <!-- <div class="card-title"></div> -->
                <h4  class="lead"><?= Yii::t('app', 'Participant Journal') ?></h4>
                <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('export')) {
                            echo ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $exportColumns,                        
                            // 'fontAwesome' => true,
                            'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                            'showConfirmAlert' => false,
                            'target' => ExportMenu::TARGET_SELF,
                            'deleteAfterSave' => true,
                            'showPageSummary' => false,
                            'showFooter' => false,
                            // 'disabledColumns' => [0, 1, 2,3,4,5],
                            'dropdownOptions' => [
                                'class' => 'btn btn-success text-white',
                                'label' => '<span class="kt-hidden-mobile">Export</span>',
                                'icon' => '<i class="la la-download"></i>',
                            ],
                            'showColumnSelector' => false,
                            'columnSelectorOptions' => [
                                'class' => 'btn btn-success',
                                'label' => '<span class="kt-hidden-mobile">Select Col</span>',
                                'icon' => '<i class="la la-list"></i>',
                            ],
                            'contentBefore' => $headerRow,
                            'filename' => $fileName,
                            'enableFormatter' => true,
                            'clearBuffers' => true,
                            'exportConfig' => [
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_EXCEL => [
                                    'label' => 'Excel 95+',
                                    // 'icon' => 'file-excel-o',
                                    // 'iconOptions' => ['class' => 'text-success'],
                                    'linkOptions' => [],
                                    'options' => ['title' => 'Microsoft Excel 95+ (xls)'],
                                    'alertMsg' => 'File excel akan dibuat untuk diunduh.',                            
                                    'mime' => 'application/vnd.ms-excel',
                                    'extension' => 'xls',
                                    'config' => [
                                        'worksheet' => $fileName,
                                        'cssFile' => ''
                                    ],
                                    'writer' => ExportMenu::FORMAT_EXCEL
                                ],
                                ExportMenu::FORMAT_EXCEL_X => [
                                    'label' => 'Excel',
                                    // 'icon' => 'file-excel-o',
                                    // 'iconOptions' => ['class' => 'text-success'],
                                    'linkOptions' => [],                            
                                    'options' => ['title' => 'Export ke excel'],
                                    'alertMsg' => 'File excel akan dibuat untuk diunduh.',
                                    'mime' => 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                    'extension' => 'xlsx',                                                    
                                    'writer' => ExportMenu::FORMAT_EXCEL_X
                                ],
                            ],
                            'onRenderSheet' => function($sheet, $widget) use ($model) {
                                $highestRow = $sheet->getHighestRow(); // e.g. 10
                                $highestColumn = $sheet->getHighestColumn(); // e.g 'F'
                                $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
                                $styleArray = [
                                    'font' => ['name' => 'Roboto', 'bold' => false, 'size' => 10],
                                    'borders' => [
                                        'outline' => [
                                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                        ],
                                        'inside' => [
                                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                        ],                            
                                    ],
                                ];

                                $headerStyleArray = [
                                    'font' => ['name' => 'Roboto', 'bold' => true, 'size' => 10],
                                    'borders' => [
                                        'outline' => [
                                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                        ],
                                        'inside' => [
                                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                        ],                            
                                    ],
                                    'alignment' => [
                                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                    ],
                                    'fill' => [
                                        'fillType' => Fill::FILL_SOLID,
                                        'color' => [
                                            'argb' => 'FFE5E5E5',
                                        ],
                                    ],
                                ];

                                // $footerStyleArray = [
                                //     'font' => ['name' => 'Roboto', 'bold' => true, 'size' => 10],
                                //     'borders' => [
                                //         'outline' => [
                                //             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                //         ],
                                //         'inside' => [
                                //             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                //         ],                            
                                //     ],
                                //     'alignment' => [
                                //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                                //     ],
                                //     'fill' => [
                                //         'fillType' => Fill::FILL_SOLID,
                                //         'color' => [
                                //             'argb' => 'FFE5E5E5',
                                //         ],
                                //     ],                        
                                // ];

                                // var_dump($sheet->getCell('C6')->getValue()); 
                                // var_dump($sheet->getCell('h6')->getValue()); 

                                // die(); 
                                                  
                                $sheet->removeAutoFilter(); 
                                $sheet->unfreezePane(); 



                                $sheet->getStyle("A5:" . $highestColumn . "5")->applyFromArray($headerStyleArray);
                                $sheet->getStyle("A6:" . $highestColumn . ($highestRow))->applyFromArray($styleArray);
                                $sheet->getColumnDimension('C')->setAutoSize(false);
                                $sheet->getColumnDimension('E')->setAutoSize(false);
                                $sheet->getColumnDimension('F')->setAutoSize(false);
                                $sheet->getColumnDimension('H')->setAutoSize(false);
                                $sheet->getColumnDimension('C')->setWidth(75);
                                $sheet->getColumnDimension('E')->setWidth(75);
                                $sheet->getColumnDimension('F')->setWidth(75);
                                $sheet->getColumnDimension('H')->setWidth(75);
                                $sheet->getStyle("A6:" . $highestColumn . ($highestRow))->getAlignment()->setWrapText(true);
                            },
                        ]);
                        }
                    ?>
                </div>
                
            </div>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'coaching-participant-journal-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'vAlign' => 'top',
                    ],

                    // [
                    //     'attribute' => 'coaching_participant_id',
                    //     'mergeHeader' => true, 
                    //     'vAlign' => 'top',
                    //     'value' => function ($model) {
                    //         return $model->coachingParticipant->name;
                    //     },
                    // ],
                    [
                        'attribute' => 'date',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'problem_and_situation',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'problem_and_situation_scale',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'value' => function ($model) {
                            if ($model->problem_and_situation_scale == 30) {
                                $label = 'Highly Constructive'; 
                            } elseif ($model->problem_and_situation_scale == 20) {
                                $label = 'Constructive';
                            } elseif ($model->problem_and_situation_scale == 10) {
                                $label = 'Destructive';
                            } elseif ($model->problem_and_situation_scale == 0) {
                                $label = 'Highly Destructive';
                            } else {
                                $label = '';
                            }
                            return $label; 
                        }
                    ],
                    [
                        'attribute' => 'reappraise',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'visioning',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'label_bias',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                        'value' => function ($model) {
                            $label = '';
                            if ($model->label_bias != 'null') {            
                                if (!empty($model->label_bias)) {
                                    $values = json_decode($model->label_bias); 
                                    $label .= '<ul>';
                                    foreach ($values as $key => $val) {
                                        $label .= '<li>' . $val . '</li>';
                                    }
                                    $label .= '</ul>'; 
                                }
                            }
                            return $label; 
                        }
                    ],
                    [
                        'attribute' => 'decide_and_action',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'top',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    // [
                    //     'class' => 'kartik\grid\ActionColumn',
                    //     // 'width' => '110px',
                    //     'vAlign' => 'top',
                    //     'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                    //     'buttons' => [
                    //         'view' => function ($url, $model) {
                    //             return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                    //             [
                    //                 'class' => 'action-icon',
                    //             ]);
                    //         },
                    //         'update' => function ($url, $model) {
                    //             return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                    //             [
                    //                 'class' => 'action-icon',                            
                    //             ]);
                    //         },                    
                    //         'delete' => function ($url, $model) {
                    //             return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                    //             [
                    //                 'class' => 'action-icon',
                    //                 'data' => [
                    //                     'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                    //                     'method' => 'post'
                    //                 ]
                    //             ]);
                    //         },
                    //     ],
                    // ],
                ],
            ]); ?>
        </div>
    </div>

</div>        

<?php 
    $this->registerJsFile('@web/themes/minton-5/libs/jquery-knob/jquery.knob.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCss('
        ul {
            padding-left: 15px!important;
        }
        .small-font{
            font-size: 8px;
        } 
    ');
?>