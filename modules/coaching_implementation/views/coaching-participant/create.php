<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipant */

$this->title = Yii::t('app', 'Create Coaching Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Participant'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coaching-participant-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
