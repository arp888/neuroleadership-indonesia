<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use app\modules\coaching_implementation\models\CoachingParticipant;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-participant-journal-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
                    <?= $form->field($model, 'coaching_participant_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(CoachingParticipant::find()->all(), 'id', 'coaching_participant_name'),
                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('coaching_participant_id')) ?>

            <?= $form->field($model, 'date', [
                'selectors' => ['input' => '#coachingparticipantjournal-date-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'coachingparticipantjournal-date'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('date')) ?>

            <?= $form->field($model, 'problem_and_situation')->widget(\dosamigos\tinymce\TinyMce::classname(), [
'options' => ['rows' => 3],
// 'language' => 'id',
'clientOptions' => [
'branding' => false,
'menubar' => false,
'relative_urls' => false,
'remove_script_host' => false,
'convert_urls' => true,
'plugins' => [
'advlist autolink lists link charmap print preview anchor',
'searchreplace visualblocks code fullscreen',
'insertdatetime media table contextmenu paste image'
],
'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
] ])->hint(Yii::t('app', 'Pilih' . ' ' . $model->getAttributeLabel('problem_and_situation'))) ?>

            <?= $form->field($model, 'problem_and_situation_scale')->textInput() ?>

            <?= $form->field($model, 'reappraise')->widget(\dosamigos\tinymce\TinyMce::classname(), [
'options' => ['rows' => 3],
// 'language' => 'id',
'clientOptions' => [
'branding' => false,
'menubar' => false,
'relative_urls' => false,
'remove_script_host' => false,
'convert_urls' => true,
'plugins' => [
'advlist autolink lists link charmap print preview anchor',
'searchreplace visualblocks code fullscreen',
'insertdatetime media table contextmenu paste image'
],
'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
] ])->hint(Yii::t('app', 'Pilih' . ' ' . $model->getAttributeLabel('reappraise'))) ?>

            <?= $form->field($model, 'visioning')->widget(\dosamigos\tinymce\TinyMce::classname(), [
'options' => ['rows' => 3],
// 'language' => 'id',
'clientOptions' => [
'branding' => false,
'menubar' => false,
'relative_urls' => false,
'remove_script_host' => false,
'convert_urls' => true,
'plugins' => [
'advlist autolink lists link charmap print preview anchor',
'searchreplace visualblocks code fullscreen',
'insertdatetime media table contextmenu paste image'
],
'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
] ])->hint(Yii::t('app', 'Pilih' . ' ' . $model->getAttributeLabel('visioning'))) ?>

            <?= $form->field($model, 'label_bias')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'decide_and_action')->widget(\dosamigos\tinymce\TinyMce::classname(), [
'options' => ['rows' => 3],
// 'language' => 'id',
'clientOptions' => [
'branding' => false,
'menubar' => false,
'relative_urls' => false,
'remove_script_host' => false,
'convert_urls' => true,
'plugins' => [
'advlist autolink lists link charmap print preview anchor',
'searchreplace visualblocks code fullscreen',
'insertdatetime media table contextmenu paste image'
],
'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
] ])->hint(Yii::t('app', 'Pilih' . ' ' . $model->getAttributeLabel('decide_and_action'))) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
