<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournalSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-participant-journal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'coaching_participant_id')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('coaching_participant_id'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'date')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('date'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'problem_and_situation')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('problem_and_situation'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'problem_and_situation_scale')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('problem_and_situation_scale'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'reappraise')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('reappraise'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'visioning')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('visioning'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'label_bias')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('label_bias'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'decide_and_action')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('decide_and_action'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

