<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */

$this->title = Yii::t('app', 'Detail Coaching Participant Journal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Participant Journal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
\yii\web\YiiAsset::register($this);
?>

<div class="coaching-participant-journal-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>   
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => false],
                'template' => '<div class="mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
                    [
                        'attribute' => 'coaching_participant_id',
                        'label' => Yii::t('app', 'Name'),
                        'value' => function ($model) {
                            return $model->coachingParticipant->name;
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'problem_and_situation',                        
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'problem_and_situation_scale',                        
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->problem_and_situation_scale == 30) {
                                $label = 'Highly Constructive'; 
                                $percentage = 100; 
                                $class = 'bg-success';
                            } elseif ($model->problem_and_situation_scale == 20) {
                                $label = 'Constructive';
                                $percentage = 75; 
                                $class = 'bg-info';
                            } elseif ($model->problem_and_situation_scale == 10) {
                                $label = 'Destructive';
                                $percentage = 50; 
                                $class = 'bg-warning';                                
                            } elseif ($model->problem_and_situation_scale == 0) {
                                $label = 'Highly Destructive';
                                $percentage = 25;                                 
                                $class = 'bg-danger';
                            } else {
                                $label = '';
                            }                           

                            return $label; 
                        }
                    ],
                    [
                        'attribute' => 'reappraise',                        
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'visioning',                        
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'label_bias',                     
                        'format' => 'raw',
                        'value' => function ($model) {
                            $label = '';
                            $values = json_decode($model->label_bias); 
                            if ($values != false) {
                                $label .= '<ul>';
                                foreach ($values as $key => $val) {
                                    $label .= '<li>' . $val . '</li>';
                                }
                                $label .= '</ul>'; 
                            }


                            // if ($model->label_bias != 'null') {            
                            //     if (!empty($model->label_bias)) {

                            //         $values = json_decode($model->label_bias); 

                            //         $label .= '<ul>';
                            //         foreach ($values as $key => $val) {
                            //             $label .= '<li>' . $val . '</li>';
                            //         }
                            //         $label .= '</ul>'; 
                            //     }
                            // }
                            return $label; 
                        }
                    ],
                    [
                        'attribute' => 'decide_and_action',                        
                        'format' => 'raw',
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>        