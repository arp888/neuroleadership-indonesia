<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\ArrayHelper;
use app\modules\coaching_implementation\models\CoachingParticipant;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\coaching_implementation\models\CoachingParticipantJournalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Coaching Participant Journal');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="coaching-participant-journal-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <!-- <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create Coaching Participant Journal'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div>
            </div>
        </div> -->

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'coaching-participant-journal-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

                    [
                        'attribute' => 'coaching_participant_id',
                        'label' => Yii::t('app', 'Name'),
                        'filter' => ArrayHelper::map(CoachingParticipant::find()->all(), 'id', 'name'),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->coachingParticipant->name;
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'problem_and_situation',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'problem_and_situation_scale',
                        'width' => '180px',
                        'filter' => [0 => 'Highly Destructive', 10 => 'Destructive', 20 => 'Constructive', 30 => 'Highly Constructive'],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'vAlign' => 'middle',
                        // 'mergeHeader' => true, 
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->problem_and_situation_scale == 30) {
                                $label = 'Highly Constructive'; 
                                $percentage = 100; 
                                $class = 'bg-success';
                            } elseif ($model->problem_and_situation_scale == 20) {
                                $label = 'Constructive';
                                $percentage = 75; 
                                $class = 'bg-info';
                            } elseif ($model->problem_and_situation_scale == 10) {
                                $label = 'Destructive';
                                $percentage = 50; 
                                $class = 'bg-warning';                                
                            } elseif ($model->problem_and_situation_scale == 0) {
                                $label = 'Highly Destructive';
                                $percentage = 25;                                 
                                $class = 'bg-danger';
                            } else {
                                $label = '';
                            }                           

                            return $label; 
                        }
                    ],
                    [
                        'attribute' => 'reappraise',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'visioning',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'label_bias',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                        'value' => function ($model) {
                            $label = '';
                            $values = json_decode($model->label_bias); 
                            if ($values != false) {
							    $label .= '<ul>';
                                foreach ($values as $key => $val) {
                                    $label .= '<li>' . $val . '</li>';
                                }
                                $label .= '</ul>'; 
							}


                            // if ($model->label_bias != 'null') {            
                            //     if (!empty($model->label_bias)) {

                            //         $values = json_decode($model->label_bias); 

                            //         $label .= '<ul>';
                            //         foreach ($values as $key => $val) {
                            //             $label .= '<li>' . $val . '</li>';
                            //         }
                            //         $label .= '</ul>'; 
                            //     }
                            // }
                            return $label; 


                        }
                    ],
                    [
                        'attribute' => 'decide_and_action',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>

<?php 
    $this->registerCss('
        .w0 > p {
            margin-bottom: 0; 
        }

        .bg-gradient {
            background-color: #00d2ff;
            background-image: linear-gradient(to right, #00d2ff 0%, #3a7bd5 100%);
            box-shadow: 2px 2px 10px rgba(57, 123, 213, 0.36)
        }
        ul {
            padding-left: 15px!important;
        }
    ');
?>