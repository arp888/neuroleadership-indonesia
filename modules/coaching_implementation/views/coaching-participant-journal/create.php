<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */

$this->title = Yii::t('app', 'Create Coaching Participant Journal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Participant Journal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coaching-participant-journal-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
