<?php

namespace app\modules\coaching_implementation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\coaching_implementation\models\CoachingParticipant;

/**
 * CoachingParticipantSearch represents the model behind the search form about `app\modules\coaching_implementation\models\CoachingParticipant`.
 */
class CoachingParticipantSearch extends CoachingParticipant
{
    public $user_status; 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'coaching_group_id', 'province', 'city', 'district', 'subdistrict', 'welcome_email_status', 'user_status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'email', 'phone_number', 'street', 'postal_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoachingParticipant::find();

        // add conditions that should always apply here

        $query->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['user_status'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['external_user.status' => SORT_ASC],
            'desc' => ['external_status.status' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'coaching_group_id' => $this->coaching_group_id,
            'province' => $this->province,
            'city' => $this->city,
            'district' => $this->district,
            'subdistrict' => $this->subdistrict,
            'welcome_email_status' => $this->welcome_email_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'external_user.status' => $this->user_status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code]);

        return $dataProvider;
    }
}
