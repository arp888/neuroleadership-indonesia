<?php

namespace app\modules\coaching_implementation\models;

use Yii;

/**
 * This is the model class for table "coaching_participant_journal".
 *
 * @property int $id
 * @property int $coaching_participant_id
 * @property string $date
 * @property string $problem_and_situation
 * @property int $problem_and_situation_scale
 * @property string $reappraise
 * @property string $visioning
 * @property string $label_bias
 * @property string $decide_and_action
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CoachingParticipant $coachingParticipant
 */
class CoachingParticipantJournal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coaching_participant_journal';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coaching_participant_id', 'date', 'problem_and_situation', 'problem_and_situation_scale'], 'required'],
            [['coaching_participant_id', 'problem_and_situation_scale', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'validateDate'],
            [['date'], 'unique', 'targetAttribute' => ['date', 'coaching_participant_id'], 'message' => Yii::t('app', 'You have made a coaching journal on this date.')],
            [['problem_and_situation', 'reappraise', 'visioning', 'decide_and_action'], 'string'],
            [['label_bias'], 'safe'],
            [['coaching_participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => CoachingParticipant::className(), 'targetAttribute' => ['coaching_participant_id' => 'id']],
        ];
    }

    public function validateDate()
    {
        $coachingGroup = $this->coachingParticipant->coachingGroup; 

        $start_date = $coachingGroup->start_date; 
        $end_date = $coachingGroup->end_date; 

        if (strtotime($this->date) < strtotime($start_date) || strtotime($this->date) > strtotime($end_date) ) {            
            $this->addError('date', Yii::t('app', 'The date you entered is not valid. Select between ' . Yii::$app->formatter->asDate($start_date, 'long') . ' and ' . Yii::$app->formatter->asDate($end_date, 'long') . '.'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'coaching_participant_id' => Yii::t('app', 'Coaching Participant'),
            'date' => Yii::t('app', 'Date'),
            'problem_and_situation' => Yii::t('app', 'Problem and Situation'),
            'problem_and_situation_scale' => Yii::t('app', 'Type'),
            'reappraise' => Yii::t('app', 'Reappraise'),
            'visioning' => Yii::t('app', 'Visioning'),
            'label_bias' => Yii::t('app', 'Label Bias'),
            'decide_and_action' => Yii::t('app', 'Decide and Action'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingParticipant()
    {
        return $this->hasOne(CoachingParticipant::className(), ['id' => 'coaching_participant_id']);
    }

    public function getLabelBiasOptions()
    {
        return [
            'Similarity' => 'I tend to trust those who have something in common with me (Saya cenderung lebih percaya kepada yang memiliki kesamaan dengan diri saya)',
            'Expedience' => 'My brain tends to take shortcuts – if it feels right it must be (Otak saya cenderung mengambil jalan pintas – kalau rasanya benar itu pasti benar)',
            'Experience' => 'What I feel is the same as what others feel (Apa yang saya rasakan sama seperti yang orang lain rasakan)' ,
            'Distance' => 'Distance in time, place, and emotion becomes an important determinant of whether a decision is made or not (Jarak secara waktu, tempat, emosional menjadi penentu penting/tidaknya sebuah keputusan)',
            'Safety' => 'Avoiding threats is more powerful than creating new opportunities (Menghindari ancaman lebih kuat ketimbang menciptakan kesempatan baru)'
        ];
    } 

    public function getLabelBiasValues()
    {
        $label = '';
        $values = json_decode($this->label_bias); 
        if ($values != false) {
            $label .= '<ul>';
            foreach ($values as $key => $val) {
                $label .= '<li>' . $val . '<p class="small">' . $this->labelBiasOptions[$val] . '</p></li>';
            }
            $label .= '</ul>'; 
        }

        return $label;
    }
}
