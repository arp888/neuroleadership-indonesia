<?php

namespace app\modules\coaching_implementation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\coaching_implementation\models\CoachingParticipantJournal;

/**
 * CoachingParticipantJournalSearch represents the model behind the search form about `app\modules\coaching_implementation\models\CoachingParticipantJournal`.
 */
class CoachingParticipantJournalSearch extends CoachingParticipantJournal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'coaching_participant_id', 'problem_and_situation_scale', 'created_at', 'updated_at'], 'integer'],
            [['date', 'problem_and_situation', 'reappraise', 'visioning', 'label_bias', 'decide_and_action'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoachingParticipantJournal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'coaching_participant_id' => $this->coaching_participant_id,
            'date' => $this->date,
            'problem_and_situation_scale' => $this->problem_and_situation_scale,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'problem_and_situation', $this->problem_and_situation])
            ->andFilterWhere(['like', 'reappraise', $this->reappraise])
            ->andFilterWhere(['like', 'visioning', $this->visioning])
            ->andFilterWhere(['like', 'label_bias', $this->label_bias])
            ->andFilterWhere(['like', 'decide_and_action', $this->decide_and_action]);

        return $dataProvider;
    }
}
