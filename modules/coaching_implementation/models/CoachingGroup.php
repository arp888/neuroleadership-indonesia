<?php

namespace app\modules\coaching_implementation\models;

use Yii;

/**
 * This is the model class for table "coaching_group".
 *
 * @property int $id
 * @property string $group_name
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property CoachingParticipant[] $coachingParticipants
 */
class CoachingGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coaching_group';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_name', 'start_date', 'end_date'], 'required'],
            [['description'], 'string'],
            [['start_date', 'end_date'], 'safe'],
            [['is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['group_name'], 'string', 'max' => 255],
            [['is_active'], 'default', 'value' => Yii::$app->appHelper::STATUS_ACTIVE],
            [['is_active'], 'in', 'range' => [Yii::$app->appHelper::STATUS_INACTIVE, Yii::$app->appHelper::STATUS_ACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_name' => Yii::t('app', 'Group Name'),
            'description' => Yii::t('app', 'Description'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'is_active' => Yii::t('app', 'Is Active?'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingParticipants()
    {
        return $this->hasMany(CoachingParticipant::className(), ['coaching_group_id' => 'id']);
    }
}
