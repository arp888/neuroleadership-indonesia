<?php

namespace app\modules\coaching_implementation\models;

use Yii;
use app\models\ExternalUser;

/**
 * This is the model class for table "coaching_participant".
 *
 * @property int $id
 * @property int $user_id
 * @property int $coaching_group_id
 * @property string $name
 * @property string $email
 * @property string $phone_number
 * @property string $street
 * @property int $province
 * @property int $city
 * @property int $district
 * @property int $subdistrict
 * @property string $postal_code
 * @property int $welcome_email_status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CoachingGroup $coachingGroup
 * @property \app\models\ExternalUser $user
 * @property CoachingParticipantJournal[] $coachingParticipantJournals
 */
class CoachingParticipant extends \yii\db\ActiveRecord
{
    public $user_status;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coaching_participant';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coaching_group_id', 'name'], 'required'],
            [['user_id', 'coaching_group_id', 'province', 'city', 'district', 'subdistrict', 'welcome_email_status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'street'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 100],
            [['postal_code'], 'string', 'max' => 6],
            [['coaching_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CoachingGroup::className(), 'targetAttribute' => ['coaching_group_id' => 'id']],
            
            // ['username', 'filter', 'filter' => 'trim'],
            // ['username', 'required'],
            // [['username'], 'unique', 'targetClass' => 'app\models\ExternalUser', 'message' => Yii::t('app', 'This username has already been taken by other user.')],
            // ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            // [['email'], 'unique'],
            [['email'], 'unique', 'targetClass' => '\app\models\ExternalUser', 'on' => 'create', 'message' => Yii::t('app', 'This email has already been taken by other user.')],
            
            ['temporary_password', 'required'],
            ['temporary_password', 'string', 'min' => 6],

            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\ExternalUser::className(), 'targetAttribute' => ['user_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'coaching_group_id' => Yii::t('app', 'Coaching Group'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'street' => Yii::t('app', 'Street'),
            'province' => Yii::t('app', 'Province'),
            'city' => Yii::t('app', 'City'),
            'district' => Yii::t('app', 'District'),
            'subdistrict' => Yii::t('app', 'Subdistrict'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'welcome_email_status' => Yii::t('app', 'Welcome Email Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingGroup()
    {
        return $this->hasOne(CoachingGroup::className(), ['id' => 'coaching_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\ExternalUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingParticipantJournals()
    {
        return $this->hasMany(CoachingParticipantJournal::className(), ['coaching_participant_id' => 'id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinceName()
    {
        return $this->hasOne(\app\models\Province::className(), ['prov_id' => 'province']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityName()
    {
        return $this->hasOne(\app\models\City::className(), ['city_id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrictName()
    {
        return $this->hasOne(\app\models\District::className(), ['dis_id' => 'district']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistrictName()
    {
        return $this->hasOne(\app\models\Subdistrict::className(), ['subdis_id' => 'subdistrict']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostalCode()
    {
        return $this->hasOne(\app\models\Postalcode::className(), ['postal_code' => 'postal_code']);
    }

    public function getAddress() {
        $label = ''; 
        if ($this->street) {
            $label .= $this->street;
        }

        if ($this->subdistrict) {
            $label .= ', ' . $this->subdistrictName->subdis_name;
        }

        if ($this->district) {
            $label .= ', ' . $this->districtName->dis_name;
        }

        if ($this->city) {
            $label .= ', ' . $this->cityName->city_name;
        } 

        if ($this->province) {
            $label .= ', ' . $this->provinceName->prov_name;
        } 

        if ($this->postal_code) {
            $label .= ', ' . $this->postalCode->postal_code;
        } 

        return ($label ? $label : null); 
    }


    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * register participant.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function registerParticipant()
    {
        // var_dump($this->validate()); die(); 

        // if ($this->validate()) {
            $user = new ExternalUser();
            $user->username = $this->email;
            $user->email = $this->email;   
            $user->setPassword($this->temporary_password);     
            $user->generateAuthKey();
            $user->generatePasswordResetToken();
            $user->status = 0;
            if ($user->save()) {
                return $user;                
            }
        // }
        return null;
    }


}
