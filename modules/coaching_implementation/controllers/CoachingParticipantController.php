<?php

namespace app\modules\coaching_implementation\controllers;

use Yii;
use app\modules\coaching_implementation\models\CoachingParticipant;
use app\modules\coaching_implementation\models\CoachingParticipantSearch;
use app\modules\coaching_implementation\models\CoachingParticipantJournal;
use app\modules\coaching_implementation\models\CoachingParticipantJournalSearch;
use app\models\ExternalUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDropAction;

/**
 * CoachingParticipantController implements the CRUD actions for CoachingParticipant model.
 */
class CoachingParticipantController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'city-list' => [
                'class' => DepDropAction::className(),
                'outputCallback' => function ($selectedId, $params) {
                    $cities = \app\models\City::find()->where(['prov_id' => $selectedId])->asArray()->all();
                    $data = []; 
                    foreach ($cities as $key => $city) {
                        $data[] = [
                            'id' => $city['city_id'], 
                            'name' => $city['city_name'],
                        ];
                    }
                    return $data;   
                }
            ],
            'district-list' => [
                'class' => DepDropAction::className(),
                'outputCallback' => function ($selectedId, $params) {
                    $districts = \app\models\District::find()->where(['city_id' => $selectedId])->asArray()->all();
                    $data = []; 
                    foreach ($districts as $key => $district) {
                        $data[] = [
                            'id' => $district['dis_id'], 
                            'name' => $district['dis_name'],
                        ];
                    }
                    return $data;   
                }
            ],

            'subdistrict-list' => [
                'class' => DepDropAction::className(),
                'outputCallback' => function ($selectedId, $params) {
                    $subdistricts = \app\models\Subdistrict::find()->where(['dis_id' => $selectedId])->asArray()->all();
                    $data = []; 
                    foreach ($subdistricts as $key => $subdistrict) {
                        $data[] = [
                            'id' => $subdistrict['subdis_id'], 
                            'name' => $subdistrict['subdis_name'],
                        ];
                    }
                    return $data;   
                }
            ],
            'postal-code-list' => [
                'class' => DepDropAction::className(),
                'outputCallback' => function ($selectedId, $params) {
                    $postalcodes = \app\models\Postalcode::find()->where(['subdis_id' => $selectedId])->asArray()->all();
                    $data = []; 
                    foreach ($postalcodes as $key => $postalcode) {
                        $data[] = [
                            'id' => $postalcode['postal_code'], 
                            'name' => $postalcode['postal_code'],
                        ];
                    }
                    return $data;   
                }
            ],            
        ]);
    }

    /**
     * Lists all CoachingParticipant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoachingParticipantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CoachingParticipant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $searchModel = new CoachingParticipantJournalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['coaching_participant_id' => $id]);


        // $participant = $model; 
        // $journals = CoachingParticipantJournal::find()->where(['coaching_participant_id' => $model->id])->all(); 
        $journals = $dataProvider->getModels();

        // var_dump($journals); die();  

        $total = 0;
        $highly_destructive = 0; 
        $value_highly_destructive = 0; 
        $destructive = 0; 
        $value_destructive = 0; 
        $constructive = 0; 
        $value_constructive = 0; 
        $highly_constructive = 0;  
        $value_highly_constructive = 0;  

        $knob_val = []; 

        if ($journals) {
            foreach ($journals as $key => $val) {  
                if ($val->problem_and_situation_scale == 30) {
                    $label = 'Highly Constructive'; 
                    $fgColor = '#1abc9c';
                    $bgColor = '#d1f2eb'; 
                    $highly_constructive = $highly_constructive + 1;
                    $value_highly_constructive = $highly_constructive / count($journals) * 100;                      
                } elseif ($val->problem_and_situation_scale == 20) {
                    $label = 'Constructive';
                    $fgColor = '#1abc9c';
                    $bgColor = '#d8eff8'; 
                    $constructive = $constructive + 1;
                    $value_constructive = $constructive / count($journals) * 100;   
                } elseif ($val->problem_and_situation_scale == 10) {
                    $label = 'Destructive';
                    $fgColor = '#ffbd4a';
                    $bgColor = '#FFE6BA'; 
                    $destructive = $destructive + 1;
                    $value_destructive = $destructive / count($journals) * 100;                 
                } elseif ($val->problem_and_situation_scale == 0) {
                    $label = 'Highly Destructive';
                    $fgColor = '#f05050';
                    $bgColor = '#F9B9B9'; 
                    $highly_destructive = $highly_destructive + 1;
                    $value_highly_destructive = $highly_destructive / count($journals) * 100;
                } else {
                    $label = '';
                }
            }             

        }

        $knob_val = [
            'highly_destructive' => [
                'total' => $highly_destructive,
                'value' => $value_highly_destructive
            ], 
            'destructive' => [
                'total' => $destructive,
                'value' => $value_destructive
            ], 
            'constructive' => [
                'total' => $constructive,
                'value' => $value_constructive
            ],
            'highly_constructive' => [
                'total' => $highly_constructive,
                'value' => $value_highly_constructive
            ]
        ];   


        $start = new \DateTime($model->coachingGroup->start_date);
        $end = new \DateTime($model->coachingGroup->end_date);
        $oneday = new \DateInterval("P1D");

        $days = array();
        $dates = [];

        /* Iterate from $start up to $end+1 day, one day in each iteration.
           We add one day to the $end date, because the DatePeriod only iterates up to,
           not including, the end date. */
        foreach(new \DatePeriod($start, $oneday, $end->add($oneday)) as $day) {
            $day_num = $day->format("N"); /* 'N' number days 1 (mon) to 7 (sun) */
            if($day_num < 6) { /* weekday */
                $dates[] = $day->format("Y-m-d");
            } 
        } 

        $progressBarWidth = 100/count($dates); 

        $caseClosed = [];

        $totalCase = 0; 
        $totalCaseClosed = 0; 

        foreach ($dates as $key => $date) {
            $caseClosed[$date] = ['status' => 0];
            foreach ($journals as $journal) {
                if ($date == $journal->date) {
                    $caseClosed[$date] = ['status' => 10, 'journalId' => $journal->id];
                    $totalCase = $totalCase + 1;
                    if ($journal->reappraise != null && $journal->label_bias != 'null' && $journal->decide_and_action != null) {
                        $caseClosed[$date] = ['status' => 20, 'journalId' => $journal->id];
                        $totalCaseClosed = $totalCaseClosed + 1;
                    }
                }
            }
        }

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'knob_val' => $knob_val,
            'journals' => $journals,
            'dates' => $dates,
            'progressBarWidth' => $progressBarWidth,
            'caseClosed' => $caseClosed,
            'totalCase' => $totalCase,
            'totalCaseClosed' => $totalCaseClosed
        ]);
    }

    /**
     * Creates a new CoachingParticipant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CoachingParticipant();        
        $model->scenario = 'create';
        $model->temporary_password = $model->randomPassword();
        $model->welcome_email_status = 0;

        // var_dump($model->temporary_password); die(); 
                        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->registerParticipant()) {
                $model->user_id = $user->id;
                $model->save(false); 
                \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data created.'), 'message' => Yii::t('app', 'Data has been successfully saved.')]);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }             
        } 
        
        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Updates an existing CoachingParticipant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            $user = \app\models\ExternalUser::findOne($model->user_id); 
            $user->username = $model->email; 
            $user->email = $model->email; 
            $user->save(false); 
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data updated'), 'message' => Yii::t('app', 'Data has been successfully saved.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } 
        
        return $this->render('update', [
            'model' => $model,
        ]);
        
    }

    /**
     * Deletes an existing CoachingParticipant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $user_id = $model->user_id;
        try {
            if ($model->delete()) {
                $user = \app\models\ExternalUser::findOne($user_id);
                $user->delete(); 
                \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data has been successfully deleted.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Could not delete data.')]);
        }
        return $this->redirect(ReturnUrl::getUrl(['index']));
    }


    public function actionActivateUser($id)
    {
        $model = $this->findModel($id); 
        $modelUser = \app\models\ExternalUser::findOne($model->user_id); 

        $mail_setting = \app\models\MailSetting::find()->one(); 
        // var_dump($mail_setting); die(); 
        if ($modelUser) {
            // $modelUser->status = 10; 
            // $modelUser->save();      
            if ($model->welcome_email_status == 0 && !empty($model->temporary_password)) {

                $mail_setting = \app\models\MailSetting::find()->one(); 

                Yii::$app->mailer->setTransport([
                    'class' => 'Swift_SmtpTransport',
                    'host' => $mail_setting->smtp_host, 
                    'username' => $mail_setting->smtp_username,
                    'password' => $mail_setting->smtp_pass,
                    'port' => $mail_setting->smtp_port,
                    'encryption' => $mail_setting->smtp_encryption,
                    'streamOptions' => [
                        'ssl' => [
                            'allow_self_signed' => true,
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        ],
                    ],
                ]);

                $message = Yii::$app->mailer->compose(['html' => 'participant-welcome-email-html'], [
                        'participant_name' => $model->name, 
                        'username' => $modelUser->username,
                        'password' => $model->temporary_password,
                    ]);

                $message->setFrom([$mail_setting->smtp_username => 'NeuroLeadership Indonesia'])
                    ->setTo($model->email)
                    ->setSubject('Selamat Datang di NeuroLeadership Indonesia Coaching Platform');

                if ($message->send()) {
                    $modelUser->status = 10; 
                    if ($modelUser->save(false)) {
                        $model->welcome_email_status = 10; 
                        $model->temporary_password = ''; 
                        $model->save(false); 
                    }
                }

            } else {
                $modelUser->status = 10;
                $modelUser->save();
            }

        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }

        return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
    }


    public function actionDeactivateUser($id)
    {
        $model = $this->findModel($id); 
        $modelUser = \app\models\ExternalUser::findOne($model->user_id); 
        if ($modelUser) {
            $modelUser->status = 0; 
            $modelUser->save();             
        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }

        return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
    }

    /**
     * Finds the CoachingParticipant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoachingParticipant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoachingParticipant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
