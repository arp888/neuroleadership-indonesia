<?php

namespace app\modules\coaching_implementation\controllers;

use yii\web\Controller;

/**
 * Default controller for the `coaching_implementation` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['/coaching-implementation/coaching-group/index']);
    }
}
