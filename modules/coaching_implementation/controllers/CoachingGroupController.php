<?php

namespace app\modules\coaching_implementation\controllers;

use Yii;
use app\modules\coaching_implementation\models\CoachingGroup;
use app\modules\coaching_implementation\models\CoachingGroupSearch;
use app\modules\coaching_implementation\models\CoachingParticipant;
use app\modules\coaching_implementation\models\CoachingParticipantSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use cornernote\returnurl\ReturnUrl; 

/**
 * CoachingGroupController implements the CRUD actions for CoachingGroup model.
 */
class CoachingGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoachingGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoachingGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CoachingGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $searchModel = new CoachingParticipantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['coaching_group_id' => $id]);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CoachingGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CoachingGroup();        
                $model->is_active = Yii::$app->params['statusActive'];
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data dibuat.'), 'message' => Yii::t('app', 'Data berhasil dibuat.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new CoachingParticipant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateParticipant($id)
    {
        $group = $this->findModel($id); 

        $model = new CoachingParticipant();
        $model->scenario = 'create';
        $model->coaching_group_id = $group->id;         
        $model->temporary_password = $model->randomPassword();
        $model->welcome_email_status = 0;

        // var_dump($model->temporary_password); die(); 
                        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->registerParticipant()) {
                $model->user_id = $user->id;                
                if ($model->save(false)) {
                    \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data created.'), 'message' => Yii::t('app', 'Data has been successfully saved.')]);
                    return $this->redirect(['view', 'id' => $group->id, 'ru' => ReturnUrl::getRequestToken()]);
                }
            } 
        }            
        
        return $this->render('create-participant', [
            'model' => $model,
            'group' => $group,
        ]);
        
    }

    /**
     * Updates an existing CoachingGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {        
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data diperbarui'), 'message' => Yii::t('app', 'Data berhasil diperbarui.')]);
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CoachingParticipant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateParticipant($id)
    {        
        $model = CoachingParticipant::findOne($id);
        $group_id = $model->coaching_group_id; 

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            $user = \app\models\ExternalUser::findOne($model->user_id); 
            $user->username = $model->email; 
            $user->email = $model->email; 
            $user->save(false); 
            \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Data updated'), 'message' => Yii::t('app', 'Data has been successfully saved.')]);
            return $this->redirect(['view', 'id' => $group_id, 'ru' => ReturnUrl::getRequestToken()]);
        } 
        return $this->render('update-participant', [
            'model' => $model,
        ]);
        
    }

    /**
     * Deletes an existing CoachingGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $participants = $model->coachingParticipants;

        $users = [];

        foreach ($participants as $key => $participant) {
            $users[] = $participant->user_id;
        }

        // var_dump($users); die(); 

        try {
            if ($model->delete()) {
                foreach ($users as $user) {
                    $user_model = \app\models\ExternalUser::findOne($user);
                    $user_model->delete(); 
                }

               \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data berhasil dihapus.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Data ini tidak dapat dihapus karena terkait dengan data lain.')]);
        }
        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Deletes an existing CoachingParticipant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteParticipant($id)
    {
        $model = CoachingParticipant::findOne($id); 
        $group_id = $model->coaching_group_id; 
        $user_id = $model->user_id; 
        try {
            if ($model->delete()) {
                $user = \app\models\ExternalUser::findOne($user_id);
                $user->delete(); 
                \Yii::$app->getSession()->setFlash('success', ['type' => 'success', 'title' => Yii::t('app', 'Deleted'), 'message' => Yii::t('app', 'Data has been successfully deleted.')]);               
            }
        } catch (\yii\db\IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'error', 'title' => $e->getName(), 'message' =>Yii::t('app', 'Could not delete data.')]);
        }
       return $this->redirect(['view', 'id' => $group_id, 'ru' => ReturnUrl::getRequestToken()]);
    }


    public function actionActivate($id)
    {
        $model = CoachingParticipant::findOne($id); 
        $modelUser = \app\models\ExternalUser::findOne($model->user_id); 

        $mail_setting = \app\models\MailSetting::find()->one(); 
        // var_dump($mail_setting); die(); 
        if ($modelUser) {
            // $modelUser->status = 10; 
            // $modelUser->save();      
            if ($model->welcome_email_status == 0 && !empty($model->temporary_password)) {

                $mail_setting = \app\models\MailSetting::find()->one(); 

                Yii::$app->mailer->setTransport([
                    'class' => 'Swift_SmtpTransport',
                    'host' => $mail_setting->smtp_host, 
                    'username' => $mail_setting->smtp_username,
                    'password' => $mail_setting->smtp_pass,
                    'port' => $mail_setting->smtp_port,
                    'encryption' => $mail_setting->smtp_encryption,
                    'streamOptions' => [
                        'ssl' => [
                            'allow_self_signed' => true,
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        ],
                    ],
                ]);

                $message = Yii::$app->mailer->compose(['html' => 'participant-welcome-email-html'], [
                        'participant_name' => $model->name, 
                        'username' => $modelUser->username,
                        'password' => $model->temporary_password,
                    ]);

                $message->setFrom([$mail_setting->smtp_username => 'NeuroLeadership Indonesia'])
                    ->setTo($model->email)
                    ->setSubject('Selamat Datang di NeuroLeadership Indonesia Coaching Platform');

                if ($message->send()) {
                    $modelUser->status = 10; 
                    if ($modelUser->save(false)) {
                        $model->welcome_email_status = 10; 
                        $model->temporary_password = ''; 
                        $model->save(false); 
                    }
                }

            } else {
                $modelUser->status = 10;
                $modelUser->save();
            }

        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }

        return $this->redirect(['view', 'id' => $model->coaching_group_id, 'ru' => ReturnUrl::getRequestToken()]);
    }

    public function actionDeactivate($id)
    {
        $model = CoachingParticipant::findOne($id);  
        $modelUser = \app\models\ExternalUser::findOne($model->user_id); 
        if ($modelUser) {
            $modelUser->status = 0; 
            $modelUser->save();             
        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }

        return $this->redirect(['view', 'id' => $model->coaching_group_id, 'ru' => ReturnUrl::getRequestToken()]);
    }

    /**
     * Finds the CoachingGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoachingGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoachingGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
