<?php

namespace app\modules\assessment\controllers;

use Yii;
use app\modules\assessment\models\MbtcAssessmentGroup;
use app\modules\assessment\models\MbtcAssessmentGroupSearch;
use app\modules\assessment\models\MbtcAssessment;
use app\modules\assessment\models\MbtcAssessmentSearch;
use app\modules\assessment\models\MbtcItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use cornernote\returnurl\ReturnUrl; 

/**
 * MbtcAssessmentGroupController implements the CRUD actions for MbtcAssessmentGroup model.
 */
class MbtcAssessmentGroupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MbtcAssessmentGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MbtcAssessmentGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['created_at' => SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MbtcAssessmentGroup model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new MbtcAssessmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['mbtc_assessment_group_id' => $id, 'is_completed' => 1]);
        $dataProvider->pagination = false; 

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MbtcAssessmentGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MbtcAssessmentGroup();
        $model->phone_required = 1; 
        $model->email_required = 1;
        $model->is_active = 10; 

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MbtcAssessmentGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MbtcAssessmentGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing MbtcAssessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionMbtcAssessmentDelete($id)
    {
        if (($model = MbtcAssessment::findOne($id)) !== null) {
            $groupId = $model->mbtc_assessment_group_id;
            $model->delete();
            return $this->redirect(['view', 'id' => $groupId, 'ru' => ReturnUrl::getRequestToken()]);
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionViewCompositeChart($id)
    {
        $model = $this->findModel($id);

        $request = Yii::$app->request;
        $post = $request->get('params');

        // // $param1 = Yii::$app->request->dataParams;
        // // $dataProvider = $request['params']['dataProvider'];


        // var_dump(Yii::$app->request->queryParams); die();

        // $respondents = $model->mbtcAssessments;

        $searchRespondentModel = new MbtcAssessmentSearch();

        $dataProviderRespondent = $searchRespondentModel->search(Yii::$app->request->queryParams);
        $dataProviderRespondent->query->andWhere(['mbtc_assessment_group_id' => $id]); 
        $dataProviderRespondent->query->andWhere(['is_completed' => 1]); 
        $dataProviderRespondent->pagination = false; 
        $respondents = $dataProviderRespondent->getModels();

        // var_dump(Yii::$app->request->queryParams); 
        // var_dump($respondents); die(); 



        $grand_total_analytical = 0; 
        $grand_total_experimental = 0; 
        $grand_total_practical = 0; 
        $grand_total_relational = 0;

        foreach ($respondents as $key => $respondent) {
            $details = $respondent->mbtcAssessmentDetails;    

            $total_analytical = 0; 
            $total_experimental = 0; 
            $total_practical = 0; 
            $total_relational = 0;
            
            $items = MbtcItem::find()->all();          

            foreach ($details as $key => $detail) {
                 if ($detail->mbtcItem->type == 10) {
                    $total_analytical = $total_analytical + $detail->score; 
                 } elseif ($detail->mbtcItem->type == 20) {
                    $total_experimental = $total_experimental + $detail->score; 
                 } elseif ($detail->mbtcItem->type == 30) {
                    $total_practical = $total_practical + $detail->score; 
                 } else {
                    $total_relational = $total_relational + $detail->score;                 
                 }
            } 

            $grand_total_analytical = $grand_total_analytical + $total_analytical; 
	        $grand_total_experimental = $grand_total_experimental + $total_experimental; 
	        $grand_total_practical = $grand_total_practical + $total_practical; 
	        $grand_total_relational = $grand_total_relational + $total_relational;


            // $score_categories = ['FACTS', 'FUTURE', 'FEELING', 'FORM'];
            $score_categories = ['D. FUTURE', 'C. FEELING', 'B. FORM', 'A. FACT'];

            $score[$respondent->name] = [
                'analytical' => $total_analytical,
                'experimental' => $total_experimental,
                'practical' => $total_practical,
                'relational' => $total_relational,
            ];            

            $series[] = [
                'name' => 'Value',
                'type' => 'line',                
                'data' => [$total_relational, $total_practical, $total_experimental, $total_analytical],
            ];  

            // $series['name'] = 'Value';
            // $series['type'] = 'line';
            // $series['data'] = [$total_relational, $total_practical, $total_experimental, $total_analytical];                
        }

        // var_dump($grand_total_relational); die(); 

        $seriesAverage = $series; 

        $seriesGrandTotal = [
        	'relational' => $grand_total_relational/count($respondents),
        	'practical' => $grand_total_practical/count($respondents),
        	'experimental' => $grand_total_experimental/count($respondents),
        	'analytical' => $grand_total_analytical/count($respondents)
        ];

        // var_dump($seriesGrandTotal); die(); 

        $seriesGrandTotalVal = [
            'name' => 'Value',
            'type' => 'line',
            'color' => '#ff0000',
            'data' => [$grand_total_relational/count($respondents), $grand_total_practical/count($respondents), $grand_total_experimental/count($respondents), $grand_total_analytical/count($respondents)],
        ]; 

        array_push($seriesAverage, $seriesGrandTotalVal);

        // var_dump($seriesAverage); die(); 


        // $score = [
        //   'Aqila' => 
        //     [
        //       'analytical' => 100,
        //       'experimental' => 92,
        //       'practical' => 66,
        //       'relational' => 90,
        //     ],
          
        //   'Kayla' => 
        //     [
        //       'analytical' => 100,
        //       'experimental' => 100,
        //       'practical' => 96,
        //       'relational' => 96,
        //     ],
        //   'Zahra' => 
        //     [
        //       'analytical' => 100,
        //       'experimental' => 62,
        //       'practical' => 108,
        //       'relational' => 90,
        //     ],
        //   'Annisa' => 
        //     [
        //       'analytical' => 100,
        //       'experimental' => 56,
        //       'practical' => 94,
        //       'relational' => 100,
        //     ],
        // ];

        // var_dump($score);

        
        $keys = [];

        $highVal = [];

        foreach ($score as $j => $value) {            
            $maxValue = 0;
            $keys = [];
            foreach($value as $k => $v){
                if($v > $maxValue){
                    $maxValue = $v;
                    $keys = [$k => $j . ' (' . $maxValue . ')'];
                } else if($v == $maxValue){
                    $keys[$k] = $j . ' (' . $maxValue . ')';
                }
            }

            foreach ($keys as $l => $key) {
                $highVal[$l][] = $key;
            }
        }

        // echo '<pre>';
        // var_dump($highVal);
        // die(); 

        // var_dump($series); die(); 
        // var_dump($seriesAverage); die(); 


        // echo '<pre>'; 
        // var_dump(array_values($ind_series)); die(); 

        return $this->render('view-composite-chart', [
            'model' => $model,            
            'details' => $details,
            'score_categories' => $score_categories,
            'score' => $score,
            'series' => $series,
            'highVal' => $highVal,
            'seriesAverage' => $seriesAverage,
            'seriesGrandTotal' => $seriesGrandTotal,
        ]);

    }




    /**
     * Generate all report.
     * @param string $id
     * @return mixed
     */

    public function actionGenerateReports($id, $lang)
    {
        $group = $this->findModel($id); 
        $respondents = $group->mbtcAssessments;
        $count = 0; 

        foreach ($respondents as $key => $respondent) {
            // Keep current application
            // if ($respondent->report_status == 0) {

                $oldApp = \Yii::$app;
                $userId = $oldApp->user->identity->id; 

                // Load Console Application config
                $config = require \Yii::getAlias('@app'). '/config/console.php';
                new \yii\console\Application($config);
                $result = \Yii::$app->runAction('generate-report-queue/generate-report', [$respondent->id, $userId, $lang]);

                // Revert application
                \Yii::$app = $oldApp;
            // }

            $count++;
        }        

        \Yii::$app->getSession()->setFlash('error', ['type' => 'info', 'title' => 'Processing', 'message' =>Yii::t('app', $count . ' request has been queued for processing. This process may take 1 minute or more.')]);
                
        return $this->redirect(['view', 'id' => $id]);
    }


    public function actionGenerateReportsPrevious($id, $lang)
    {
        $group = $this->findModel($id); 

        $models = $group->mbtcAssessments;

        foreach ($models as $key => $model) {
            $details = $model->mbtcAssessmentDetails;    

            $total_analytical = 0; 
            $total_experimental = 0; 
            $total_practical = 0; 
            $total_relational = 0;

            foreach ($details as $key => $detail) {
                 if ($detail->mbtcItem->type == 10) {
                    $total_analytical = $total_analytical + $detail->score; 
                 } elseif ($detail->mbtcItem->type == 20) {
                    $total_experimental = $total_experimental + $detail->score; 
                 } elseif ($detail->mbtcItem->type == 30) {
                    $total_practical = $total_practical + $detail->score; 
                 } else {
                    $total_relational = $total_relational + $detail->score;                 
                 }
            }

            $cerebral = $total_analytical + $total_relational; 
            $limbic = $total_experimental + $total_practical; 
            $left_mode = $total_analytical + $total_experimental; 
            $right_mode = $total_practical + $total_relational;

            $gapCerebralLimbic = abs($cerebral - $limbic);

            $arr = array();
            $pattern = '/([;:,-.\/ X])/';
            $responden_name = '';
            $array = preg_split($pattern, $model->name, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
            foreach($array as $k => $v) {
                $responden_name .= ucwords(strtolower($v));
            }

            $path = \Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png';
            $url = \yii\helpers\Url::toRoute(['/assessment/mbtc-assessment/view', 'id' => $model->id], true);

            $x = 288;
            // $y = 672;
            $y = 665;
            $width = 919.33;
            $height = 480;
           
            // $executablePath = '/usr/bin/google-chrome';
            $executablePath = '/usr/bin/chromium-browser';

            $puppeteer = new \Nesk\Puphpeteer\Puppeteer([
                'read_timeout' => 240,
                'idle_timeout' => 240
            ]);
            $browser = $puppeteer->launch([
                // 'executablePath' => $executablePath,
                // 'executable_path' => '/usr/bin/node',
                'headless' => true,
                'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
                // 'args' => ['--disable-gpu', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'],
            ]);

            $page = $browser->newPage();        
            $page->setViewport(['width' => 1263, 'height' => 1200, 'deviceScaleFactor' => 2]);
            $page->goto($url, ['waitUntil' => 'networkidle2', 'timeout' => 6000]);
            $page->screenshot(['path' => $path, 'clip' => [
                'x' => $x, 'y' => $y, 'width' => $width, 'height' => $height
            ]]);

            $browser->close();

            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $mpdf = new \Mpdf\Mpdf([
                'format' => 'A4-L',
                'margin_left' => 16.5,
                'margin_right' => 0,
                'margin_top' => 0,
                'margin_bottom' => 0,
                'fontDir' => array_merge($fontDirs, [
                    Yii::getAlias('@app/web/css/fonts')
                ]),
                'fontdata' => $fontData + [
                    'roboto' => [
                        'R' => 'Roboto-Regular.ttf',
                        'I' => 'Roboto-Italic.ttf',
                        'B' => 'Roboto-Bold.ttf',
                    ],
                    'liberationsans' => [
                        'R' => 'LiberationSans-Regular.ttf',
                        'I' => 'LiberationSans-Italic.ttf',
                        'B' => 'LiberationSans-Bold.ttf',
                    ],
                    'poppins' => [
                        'R' => 'Poppins-Regular.ttf',
                        'I' => 'Poppins-Italic.ttf',
                        'B' => 'Poppins-Bold.ttf',
                    ]
                ],
                'default_font' => 'poppins',
                'default_font_size' => 12,
            ]);

            $template = Yii::getAlias('@app/web/files/templates/report-temp.pdf');
            if ($lang == 'eng') {
                $template = Yii::getAlias('@app/web/files/templates/report-temp-eng.pdf');
            }

            $pagecount = $mpdf->SetSourceFile($template);

            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $mpdf->ImportPage($i);
                $mpdf->UseTemplate($import_page);

                if ($i == 1) {
                    $name = $responden_name;
                    $position = $model->position;
                    $organization = $model->organization;
                    $mpdf->text_input_as_HTML = true;
                    $mpdf->SetFont('poppins', 'B', 25);
                    $mpdf->SetTextColor(255,255,255);
                    $mpdf->SetXY(16.5,112);
                    $mpdf->Write(0, $name);

                    $mpdf->SetFont('poppins', 'I', 13);
                    $mpdf->SetTextColor(255,255,255);
                    $mpdf->SetXY(16.5,124);
                    $mpdf->Write(0, $position);

                    $mpdf->SetFont('poppins', 'R', 18);
                    $mpdf->SetTextColor(255,255,255);
                    $mpdf->SetXY(16.5,130);
                    $mpdf->Write(0, $organization);
                }

                if ($i == 2) {
                    $img = \yii\helpers\Html::img(Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png');
                    $mpdf->SetXY(8.25,25.87);
                    $mpdf->SetMargins(10, 10, 10, true);
                    $mpdf->WriteHtml($img);

                    $mpdf->SetFont('poppins', 'R', 26);
                    $mpdf->SetTextColor(27,117,188);
                    $mpdf->SetMargins(10, 10, 10);
                    $mpdf->SetXY(10, 185);
                    $mpdf->Cell(0, 0, "Cerebral - Limbic = " . $gapCerebralLimbic, 0, true, 'C');
                }

                if ($i < $pagecount)
                    $mpdf->AddPage();
            }

            $filename = \Yii::getAlias('@app/web/files/') . $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
            
            $mpdf->Output($filename, 'F');
            $model->filename = $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
            $model->report_status = $model::REPORT_GENERATED;
            $model->save(false); 
        }
        
        return $this->redirect(['view', 'id' => $id]);
    }


    /**
     * Send instructor notification email to queue.
     * @return mixed
     */

    public function actionSendReportToEmail($id)
    {
        $group = $this->findModel($id); 
        $respondents = $group->mbtcAssessments;

        if ($respondents) {
            foreach ($respondents as $respondent) {
                $oldApp = \Yii::$app;
                $userId = $oldApp->user->identity->id; 
                
                // Load Console Application config
                $config = require \Yii::getAlias('@app'). '/config/console.php';
                new \yii\console\Application($config);
                $result = \Yii::$app->runAction('mail-queue/send-report-to-email', [$id, $respondent->id, $userId]);
                
                // Revert application
                \Yii::$app = $oldApp;
                if ($respondent->report_status == $respondent::REPORT_GENERATED) {
                    $respondent->report_status = $respondent::REPORT_QUEUED;               
                    $respondent->save();                           
                }
            }

            \Yii::$app->getSession()->setFlash('error', ['type' => 'info', 'title' => 'Processing', 'message' =>Yii::t('app', '{count} request has been queued for processing.', ['count' => count($respondents)])]);
        } else {
            \Yii::$app->getSession()->setFlash('error', ['type' => 'info', 'title' => 'No respondent selected', 'message' =>Yii::t('app', 'All respondent was already processed.')]);
        }

        return $this->redirect(['view', 'id' => $id, 'ru' => ReturnUrl::getRequestToken()]);

    } 

     /**
     * View course file.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $file
     * @return mixed
     */

    public function actionDownloadReport($file) 
    { 
        $path = Yii::getAlias('@webroot/files/');

        // check filename for allowed chars (do not allow ../ to avoid security issue: downloading arbitrary files)
        // if (!preg_match('/^[a-z0-9\-]+\.[a-z0-9\-]+$/i', $file) || !is_file("$path/$file")) {
        //     throw new \yii\web\NotFoundHttpException('File not found.');
        // }

        if (file_exists($path . '/' . $file)) {
            return Yii::$app->response->sendFile("$path/$file", $file);
        }
    }



    /**
     * Finds the MbtcAssessmentGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MbtcAssessmentGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MbtcAssessmentGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
