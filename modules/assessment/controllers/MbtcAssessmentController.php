<?php

namespace app\modules\assessment\controllers;

use Yii;
use app\modules\assessment\models\MbtcAssessment;
use app\modules\assessment\models\MbtcAssessmentSearch;
use app\modules\assessment\models\MbtcAssessmentDetail;
use app\modules\assessment\models\MbtcItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use Nesk\Puphpeteer\Puppeteer;
use Nesk\Rialto\Data\JsFunction;

/**
 * MbtcAssessmentController implements the CRUD actions for MbtcAssessment model.
 */
class MbtcAssessmentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MbtcAssessment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MbtcAssessmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['is_completed' => 1]); 
        $dataProvider->query->orderBy(['date' => SORT_DESC]); 

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MbtcAssessment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
        $model = $this->findModel($id);
        $items = MbtcItem::find()->all(); 
        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;
        
        $items = MbtcItem::find()->all();          

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        } 

        // $score_categories = ['FACTS', 'FUTURE', 'FEELING', 'FORM'];
        $score_categories = ['D. FUTURE', 'C. FEELING', 'B. FORM', 'A. FACT'];

        $score = [
            'analytical' => $total_analytical,
            'experimental' => $total_experimental,
            'practical' => $total_practical,
            'relational' => $total_relational,
        ];            

        $series = [
            [
                'name' => 'Value',
                'data' => [$total_relational, $total_practical, $total_experimental, $total_analytical],
            ],
        ]; 

        // $total_analytical = 0; 
        // $total_experimental = 0; 
        // $total_practical = 0; 
        // $total_relational = 0;

        // foreach ($details as $key => $detail) {
        //      if ($detail->mbtcItem->type == 10) {
        //         $total_analytical = $total_analytical + $detail->score; 
        //      } elseif ($detail->mbtcItem->type == 20) {
        //         $total_experimental = $total_experimental + $detail->score; 
        //      } elseif ($detail->mbtcItem->type == 30) {
        //         $total_practical = $total_practical + $detail->score; 
        //      } else {
        //         $total_relational = $total_relational + $detail->score;                 
        //      }
        // } 

        // // $score_categories = ['FACTS', 'FUTURE', 'FEELING', 'FORM'];
        // $score_categories = ['D. FUTURE = ' . $total_relational, 'C. FEELING = ' . $total_practical, 'B. FORM = ' . $total_experimental, 'A. FACT = ' . $total_analytical];

        // $score = [
        //     'analytical' => $total_analytical,
        //     'experimental' => $total_experimental,
        //     'practical' => $total_practical,
        //     'relational' => $total_relational,
        // ];

        // $series = [
        //     [
        //         'name' => 'Value',
        //         'data' => [$total_relational, $total_practical, $total_experimental, $total_analytical],
        //     ],            
        // ];

        // echo '<pre>'; 
        // var_dump($score); die(); 

        return $this->render('view', [
            'model' => $model,            
            'details' => $details,
            'score_categories' => $score_categories,
            'score' => $score,
            'series' => $series,
        ]);
    }

    /**
     * Creates a new MbtcAssessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MbtcAssessment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MbtcAssessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MbtcAssessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionGenerateReport($id) {

        $model = $this->findModel($id); 

        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        }

        $cerebral = $total_analytical + $total_relational; 
        $limbic = $total_experimental + $total_practical; 
        $left_mode = $total_analytical + $total_experimental; 
        $right_mode = $total_practical + $total_relational;

        $gapCerebralLimbic = abs($cerebral - $limbic);

        $arr = array();
        $pattern = '/([;:,-.\/ X])/';
        $responden_name = '';
        $array = preg_split($pattern, $model->name, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        foreach($array as $k => $v) {
            $responden_name .= ucwords(strtolower($v));
        }

        // $cerebral = ($score['analytical'] + $score['relational']); 
        // $limbic = ($score['experimental'] + $score['practical']); 
        // $left_mode = ($score['analytical'] + $score['experimental']); 
        // $right_mode = ($score['practical'] + $score['relational']);


        $path = \Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png';
        $url = \yii\helpers\Url::toRoute(['/assessment/mbtc-assessment/view', 'id' => $model->id], true);

        // var_dump($url);

        // $x = 288;
        // $y = 570.39;
        // $width = 919.33;
        // $height = 513.33;

        // $x = 288;
        // $y = 570.39;
        // $width = 919.33;
        // $height = 480;

        $x = 288;
        // $y = 660.62;
        $y = 636;
        $width = 919.33;
        $height = 480;
       
        // $executablePath = '/usr/bin/google-chrome';
        $executablePath = '/usr/bin/chromium-browser';

        $puppeteer = new \Nesk\Puphpeteer\Puppeteer([
            'read_timeout' => 240,
            'idle_timeout' => 240
        ]);
        $browser = $puppeteer->launch([
            'executablePath' => $executablePath,
            // 'executable_path' => '/usr/bin/node',
            'headless' => true,
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
            // 'args' => ['--disable-gpu', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'],
        ]);

        $page = $browser->newPage();        
        $page->setViewport(['width' => 1263, 'height' => 1200, 'deviceScaleFactor' => 2]);
        $page->goto($url, ['waitUntil' => 'networkidle2', 'timeout' => 6000]);
        $page->screenshot(['path' => $path, 'clip' => [
            'x' => $x, 'y' => $y, 'width' => $width, 'height' => $height
        ]]);

        $browser->close();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4-L',
            'margin_left' => 16.5,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'fontDir' => array_merge($fontDirs, [
                Yii::getAlias('@app/web/css/fonts')
            ]),
            'fontdata' => $fontData + [
                'roboto' => [
                    'R' => 'Roboto-Regular.ttf',
                    'I' => 'Roboto-Italic.ttf',
                    'B' => 'Roboto-Bold.ttf',
                ],
                'liberationsans' => [
                    'R' => 'LiberationSans-Regular.ttf',
                    'I' => 'LiberationSans-Italic.ttf',
                    'B' => 'LiberationSans-Bold.ttf',
                ],
                'poppins' => [
                    'R' => 'Poppins-Regular.ttf',
                    'I' => 'Poppins-Italic.ttf',
                    'B' => 'Poppins-Bold.ttf',
                ]
            ],
            'default_font' => 'poppins',
            'default_font_size' => 12,
        ]);

        $template = Yii::getAlias('@app/web/files/templates/report-temp.pdf');
        $pagecount = $mpdf->SetSourceFile($template);

        for ($i=1; $i<=$pagecount; $i++) {
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);

            if ($i == 1) {
                $name = $responden_name;
                $position = $model->position;
                $organization = $model->organization;
                $mpdf->text_input_as_HTML = true;
                $mpdf->SetFont('poppins', 'B', 25);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,112);
                $mpdf->Write(0, $name);

                $mpdf->SetFont('poppins', 'I', 13);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,124);
                $mpdf->Write(0, $position);

                $mpdf->SetFont('poppins', 'R', 18);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,130);
                $mpdf->Write(0, $organization);
            }

            if ($i == 2) {
                $img = \yii\helpers\Html::img(Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png');
                $mpdf->SetXY(8.25,25.87);
                $mpdf->SetMargins(10, 10, 10, true);
                $mpdf->WriteHtml($img);

                $mpdf->SetFont('poppins', 'R', 26);
                $mpdf->SetTextColor(27,117,188);
                $mpdf->SetMargins(10, 10, 10);
                $mpdf->SetXY(10, 185);
                $mpdf->Cell(0, 0, "Cerebral - Limbic = " . $gapCerebralLimbic, 0, true, 'C');
            }

            if ($i < $pagecount)
                $mpdf->AddPage();
        }

        $filename = \Yii::getAlias('@app/web/files/') . $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
        
        $mpdf->Output($filename, 'F');
        $model->filename = $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
        $model->report_status = $model::REPORT_GENERATED;
        $model->save(false); 
        return $this->redirect(['view', 'id' => $id]);

    }

    public function actionGenerateReportEnglish($id, $lang) {

        $model = $this->findModel($id); 

        $details = $model->mbtcAssessmentDetails;    

        $total_analytical = 0; 
        $total_experimental = 0; 
        $total_practical = 0; 
        $total_relational = 0;

        foreach ($details as $key => $detail) {
             if ($detail->mbtcItem->type == 10) {
                $total_analytical = $total_analytical + $detail->score; 
             } elseif ($detail->mbtcItem->type == 20) {
                $total_experimental = $total_experimental + $detail->score; 
             } elseif ($detail->mbtcItem->type == 30) {
                $total_practical = $total_practical + $detail->score; 
             } else {
                $total_relational = $total_relational + $detail->score;                 
             }
        }

        $cerebral = $total_analytical + $total_relational; 
        $limbic = $total_experimental + $total_practical; 
        $left_mode = $total_analytical + $total_experimental; 
        $right_mode = $total_practical + $total_relational;

        $gapCerebralLimbic = abs($cerebral - $limbic);

        $arr = array();
        $pattern = '/([;:,-.\/ X])/';
        $responden_name = '';
        $array = preg_split($pattern, $model->name, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        foreach($array as $k => $v) {
            $responden_name .= ucwords(strtolower($v));
        }

        // $cerebral = ($score['analytical'] + $score['relational']); 
        // $limbic = ($score['experimental'] + $score['practical']); 
        // $left_mode = ($score['analytical'] + $score['experimental']); 
        // $right_mode = ($score['practical'] + $score['relational']);


        $path = \Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png';
        $url = \yii\helpers\Url::toRoute(['/assessment/mbtc-assessment/view', 'id' => $model->id], true);

        // var_dump($url);

        // $x = 288;
        // $y = 570.39;
        // $width = 919.33;
        // $height = 513.33;

        $x = 288;
        $y = 636;
        $width = 919.33;
        $height = 480;
       
        // $executablePath = '/usr/bin/google-chrome';
        $executablePath = '/usr/bin/chromium-browser';

        $puppeteer = new \Nesk\Puphpeteer\Puppeteer([
            'read_timeout' => 240,
            'idle_timeout' => 240
        ]);
        $browser = $puppeteer->launch([
            'executablePath' => $executablePath,
            // 'executable_path' => '/usr/bin/node',
            'headless' => true,
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
            // 'args' => ['--disable-gpu', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'],
        ]);

        $page = $browser->newPage();        
        $page->setViewport(['width' => 1263, 'height' => 1200, 'deviceScaleFactor' => 2]);
        $page->goto($url, ['waitUntil' => 'networkidle2', 'timeout' => 6000]);
        $page->screenshot(['path' => $path, 'clip' => [
            'x' => $x, 'y' => $y, 'width' => $width, 'height' => $height
        ]]);

        $browser->close();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4-L',
            'margin_left' => 16.5,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'fontDir' => array_merge($fontDirs, [
                Yii::getAlias('@app/web/css/fonts')
            ]),
            'fontdata' => $fontData + [
                'roboto' => [
                    'R' => 'Roboto-Regular.ttf',
                    'I' => 'Roboto-Italic.ttf',
                    'B' => 'Roboto-Bold.ttf',
                ],
                'liberationsans' => [
                    'R' => 'LiberationSans-Regular.ttf',
                    'I' => 'LiberationSans-Italic.ttf',
                    'B' => 'LiberationSans-Bold.ttf',
                ],
                'poppins' => [
                    'R' => 'Poppins-Regular.ttf',
                    'I' => 'Poppins-Italic.ttf',
                    'B' => 'Poppins-Bold.ttf',
                ]
            ],
            'default_font' => 'poppins',
            'default_font_size' => 12,
        ]);

        $template = Yii::getAlias('@app/web/files/templates/report-temp-eng.pdf');
        $pagecount = $mpdf->SetSourceFile($template);

        for ($i=1; $i<=$pagecount; $i++) {
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);

            if ($i == 1) {
                $name = $responden_name;
                $position = $model->position;
                $organization = $model->organization;
                $mpdf->text_input_as_HTML = true;
                $mpdf->SetFont('poppins', 'B', 25);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,112);
                $mpdf->Write(0, $name);

                $mpdf->SetFont('poppins', 'I', 13);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,124);
                $mpdf->Write(0, $position);

                $mpdf->SetFont('poppins', 'R', 18);
                $mpdf->SetTextColor(255,255,255);
                $mpdf->SetXY(16.5,130);
                $mpdf->Write(0, $organization);
            }

            if ($i == 2) {
                $img = \yii\helpers\Html::img(Yii::getAlias('@app/web/images/charts/') . \yii\helpers\Inflector::slug($model->name, '-', true) . '-' . $model->id . '-chart.png');
                $mpdf->SetXY(8.25,25.87);
                $mpdf->SetMargins(10, 10, 10, true);
                $mpdf->WriteHtml($img);

                $mpdf->SetFont('poppins', 'R', 26);
                $mpdf->SetTextColor(27,117,188);
                $mpdf->SetMargins(10, 10, 10);
                $mpdf->SetXY(10, 185);
                $mpdf->Cell(0, 0, "Cerebral - Limbic = " . $gapCerebralLimbic, 0, true, 'C');
            }

            if ($i < $pagecount)
                $mpdf->AddPage();
        }

        $filename = \Yii::getAlias('@app/web/files/') . $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
        
        $mpdf->Output($filename, 'F');
        $model->filename = $model->name . ' - ' . 'MBTC Assessment Report ' . '(' . $model->date . ')' . '.pdf';
        $model->report_status = $model::REPORT_GENERATED;
        $model->save(false); 
        return $this->redirect(['view', 'id' => $id]);

    }

    /**
     * View course file.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $file
     * @return mixed
     */

    public function actionDownloadReport($file) 
    { 
        $path = Yii::getAlias('@webroot/files/');

        // check filename for allowed chars (do not allow ../ to avoid security issue: downloading arbitrary files)
        // if (!preg_match('/^[a-z0-9\-]+\.[a-z0-9\-]+$/i', $file) || !is_file("$path/$file")) {
        //     throw new \yii\web\NotFoundHttpException('File not found.');
        // }

        if (file_exists($path . '/' . $file)) {
            return Yii::$app->response->sendFile("$path/$file", $file);
        }
    }

    /**
     * Finds the MbtcAssessment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MbtcAssessment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MbtcAssessment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionTestPuppet() {
        $puppeteer = new Puppeteer;
        $browser = $puppeteer->launch();

        $page = $browser->newPage();
        $page->goto('https://example.com');
        $page->screenshot(['path' => 'example.png']);

        $browser->close();
    }
}
