<?php

namespace app\modules\assessment\controllers;

use yii\web\Controller;

/**
 * Default controller for the `assessment` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['/assessment/mbtc-assessment-group/index']);
    }
}
