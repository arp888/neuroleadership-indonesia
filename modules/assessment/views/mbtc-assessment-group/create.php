<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\assessment\models\MbtcAssessmentGroup */

$this->title = Yii::t('app', 'Create MBTC Assessment Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="mbtc-assessment-group-create">
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>
</div>
