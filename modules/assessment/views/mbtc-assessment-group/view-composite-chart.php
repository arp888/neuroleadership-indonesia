<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\HbdiAssessment */

$this->title = 'Composite Chart';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

// \yii\helpers\Json::htmlEncode($var)

$dataTable = \yii\helpers\Json::htmlEncode($highVal); 

// var_dump($dataTable); die(); 

?>
<div class="mbtc-assessment-view">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['view', 'id' => $model->id], ['class' => 'btn btn-light']) ?>                    
                </div>
            </div>
        </div>

        <div class="card-body">

            <h5 class="card-title">MBTC Assessment Composite Chart</h5>

            <div>
        
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-12 text-center">                       
                        <?php                     
                            echo Highcharts::widget([            
                                'id' => 'renderTo',
                                'scripts' => [
                                    'highcharts-more',
                                    'modules/no-data-to-display',
                                    'modules/accessibility',
                                    'modules/exporting',
                                    'modules/export-data',
                                    'modules/offline-exporting',
                                    'modules/parallel-coordinates'
                                    // 'themes/grid-light',
                                ],
                                'options' => [
                                    'chart'  => [
                                        'polar' => true,                            
                                        'marginTop' => 50,
                                        'height' => 540,
                                        'events' => [
                                            'load' => new JsExpression('function(){
                                                var chart = this,
                                                parts = this.xAxis[0].max
                                                customColors = [                                        
                                                    "#ffc107", // YELLOW
                                                    "#ff5722", // RED
                                                    "#009688", // GREEN
                                                    "#007bff", // BLUE                                        
                                                ],
                                                series = chart.series,
                                                renderer = chart.renderer,
                                                cellLeft = tableLeft;

                                                // console.log("parts",parts);

                                                var colorsLength = customColors.length - 1;

                                                var count = 0;

                                                for(var i = 0; i < parts; i++) {
                                                    //setting count back to 0 if colours exceed 20.
                                                    if(count > colorsLength) {
                                                        count = 0;
                                                    }

                                                    var centerX = chart.plotLeft + chart.yAxis[0].center[0];
                                                    var centerY = chart.plotTop + chart.yAxis[0].center[1];
                                                    var outerRradius = chart.yAxis[0].height;
                                                    var innerRadius = 0;
                                                    var start = -Math.PI/2 + (Math.PI/(parts/2) * i);
                                                    var end = -Math.PI/2 + (Math.PI/(parts/2) * (i+1));
                                                    var color = new Highcharts.Color(customColors[count]).setOpacity(0.7).get();
                                                    count++;

                                                    chart.renderer.arc(centerX,centerY,outerRradius,innerRadius,start,end).attr({
                                                        id: "renderedArc",
                                                        fill: color,
                                                        "stroke-width": 1
                                                    }).add();
                                                };

                                                // user options
                                                var tableTop = 10,
                                                    colWidth = 100,
                                                    tableLeft = 20,
                                                    rowHeight = 20,
                                                    cellPadding = 2.5,
                                                    valueDecimals = 1,
                                                    valueSuffix = " °C";                                               

                                                var dataTable = ' . $dataTable . '; 

                                                console.log(dataTable["experimental"]); 

                                                var labelAnalytical = ""; 
                                                var labelExperimental = ""; 
                                                var labelPractical = ""; 
                                                var labelRelational = "";
                                                
                                                if (Array.isArray(dataTable["analytical"])) {
                                                    dataTable["analytical"].forEach(function (serie, i) {
                                                        labelAnalytical += serie + "<br/>";
                                                    });               
                                                }

                                                if (Array.isArray(dataTable["experimental"])) {
                                                    dataTable["experimental"].forEach(function (serie, i) {
                                                        labelExperimental += serie + "<br/>";
                                                    });
                                                    // labelExperimental = "<div id=\'exp\'>" + labelExperimental + "</div>";
                                                }

                                                if (Array.isArray(dataTable["practical"])) {
                                                    dataTable["practical"].forEach(function (serie, i) {
                                                        labelPractical += serie + "<br/>";
                                                    });
                                                }

                                                if (Array.isArray(dataTable["relational"])) {
                                                    dataTable["relational"].forEach(function (serie, i) {
                                                        labelRelational += serie + "<br/>";
                                                    });
                                                }

                                                 var left = chart.plotLeft,
                                                    right = chart.plotWidth,
                                                    top = chart.plotTop + 10,
                                                    bottom = chart.plotHeight - chart.plotTop;

                                                // console.log(left); 
                                                // console.log(right);
                                                // console.log(top);
                                                console.log(bottom);

                                                chart.renderer.text(labelAnalytical, left, top)
                                                   .attr({
                                                    zIndex: 5,
                                                    align: "left",
                                                    id: "analytic",
                                                   }) 
                                                  .css({
                                                    fontSize: "6px",
                                                    fontFamily: "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    color: "#666666"
                                                  })
                                                  .add();

                                                chart.renderer.text(labelExperimental, left, 310)
                                                   .attr({
                                                    zIndex: 5,
                                                    align: "left"
                                                  }) 
                                                  .css({
                                                    fontSize: "6px",
                                                    fontFamily: "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    color: "#666666"
                                                  })
                                                  .add();

                                                chart.renderer.text(labelPractical, right, 310)
                                                   .attr({
                                                    zIndex: 5,
                                                    align: "right",
                                                    id: "practical",
                                                  }) 
                                                  .css({
                                                    fontSize: "6px",
                                                    fontFamily: "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    color: "#666666"
                                                  })
                                                  .add();

                                                chart.renderer.text(labelRelational, right, top)
                                                   .attr({
                                                    zIndex: 5,
                                                    align: "right"
                                                  }) 
                                                  .css({
                                                    fontSize: "6px",
                                                    fontFamily: "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    color: "#666666"
                                                  })
                                                  .add();


                                                
                                            }')
                                        ],
                                    ],
                                    
                                    'title' => [
                                        'text' => null,
                                        // 'text' => $model->group_name . ' Composite Chart',
                                        // 'x' => 0,
                                        'enabled' => false,
                                    ],

                                    'plotOptions' => [
                                        'line' => [
                                            'dataLabels' => [
                                                'enabled' => false,
                                                'allowOverlap' => true,
                                                'inside' => false,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '8px',
                                                    'fontWeight' => '500',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'enableMouseTracking' => false
                                        ],
                                        'series' => [                        
                                            'enableMouseTracking' => false,                        
                                            'shadow' => false, 
                                            'animation' => false,    
                                            'color' => '#1a7da1',      
                                            'lineWidth' => 3,
                                            // 'marker' => false,
                                            'marker' => [
                                                "radius" => 4,
                                                "symbol" => "circle"
                                            ],
                                            '_colorIndex' => 0,
                                            '_symbolIndex' => 0
                                        ],
                                        'column' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'allowOverlap' => true,
                                                'inside' => true,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '16px',
                                                    'fontWeight' => 'normal',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'grouping' => false,
                                            'shadow'  => false,
                                            'borderWidth' => 0
                                        ]
                                    ],   

                                    'pane' => [
                                        'startAngle' => 45,
                                        'endAngle' => 405,
                                        // 'size' => 520,
                                    ],

                                    'xAxis' => [                    
                                        // 'categories' => $score_categories,
                                        'tickmarkPlacement' => 'on',
                                        'gridLineColor' => '#d1d3d4',
                                        'lineWidth' => 0,
                                        'gridLineColor' => "#d1d3d4",
                                        'tickInterval' => 0.5,
                                        'labels' => [
                                            'rotation' => 0,
                                            'overflow' => 'justified', 
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '1.1rem',
                                                'fontWeight' => '600',
                                                'textOverflow' => 'none',
                                                'color' => '#414042',
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false  
                                            ],
                                            'formatter' => new JsExpression('function(){
                                                let label;
                                                switch (this.value) {                                                    
                                                    case 0:
                                                        label = "D. FUTURE";
                                                        break;
                                                    case 0.5:
                                                        label = "Right Mode (C + D)";
                                                        break;
                                                    case 1:
                                                        label = "C. FEELING";
                                                        break;
                                                    case 1.5:
                                                        label = "Limbic Mode (B + C)";
                                                        break;
                                                    case 2:
                                                        label = "B. FORM";
                                                        break;
                                                    case 2.5:
                                                        label = "Left Mode (A + B)";
                                                        break; 
                                                    case 3:
                                                        label = "A. FACT";
                                                        break;
                                                    case 3.5:
                                                        label = "Cerebral Mode (A + D)";
                                                        break;                                                  
                                                }                                                
                                                return label;
                                            }')
                                        ],
                                        // 'labels' => [
                                        //     'rotation' => 0,
                                        //     'style' => [
                                        //         "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                        //         'fontSize' => '16px',
                                        //         'fontWeight' => 'normal',
                                        //         'color' => "#414042",
                                        //         'borderWidth' => 0,
                                        //         'textShadow' => false,
                                        //         'textOutline' => false  
                                        //     ]
                                        // ],
                                    ],

                                    'yAxis' => [
                                        // 'gridLineInterpolation' => 'polygon',
                                        "gridLineColor" => "#e5e7e9",
                                        "lineWidth" => 1,
                                        "tickmarkPlacement" => "between",
                                        "angle" => -45,
                                        // "tickPixelInterval" => 100,
                                        "tickPosition" => "center",
                                        'tickPositions' => [0, 40, 80, 120, 160],
                                        'min' => 0,
                                        // 'max' => 5,

                                        'color' => '#fdebea',
                                        'showFirstLabel' => true,
                                        'showLastLabel' => true,
                                        'gridLineWidth' => 1,
                                        'tickInterval' => 1,
                                        'endOfTick' => true,
                                        // 'minPadding' => 0,
                                        // 'maxPadding' => 0,
                                        'labels' => [
                                            'align' => 'center',
                                            'x' => 0,
                                            'y' => -4,
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '10px',
                                                'fontWeight' => 'normal',
                                                'color' => "#414042",
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false  
                                            ],
                                        ],                            
                                    ],
                                    'legend' => [
                                        'enabled' => false,
                                        'align' => 'center',
                                        'verticalAlign' => 'bottom',
                                        'layout' => 'horizontal',
                                        'itemMarginTop' => 5,
                                        'itemMarginBottom' => 10,
                                        'backgroundColor' => '#f1f2f2',
                                        'borderRadius' => 5,
                                        'padding' => 10,
                                        'itemDistance' => 40,
                                        'reversed' => true, 
                                        'itemStyle' => [
                                            "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12pt',
                                            'fontWeight' => 'normal',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],

                                    'series' => array_values($series),

                                    'lang' => [
                                        'noData' => 'No Data Available.'
                                    ],
                                    'credits' => ['enabled' => false],
                                    'exporting' => [
                                        'enabled' => true,
                                        'sourceWidth' => 960,
                                        'sourceHeight' => 540,
                                        'scale' => 2,
                                        'filename' => $model->group_name . ' Composite Chart',
                                        'buttons' => [
                                            'contextButton' => [
                                                // 'menuItems' => ['downloadPNG', 'downloadJPEG'],
                                                // 'x' => 100,
                                                // 'y' => 530,
                                                'align' => 'right',
                                                // 'marginTop' => 30,
                                                'verticalAlign' => 'top',
                                                'symbol' => null,
                                                'menuItems' => null,
                                                'text' => 'Download Image',
                                                'theme' => [
                                                    'fill' => '#ddd',
                                                    'stroke' => '#888',
                                                    'states' =>  [
                                                        'hover' => [
                                                            'fill' => '#fcc',
                                                            'stroke' => '#f00'
                                                        ],
                                                        'select' => [
                                                            'fill' => '#cfc',
                                                            'stroke' => '#0f0'
                                                        ]
                                                    ]
                                                ],
                                                'onclick' =>  new JsExpression('function() {
                                                    this.exportChart({
                                                      type: "image/png"
                                                    });
                                                }'),
                                            ],
                                            // 'exportButton' => [
                                            //     'align' => 'top',
                                            //     // 'y' => 10
                                            // ]
                                        ],
                                        'showTable' => false,
                                        // 'chartOptions' => [
                                        //     'title' => [
                                        //         'style' => [
                                        //             'fontSize' => '12px',
                                        //         ]
                                        //     ]
                                        // ]
                                    ],
                                    'navigation' => [
                                        'buttonOptions' => [
                                            'verticalAlign' => 'middle',
                                            // 'horizontalAlign' => 'bottom',
                                            'x' => -5,
                                            // 'y' => -30,
                                        ]
                                    ],
                                    'colors' => [
                                        "#90ed7d",
                                        "#f15a29",
                                        "#f7a35c",
                                        "#8085e9",
                                        "#f15c80",
                                        "#e4d354",
                                        "#2b908f",
                                        "#f45b5b",
                                        "#91e8e1"
                                    ],                
                                ]
                            ]);                             
                        ?>
                    </div>                
                </div>
            </div>

            <hr>

            <?php 

                // var_dump($seriesGrandTotal['relational']); die(); 
            ?>

            <div>
            
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-12 text-center">                       
                        <?php                     
                            echo Highcharts::widget([            
                                'id' => 'renderChart',
                                'scripts' => [
                                    'highcharts-more',
                                    'modules/no-data-to-display',
                                    'modules/accessibility',
                                    'modules/exporting',
                                    'modules/export-data',
                                    'modules/offline-exporting',
                                    'modules/parallel-coordinates'
                                    // 'themes/grid-light',
                                ],
                                'options' => [
                                    'chart'  => [
                                        'polar' => true,                            
                                        // 'marginBottom' => 25,
                                        'height' => 540,
                                        'events' => [
                                            'load' => new JsExpression('function(){
                                                var chart = this,
                                                parts = this.xAxis[0].max
                                                customColors = [                                        
                                                    "#ffc107", // YELLOW
                                                    "#ff5722", // RED
                                                    "#009688", // GREEN
                                                    "#007bff", // BLUE                                        
                                                ],
                                                series = chart.series,
                                                renderer = chart.renderer,
                                                cellLeft = tableLeft;

                                                // console.log("parts",parts);

                                                var colorsLength = customColors.length - 1;

                                                var count = 0;

                                                for(var i = 0; i < parts; i++) {
                                                    //setting count back to 0 if colours exceed 20.
                                                    if(count > colorsLength) {
                                                        count = 0;
                                                    }

                                                    var centerX = chart.plotLeft + chart.yAxis[0].center[0];
                                                    var centerY = chart.plotTop + chart.yAxis[0].center[1];
                                                    var outerRradius = chart.yAxis[0].height;
                                                    var innerRadius = 0;
                                                    var start = -Math.PI/2 + (Math.PI/(parts/2) * i);
                                                    var end = -Math.PI/2 + (Math.PI/(parts/2) * (i+1));
                                                    var color = new Highcharts.Color(customColors[count]).setOpacity(0.7).get();
                                                    count++;

                                                    chart.renderer.arc(centerX,centerY,outerRradius,innerRadius,start,end).attr({
                                                        id: "renderedArc",
                                                        fill: color,
                                                        "stroke-width": 1
                                                    }).add();
                                                };

                                                // user options
                                                var tableTop = 10,
                                                    colWidth = 100,
                                                    tableLeft = 20,
                                                    rowHeight = 20,
                                                    cellPadding = 2.5,
                                                    valueDecimals = 1,
                                                    valueSuffix = " °C";      

                                                var dataTable = ' . $dataTable . '; 

                                                console.log(dataTable["experimental"]); 

                                                 var left = chart.plotLeft,
                                                    right = chart.plotWidth,
                                                    top = chart.plotTop + 10,
                                                    bottom = chart.plotHeight - chart.plotTop;

                                                // console.log(left); 
                                                // console.log(right);
                                                // console.log(top);
                                                console.log(bottom);

                                                
                                            }')
                                        ],
                                    ],
                                    
                                    'title' => [
                                        'text' => null,
                                        'x' => 0,
                                        'enabled' => false,
                                    ],

                                    'plotOptions' => [
                                        'line' => [
                                            'dataLabels' => [
                                                'enabled' => false,
                                                'allowOverlap' => true,
                                                'inside' => false,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '8px',
                                                    'fontWeight' => '500',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'enableMouseTracking' => false
                                        ],
                                        'series' => [                        
                                            'enableMouseTracking' => false,                        
                                            'shadow' => false, 
                                            'animation' => false,    
                                            'color' => '#1a7da1',      
                                            'lineWidth' => 3,
                                            // 'marker' => false,
                                            'marker' => [
                                                "radius" => 4,
                                                "symbol" => "circle"
                                            ],
                                            '_colorIndex' => 0,
                                            '_symbolIndex' => 0
                                        ],
                                        'column' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'allowOverlap' => true,
                                                'inside' => true,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '16px',
                                                    'fontWeight' => 'normal',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'grouping' => false,
                                            'shadow'  => false,
                                            'borderWidth' => 0
                                        ]
                                    ],   

                                    'pane' => [
                                        'startAngle' => 45,
                                        'endAngle' => 405,
                                        // 'size' => 520,
                                    ],

                                    'xAxis' => [                    
                                        // 'categories' => $score_categories,
                                        'tickmarkPlacement' => 'on',
                                        'gridLineColor' => '#d1d3d4',
                                        'lineWidth' => 0,
                                        'gridLineColor' => "#d1d3d4",
                                        'tickInterval' => 0.5,
                                        'labels' => [
                                            'rotation' => 0,
                                            'overflow' => 'justified', 
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '1.1rem',
                                                'fontWeight' => '600',
                                                'textOverflow' => 'none',
                                                'color' => '#414042',
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false  
                                            ],
                                            'formatter' => new JsExpression('function(){
                                                let label;
                                                switch (this.value) {                                                    
                                                    case 0:
                                                        label = "D. FUTURE = ' . round($seriesGrandTotal['relational']) . '";
                                                        break;
                                                    case 0.5:
                                                        label = "Right Mode (C + D)";
                                                        break;
                                                    case 1:
                                                        label = "C. FEELING = ' . round($seriesGrandTotal['practical']) . '";
                                                        break;
                                                    case 1.5:
                                                        label = "Limbic Mode (B + C)";
                                                        break;
                                                    case 2:
                                                        label = "B. FORM = ' . round($seriesGrandTotal['experimental']) . '";
                                                        break;
                                                    case 2.5:
                                                        label = "Left Mode (A + B)";
                                                        break; 
                                                    case 3:
                                                        label = "A. FACT = ' . round($seriesGrandTotal['analytical']) . '";
                                                        break;
                                                    case 3.5:
                                                        label = "Cerebral Mode (A + D)";
                                                        break;                                                  
                                                }                                                
                                                return label;
                                            }')
                                        ],
                                        // 'labels' => [
                                        //     'rotation' => 0,
                                        //     'style' => [
                                        //         "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                        //         'fontSize' => '16px',
                                        //         'fontWeight' => 'normal',
                                        //         'color' => "#414042",
                                        //         'borderWidth' => 0,
                                        //         'textShadow' => false,
                                        //         'textOutline' => false  
                                        //     ]
                                        // ],
                                    ],

                                    'yAxis' => [
                                        // 'gridLineInterpolation' => 'polygon',
                                        "gridLineColor" => "#e5e7e9",
                                        "lineWidth" => 1,
                                        "tickmarkPlacement" => "between",
                                        "angle" => -45,
                                        // "tickPixelInterval" => 100,
                                        "tickPosition" => "center",
                                        'tickPositions' => [0, 40, 80, 120, 160],
                                        'min' => 0,
                                        // 'max' => 5,

                                        'color' => '#fdebea',
                                        'showFirstLabel' => true,
                                        'showLastLabel' => true,
                                        'gridLineWidth' => 1,
                                        'tickInterval' => 1,
                                        'endOfTick' => true,
                                        // 'minPadding' => 0,
                                        // 'maxPadding' => 0,
                                        'labels' => [
                                            'align' => 'center',
                                            'x' => 0,
                                            'y' => -4,
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '10px',
                                                'fontWeight' => 'normal',
                                                'color' => "#414042",
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false  
                                            ],
                                        ],                            
                                    ],
                                    'legend' => [
                                        'enabled' => false,
                                        'align' => 'center',
                                        'verticalAlign' => 'bottom',
                                        'layout' => 'horizontal',
                                        'itemMarginTop' => 5,
                                        'itemMarginBottom' => 10,
                                        'backgroundColor' => '#f1f2f2',
                                        'borderRadius' => 5,
                                        'padding' => 10,
                                        'itemDistance' => 40,
                                        'reversed' => true, 
                                        'itemStyle' => [
                                            "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12pt',
                                            'fontWeight' => 'normal',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],

                                    'series' => array_values($seriesAverage),

                                    'lang' => [
                                        'noData' => 'No Data Available.'
                                    ],
                                    'credits' => ['enabled' => false],
                                    'exporting' => [
                                        'enabled' => true,
                                        'sourceWidth' => 960,
                                        'sourceHeight' => 540,
                                        'filename' => $model->group_name . ' Composite Chart Average',
                                        'scale' => 2,
                                        'buttons' => [
                                            'contextButton' => [
                                                // 'menuItems' => ['downloadPNG', 'downloadJPEG'],
                                                // 'x' => 100,
                                                // 'y' => 530,
                                                'align' => 'right',
                                                // 'marginTop' => 30,
                                                'verticalAlign' => 'top',
                                                'symbol' => null,
                                                'menuItems' => null,
                                                'text' => 'Download Image',
                                                'theme' => [
                                                    'fill' => '#ddd',
                                                    'stroke' => '#888',
                                                    'states' =>  [
                                                        'hover' => [
                                                            'fill' => '#fcc',
                                                            'stroke' => '#f00'
                                                        ],
                                                        'select' => [
                                                            'fill' => '#cfc',
                                                            'stroke' => '#0f0'
                                                        ]
                                                    ]
                                                ],
                                                'onclick' =>  new JsExpression('function() {
                                                    this.exportChart({
                                                      type: "image/png"
                                                    });
                                                }'),
                                            ],
                                            // 'exportButton' => [
                                            //     'align' => 'top',
                                            //     // 'y' => 10
                                            // ]
                                        ],
                                        'showTable' => false,
                                        // 'chartOptions' => [
                                        //     'title' => [
                                        //         'style' => [
                                        //             'fontSize' => '12px',
                                        //         ]
                                        //     ]
                                        // ]
                                    ],
                                    'navigation' => [
                                        'buttonOptions' => [
                                            'verticalAlign' => 'middle',
                                            // 'horizontalAlign' => 'bottom',
                                            'x' => -5,
                                            // 'y' => -30,
                                        ]
                                    ],
                                    'colors' => [
                                        "#90ed7d",
                                        "#f15a29",
                                        "#f7a35c",
                                        "#8085e9",
                                        "#f15c80",
                                        "#e4d354",
                                        "#2b908f",
                                        "#f45b5b",
                                        "#91e8e1"
                                    ],                
                                ]
                            ]);                             
                        ?>
                    </div>                
                </div>
            </div>
        </div>
    </div>

        


        <!-- <div class="col-lg-12 mt-3 mb-3 d-flex justify-content-end align-items-center">
            <?= Html::a('Download', ['download-assessment-result', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div> -->

    
</div>

<?php 
$this->registerCss('
    .lead {
        font-size: 1.05rem;
        font-weight: 500;
    }
');

$js = <<< JS
Highcharts.tableLine = function (renderer, x1, y1, x2, y2) {
    renderer.path(['M', x1, y1, 'L', x2, y2])
        .attr({
            stroke: 'silver',
            'stroke-width': 1
        })
        .add();
};
JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>