<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\modules\assessment\models\MbtcAssessmentGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mbtc-assessment-group-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>

		    <?= $form->field($model, 'group_name')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'description')->textarea(['rows' => 6])->hint('Enter brief description of assessment group.') ?>

		     <?= $form->field($model, 'phone_required')->widget(SwitchInput::classname(), [
                'inlineLabel' => true,
                'containerOptions' => ['class' => 'form-group mb-1'],
                'pluginOptions' => [
                   'onColor' => 'success',
                   'onText' => Yii::t('app', 'Yes'),
                   'offText' => Yii::t('app', 'No'),
                ],
            ])->hint('Select "Yes" if respondent must enter phone number.') ?>

		    <?= $form->field($model, 'email_required')->widget(SwitchInput::classname(), [
                'inlineLabel' => true,
                'containerOptions' => ['class' => 'form-group mb-1'],
                'pluginOptions' => [
                   'onColor' => 'success',
                   'onText' => Yii::t('app', 'Yes'),
                   'offText' => Yii::t('app', 'No'),
                ],
            ])->hint('Select "Yes" if respondent must enter email.') ?>       

		    <?= $form->field($model, 'is_active')->widget(SwitchInput::classname(), [
                'options' => ['uncheck' => Yii::$app->appHelper::STATUS_INACTIVE, 'value' => Yii::$app->appHelper::STATUS_ACTIVE],
                'inlineLabel' => true,
                'containerOptions' => ['class' => 'form-group mb-1'],
                'pluginOptions' => [
                   'onColor' => 'success',
                   'onText' => Yii::t('app', 'Active'),
                   'offText' => Yii::t('app', 'Inactive'),
                ],
             ])->hint('Choose whether this group is still active or not.') ?>

    	</div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>
