<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\assessment\models\MbtcAssessment;

/* @var $this yii\web\View */
/* @var $model app\modules\assessment\models\MbtcAssessmentGroup */

$this->title = Yii::t('app', 'Detail MBTC Assessment Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->group_name;

\yii\web\YiiAsset::register($this);

$request = Yii::$app->request->queryParams;
// var_dump($request); 

?>
<div class="mbtc-assessment-group-view">    

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?> 
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => 'div', 'class' => 'row'],
                'template' => '<div class="col-md-4 mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
                    [
                        'attribute' => 'group_name',
                    ],

                    [
                        'attribute' => 'description',
                        'contentOptions' => ['class' => 'col-md-8'],
                        'format' => 'raw',            
                        'value' => function ($model) {
                            return ($model->description ? $model->description : '-');
                        }                 
                    ],

                    [
                        'attribute' => 'phone_required',                            
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->phone_required == 1 ? 'Yes' : 'No');
                        },
                    ],

                    [
                        'attribute' => 'email_required',                            
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->email_required == 1 ? 'Yes' : 'No');
                        },
                    ],

                    [
                        'attribute' => 'is_active',                            
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Yii::$app->appHelper->getStatusLabel($model->is_active);
                        },
                    ],
                ],
            ]) ?>            
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="d-flex row justify-content-between">
                <div class="ml-auto">
                    <form class="form-inline">
                        <?php if ($model->is_active == 10): ?>
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" id="assessmentLink" value=<?= \yii\helpers\Url::to(['/mbtc/assessment', 'id' => $model->id], true) ?>>
                            <div class="input-group-append">
                                <button class="btn btn-info" type="button" data-clipboard-action="copy" data-clipboard-target="#assessmentLink"><?= Yii::t('app', 'Copy Assessment Link') ?></button>
                            </div>
                        </div>               
                        <?php endif; ?>     


                        <?php if (Helper::checkRoute('generate-report') || Helper::checkRoute('generate-report-english')): ?>

                        <div class="btn-group btn-group-sm dropdown ml-1">                            
                            <button type="button" class="btn btn-success">Generate Reports</button>                            

                            <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-chevron-down sr-only"></i>
                            </button>

                            <div class="dropdown-menu">
                                <?= Html::a('Indonesia', ['generate-reports', 'id' => $model->id, 'lang' => 'id'], ['class' => 'dropdown-item']) ?>
                                <?= Html::a('English', ['generate-reports', 'id' => $model->id, 'lang' => 'en'], ['class' => 'dropdown-item']) ?>                            
                            </div>
                        </div>

                        <?php endif; ?>
                        
                        <?php if (Helper::checkRoute('view-composite-chart')): ?>
                        <?php if ($model->mbtcAssessments): ?>
                        <?= Html::a(Yii::t('app', 'View Composite Chart'), ['view-composite-chart', 'id' => $model->id, 'MbtcAssessmentSearch' =>  (isset($request['MbtcAssessmentSearch']) ? $request['MbtcAssessmentSearch'] : null) ,  'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-primary btn-sm ml-1',                           
                                                      
                        ]) ?>
                        <?php endif; ?>
                        <?php endif; ?>

                        <?= Html::a(Yii::t('app', 'Send Report'), ['send-report-to-email', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-warning btn-sm ml-1']) ?>

                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => false,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'mbtc-assessment-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'vAlign' => 'middle'
                    ],                    

                    [
                        'attribute' => 'date',
                        'mergeHeader' => true, 
                        'vAlign' => 'middle',
                        'format' => ['datetime', 'medium'],
                    ],

                    [
                        'attribute' => 'name',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        'vAlign' => 'middle',
                        'format' => 'raw', 
                        'value' => function ($model) {
                            $label = Html::a($model->name, ['mbtc-assessment/view', 'id' => $model->id], ['target' => 'blank']);                            
                            $label .= '<br><small>' . ($model->email ? $model->email : '-') . '</small>';
                            $label .= '<br><small>' . ($model->phone ? $model->phone : '-') . '</small>';

                            return $label; 
                        }
                    ],      

                    // [
                    //     'attribute' => 'phone',
                    //     'mergeHeader' => true, 
                    //     'vAlign' => 'middle',
                    //     'value' => function ($model) {
                    //         return ($model->phone ? $model->phone : '-');
                    //     }
                    // ],              

                    // [
                    //     'attribute' => 'email',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                    //     'mergeHeader' => true, 
                    //     'format' => 'email',
                    //     'vAlign' => 'middle',
                    //     'value' => function ($model) {
                    //         return ($model->email ? $model->email : '-');
                    //     }
                    // ],                   

                    // [
                    //     'attribute' => 'organization',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                    //     // 'mergeHeader' => true, 
                    //     'vAlign' => 'middle',
                    // ],

                    [
                        'attribute' => 'work_unit',
                        // 'width' => '350px',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        // 'mergeHeader' => true, 
                        'vAlign' => 'middle',
                    ],

                    [
                        'attribute' => 'position',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        // 'mergeHeader' => true, 
                        'vAlign' => 'middle',
                    ],

                    
                    [
                        'attribute' => 'location',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        // 'mergeHeader' => true, 
                        'vAlign' => 'middle',
                    ],

                    [
                        'attribute' => 'filename',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        'mergeHeader' => true, 
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->filename != null && is_file(Yii::getAlias('@webroot/files/') . $model->filename) ? Html::a(Yii::t('app', 'Download'), ['download-report', 'file' => $model->filename], ['class' => 'btn btn-primary btn-xs text-white', 'target' => 'blank', 'data-pjax' => 0]) : null);
                        }
                    ],
                                       
                    [
                        'attribute' => 'report_status',
                        // 'width' => '350px',
                        'filter' => MbtcAssessment::getReportStatusLists(),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'format' => 'raw',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->reportStatusLabel;
                        }
                    ],

                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['mbtc-assessment/view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },                                          
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['mbtc-assessment-delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>

<?php 
$this->registerCss('
    .avatar > img {
        border-radius: 50%;
        display: block;
        overflow: hidden;
        width: 100%;
        height: 100%;
    }
');
$this->registerJsFile('@web/js/clipboard.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs("
    var clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
");
?>