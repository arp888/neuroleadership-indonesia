<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\assessment\models\MbtcAssessmentGroup */

$this->title = Yii::t('app', 'Update MBTC Assessment Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mbtc-assessment-group-update">    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
