<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\HbdiItem */

$this->title = Yii::t('app', 'Create MBTC Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Item'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');

?>
<div class="hbdi-item-create">
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
