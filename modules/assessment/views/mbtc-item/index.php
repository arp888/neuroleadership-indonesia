<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;

use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\administrator\models\HbdiItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Item MBTC');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hbdi-item-index">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php
                        if (Helper::checkRoute('create')) {
                            echo Html::a(Yii::t('app', 'Create MBTC Item'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="card-body">           

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'mbtc-item-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'vAlign' => 'top'
                    ],


                    // 'id',
                    [
                        'attribute' => 'item',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],                
                    ],

                    [
                        'attribute' => 'item_eng',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search...'],                
                    ],

                    [
                        'attribute' => 'type',
                        'filter' => $searchModel::typeOptions(),
                        'filterInputOptions' => ['class' => 'form-control custom-select', 'prompt' => 'All Type'],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->typeLabel(); 
                        }
                    ],

                    // [
                    //     'class' => 'kartik\grid\ActionColumn',
                    //     'template' => '{update}{delete}',
                    //     'vAlign' => 'top',
                    //     'deleteOptions' => ['class' => 'ml-2', 'data-confirm' => "Anda yakin ingin menghapus item ini?"],
                    // ],

                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{update}{delete}') . '</div>',
                        'buttons' => [                            
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
