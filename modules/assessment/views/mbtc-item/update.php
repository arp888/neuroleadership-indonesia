<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\HbdiItem */

$this->title = Yii::t('app', 'Update MBTC Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Item'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="hbdi-item-update">
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
