<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\HbdiAssessment */

$this->title = 'Detail MBTC Assessment Result';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment Result'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
\yii\web\YiiAsset::register($this);


// var_dump($score['relational']); 

?>
<div class="hbdi-assessment-view">

    <div class="card">

        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    

                    <?php if (Helper::checkRoute('generate-report') || Helper::checkRoute('generate-report-english')): ?>

                    <div class="btn-group dropdown me-1">
                        <?php if ($model->filename == null && !is_file(Yii::getAlias('@webroot/files/') . $model->filename)): ?>
                        <button type="button" class="btn btn-info">Generate Report</button>
                        <?php else: ?>
                        <button type="button" class="btn btn-info">Regenerate Report</button>
                        <?php endif; ?>

                        <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-chevron-down sr-only"></i>
                        </button>

                        <div class="dropdown-menu">
                            <?= Html::a('Indonesia', ['generate-report', 'id' => $model->id], ['class' => 'dropdown-item']) ?>
                            <?= Html::a('English', ['generate-report-english', 'id' => $model->id], ['class' => 'dropdown-item']) ?>                            
                        </div>
                    </div>

                    <?php endif; ?>
               
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                   
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'model' => $model,
                'options' => ['tag' => 'div', 'class' => 'row'],
                'template' => '<div class="col-md-4 mb-3"><h5>{label}</h5>{value}</div>',
                'attributes' => [
                    // 'id',

                    [
                        'attribute' => 'mbtc_assessment_group_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->mbtc_assessment_group_id) {
                                return Html::a($model->mbtcAssessmentGroup->group_name, ['mbtc-assessment-group/view', 'id' => $model->mbtc_assessment_group_id]); 
                            } else {
                                echo '-';
                            }

                        },
                    ],

                    'name',
                    [
                        'attribute' => 'email',                                
                        'format' => 'email',                                
                        'value' => function ($model) {
                            return ($model->email ? $model->email : '-');
                        }
                    ],

                    [
                        'attribute' => 'phone',                                
                        'value' => function ($model) {
                            return ($model->phone ? $model->phone : '-');
                        }
                    ],

                    [
                        'attribute' => 'date',
                        'format' => ['datetime', 'medium'],
                    ],

                    [
                        'attribute' => 'position',
                    ],

                    [
                        'attribute' => 'organization',
                    ],

                    [
                        'attribute' => 'work_unit',
                    ],

                    [
                        'attribute' => 'location',
                    ],

                    [
                        'attribute' => 'filename',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->filename != null && is_file(Yii::getAlias('@webroot/files/') . $model->filename) ? Html::a(Yii::t('app', 'Download Report'), ['download-report', 'file' => $model->filename], ['class' => 'btn btn-primary btn-sm text-white', 'target' => 'blank', 'data-pjax' => 0]) : null);
                        }
                    ],
                    [
                        'attribute' => 'report_status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->reportStatusLabel;
                        }
                    ],
                ],
            ]) ?>
                
        </div>
    </div>

    

    <div class="card">
        <div class="card-body">

            <!-- <h5 class="card-title">MBTC Assessment Chart</h5> -->
            
            <div>
        
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-12 text-center">                       
                        <?php                     

                            $total_score = $score['analytical'] + $score['experimental'] + $score['practical'] + $score['relational'];
                            $cerebral = ($score['analytical'] + $score['relational']); 
                            $limbic = ($score['experimental'] + $score['practical']); 
                            $left_mode = ($score['analytical'] + $score['experimental']); 
                            $right_mode = ($score['practical'] + $score['relational']); 

                            echo Highcharts::widget([            
                                'id' => 'renderTo',
                                'scripts' => [
                                    'highcharts-more',
                                    'modules/no-data-to-display',
                                    'modules/accessibility',
                                    'modules/exporting',
                                    'modules/export-data',
                                    'modules/offline-exporting',
                                    'modules/parallel-coordinates'
                                    // 'themes/grid-light',
                                ],
                                'options' => [
                                    'chart'  => [
                                        'polar' => true,                            
                                        // 'marginBottom' => 5,
                                        'height' => 480,
                                        'margin' => 0,
                                        'padding' => 0,
                                        'events' => [
                                            'load' => new JsExpression('function(){
                                                var chart = this,
                                                parts = this.xAxis[0].max
                                                customColors = [                                        
                                                    "#ffc107", // YELLOW
                                                    "#ff5722", // RED
                                                    "#009688", // GREEN
                                                    "#007bff", // BLUE                                        
                                                ],
                                                series = chart.series,
                                                renderer = chart.renderer,
                                                cellLeft = tableLeft;

                                                // console.log("parts",parts);

                                                var colorsLength = customColors.length - 1;

                                                var count = 0;

                                                for(var i = 0; i < parts; i++) {
                                                    //setting count back to 0 if colours exceed 20.
                                                    if(count > colorsLength) {
                                                        count = 0;
                                                    }
                                                    var centerX = chart.plotLeft + chart.yAxis[0].center[0];
                                                    var centerY = chart.plotTop + chart.yAxis[0].center[1];
                                                    var outerRradius = chart.yAxis[0].height;
                                                    var innerRadius = 0;
                                                    var start = -Math.PI/2 + (Math.PI/(parts/2) * i);
                                                    var end = -Math.PI/2 + (Math.PI/(parts/2) * (i+1));
                                                    var color = new Highcharts.Color(customColors[count]).setOpacity(0.7).get();
                                                    count++;

                                                    chart.renderer.arc(centerX,centerY,outerRradius,innerRadius,start,end).attr({
                                                        id: "renderedArc",
                                                        fill: color,
                                                        "stroke-width": 1
                                                    }).add();
                                                };

                                                // user options
                                                var tableTop = 10,
                                                    colWidth = 100,
                                                    tableLeft = 20,
                                                    rowHeight = 20,
                                                    cellPadding = 2.5,
                                                    valueDecimals = 1,
                                                    valueSuffix = " °C";                                                
                                            }')
                                        ],
                                    ],
                                    
                                    'title' => [
                                        'text' => '',
                                        'x' => 0,
                                        'enabled' => false,
                                    ],

                                    'plotOptions' => [
                                        'line' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'allowOverlap' => true,
                                                'inside' => false,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '15px',
                                                    'fontWeight' => 'bold',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'enableMouseTracking' => false
                                        ],
                                        // 'series' => [                        
                                        //     'enableMouseTracking' => false,                        
                                        //     'shadow' => false, 
                                        //     'animation' => false,    
                                        //     'color' => '#1a7da1',      
                                        //     'lineWidth' => 3,
                                        //     // 'marker' => false,
                                        //     'marker' => [
                                        //         "radius" => 4,
                                        //         "symbol" => "circle"
                                        //     ],
                                        //     '_colorIndex' => 0,
                                        //     '_symbolIndex' => 0
                                        // ],
                                        'series' => [                        
                                            'enableMouseTracking' => false,                        
                                            'shadow' => false, 
                                            'animation' => false,                        
                                            'lineWidth' => 3,
                                            'marker' => [
                                                "radius" => 6,
                                                "symbol" => "circle"
                                            ],
                                            '_colorIndex' => 0,
                                            '_symbolIndex' => 0
                                        ],
                                        'column' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'allowOverlap' => true,
                                                'inside' => true,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '16px',
                                                    'fontWeight' => 'normal',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'grouping' => false,
                                            'shadow'  => false,
                                            'borderWidth' => 0
                                        ]
                                    ],   

                                    'pane' => [
                                        'startAngle' => 45,
                                        'endAngle' => 405,
                                        // 'size' => 330,
                                    ],

                                    'xAxis' => [                    
                                        // 'categories' => $score_categories,
                                        'tickmarkPlacement' => 'on',
                                        'gridLineColor' => '#d1d3d4',
                                        'lineWidth' => 0,
                                        'gridLineColor' => "#d1d3d4",
                                        'tickInterval' => 0.5,
                                        'labels' => [
                                            'rotation' => 0,                                            
                                            // 'useHTML' =>  true,
                                            'overflow' => 'justified',  
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '14px',
                                                'fontWeight' => '500',
                                                'textOverflow' => 'none',
                                                'color' => '#414042',
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false,
                                                // 'paddingLeft' => '10px',
                                                // 'paddingRight' => '10px',        
                                            ],
                                            'formatter' => new JsExpression('function(){
                                                let label;
                                                switch (this.value) {                                                    
                                                    case 0:
                                                        label = "D. FUTURE = ' . $score['relational'] . '";
                                                        break;
                                                    case 0.5:
                                                        label = "<span style=\"font-size:1rem;\">Right Mode (C + D) = </span><span style=\"font-size:1rem;font-weight: 700; color: #00bcd4;\">' . $right_mode . '</span>";
                                                        break;
                                                    case 1:
                                                        label = "C. FEELING = ' . $score['practical'] . '";
                                                        break;
                                                    case 1.5:
                                                        label = "<span style=\"font-size:1rem;top:15px;\">Limbic Mode (B + C) = </span><span style=\"font-size:1rem;font-weight: 700; color: #00bcd4;\">' . $limbic . '";
                                                        break;
                                                    case 2:
                                                        label = "B. FORM = ' . $score['experimental'] . '";
                                                        break;
                                                    case 2.5:
                                                        label = "<span style=\"font-size:1rem;\">Left Mode (A + B) = </span><span style=\"font-size:1rem;font-weight: 700; color: #00bcd4;\">' . $left_mode .'";
                                                        break; 
                                                    case 3:
                                                        label = "A. FACT = ' . $score['analytical'] . '";
                                                        break;
                                                    case 3.5:
                                                        label = "<span style=\"font-size:1rem;\">Cerebral Mode (A + D) = </span><span style=\"font-size:1rem;font-weight: 700; color: #00bcd4;\">' . $cerebral . '";
                                                        break;                                                  
                                                }                                                
                                                return label;
                                            }')
                                        ],
                                        // 'labels' => [
                                        //     'rotation' => 0,
                                        //     'style' => [
                                        //         "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                        //         'fontSize' => '16px',
                                        //         'fontWeight' => 'normal',
                                        //         'color' => "#414042",
                                        //         'borderWidth' => 0,
                                        //         'textShadow' => false,
                                        //         'textOutline' => false  
                                        //     ]
                                        // ],
                                    ],

                                    'yAxis' => [
                                        // 'gridLineInterpolation' => 'polygon',
                                        "gridLineColor" => "#e5e7e9",
                                        "lineWidth" => 1,
                                        "tickmarkPlacement" => "between",
                                        "angle" => -45,
                                        // "tickPixelInterval" => 100,
                                        "tickPosition" => "center",
                                        'tickPositions' => [0, 40, 80, 120, 160],
                                        'min' => 0,
                                        // 'max' => 5,

                                        'color' => '#fdebea',
                                        'showFirstLabel' => true,
                                        'showLastLabel' => true,
                                        'gridLineWidth' => 1,
                                        'tickInterval' => 1,
                                        'endOfTick' => true,
                                        // 'minPadding' => 0,
                                        // 'maxPadding' => 0,
                                        'labels' => [
                                            'align' => 'center',
                                            'x' => 0,
                                            'y' => -4,
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '10px',
                                                'fontWeight' => 'normal',
                                                'color' => "#414042",
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false  
                                            ],
                                        ],                            
                                    ],
                                    'legend' => [
                                        'enabled' => false,
                                        'align' => 'center',
                                        'verticalAlign' => 'bottom',
                                        'layout' => 'horizontal',
                                        'itemMarginTop' => 5,
                                        'itemMarginBottom' => 10,
                                        'backgroundColor' => '#f1f2f2',
                                        'borderRadius' => 5,
                                        'padding' => 10,
                                        'itemDistance' => 40,
                                        'reversed' => true, 
                                        'itemStyle' => [
                                            "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12pt',
                                            'fontWeight' => 'normal',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],

                                    'series' => array_values($series),

                                    'lang' => [
                                        'noData' => 'No Data Available.'
                                    ],
                                    'credits' => ['enabled' => false],
                                    'exporting' => [
                                        'enabled' => false,
                                        'sourceWidth' => 960,
                                        'sourceHeight' => 540,
                                        'scale' => 2,
                                        'buttons' => [
                                            'contextButton' => [
                                                'menuItems' => ['downloadPNG', 'downloadJPEG'],
                                            ],
                                        ],
                                        'showTable' => false,
                                        // 'chartOptions' => [
                                        //     'title' => [
                                        //         'style' => [
                                        //             'fontSize' => '12px',
                                        //         ]
                                        //     ]
                                        // ]
                                    ],
                                    'navigation' => [
                                        'buttonOptions' => [
                                            'verticalAlign' => 'middle',
                                            // 'horizontalAlign' => 'bottom',
                                            'x' => -5,
                                            // 'y' => -30,
                                        ]
                                    ],
                                    'colors' => [
                                        "#90ed7d",
                                        "#f15a29",
                                        "#f7a35c",
                                        "#8085e9",
                                        "#f15c80",
                                        "#e4d354",
                                        "#2b908f",
                                        "#f45b5b",
                                        "#91e8e1"
                                    ],                
                                ]
                            ]);                             
                        ?>
                    </div>                
                </div>
            </div>

        </div>
        </div>
    </div>

        


        <!-- <div class="col-lg-12 mt-3 mb-3 d-flex justify-content-end align-items-center">
            <?= Html::a('Download', ['download-assessment-result', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div> -->

    
</div>

<?php 
$this->registerCss('
    .lead {
        font-size: 1.05rem;
        font-weight: 500;
    }
');
?>