<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\HbdiAssessment */

$this->title = Yii::t('app', 'Create MBTC Assessment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="hbdi-assessment-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
