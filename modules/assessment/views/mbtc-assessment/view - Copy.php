<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\HbdiAssessment */

$this->title = 'Detail MBTC Assessment Result';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment Result'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
\yii\web\YiiAsset::register($this);

?>
<div class="hbdi-assessment-view">

    <div class="card">

        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-light']) ?>    

                    <?php if ($model->filename == null && !is_file(Yii::getAlias('@webroot/files/') . $model->filename)): ?>
                    <?= Html::a('Generate Report', ['generate-report', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
                    <?php else: ?>
                    <?= Html::a('Regenerate Report', ['generate-report', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
                    <?php endif; ?>
                
                    <div class="btn-group" role="group" aria-label="...">
                        <?php if (Helper::checkRoute('update')): ?>
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?php endif; ?>
                        <?php if (Helper::checkRoute('delete')): ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                    </div> 
                   
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'model' => $model,
                'options' => ['tag' => 'div', 'class' => 'row'],
                'template' => '<div class="col-md-4 mb-3"><h5>{label}</h5>{value}</div>',
                'attributes' => [
                    // 'id',

                    [
                        'attribute' => 'mbtc_assessment_group_id',
                        'value' => function ($model) {
                            return ($model->mbtc_assessment_group_id ? $model->mbtcAssessmentGroup->group_name : '-');
                        },
                    ],

                    'name',
                    [
                        'attribute' => 'email',                                
                        'format' => 'email',                                
                        'value' => function ($model) {
                            return ($model->email ? $model->email : '-');
                        }
                    ],

                    [
                        'attribute' => 'phone',                                
                        'value' => function ($model) {
                            return ($model->phone ? $model->phone : '-');
                        }
                    ],

                    [
                        'attribute' => 'date',
                        'format' => ['datetime', 'medium'],
                    ],

                    [
                        'attribute' => 'position',
                    ],

                    [
                        'attribute' => 'organization',
                    ],
                    [
                        'attribute' => 'location',
                    ],

                    [
                        'attribute' => 'filename',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->filename != null && is_file(Yii::getAlias('@webroot/files/') . $model->filename) ? Html::a(Yii::t('app', 'Download Report'), ['download-report', 'file' => $model->filename], ['class' => 'btn btn-primary btn-sm text-white', 'target' => 'blank', 'data-pjax' => 0]) : null);
                        }
                    ],
                ],
            ]) ?>
                
        </div>
    </div>

    

    <div class="card">
        <div class="card-body">

            <!-- <h5 class="card-title">MBTC Assessment Chart</h5> -->

            <div>
    
            <?php 

            $total_score = $score['analytical'] + $score['experimental'] + $score['practical'] + $score['relational'];

            $cerebral = ($score['analytical'] + $score['relational']); 
            $limbic = ($score['experimental'] + $score['practical']); 
            $left_mode = ($score['analytical'] + $score['experimental']); 
            $right_mode = ($score['practical'] + $score['relational']); 


            // var_dump($cerebral); 
            // var_dump($limbic); 
            // var_dump($left_mode); 
            // var_dump($right_mode); 

            // var_dump($limbic + $cerebral); 
            // var_dump($left_mode + $right_mode); 
            ?>

            <div class="d-flex justify-content-center align-items-center">
                <div class="lead mb-0 text-center">Cerebral Mode (A + D)<br><span style="font-size:1.35rem;font-weight: 500; color: #00bcd4;"><?= Yii::$app->formatter->asDecimal($cerebral, 0); ?></span></div>
            </div>

            <div class="row d-flex justify-content-center align-items-center">
                <div class="col lead mb-0 text-sm-right text-center mt-2 mt-sm-0">Left Mode (A + B)<br><span style="font-size:1.35rem;font-weight: 500; color: #00bcd4;"><?= Yii::$app->formatter->asDecimal($left_mode, 0); ?></span></div>                
                <div class="col-7 text-center">                       
                <?php 
                echo Highcharts::widget([            
                    'id' => 'renderTo',
                    'scripts' => [
                        'highcharts-more',
                        'modules/no-data-to-display',
                        'modules/accessibility',
                        'modules/exporting',
                        'modules/offline-exporting',
                        'modules/parallel-coordinates'
                        // 'themes/grid-light',
                    ],
                    'options' => [
                        'chart'  => [
                            'polar' => true,
                            'type' => 'line',
                            'marginBottom' => 0,
                            // 'height' => 450,
                            // 'marginLeft' => 0,
                            // 'marginRight' => 0,
                            // 'spacingLeft' => 0,
                            // 'spacingRight' => 0,
                            // 'width' => 1000, 
                            'events' => [
                                'load' => new JsExpression('function(){
                                    var chart = this,
                                    parts = this.xAxis[0].max
                                    customColors = [

                                        
                                        "#ffc107", // YELLOW
                                        "#ff5722", // RED

                                        "#009688", // GREEN
                                        "#007bff", // BLUE

                                        
                                        // "#24CBE5",
                                    ];

                                    // console.log("parts",parts);

                                    var colorsLength = customColors.length - 1;

                                    var count = 0;

                                    for(var i = 0; i < parts; i++) {
                                        //setting count back to 0 if colours exceed 20.
                                        if(count > colorsLength) {
                                            count = 0;
                                        }

                                        var centerX = chart.plotLeft + chart.yAxis[0].center[0];
                                        var centerY = chart.plotTop + chart.yAxis[0].center[1];
                                        var outerRradius = chart.yAxis[0].height;
                                        var innerRadius = 0;
                                        var start = -Math.PI/2 + (Math.PI/(parts/2) * i);
                                        var end = -Math.PI/2 + (Math.PI/(parts/2) * (i+1));
                                        var color = new Highcharts.Color(customColors[count]).setOpacity(0.7).get();
                                        count++;

                                        chart.renderer.arc(centerX,centerY,outerRradius,innerRadius,start,end).attr({
                                            id: "renderedArc",
                                            fill: color,
                                            "stroke-width": 1
                                        }).add();
                                    };
                                }')
                            ],
                        ],
                        
                        'title' => [
                            'text' => '',
                            'x' => 0,
                            'enabled' => false,
                        ],

                        // 'pane' => [
                        //     'size' => '80%',
                        // ],

                        'plotOptions' => [
                            'line' => [
                                'dataLabels' => [
                                    'enabled' => true,
                                    'allowOverlap' => false,
                                    'inside' => false,
                                    'style' => [
                                        "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                        'fontSize' => '15px',
                                        'fontWeight' => 'bold',
                                        'color' => "#414042",
                                        'borderWidth' => 0,
                                        'textShadow' => false,
                                        'textOutline' => false  
                                    ]
                                ],
                                'enableMouseTracking' => false
                            ],
                            'series' => [                        
                                'enableMouseTracking' => false,                        
                                'shadow' => false, 
                                'animation' => false,                        
                                'lineWidth' => 3,
                                'marker' => [
                                    "radius" => 6,
                                    "symbol" => "circle"
                                ],
                                '_colorIndex' => 0,
                                '_symbolIndex' => 0
                            ],
                            'column' => [
                                'dataLabels' => [
                                    'enabled' => true,
                                    'allowOverlap' => true,
                                    'inside' => true,
                                    'style' => [
                                        "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                        'fontSize' => '16px',
                                        'fontWeight' => 'normal',
                                        'color' => "#414042",
                                        'borderWidth' => 0,
                                        'textShadow' => false,
                                        'textOutline' => false  
                                    ]
                                ],
                                'grouping' => false,
                                'shadow'  => false,
                                'borderWidth' => 0
                            ]
                        ],   

                        'xAxis' => [                    
                            'categories' => $score_categories,
                            'tickmarkPlacement' => 'on',
                            'gridLineColor' => '#d1d3d4',
                            'lineWidth' => 0,
                            'gridLineColor' => "#d1d3d4",
                            // 'minPadding' => 0,
                            // 'maxPadding' => 0,
                            // 'labels' => [
                            //     'rotation' => 0,
                            //     'style' => [
                            //         "fontFamily" => "liberation_sansregular, roboto, Arial, Helvetica, sans-serif",
                            //         'fontSize' => '12pt',
                            //         'fontWeight' => 'normal',
                            //         'color' => "#414042",
                            //         'borderWidth' => 0,
                            //         'textShadow' => false,
                            //         'textOutline' => false  
                            //     ]
                            // ],
                            'labels' => [
                                // 'useHTML' => true,    
                                // 'allowOverlap' => true,                    
                                // 'formatter' => new \yii\web\JsExpression('
                                //     function () {
                                //         var axis = this.axis,
                                //         index = axis.categories.indexOf(this.value);      
                                //         var data = ' . \yii\helpers\Json::HtmlEncode($series) . '
                                //         var self = data[0].data[index];
                                //         var others = data[1].data[index];
                                //         console.log(data[0].data[index]);    
                                //         var label = 
                                //             "<div style=\'text-align:center;margin-top:35px;\'>" 
                                //                 + this.value + 
                                //                 "<div style=\'display:block; margin-top:10px;\'>" +
                                //                     "<span style=\'margin-right: 5px; padding:5px 10px; border-radius: 5px; background: #90ed7d;\'>"
                                //                         + parseFloat(others).toFixed(2) +
                                //                     "</span>" +
                                //                     "<span style=\'padding:5px 10px; color: #ffffff; border-radius: 5px; background: #f15a29;\'>"                                                 
                                //                         + parseFloat(self).toFixed(2) +
                                //                     "</span>" +
                                //                 "</div>" +
                                  

                                //             "</div>";
                                //         return label; 
                                //     }
                                // '),
                                'rotation' => 0,
                                'style' => [
                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                    'fontSize' => '16px',
                                    'fontWeight' => 'normal',
                                    'color' => "#414042",
                                    'borderWidth' => 0,
                                    'textShadow' => false,
                                    'textOutline' => false  
                                ]
                            ],
                        ],

                        'yAxis' => [
                            // 'gridLineInterpolation' => 'polygon',
                            "gridLineColor" => "#e5e7e9",
                            "lineWidth" => 1,
                            "tickmarkPlacement" => "between",
                            // "tickPixelInterval" => 100,
                            "tickPosition" => "center",
                            'tickPositions' => [0, 40, 80, 120, 160],
                            'min' => 0,
                            // 'max' => 5,
                            'color' => '#fdebea',
                            'showFirstLabel' => true,
                            'showLastLabel' => true,
                            'gridLineWidth' => 1,
                            'tickInterval' => 1,
                            'endOfTick' => true,
                            // 'minPadding' => 0,
                            // 'maxPadding' => 0,
                            'labels' => [
                                'align' => 'center',
                                'x' => 0,
                                'y' => -4,
                                'style' => [
                                    "fontFamily" => "liberation_sansregular, roboto, Arial, Helvetica, sans-serif",
                                    'fontSize' => '12px',
                                    'fontWeight' => 'normal',
                                    'color' => "#414042",
                                    'borderWidth' => 0,
                                    'textShadow' => false,
                                    'textOutline' => false  
                                ],
                            ],
                            // 'tickAmount' => 5,
                            // 'plotBands' => [[
                            //     "from" => 0,
                            //     "to" => 40,
                            //     "color" => "#e8f3f9",
                            //     "outerRadius" => "105%",
                            //     "thickness" => "50%"
                            // ], [
                            //     "from" => 40,
                            //     "to" => 80,
                            //     "color" => "#d7e8f7",
                            //     "outerRadius" => "105%",
                            //     "thickness" => "50%"
                            // ], [
                            //     "from" => 80,
                            //     "to" => 120,
                            //     "color" => "#b2d6f1",
                            //     "outerRadius" => "105%",
                            //     "thickness" => "50%"
                            // ], [
                            //     "from" => 120,
                            //     "to" => 160,
                            //     "color" => "#63b7e6",
                            //     "outerRadius" => "105%",
                            //     "thickness" => "50%"
                            // ]],
                        ],
                        'legend' => [
                            'enabled' => false,
                            'align' => 'center',
                            'verticalAlign' => 'bottom',
                            'layout' => 'horizontal',
                            'itemMarginTop' => 5,
                            'itemMarginBottom' => 10,
                            'backgroundColor' => '#f1f2f2',
                            'borderRadius' => 5,
                            'padding' => 10,
                            'itemDistance' => 40,
                            'reversed' => true, 
                            'itemStyle' => [
                                "fontFamily" => "liberation_sansregular, roboto, Arial, Helvetica, sans-serif",
                                'fontSize' => '12pt',
                                'fontWeight' => 'normal',
                                'color' => "#414042",
                                'borderWidth' => 0,
                                'textShadow' => false,
                                'textOutline' => false  
                            ]
                        ],

                        'series' => $series,

                        // 'responsive' => [
                        //     'rules' => [[
                        //         'condition' => [
                        //             'maxWidth' => 600
                        //         ],
                        //         'chartOptions' => [
                        //             'legend' => [
                        //                 'align' => 'center',
                        //                 'verticalAlign' => 'bottom',
                        //                 'layout' => 'horizontal'
                        //             ],
                        //             'pane' => [
                        //                 'size' => '70%'
                        //             ]
                        //         ]
                        //     ]]
                        // ],

                        'lang' => [
                            'noData' => 'No Data Available.'
                        ],
                        'credits' => ['enabled' => false],
                        'exporting' => [
                            'enabled' => false
                        ],
                        'colors' => [
                            "#90ed7d",
                            "#f15a29",
                            "#f7a35c",
                            "#8085e9",
                            "#f15c80",
                            "#e4d354",
                            "#2b908f",
                            "#f45b5b",
                            "#91e8e1"
                        ],                
                    ]
                ]);                             
                ?>
                </div>
                <div class="col lead mb-0 text-sm-left text-center mb-2 mb-sm-0">Right Mode (C + D)<br><span style="font-size:1.35rem;font-weight: 500; color: #00bcd4;"><?= Yii::$app->formatter->asDecimal($right_mode, 0); ?></span></div>
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <div class="lead mb-0 text-center">Limbic Mode (B + C)<br><span style="font-size:1.35rem;font-weight: 500; color: #00bcd4;"><?= Yii::$app->formatter->asDecimal($limbic, 0); ?></span></div>
            </div>

        </div>
        </div>
    </div>

        


        <!-- <div class="col-lg-12 mt-3 mb-3 d-flex justify-content-end align-items-center">
            <?= Html::a('Download', ['download-assessment-result', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div> -->

    
</div>

<?php 
$this->registerCss('
    .lead {
        font-size: 1.05rem;
        font-weight: 500;
    }
');
?>