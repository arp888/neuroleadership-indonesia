<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\ArrayHelper;
use app\modules\assessment\models\MbtcAssessmentGroup;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\administrator\models\HbdiAssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'MBTC Assessment');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mbtc-assessment-index">
    <div class="card">
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'mbtc-assessment-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'vAlign' => 'middle'
                    ],

                    // 'id',

                    [
                        'attribute' => 'mbtc_assessment_group_id',
                        'filter' => ArrayHelper::map(MbtcAssessmentGroup::find()->all(), 'id', 'group_name'),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return ($model->mbtc_assessment_group_id ? $model->mbtcAssessmentGroup->group_name : '-');
                        },
                    ],

                    [
                        'attribute' => 'name',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        'vAlign' => 'middle',
                        'format' => 'raw', 
                        'value' => function ($model) {
                            return Html::a($model->name, ['view', 'id' => $model->id]); 
                        }
                    ],

                    [
                        'attribute' => 'organization',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        // 'mergeHeader' => true, 
                        'vAlign' => 'middle',
                    ],

                    [
                        'attribute' => 'email',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],
                        'mergeHeader' => true, 
                        'format' => 'email',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return ($model->email ? $model->email : '-');
                        }
                    ],

                    // [
                    //     'attribute' => 'phone',
                    //     'mergeHeader' => true, 
                    //     'vAlign' => 'middle',
                    //     'value' => function ($model) {
                    //         return ($model->phone ? $model->phone : '-');
                    //     }
                    // ],

                    [
                        'attribute' => 'date',
                        'mergeHeader' => true, 
                        'format' => ['datetime', 'medium'],
                    ],

                    // [
                    //     'attribute' => 'position',
                    //     'mergeHeader' => true, 
                    //     'vAlign' => 'middle',
                    // ],

                    
                    // [
                    //     'attribute' => 'location',
                    //     'mergeHeader' => true, 
                    //     'vAlign' => 'middle',
                    // ],

                    // [
                    //     'attribute' => 'filename',
                    //     'mergeHeader' => true,
                    //     'vAlign' => 'middle', 
                    //     'format' => 'raw',
                    //     'value' => function ($model) {
                    //         return ($model->filename != null && is_file(Yii::getAlias('@webroot/files/') . $model->filename) ? Html::a(Yii::t('app', 'Download'), ['download-report', 'file' => $model->filename], ['class' => 'btn btn-info btn-sm text-white', 'target' => 'blank', 'data-pjax' => 0]) : null);
                    //     }
                    // ],

                    // [
                    //     'attribute' => 'filename',
                    //     'mergeHeader' => true,
                    //     'vAlign' => 'middle', 
                    //     'format' => 'raw',
                    //     'value' => function ($model) {
                    //         return ($model->filename != null && is_file(Yii::getAlias('@webroot/files/') . $model->filename) ? Html::a(Yii::t('app', 'Download'), ['download-report', 'file' => $model->filename], ['class' => 'btn btn-info btn-sm text-white', 'target' => 'blank', 'data-pjax' => 0]) : Html::a(Yii::t('app', 'Generate'), ['generate-report', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm text-white']));
                    //     }
                    // ],

                    // [
                    //     'class' => 'kartik\grid\ActionColumn',
                    //     'template' => '<div class="btn-group">{view}{delete}</div>',
                    //     'vAlign' => 'middle',
                    //     'width' => '100px',
                    //     'viewOptions' => ['label' => '<i class="mdi mdi-eye"></i>', 'class' => 'action-icon'],
                    //     // 'viewOptions' => ['label' => '<i class="mdi mdi-square-edit-outline"></i>', 'class' => 'action-icon'],
                    //     'deleteOptions' => ['label' => '<i class="mdi mdi-delete"></i>', 'class' => 'action-icon', 'data-confirm' => "Anda yakin ingin menghapus item ini?"],
                    //     'buttons' => [
                    //         'generate' => function ($url, $model) {
                    //             if ($model->filename == null)
                    //             return Html::a('Generate', ['generate-report', 'id' => $model->id],
                    //             [
                    //                 'class' => 'btn btn-primary btn-sm text-white',
                    //             ]);
                    //         },                    
                    //     ],
                    // ],

                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],


                ],
            ]); ?>
        </div>
    </div>
</div>
