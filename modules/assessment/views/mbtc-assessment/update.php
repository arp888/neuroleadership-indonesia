<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\MbtcAssessment */

$this->title = Yii::t('app', 'Update MBTC Assessment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MBTC Assessment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mbtc-assessment-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
