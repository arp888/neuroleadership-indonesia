<?php

namespace app\modules\assessment\models;

use Yii;
use app\modules\assessment\models\MbtcAssessmentGroup;

/**
 * This is the model class for table "mbtc_assessment".
 *
 * @property int $id
 * @property int $mbtc_assessment_group_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $position
 * @property string $organization
 * @property string $work_unit
 * @property string $location
 * @property int $date
 * @property string $filename
 * @property int $is_completed
 * @property int $report_status 
 *
 * @property MbtcAssessmentDetail[] $MbtcAssessmentDetails
 */
class MbtcAssessment extends \yii\db\ActiveRecord
{    
	const REPORT_GENERATED = 10;
	const REPORT_QUEUED = 20;
	CONST REPORT_SENT = 30;
    CONST REPORT_FAILED_TO_SEND = 40;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mbtc_assessment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','position', 'organization', 'location'], 'required'],
            [['mbtc_assessment_group_id', 'date', 'is_completed', 'report_status'], 'integer'],

            [['report_status'], 'default', 'value' => 0],
            [['report_status'], 'in', 'range' => [0, self::REPORT_GENERATED, self::REPORT_QUEUED, self::REPORT_SENT, self::REPORT_FAILED_TO_SEND]],

            [['name', 'position', 'organization', 'work_unit'], 'string', 'max' => 150],
            ['email', 'email', 'message' => 'Format email salah.'],
            // ['email', 'validateEmail'],            
            [['phone'], 'string', 'max' => 50],
            // ['phone', 'validatePhone'],
            [['location', 'filename'], 'string', 'max' => 255],
        ];
    }

    // public function validateEmail($attribute, $params, $validator)
    // {

    // 	$group = MbtcAssessmentGroup::findOne($this->mbtc_assessment_group_id);
    	
    //     if ($group->email_required == 1 && $this->attribute == null) {
    //         $this->addError($attribute, 'Alamat email tidak boleh kosong.');
    //     }
    // }

    // public function validatePhone($attribute, $params, $validator)
    // {

    // 	$group = MbtcAssessmentGroup::findOne($this->mbtc_assessment_group_id);

    	
    //     if ($group->phone_required == 1 && $this->attribute == null) {
    //         $this->addError($attribute, 'No telepon/wa tidak boleh kosong.');
    //     }
    // }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mbtc_assessment_group_id' => Yii::t('app', 'Assessment Group'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'position' => Yii::t('app', 'Position'),
            'organization' => Yii::t('app', 'Organization'),
            'work_unit' => Yii::t('app', 'Work Unit'),
            'location' => Yii::t('app', 'Location'),
            'date' => Yii::t('app', 'Date'),
            'filename' => Yii::t('app', 'Report'),
            'report_status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcAssessmentDetails()
    {
        return $this->hasMany(MbtcAssessmentDetail::className(), ['mbtc_assessment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcAssessmentGroup()
    {
        return $this->hasOne(MbtcAssessmentGroup::className(), ['id' => 'mbtc_assessment_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getReportStatusLists()
    {
        return [
            self::REPORT_GENERATED => Yii::t('app', 'Report Generated'), 
            self::REPORT_QUEUED => Yii::t('app', 'Ready to Send'), 
            self::REPORT_SENT => Yii::t('app', 'Report Sent'), 
            self::REPORT_FAILED_TO_SEND => Yii::t('app', 'Failed to Send'), 
        ];
    }
    
    /**
     * @return string
     */
    public function getReportStatusLabel()
    {
        if (isset($this->report_status)) {
            switch ($this->report_status) {
                case self::REPORT_GENERATED:
                    $spanClass = 'warning';
                    break;

                case self::REPORT_QUEUED:
                    $spanClass = 'info';
                    break;

                case self::REPORT_SENT:
                    $spanClass = 'success';
                    break;

                case self::REPORT_FAILED_TO_SEND:
                    $spanClass = 'danger';
                    break;                
            }

            $val = self::getReportStatusLists();
            if ($this->report_status != 0) {
            	$label = '<span class="badge badge-' . $spanClass . '">' . strtoupper($val[$this->report_status]) . '</span>';
            } else {
            	$label = '-';
            }
            // $label = '<span class="btn btn-label-' . $spanClass . ' btn-sm">' . strtoupper($val[$this->status]) . '</span>';
            
            return $label; 
        }
        return false; 
    }

}
