<?php

namespace app\modules\assessment\models;

use Yii;

/**
 * This is the model class for table "mbtc_assessment_group".
 *
 * @property int $id
 * @property string $group_name
 * @property string $description
 * @property int $email_required
 * @property int $phone_required
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 */
class MbtcAssessmentGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mbtc_assessment_group';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
            ],                           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_name'], 'required'],
            [['description'], 'string'],
            [['email_required', 'phone_required', 'is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['group_name'], 'string', 'max' => 255],
            [['email_required', 'phone_required'], 'default', 'value' => 1],
            [['is_active'], 'default', 'value' => Yii::$app->appHelper::STATUS_ACTIVE],
            [['is_active'], 'in', 'range' => [Yii::$app->appHelper::STATUS_INACTIVE, Yii::$app->appHelper::STATUS_ACTIVE]],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_name' => Yii::t('app', 'Group Name'),
            'description' => Yii::t('app', 'Description'),
            'email_required' => Yii::t('app', 'Email Required?'),
            'phone_required' => Yii::t('app', 'Phone Required?'),
            'is_active' => Yii::t('app', 'Is Active?'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcAssessments()
    {
        return $this->hasMany(MbtcAssessment::className(), ['mbtc_assessment_group_id' => 'id'])->andWhere(['is_completed' => 1]);
    }
}
