<?php

namespace app\modules\assessment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\assessment\models\MbtcAssessment;

/**
 * MbtcAssessmentSearch represents the model behind the search form of `app\modules\assessment\models\MbtcAssessment`.
 */
class MbtcAssessmentSearch extends MbtcAssessment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'mbtc_assessment_group_id', 'date', 'is_completed', 'report_status'], 'integer'],
            [['name', 'phone', 'email', 'position', 'organization', 'location', 'work_unit', 'filename'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MbtcAssessment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mbtc_assessment_group_id' => $this->mbtc_assessment_group_id,
            'date' => $this->date,
            'is_completed' => $this->is_completed,
            'report_status' => $this->report_status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'organization', $this->organization])
            ->andFilterWhere(['like', 'work_unit', $this->work_unit])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'filename', $this->filename]);

        return $dataProvider;
    }
}
