<?php

namespace app\modules\assessment\models;

use Yii;

/**
 * This is the model class for table "mbtc_item".
 *
 * @property int $id
 * @property string $item
 * @property string $item_eng
 * @property int $type
 */
class MbtcItem extends \yii\db\ActiveRecord
{
	const TYPE_ANALYTICAL = 10; 
	const TYPE_EXPERIMENTAL = 20; 
	const TYPE_PRACTICAL = 30; 
	const TYPE_RELATIONAL = 40; 
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mbtc_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item', 'item_eng', 'type'], 'required'],
            [['type'], 'integer'],
            [['item', 'item_eng'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item' => Yii::t('app', 'Item (Indonesia)'),
            'item_eng' => Yii::t('app', 'Item (English)'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

     /**
     * @return array
     */
    public static function typeOptions()
    {
        return [
            self::TYPE_ANALYTICAL => 'Analytical', 
			self::TYPE_EXPERIMENTAL => 'Experimental', 
			self::TYPE_PRACTICAL => 'Practical', 
			self::TYPE_RELATIONAL => 'Relational', 
        ];
    }

    /**
     * @return string
     */
    public function typeLabel()
    {
        if (isset($this->type)) {
            switch ($this->type) {
                case self::TYPE_ANALYTICAL:
                    $spanClass = 'info';
                    break;
                case self::TYPE_EXPERIMENTAL:
                    $spanClass = 'success';
                    break; 
                case self::TYPE_PRACTICAL:
                    $spanClass = 'warning';
                    break;
                case self::TYPE_RELATIONAL:
                    $spanClass = 'danger';
                    break;                               
            }

            $val = self::typeOptions();
            $label = '<span class="badge badge-' . $spanClass . '">' . $val[$this->type] . '</span>';           
            return $label; 
        }
        return false; 
    }
}
