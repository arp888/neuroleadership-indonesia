<?php

namespace app\modules\assessment\models;

use Yii;

/**
 * This is the model class for table "mbtc_assessment_detail".
 *
 * @property int $id
 * @property int $mbtc_assessment_id
 * @property int $mbtc_item_id
 * @property double $score
 *
 * @property MbtcAssessment $mbtcAssessment
 * @property MbtcItem $MbtcItem
 */
class MbtcAssessmentDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mbtc_assessment_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mbtc_assessment_id', 'mbtc_item_id'], 'required'],
            [['mbtc_assessment_id', 'mbtc_item_id'], 'integer'],
            [['score'], 'number'],
            [['mbtc_assessment_id'], 'exist', 'skipOnError' => true, 'targetClass' => MbtcAssessment::className(), 'targetAttribute' => ['mbtc_assessment_id' => 'id']],
            [['mbtc_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MbtcItem::className(), 'targetAttribute' => ['mbtc_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mbtc_assessment_id' => Yii::t('app', 'Hbdi Assessment ID'),
            'mbtc_item_id' => Yii::t('app', 'Hbdi Item ID'),
            'score' => Yii::t('app', 'Score'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcAssessment()
    {
        return $this->hasOne(MbtcAssessment::className(), ['id' => 'mbtc_assessment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcItem()
    {
        return $this->hasOne(MbtcItem::className(), ['id' => 'mbtc_item_id']);
    }
}
