<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MintonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/bootstrap.min.css',
        // '//use.fontawesome.com/releases/v5.3.1/css/all.css',
        'themes/minton-5/css/bootstrap.css',
        'themes/minton-5/css/app.css',
        // 'css/app-modern.css',
        'themes/minton-5/css/icons.min.css',
        'css/site.css',
    ];
    public $js = [
        // 'js/jquery.min.js',
        // 'js/popper.min.js',
        // 'js/bootstrap.min.js',
        'themes/minton-5/js/vendor.js',
        'themes/minton-5/js/app.js',
        'js/yii.override.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'yii2mod\alert\AlertAsset',
    ];
}
