<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontEndAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Poppins:300,400,400i,700|Rubik:300,400,400i,700|Raleway:300,400,500,600,700|Playfair+Display:400,400i,700i',
        // 'css/bootstrap.css',
        '//use.fontawesome.com/releases/v5.3.1/css/all.css',
        'css/style.css',
        'css/swiper.css',
        'css/dark.css',
        'css/font-icons.css',
        'css/intro-fonts.css',
        'css/animate.css',
        'css/magnific-popup.css',
        'css/responsive.css',
        'css/site.css',
    ];
    public $js = [
        'js/plugins.js',
        'js/functions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii2mod\alert\AlertAsset',
    ];
}
