<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BackEndAsset extends AssetBundle
{
    public $basePath = '@webroot/admin-assets';
    public $baseUrl = '@web/admin-assets';
    public $css = [
        // 'css/bootstrap.min.css',
        '//use.fontawesome.com/releases/v5.3.1/css/all.css',
        'css/icons.css',
        'css/style.css',
        'css/glyphicons.css',
        'css/custom.css',
        'plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
        'plugins/switchery/switchery.min.css',
    ];
    public $js = [
        // 'js/jquery.min.js',
        // 'js/popper.min.js',
        // 'js/bootstrap.min.js',
        'plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
        'plugins/switchery/switchery.min.js',
        'js/waves.js',
        'js/jquery.slimscroll.js',
        'js/jquery.scrollTo.min.js',
        // 'pages/jquery.dashboard.js',
        'js/jquery.core.js',
        'js/jquery.app.js',
        'js/yii.override.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'yii2mod\alert\AlertAsset',
    ];
}
