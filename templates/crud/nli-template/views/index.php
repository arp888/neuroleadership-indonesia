<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
<?php if ($fks = $generator->hasForeignRelations()) {
    echo "use yii\helpers\ArrayHelper;\n";
    foreach ($fks as $fk) {
        echo "use " . $fk . ";\n";
    }
} ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
    <div class="card">
        <div class="card-body">
            <div class="d-flex align-items-between mb-3">
                <div class="ml-auto text-right">
                    <?= "<?php\n" ?>
                    <?= "    if (Helper::checkRoute('create')) {\n" ?>
                    <?= "        echo " ?>Html::a(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                    <?= "    }\n" ?>
                    <?= "?>" ?>
                </div>
            </div>
            <?= "<?php // echo " ?>$this->render('_search', ['model' => $searchModel]) ?>           
        <?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>    
        <?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
                'dataProvider' => $dataProvider,
                <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel," : null ?>        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => true,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Belum ada data',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination mb-0'],
                    // 'linkOptions' => ['class' => 'custom__pager-link custom__pager-link-number'],
                    // 'linkContainerOptions' => ['class' => ''],
                    'prevPageCssClass' => 'prev',
                    'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
                    'nextPageCssClass' => 'next',
                    'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
                    'maxButtonCount' => 5,
                    // // 'disableCurrentPageButton' => true,
                    // 'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'custom__pager-link custom__pager-link--first m-datatable__pager-link--disabled', 'href' => '#'],
                    'firstPageCssClass' => 'first',
                    'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                    'lastPageCssClass' => 'last',
                    'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

        <?php
        $count = 0;
        if (($tableSchema = $generator->getTableSchema()) === false) {
            foreach ($generator->getColumnNames() as $name) {
                if (++$count < 6) {
                    echo "            [\n";
                    echo "                'attribute' => '" . $name . "',\n";
                    echo "                'vAlign' => 'middle',\n";
                    echo "            ],\n";
                } else {
                    echo "            //'" . $name . "',\n";
                    echo "            // [\n";
                    echo "            //    'attribute' => '" . $name . "',\n";
                    echo "            //    'vAlign' => 'middle',\n";
                    echo "            // ],\n";
                }
            }
        } else {
            foreach ($tableSchema->columns as $column) {
                $format = $generator->generateColumnFormat($column);
                if (++$count < 15) {
                    if ($column->name === 'id' || $column->name === 'is_deleted' || $column->name === 'created_at' || $column->name === 'created_by') {
                        continue; 
                    }            
                    echo "            [\n";
                    echo "                'attribute' => '" . $column->name . "',\n";            

                    if ($format === 'text' && !in_array($column->name, ['is_active', 'active', 'is_deleted', 'created_at', 'created_by', 'updated_at', 'updated_by'])) {
                        echo "                'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Search....'],\n";
                        echo "                'vAlign' => 'middle',\n";
                    } 

                    if ($format === 'date') {
                        echo "                'mergeHeader' => true,\n";
                        echo "                'vAlign' => 'middle',\n";
                        echo "                'value' => function (\$model) {\n";
                        echo "                    return Yii::$app->formatter->asDate(\$model->" . $column->name . ", 'long');\n";
                        echo "                },\n";
                    }

                    if ($format === 'number') {        
                        echo "                'mergeHeader' => true,\n";
                        echo "                'vAlign' => 'middle',\n";
                        echo "                'value' => function (\$model) {\n";
                        echo "                    return Yii::\$app->formatter->asDecimal(\$model->" . $column->name . ", 0);\n";
                        echo "                },\n";
                    }

                    if ($column->type === 'FK' || StringHelper::endsWith($column->name, '_id')) {
                        $fk = $generator->getForeignKey($column->name);
                        $fk = array_values($fk);             
                        $modelRelation = Inflector::id2camel($fk[0], '_'); 
                        $relationClass = Inflector::classify(substr($column->name, 0, -3));
                        echo "                'filter' => ArrayHelper::map($modelRelation::find()->all(), '$fk[1]', '$fk[1]'),\n";
                        echo "                'filterType' => GridView::FILTER_SELECT2,\n";
                        echo "                'filterWidgetOptions' => [\n";
                        echo "                    'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All Data')],\n";
                        echo "                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,\n";
                        echo "                    'pluginOptions' => ['allowClear' => true],\n";
                        echo "                ],\n";
                        echo "                'vAlign' => 'middle',\n";
                        echo "                'value' => function (\$model) {\n";
                        echo "                    return \$model->" . Inflector::variablize($relationClass) . "->$fk[1];\n";
                        echo "                },\n";
                    }

                    if ($column->name === 'is_active' || $column->name === 'active') {
                        echo "                'filter' => Yii::\$app->appHelper::getStatusLists(),\n";
                        echo "                'filterType' => GridView::FILTER_SELECT2,\n";
                        echo "                'filterWidgetOptions' => [\n";
                        echo "                    'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All Data')],\n";
                        echo "                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,\n";
                        echo "                    'pluginOptions' => ['allowClear' => true],\n";
                        echo "                ],\n";
                        echo "                'format' => 'raw',\n";
                        echo "                'vAlign' => 'middle',\n";
                        echo "                'value' => function (\$model) {\n";
                        echo "                    return Yii::\$app->appHelper->getStatusLabel(\$model->" . $column->name . ");\n";
                        echo "                },\n";
                    }

                    if ($column->name === 'created_at' || $column->name === 'updated_at') {
                        echo "                'mergeHeader' => true,\n";
                        echo "                'value' => function (\$model) {\n";
                        echo "                    return (new \app\components\AppHelper)->getTimestampInformation(\$model->" . $column->name . ");\n";
                        echo "                },\n";
                    }

                    if ($column->name === 'created_by' || $column->name === 'updated_by') {
                        echo "                'mergeHeader' => true,\n";
                        echo "                'value' => function (\$model) {\n";
                        echo "                    return (new \app\components\AppHelper)->getBlameableInformation(\$model->" . $column->name . ");\n";
                        echo "                },\n";
                    }
                    // echo "                        'vAlign' => 'middle',\n";
                    echo "            ],\n";
                } else {
                    echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                }
            }
        }
        ?>
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a(<?= "'<i class=\"mdi mdi-eye\"></i>'" ?>, ['view', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a(<?= "'<i class=\"mdi mdi-square-edit-outline\"></i>'" ?>, ['update', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a(<?= "'<i class=\"mdi mdi-delete\"></i>'" ?>, ['delete', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => <?= $generator->generateString('Are you sure to delete this item?') ?>,
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        <?php else: ?>
            <?= "<?= " ?>ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
                    return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                },
            ]) ?>
        <?php endif; ?>
        <?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>     
        </div>
    </div>   
</div>
