<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
<?php if ($generator->hasColumnType('FK')) {
    echo "use kartik\select2\Select2;\n";
    echo "use yii\helpers\ArrayHelper;\n";
} ?>
<?php if ($generator->hasColumnType('date')) {
    echo "use kartik\datecontrol\DateControl;\n";
} ?>
<?php if ($generator->hasIsActiveColumn()) {
    echo "use kartik\switchinput\SwitchInput;\n";
} ?>
<?php if ($fks = $generator->hasForeignRelations()) {
    foreach ($fks as $fk) {
        echo "use " . $fk . ";\n";
    }
} ?>

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<?= "<?php " ?>$this->beginBlock('subheader') ?>
<div class="col-auto float-right ml-auto">          
    <?= "<?= " ?>Html::a(<?= "'<i class=\"la la-arrow-left\"></i> ' . " . $generator->generateString('Index') ?>, ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>              
</div>
<?= "<?php " ?> $this->endBlock() ?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-end mb-3">
                <?= "<?=" ?>Html::a(<?= "'<i class=\"fa fa-th-list\"></i> ' . " . $generator->generateString('Back') ?>, ReturnUrl::getUrl(['index']), ['class' => 'btn btn-info btn-custom w-md waves-effect waves-light']); ?>
            </div>
         
            <?= "<?php " ?>$form = ActiveForm::begin([
                'options' => ['class' => ''],  
                'layout' => 'horizontal',
                'fieldClass' => '\app\components\CustomField',
                'fieldConfig' => [
                    'options' => ['class' => 'form-group row'],
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    // 'labelOptions' => ['class' => 'col-form-label'],
                    'horizontalCssClasses' => [
                        'label' => 'col-md-3 col-form-label text-md-right text-left',
                        'offset' => 'col-md-3',
                        'wrapper' => 'col-md-7',
                        'error' => '',
                        'hint' => '',
                    ],
                ],        
            ]); ?>

            <?= '<?= ' ?> Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
                        
        <?php foreach ($generator->getColumnNames() as $attribute) {
            if (in_array($attribute, $generator->getDefaultSkippedColumns())) {
                continue;
            }
            if (in_array($attribute, $safeAttributes)) {
                echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
            }
        } ?>
            <hr>
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= "<?= " ?>Html::submitButton(($model->isNewRecord ? <?= $generator->generateString('Submit') ?> : <?= $generator->generateString('Save Changes') ?>), ['class' => 'btn btn-success']) ?>
                    <?= "<?= " ?>Html::a(<?= "'<i class=\"la la-remove\"></i> ' . " . $generator->generateString('Cancel') ?>, ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div>      

            <?= "<?php " ?>ActiveForm::end(); ?>
        </div>
    </div>    
</div>
