<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

echo "<?php\n";

?>

namespace <?= $generator->ns ?>;

use Yii;

/**
 * This is the model class for table "<?= $generator->generateTableName($tableName) ?>".
 *
<?php foreach ($properties as $property => $data): ?>
 * @property <?= "{$data['type']} \${$property}"  . ($data['comment'] ? ' ' . strtr($data['comment'], ["\n" => ' ']) : '') . "\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->baseClass, '\\') . "\n" ?>
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db'): ?>

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>
<?php 
$behavior = false; 
$timestamp = false;
$blameable = false; 
$is_deleted = false; 
$sort = false; 
foreach ($labels as $name => $label) {
    if (in_array($name, ['created_at', 'updated_at', 'created_by', 'updated_by', 'is_delete', 'is_deleted', 'sort'])) {
        $behavior = true;            
    }
    if ($name == 'created_at' || $name == 'updated_at') {
        $timestamp = true; 
    }

    if ($name == 'created_by' || $name == 'updated_by') {
        $blameable = true; 
    }
    if ($name == 'is_delete' || $name == 'is_deleted') {
        $is_deleted = true; 
    }
    if ($name == 'sort') {
        $sort = true; 
    }
}
?>    
<?php if ($behavior): ?>
    public function behaviors()
    {
        return [
<?php
if ($timestamp) {
    echo "            [\n";
    echo "                'class' => \yii\behaviors\TimestampBehavior::className(),\n";
    echo "            ],\n";
}

if ($blameable) {
    echo "            [\n";
    echo "                'class' => \yii\behaviors\BlameableBehavior::className(),\n";
    echo "            ],\n";
} 

if ($is_deleted == true) {
    echo "            'softDeleteBehavior' => [\n";
    echo "                'class' => \yii2tech\ar\softdelete\SoftDeleteBehavior::className(),\n";
    echo "                'softDeleteAttributeValues' => [\n";
    echo "                    'is_deleted' => true,\n";
    echo "                ],\n";
    echo "            ],\n";
} 

if ($sort == true) {
    echo "            [\n";
    echo "                'class' => \arogachev\sortable\behaviors\\numerical\ContinuousNumericalSortableBehavior::className(),\n";
    echo "                'sortAttribute' => 'sort',\n";
    echo "            ],\n";
} 
?>
        ];
    }
<?php endif; ?>

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [<?= empty($rules) ? '' : ("\n            " . implode(",\n            ", $rules) . ",\n        ") ?>];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
<?php foreach ($labels as $name => $label): ?>
            <?= "'$name' => " . $generator->generateString($label) . ",\n" ?>
<?php endforeach; ?>
        ];
    }
<?php foreach ($relations as $name => $relation): ?>

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }
<?php endforeach; ?>
<?php if ($queryClassName): ?>
<?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    echo "\n";
?>
    /**
     * {@inheritdoc}
     * @return <?= $queryClassFullName ?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
}
