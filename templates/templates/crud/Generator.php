<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\templates\templates\crud;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\helpers\StringHelper;

/**
 * Generates CRUD
 *
 * @property array $columnNames Model column names. This property is read-only.
 * @property string $controllerID The controller ID (without the module ID prefix). This property is
 * read-only.
 * @property array $searchAttributes Searchable attributes. This property is read-only.
 * @property boolean|\yii\db\TableSchema $tableSchema This property is read-only.
 * @property string $viewPath The controller view path. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\generators\crud\Generator
{
    public $modelClass;
    public $controllerClass;
    public $viewPath;
    public $baseControllerClass = 'yii\web\Controller';
    public $indexWidgetType = 'grid';
    public $searchModelClass = '';
    /**
     * @var boolean whether to wrap the `GridView` or `ListView` widget with the `yii\widgets\Pjax` widget
     * @since 2.0.5
     */
    public $enablePjax = false;

    // public $attributeConfigTemplates = [];
    // public $selectedColumns = [];


    // protected function prepareAttributeTemplates() {
    //   $columns = $this->getDetailWidgetColumns();
    //   foreach ($columns as $column) {
        
    //   }
    // }


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'CRUD Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator generates a controller and views that implement CRUD (Create, Read, Update, Delete)
            operations for the specified data model.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['controllerClass', 'modelClass', 'searchModelClass', 'baseControllerClass'], 'filter', 'filter' => 'trim'],
            [['modelClass', 'controllerClass', 'baseControllerClass', 'indexWidgetType'], 'required', 'message' => '{attribute} wajib diisi.'],
            [['searchModelClass'], 'compare', 'compareAttribute' => 'modelClass', 'operator' => '!==', 'message' => 'Search Model Class must not be equal to Model Class.'],
            [['modelClass', 'controllerClass', 'baseControllerClass', 'searchModelClass'], 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['modelClass'], 'validateClass', 'params' => ['extends' => BaseActiveRecord::className()]],
            [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            [['controllerClass'], 'match', 'pattern' => '/Controller$/', 'message' => 'Controller class name must be suffixed with "Controller".'],
            [['controllerClass'], 'match', 'pattern' => '/(^|\\\\)[A-Z][^\\\\]+Controller$/', 'message' => 'Controller class name must start with an uppercase letter.'],
            [['controllerClass', 'searchModelClass'], 'validateNewClass'],
            [['indexWidgetType'], 'in', 'range' => ['grid', 'list']],
            [['modelClass'], 'validateModelClass'],
            [['enableI18N', 'enablePjax'], 'boolean'],
            [['messageCategory'], 'validateMessageCategory', 'skipOnEmpty' => false],
            ['viewPath', 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'modelClass' => 'Model Class',
            'controllerClass' => 'Controller Class',
            'viewPath' => 'View Path',
            'baseControllerClass' => 'Base Controller Class',
            'indexWidgetType' => 'Widget Used in Index Page',
            'searchModelClass' => 'Search Model Class',
            'enablePjax' => 'Enable Pjax',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'modelClass' => 'This is the ActiveRecord class associated with the table that CRUD will be built upon.
                You should provide a fully qualified class name, e.g., <code>app\models\Post</code>.',
            'controllerClass' => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
            'viewPath' => 'Specify the directory for storing the view scripts for the controller. You may use path alias here, e.g.,
                <code>/var/www/basic/controllers/views/post</code>, <code>@app/views/post</code>. If not set, it will default
                to <code>@app/views/ControllerID</code>',
            'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',
            'indexWidgetType' => 'This is the widget type to be used in the index page to display list of the models.
                You may choose either <code>GridView</code> or <code>ListView</code>',
            'searchModelClass' => 'This is the name of the search model class to be generated. You should provide a fully
                qualified namespaced class name, e.g., <code>app\models\PostSearch</code>.',
            'enablePjax' => 'This indicates whether the generator should wrap the <code>GridView</code> or <code>ListView</code>
                widget on the index page with <code>yii\widgets\Pjax</code> widget. Set this to <code>true</code> if you want to get
                sorting, filtering and pagination without page refreshing.',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['controller.php'];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass', 'indexWidgetType']);
    }

    /**
     * Checks if model class is valid
     */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();
        if (empty($pk)) {
            $this->addError('modelClass', "The table associated with $class must have primary key(s).");
        }
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');

        $files = [
            new CodeFile($controllerFile, $this->render('controller.php')),
        ];

        if (!empty($this->searchModelClass)) {
            $searchModel = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->searchModelClass, '\\') . '.php'));
            $files[] = new CodeFile($searchModel, $this->render('search.php'));
        }

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views';
        foreach (scandir($templatePath) as $file) {
            if (empty($this->searchModelClass) && $file === '_search.php') {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file"));
            }
        }

        return $files;
    }

    /**
     * @return string the controller ID (without the module ID prefix)
     */
    public function getControllerID()
    {
        $pos = strrpos($this->controllerClass, '\\');
        $class = substr(substr($this->controllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /**
     * @return string the controller view path
     */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@app/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }

            if (StringHelper::endsWith($name, '_name')) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();

        return $pk[0];
    }

    // public function getDetailWidgetColumns() {
    //   $columns = array_diff($this->getColumnNames(), $this->getDefaultSkippedColumns());
    //   sort($columns);
    //   return $columns;
    // }

    // public function getSelectedColumnNames() {
    //   return [];
    // }

    // public function isColumnChecked($columnName) {
    //   if (isset($this->selectedColumns[$columnName])) {
    //     return $this->selectedColumns[$columnName];
    //   }
    //   return !in_array($columnName, $this->getDefaultSkippedColumns());
    // }

    // /** @param \yii\db\ColumnSchema $column */
    // public function generateColumnTemplate($column) {

    //   if (StringHelper::endsWith($column->name, '_id')) {
    //     $label = Inflector::titleize($column->name);
    //     $getter = 'get'. Inflector::classify($label);

    //     return "["
    //         .PHP_EOL."  'attribute' => '{$column->name}',"
    //         .PHP_EOL."  'value' => function(\$model) use(\$searchModel) {"
    //         .PHP_EOL."    \$searchModel->{$column->name} = \$model->{$column->name};"
    //         .PHP_EOL."    return \$searchModel->$getter();"
    //         .PHP_EOL."  },"
    //         .PHP_EOL."]";
    //   }

    //   if ($column->name === 'is_active') {
    //     return "["
    //         .PHP_EOL."  'attribute' => 'is_active',"
    //         .PHP_EOL."  'label' => 'Status',"
    //         .PHP_EOL."  'filter' => ['Inactive','Active'],"
    //         .PHP_EOL."  'format' => 'html',"
    //         .PHP_EOL."  'value' => function(\$model) {"
    //         .PHP_EOL."    \$status = [['danger','Inactive'], ['success','Active']][\$model->is_active];"
    //         .PHP_EOL."    return \"<span class='label label-{\$status[0]}'>{\$status[1]}</span>\";"
    //         .PHP_EOL."  },"
    //         .PHP_EOL."]";
    //   }

    //   if (in_array($column->type, ['date','time','datetime'])) {
    //     return "'{$column->name}:{$column->type}'";
    //   }

    //   $format = $this->generateColumnFormat($column);
    //   return "'{$column->name}:$format'";
    // }

    /**
     * Generates code for active field
     * @param string $attribute
     * @return string
     */
    // public function generateActiveField($attribute)
    // {
    //     $tableSchema = $this->getTableSchema();
    //     if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
    //         if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
    //             return "\$form->field(\$model, '$attribute')->passwordInput()";
    //         } else {
    //             return "\$form->field(\$model, '$attribute')";
    //         }
    //     }
    //     $column = $tableSchema->columns[$attribute];
    //     if ($column->phpType === 'boolean') {
    //         return "\$form->field(\$model, '$attribute')->checkbox()";
    //     } elseif ($column->type === 'text') {
    //         return "\$form->field(\$model, '$attribute')->textarea(['rows' => 6])";
    //     } else {
    //         if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
    //             $input = 'passwordInput';
    //         } else {
    //             $input = 'textInput';
    //         }
    //         if (is_array($column->enumValues) && count($column->enumValues) > 0) {
    //             $dropDownOptions = [];
    //             foreach ($column->enumValues as $enumValue) {
    //                 $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
    //             }
    //             return "\$form->field(\$model, '$attribute')->dropDownList("
    //                 . preg_replace("/\n\s*/", ' ', VarDumper::export($dropDownOptions)).", ['prompt' => ''])";
    //         } elseif ($column->phpType !== 'string' || $column->size === null) {
    //             return "\$form->field(\$model, '$attribute')->$input()";
    //         } else {
    //             return "\$form->field(\$model, '$attribute')->$input(['maxlength' => true])";
    //         }
    //     }
    // }

    public function getForeignKey($fk) 
    {
        $tableSchema = $this->getTableSchema();
        $foreignKeys = $tableSchema->foreignKeys; 
        foreach ($foreignKeys as $key => $value) {
            if (array_key_exists($fk, $value)) {
                return $value;
            }
        }
    }

    public function generateActiveField($attribute) {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput(['value'=>''])";
            }
            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean' OR StringHelper::startsWith($attribute, 'is_')) {
            return "\$form->field(\$model, '$attribute')->widget(SwitchInput::classname(), ["
              .PHP_EOL. "                'options' => ['uncheck' => Yii::\$app->appHelper::STATUS_INACTIVE, 'value' => Yii::\$app->appHelper::STATUS_ACTIVE],"
              .PHP_EOL. "                'inlineLabel' => true,"
              .PHP_EOL. "                'pluginOptions' => ["
              .PHP_EOL. "                   'onColor' => 'success',"
              .PHP_EOL. "                   'onText' => Yii::t('app', 'Active'),"
              .PHP_EOL. "                   'offText' => Yii::t('app', 'Inactive'),"
              .PHP_EOL. "                ],"
              .PHP_EOL. "             ])->hint(false)";
        }
        if ($column->type === 'double' || $column->type === 'float') {
            return "\$form->field(\$model, '$attribute')->widget(\yii\widgets\MaskedInput::className(), ["
              .PHP_EOL. "                'options' => ['class' =>'form-control', 'placeholder' => \$model->getAttributeLabel('$attribute')],"
              .PHP_EOL. "                'clientOptions' => ["
              .PHP_EOL. "                    'alias' => 'decimal',"
              .PHP_EOL. "                    'groupSeparator' => '.',"
              .PHP_EOL. "                    'autoGroup' => true,"
              .PHP_EOL. "                    'allowMinus' => false,"
              .PHP_EOL. "                    'radixPoint'=> ',',"
              .PHP_EOL. "                    'removeMaskOnSubmit' => true,"
              .PHP_EOL. "                    'rightAlign' => false,"
              .PHP_EOL. "                    'unmaskAsNumber' => true"
              .PHP_EOL. "                ],"
              .PHP_EOL. "            ])->hint(Yii::t('app', 'Enter') . ' ' . \$model->getAttributeLabel('$attribute'))";
        }

        if ($column->type === 'date') {
            $contr = $this->getControllerID();
            $contr_id = str_replace('-', '', $contr); 
            return "\$form->field(\$model, '$attribute', ["
              .PHP_EOL. "                'selectors' => ['input' => '#$contr_id-$attribute-disp']"
              .PHP_EOL. "            ])->widget(DateControl::classname(), ["
              .PHP_EOL. "                'type' => DateControl::FORMAT_DATE,"
              .PHP_EOL. "                'options' => ['id' => '$contr_id-$attribute'],"
              .PHP_EOL. "                'widgetOptions' => ["
              .PHP_EOL. "                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],"
              .PHP_EOL. "                    'pluginOptions' => ["
              .PHP_EOL. "                        'autoclose' => true,"
              .PHP_EOL. "                        'todayHighlight' => true,"
              .PHP_EOL. "                    ]"
              .PHP_EOL. "                ],"
              .PHP_EOL. "            ])->hint(Yii::t('app', 'Select') . ' ' . \$model->getAttributeLabel('$attribute'))";
        }
        if ($column->type === 'FK' || StringHelper::endsWith($attribute, '_uuid') || StringHelper::endsWith($attribute, '_id') || StringHelper::startsWith($attribute, 'id_')) {
            $fk = $this->getForeignKey($attribute);
            // echo '<pre>'; 
            // var_dump($fk); die(); 
            if ($fk != null) {
                $fk = array_values($fk);             
                $modelRelation = Inflector::id2camel($fk[0], '_'); 

                return "\$form->field(\$model, '$attribute')->widget(Select2::class, ["
                  .PHP_EOL. "                'data' => ArrayHelper::map($modelRelation::find()->all(), '$fk[1]', '$fk[0]_name'),"
                  .PHP_EOL. "                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'Select...')],"
                  .PHP_EOL. "                'theme' => Select2::THEME_DEFAULT,"
                  .PHP_EOL. "                'pluginOptions' => ["
                  .PHP_EOL. "                    'allowClear' => true,"
                  .PHP_EOL. "                ],"
                  .PHP_EOL. "            ])->hint(Yii::t('app', 'Select') . ' ' . \$model->getAttributeLabel('$attribute'))";
            }
        }

        if ($attribute === 'status') {
            return "\$form->field(\$model, '$attribute')->widget(SwitchInput::classname(), ["
              .PHP_EOL. "                'options' => ['uncheck' => Yii::\$app->params['statusInactive'], 'value' => Yii::\$app->params['statusActive']],"
              .PHP_EOL. "                'inlineLabel' => true,"
              .PHP_EOL. "                'containerOptions' => ['class' => 'form-group mb-0'],"
              .PHP_EOL. "                'pluginOptions' => ["
              .PHP_EOL. "                    'onColor' => 'success',"
              .PHP_EOL. "                    'onText' => Yii::t('app', 'Aktif'),"
              .PHP_EOL. "                    'offText' => Yii::t('app', 'Nonaktif'),"
              .PHP_EOL. "                ],"
              .PHP_EOL. "            ])->hint(Yii::t('app', 'Pilih' . ' ' . \$model->getAttributeLabel('$attribute')))";
        }

        if ($column->type === 'text') {
            return "\$form->field(\$model, '$attribute')->widget(\dosamigos\\tinymce\TinyMce::classname(), ["
              .PHP_EOL. "'options' => ['rows' => 3]," 
            .PHP_EOL. "// 'language' => 'id'," 
            .PHP_EOL. "'clientOptions' => [" 
                .PHP_EOL. "'branding' => false,"
                .PHP_EOL. "'menubar' => false,"
                .PHP_EOL. "'relative_urls' => false,"
                .PHP_EOL. "'remove_script_host' => false,"
                .PHP_EOL. "'convert_urls' => true,"
                .PHP_EOL. "'plugins' => ["
                    .PHP_EOL. "'advlist autolink lists link charmap print preview anchor',"
                    .PHP_EOL. "'searchreplace visualblocks code fullscreen',"
                    .PHP_EOL. "'insertdatetime media table contextmenu paste image'"
                .PHP_EOL. "],"  
                
                .PHP_EOL. "'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'"
            .PHP_EOL. "] ])->hint(Yii::t('app', 'Pilih' . ' ' . \$model->getAttributeLabel('$attribute')))";
        }
        return parent::generateActiveField($attribute);
    }

    public function hasColumn($attr) {
      return $tableSchema = $this->getTableSchema() and isset($tableSchema->columns[$attr]);
    }

    public function hasColumnType($type) {
        if ($tableSchema = $this->getTableSchema()) {
          foreach($tableSchema->columns as $columnName => $column) {
            if ($type === 'FK' && (StringHelper::endsWith($columnName, '_id') || StringHelper::startsWith($columnName, 'id_') || StringHelper::endsWith($columnName, '_uuid'))) {
              return true;
            }

            if ($column->type === $type) {
              return true;
            }
          }
        }
    }

    public function hasForeignRelations() {
        $fks = [];
        $ns = 'app\modules\coaching_implementation\models\\';

        if ($tableSchema = $this->getTableSchema()) {
            $foreignKeys = $tableSchema->foreignKeys; 
            foreach ($foreignKeys as $key => $value) {              
                $val = $ns . Inflector::id2camel($value[0], '_');
                $fks[] = $val;
            }
        }
        return $fks; 
    }

    public function hasIsActiveColumn()
    {
        if ($tableSchema = $this->getTableSchema()) {
          foreach($tableSchema->columns as $columnName => $column) {
            if ($column->name === 'is_active' || $column->name === 'active' ) {
              return true;
            }            
          }
        }
    }

    public function hasStatusColumn()
    {
        if ($tableSchema = $this->getTableSchema()) {
          foreach($tableSchema->columns as $columnName => $column) {
            if ($column->name === 'status') {
              return true;
            }            
          }
        }
    }

    public function hasUuidColumn()
    {
        if ($tableSchema = $this->getTableSchema()) {
          foreach($tableSchema->columns as $columnName => $column) {
            if ($column->name === 'uuid') {
              return true;
            }            
          }
        }
    }

    /**
     * Generates code for active search field
     * @param string $attribute
     * @return string
     */
    public function generateActiveSearchField($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false) {
            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->name === 'is_active') {
            return "\$form->field(\$model, '$attribute', ['options' => ['class' => 'form-group form-focus select-focus']])->widget(Select2::class, ["
              .PHP_EOL. "                'data' => [Yii::\$app->appHelper::STATUS_INACTIVE => Yii::t('app', 'Inactive'), Yii::\$app->appHelper::STATUS_ACTIVE => Yii::t('app', 'Active')],"
              .PHP_EOL. "                'options' => ['class' => 'form-control', 'multiple' => false, 'placeholder' => Yii::t('app', 'All Status')],"
              .PHP_EOL. "                'theme' => Select2::THEME_DEFAULT,"
              .PHP_EOL. "                'pluginOptions' => ["
              .PHP_EOL. "                    'allowClear' => true,"
              .PHP_EOL. "                ],"
              .PHP_EOL. "            ])->label(\$model->getAttributeLabel('$attribute'), ['class' => 'focus-label'])";
        } else {
            return "\$form->field(\$model, '$attribute')->textInput(['class' => 'form-control floating'])->label(\$model->getAttributeLabel('$attribute'), ['class' => 'focus-label'])";
        }
    }

    /**
     * Generates column format
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    public function generateColumnFormat($column)
    {
        if ($column->phpType === 'boolean') {
            return 'boolean';
        } elseif ($column->type === 'text') {
            return 'ntext';
        } elseif ($column->type === 'double' || $column->type === 'float' ) {
            return 'number';
        } elseif (stripos($column->name, 'time') !== false && $column->phpType === 'integer') {
            return 'datetime';
        } elseif (stripos($column->name, 'email') !== false) {
            return 'email';
        } elseif (stripos($column->name, 'url') !== false) {
            return 'url';
        } else {
            return 'text';
        }
    }

    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][] = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][] = $column->name;
                    break;
            }
        }

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }

        return $rules;
    }

    /**
     * @return array searchable attributes
     */
    public function getSearchAttributes()
    {
        return $this->getColumnNames();
    }

    /**
     * Generates the attribute labels for the search model.
     * @return array the generated attribute labels (name => label)
     */
    public function generateSearchLabels()
    {
        /* @var $model \yii\base\Model */
        $model = new $this->modelClass();
        $attributeLabels = $model->attributeLabels();
        $labels = [];
        foreach ($this->getColumnNames() as $name) {
            if (isset($attributeLabels[$name])) {
                $labels[$name] = $attributeLabels[$name];
            } else {
                if (!strcasecmp($name, 'id')) {
                    $labels[$name] = 'ID';
                } else {
                    $label = Inflector::camel2words($name);
                    if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                        $label = substr($label, 0, -3) . ' ID';
                    }
                    $labels[$name] = $label;
                }
            }
        }

        return $labels;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                default:
                    $likeConditions[] = "->andFilterWhere(['like', '{$column}', \$this->{$column}])";
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode("\n" . str_repeat(' ', 12), $hashConditions)
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode("\n" . str_repeat(' ', 12), $likeConditions) . ";\n";
        }

        return $conditions;
    }

    /**
     * Generates URL parameters
     * @return string
     */
    public function generateUrlParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                return "'id' => (string)\$model->{$pks[0]}";
            } else {
                return "'id' => \$model->{$pks[0]}";
            }
        } else {
            $params = [];
            foreach ($pks as $pk) {
                if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                    $params[] = "'$pk' => (string)\$model->$pk";
                } else {
                    $params[] = "'$pk' => \$model->$pk";
                }
            }

            return implode(', ', $params);
        }
    }

    /**
     * Generates action parameters
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks);
        }
    }

    /**
     * Generates parameter tags for phpdoc
     * @return array parameter tags for phpdoc
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * Returns table schema for current model class or false if it is not an active record
     * @return boolean|\yii\db\TableSchema
     */
    public function getTableSchema()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema();
        } else {
            return false;
        }
    }

    /**
     * @return array model column names
     */
    public function getColumnNames()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema()->getColumnNames();
        } else {
            /* @var $model \yii\base\Model */
            $model = new $class();

            return $model->attributes();
        }
    }

    public function getDefaultSkippedColumns() {
        return [
          'id',
          'is_delete',
          'is_deleted',
          'create_at',
          'update_at',
          'update_by',
          'create_by',
          'created_at',
          'updated_at',
          'created_by',
          'updated_by',    
          'dibuat_oleh', 
          'dibuat_tanggal', 
          'diperbarui_oleh', 
          'diperbarui_tanggal',
          'uuid',
          'sort',
        ];
    }
}
