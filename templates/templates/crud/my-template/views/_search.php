<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
<?php if ($generator->hasIsActiveColumn()) {
    echo "use kartik\select2\Select2;\n";
} ?>

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search">

    <?= "<?php " ?>$form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
<?php
$count = 0;
foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $generator->getDefaultSkippedColumns())) {
        continue;
    }

    if (++$count < 12) {
        echo "        <div class=\"col-sm-4 col-md-3\">\n";
        echo "            <?= " . $generator->generateActiveSearchField($attribute) . " ?>\n";
        echo "        </div>\n";
    } 
}
?>
        <div class="col-sm-4 col-md-3">
            <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Search') ?>, ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?= "<?php " ?>ActiveForm::end(); ?>

</div>

