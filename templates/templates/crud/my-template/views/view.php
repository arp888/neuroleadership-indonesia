<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Detail ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model-><?= $generator->getNameAttribute() ?>;
\yii\web\YiiAsset::register($this);
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= "<?= " ?>Html::a(<?= "'<i class=\"mdi mdi-arrow-left\"></i> ' . " . $generator->generateString('Index') ?>, ['index'], ['class' => 'btn btn-light']) ?>    

                    <?= "<?php if (Helper::checkRoute('create')): ?>\n" ?>
                    <?= "<?= " ?>Html::a(<?= $generator->generateString('Create') ?>, ['create', 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-primary ml-1'
                    ]) ?>
                    <?= "<?php endif; ?>\n" ?>          
            
                    <div class="btn-group" role="group" aria-label="...">
                        <?= "<?php if (Helper::checkRoute('update')): ?>\n" ?>
                        <?= "<?= " ?>Html::a(<?= $generator->generateString('Update') ?>, ['update', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        <?= "<?php endif; ?>\n" ?>
                        <?= "<?php if (Helper::checkRoute('delete')): ?>\n" ?>
                        <?= "<?= " ?>Html::a(<?= $generator->generateString('Delete') ?>, ['delete', <?= $urlParams ?>, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => <?= $generator->generateString('Are you sure to delete this item?') ?>,
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?= "<?php endif; ?>\n" ?>
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= "<?= " ?>DetailView::widget([
                'model' => $model,
                'options' => ['tag' => false],
                'template' => '<div class="mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
        <?php
        $record_history = false; 
        if (($tableSchema = $generator->getTableSchema()) === false) {
        foreach ($generator->getColumnNames() as $name) {
            echo "                    '" . $name . "',\n";
        }
        } else {
            foreach ($generator->getTableSchema()->columns as $column) {
                $format = $generator->generateColumnFormat($column);
                // if (in_array($column->name, ['id', 'is_deleted'])) {
                //     continue; 
                // }

                if (in_array($column->name, ['uuid', 'id', 'is_deleted', 'created_at', 'created_by', 'updated_at', 'updated_by', 'dibuat_oleh', 'dibuat_tanggal', 'diperbarui_oleh', 'diperbarui_tanggal'])) {
                    $record_history = true;
                    continue; 
                }

                if ($column->name === 'is_active' || $column->name === 'active') {
                    echo "                    [\n";
                    echo "                        'attribute' => '" . $column->name . "',\n";
                    echo "                        'format' => 'raw',\n";
                    echo "                        'value' => function (\$model) {\n";
                    echo "                            return Yii::\$app->appHelper->getStatusLabel(\$model->" . $column->name . ");\n";
                    echo "                        },\n";
                    echo "                    ],\n";
                } elseif ($column->name === 'status') {
                    echo "                    [\n";
                    echo "                        'attribute' => '" . $column->name . "',\n";
                    echo "                        'format' => 'raw',\n";
                    echo "                        'value' => function (\$model) {\n";
                    echo "                            return (\$model->status == Yii::\$app->params['statusActive'] ? '<span class=\"badge badge-soft-success p-1\">Aktif</span>' : '<span class=\"badge badge-soft-danger p-1\">Nonaktif</span>');\n";
                    echo "                        },\n";
                    echo "                    ],\n";
                } elseif ($column->type === 'FK' || StringHelper::endsWith($column->name, '_uuid') || StringHelper::endsWith($column->name, '_id') || StringHelper::startsWith($column->name, 'id_')) {
                    $fk = $generator->getForeignKey($column->name);
                    if ($fk != null) {
                    $fk = array_values($fk);             
                    $modelRelation = Inflector::id2camel($fk[0], '_'); 
                    $relationClass = Inflector::classify(substr($column->name, 0, -3));
                    echo "                    [\n";        
                    echo "                        'attribute' => '" . $column->name . "',\n";
                    echo "                        'value' => function (\$model) {\n";
                    echo "                            return \$model->" . Inflector::variablize($modelRelation) . "->$fk[0]_name;\n";
                    echo "                        },\n"; 
                    echo "                    ],\n";
                    }
                } elseif ($format === 'number') {        
                    echo "                    [\n";        
                    echo "                        'attribute' => '" . $column->name . "',\n";
                    echo "                        'value' => function (\$model) {\n";
                    echo "                            return Yii::\$app->formatter->asDecimal(\$model->" . $column->name . ", 0);\n";
                    echo "                        },\n";
                    echo "                    ],\n";
                } elseif ($format === 'date') {
                    echo "                    [\n";
                    echo "                        'attribute' => '" . $column->name . "',\n";
                    echo "                        'value' => function (\$model) {\n";
                    echo "                            return Yii::\$app->formatter->asDate(\$model->" . $column->name . ", 'long');\n";
                    echo "                        },\n";
                    echo "                    ],\n";
                } else {
                    echo "                    '" . $column->name . "',\n";
                }
            }
        }
        ?>
                ],
            ]) ?>
        </div>
    </div>
</div>        