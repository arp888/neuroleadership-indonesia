<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
<?php if ($fks = $generator->hasForeignRelations()) {
    echo "use yii\helpers\ArrayHelper;\n";
    foreach ($fks as $fk) {
        echo "use " . $fk . ";\n";
    }
} ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
    <?= "<?php // echo " ?>$this->render('_search', ['model' => $searchModel]) ?>           
<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>    
<?php if ($generator->indexWidgetType === 'grid'): ?>
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= "<?php\n" ?>
                    <?= "    if (Helper::checkRoute('create')) {\n" ?>
                    <?= "        echo " ?>Html::a(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']); 
                    <?= "    }\n" ?>
                    <?= "?>\n" ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= "<?= " ?>GridView::widget([
                'dataProvider' => $dataProvider,
                <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel," : null ?>        
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn'
                    ],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "                  [\n";
            echo "                      'attribute' => '" . $name . "',\n";
            echo "                      'vAlign' => 'middle',\n";
            echo "                  ],\n";
        } else {
            echo "                  //'" . $name . "',\n";
            echo "                  // [\n";
            echo "                  //    'attribute' => '" . $name . "',\n";
            echo "                  //    'vAlign' => 'middle',\n";
            echo "                  // ],\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 15) {
            if (in_array($column->name, ['uuid', 'id', 'is_deleted', 'created_at', 'created_by', 'updated_at', 'updated_by', 'dibuat_oleh', 'dibuat_tanggal', 'diperbarui_oleh', 'diperbarui_tanggal'])) {
            // if ($column->name === 'id' || $column->name === 'is_deleted' || $column->name === 'created_at' || $column->name === 'created_by') {
                continue; 
            }            
                        

            // if (in_array($column->name, ['is_active', 'active', 'is_deleted', 'created_at', 'created_by', 'updated_at', 'updated_by'])) {
            //     continue;
            // } 

            if ($format === 'date') {
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'mergeHeader' => true,\n";
                echo "                        'vAlign' => 'middle',\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return Yii::$app->formatter->asDate(\$model->" . $column->name . ", 'long');\n";
                echo "                        },\n";
                echo "                    ],\n";
            }

            elseif ($format === 'number') {        
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'mergeHeader' => true,\n";
                echo "                        'vAlign' => 'middle',\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return Yii::\$app->formatter->asDecimal(\$model->" . $column->name . ", 0);\n";
                echo "                        },\n";
                echo "                    ],\n";
            }

            elseif ($column->type === 'FK' || StringHelper::endsWith($column->name, '_uuid') || StringHelper::endsWith($column->name, '_id') || StringHelper::startsWith($column->name, 'id_')) {
                $fk = $generator->getForeignKey($column->name);
                if ($fk != null) {
                $fk = array_values($fk);             
                $modelRelation = Inflector::id2camel($fk[0], '_'); 
                $relationClass = Inflector::classify(substr($column->name, 0, -3));
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'filter' => ArrayHelper::map($modelRelation::find()->all(), '$fk[1]', 'nama_$fk[0]'),\n";
                echo "                        'filterType' => GridView::FILTER_SELECT2,\n";
                echo "                        'filterWidgetOptions' => [\n";
                echo "                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],\n";
                echo "                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,\n";
                echo "                            'pluginOptions' => ['allowClear' => true],\n";
                echo "                        ],\n";
                echo "                        'vAlign' => 'middle',\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return \$model->" . Inflector::variablize($modelRelation) . "->$fk[0]_name;\n";
                echo "                        },\n";
                echo "                    ],\n";
                }
            }

            elseif ($column->name === 'is_active' || $column->name === 'active') {
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'filter' => Yii::\$app->appHelper::getStatusLists(),\n";
                echo "                        'filterType' => GridView::FILTER_SELECT2,\n";
                echo "                        'filterWidgetOptions' => [\n";
                echo "                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],\n";
                echo "                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,\n";
                echo "                            'pluginOptions' => ['allowClear' => true],\n";
                echo "                        ],\n";
                echo "                        'format' => 'raw',\n";
                echo "                        'vAlign' => 'middle',\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return Yii::\$app->appHelper->getStatusLabel(\$model->" . $column->name . ");\n";
                echo "                        },\n";
                echo "                    ],\n";
            }

            elseif ($column->name === 'status') {
                echo "                    [\n"; 
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'filter' => [Yii::\$app->params['statusInactive'] => Yii::t('app', 'Nonaktif'), Yii::\$app->params['statusActive'] => Yii::t('app', 'Aktif')],\n";
                echo "                        'filterType' => GridView::FILTER_SELECT2,\n";
                echo "                        'filterWidgetOptions' => [\n";
                echo "                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],\n";
                echo "                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,\n";
                echo "                            'pluginOptions' => ['allowClear' => true],\n";
                echo "                        ],\n";
                echo "                        'format' => 'raw',\n";
                echo "                        'vAlign' => 'middle',\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return (\$model->status == Yii::\$app->params['statusActive'] ? '<span class=\"badge badge-soft-success p-1\">Aktif</span>' : '<span class=\"badge badge-soft-danger p-1\">Nonaktif</span>');\n";
                echo "                        },\n";
                echo "                    ],\n";
            }

            elseif ($column->name === 'created_at' || $column->name === 'updated_at') {
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'mergeHeader' => true,\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return (new \app\components\AppHelper)->getTimestampInformation(\$model->" . $column->name . ");\n";
                echo "                        },\n";
                echo "                    ],\n";
            }

            elseif ($column->name === 'created_by' || $column->name === 'updated_by') {
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'mergeHeader' => true,\n";
                echo "                        'value' => function (\$model) {\n";
                echo "                            return (new \app\components\AppHelper)->getBlameableInformation(\$model->" . $column->name . ");\n";
                echo "                        },\n";
                echo "                    ],\n";
            }
            else {
                echo "                    [\n";
                echo "                        'attribute' => '" . $column->name . "',\n";
                echo "                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],\n";
                echo "                        'vAlign' => 'middle',\n";
                echo "                    ],\n";
            }
        } else {
            echo "                    [\n";
            echo "                        'attribute' => '" . $column->name . "',\n";
            echo "                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],\n";
            echo "                        'vAlign' => 'middle',\n";
            echo "                    ],\n";
        }
    }
}
?>
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">' . Helper::filterActionColumn('{view}{update}{delete}') . '</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a(<?= "'<i class=\"fe-align-left\"></i>'" ?>, ['view', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a(<?= "'<i class=\"fe-edit\"></i>'" ?>, ['update', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a(<?= "'<i class=\"fe-trash-2\"></i>'" ?>, ['delete', <?= $urlParams ?>, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => <?= $generator->generateString('Are you sure to delete this item?') ?>,
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>        
</div>