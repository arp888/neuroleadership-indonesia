<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;


$this->title = 'Metaphorical Brain Thinking Cluster (MBTC)';
$this->params['breadcrumbs'][] = $this->title;
$this->params['image_url'] = Yii::getAlias('@web/images/header-img/3661950.jpg');
?>
<div class="hbdi-assessment">
    <div class="container clearfix">  
        <div class="form-widget">

            <div class="form-result"></div>

            <div class="row shadow bg-light border align-items-center">

                <div class="col-lg-4 dark" style="background: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,.1)); min-height: 400px;">
                    <!-- <h4 class="center mt-5">Assessment Pola Berpikir berbasis MBTC (Metaphorical Brain Thinking Cluster)</h4> -->
                    <div class="calories-wrap center mt-4 w-100 px-2">
                        <!-- <span class="uppercase mt-4 h5 mb-1 ls2">Asesmen Pola Berpikir berbasis MBTC (Metaphorical Brain Thinking Cluster)</span>
                        <p class="mt-3">Diadopsi dan dimodifikasi dari buku "The Whole Brain Business Book - Unlocking the Power of Brain Thinking in Organizations, Teams & Individuals" oleh Ned Hermann & Ann Hermann-Nehdi</p>
                        
                        <div class="mt-3 mb-5 ls1">Asesmen MBTC (Metaphorical Brain Thinking Clusters) ini dapat membantu Anda mengukur preferensi mental, menginterpretasi dan menerjemahkan preferensi mental ke dalam outcome perilaku yang dapat diprediksi di tempat kerja, serta meningkatkan dan mengembangkan kemampuan Anda menghasilkan insight dari preferensi Anda dan dampaknya pada dunia Anda. </div> -->
                        <?= Html::img('@web/images/mbtc-logo.png', ['height' => 70]) ?>
                        
                        <!-- <span class="uppercase mt-4 h5 mb-1 ls2">Asesmen Pola Berpikir berbasis MBTC (Metaphorical Brain Thinking Cluster)</span> -->
                        <!-- <p class="mt-3">Diadopsi dan dimodifikasi dari buku "The Whole Brain Business Book - Unlocking the Power of Brain Thinking in Organizations, Teams & Individuals" oleh Ned Hermann & Ann Hermann-Nehdi</p> -->
                        
                        <div class="mt-4 mb-2 ls1 text-dark">Asesmen MBTC (Metaphorical Brain Thinking Cluster) ini dapat membantu Anda mengukur preferensi mental, menginterpretasi dan menerjemahkan preferensi mental ke dalam outcome perilaku yang dapat diprediksi di tempat kerja, serta meningkatkan dan mengembangkan kemampuan Anda menghasilkan insight dari preferensi Anda dan dampaknya pada dunia Anda. </div>

                        <?= Html::img('@web/images/logo-nlii.png', ['height' => 80]) ?>
                    </div>
                </div>

                <div class="col-lg-8 p-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['class' => 'row mb-0'],  
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'options' => ['class' => 'form-group row'],
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            // 'labelOptions' => ['class' => 'col-form-label'],
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-3 col-form-label',
                                'offset' => 'col-sm-3',
                                'wrapper' => 'col-sm-9',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],        
                    ]); ?>                    
                        
                    <div class="col-12 ">
                        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Masukkan Nama Lengkap'])->label('Nama') ?>
                    </div>

                    <div class="col-12 ">
                        <?= $form->field($model, 'position')->textInput(['placeholder' => 'Masukkan Jabatan'])->label('Jabatan') ?>
                    </div>

                    <div class="col-12 ">
                        <?= $form->field($model, 'organization')->textInput(['placeholder' => 'Masukkan Nama Organisasi'])->label('Organisasi') ?>
                    </div>

                    <div class="col-12">
                        <?= $form->field($model, 'location')->textInput(['placeholder' => 'Masukkan Kota sebagai Lokasi'])->label('Lokasi') ?>
                    </div>          

                    <div class="col-12 mt-3 d-flex justify-content-end align-items-center">
                        <?= Html::submitButton('Lanjut', ['class' => 'btn btn-success']) ?>
                        
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>  
    </div>    
</div>

