<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Metaphorical Brain Thinking Cluster (MBTC)';
$this->params['breadcrumbs'][] = $this->title;
$this->params['image_url'] = Yii::getAlias('@web/images/header-img/3661950.jpg');

?>
<div class="hbdi-assessment">
    <div class="container clearfix">    

        <div class="row mb-3 bg-light border">
            <div class="col-lg-12">
                <div class="calories-wrap center mt-4 w-100 px-2">
                    <span class="uppercase mt-4 h5 mb-1 ls2">Pilihlah 8 buah pernyataan yang sedikit saja menggambarkan diri Anda.</p>
                </div>
            </div>
        </div>

        <div class="row bg-light border clearfix">
            <div class="col-lg-12 mt-3 mb-3">

     
                <?php $form = ActiveForm::begin([
                    'id' => 'step-form-three',
                    'options' => ['class' => 'nobottommargin'],
                ]); ?>
                    
                <div class="row">
                <?php echo Html::activeHiddenInput($model, 'name'); ?>
                
                <?php foreach ($details as $d => $detail): ?>
                    
                    <?php if ($d == 0 || $d == 8 || $d == 16 || $d == 24): ?>        
                    <div class="col-md-3 mb-0">
                        <!-- <div class="card h-100">
                            <div class="card-body"> -->
                    <?php endif; ?>                               
                        
                        <?php echo Html::activeHiddenInput($detail, "[{$d}]hbdi_assessment_id", ['value' => $model->id]); ?>                                    
                        <?php echo Html::activeHiddenInput($detail, "[{$d}]hbdi_item_id"); ?>

                        <?= $form->field($detail, "[{$d}]score")->checkbox(['value' => 6, 'disabled' => $detail->score == 20 || $detail->score == 12, 'checked' => $detail->score == 20 || $detail->score == 12])->label($detail->hbdiItem->item) ?>
                    
                    <?php if ($d == 7 || $d == 15 || $d == 23 || $d == 31): ?> 
                            <!-- </div>
                        </div> -->
                    </div>
                    <?php endif; ?>
                
                <?php endforeach; ?>
                </div>


                <div class="mt-3 mb-3 d-flex justify-content-end align-items-center">
                    <?= Html::submitButton('Lanjut', ['class' => 'btn btn-success', 'disabled' => true, 'id' => 'toStepFour']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>

    
</div>

<?php 
$this->registerCss('
label {
    display: inline-block;
    font-size: 14px;
    font-weight: 500;
    text-transform: initial;
    letter-spacing: 0px;
    color: #555;
    margin-bottom: 10px;
    cursor: pointer;
}
');

$jstwo = <<<JS
$(":checkbox").change(function() {
    var totalCheckThree = 0;
    $(":checkbox:checked").each(function() {
        totalCheckThree = totalCheckThree + 1;
        
    });
    console.log(totalCheckThree)
    if (totalCheckThree == 24) {
        $('#toStepFour').prop('disabled', false); 
    }
    if (totalCheckThree > 24) {
        $('#toStepThree').prop('disabled', true);
        swal("Lebih dari 8!", "Anda tidak boleh memilih lebih dari 8 pernyataan.")
    }

})
JS;
$this->registerJs($jstwo);
?>