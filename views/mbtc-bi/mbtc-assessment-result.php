<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$this->title = 'Metaphorical Brain Thinking Cluster';
$this->params['breadcrumbs'][] = $this->title;
$this->params['image_url'] = Yii::getAlias('@web/images/header-img/3661950.jpg');

// echo '<pre>'; 
// var_dump($details); die(); 

?>
<div class="hbdi-assessment">
    <div class="container clearfix">    
        <div class="row d-flex justify-content-center">
            <div class="col-lg-9">
                <div class="row mb-3 bg-light border">
                    <div class="col-lg-12">
                        <div class="calories-wrap center mt-3 py-5 w-100 px-2">
                            <div class="uppercase mt-2 h5 mb-1 ls2">Terima kasih telah mengisi asesmen Pola Berpikir berbasis MBTC (Metaphorical Brain Thinking Cluster).</div>
                            <div class="uppercase mt-2 h5 mb-4 ls2">Hasil asesmen Anda akan disampaikan saat workshop.</div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
                
    </div>
</div>

