<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\assets\FrontEndAsset;

FrontEndAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="stretched">
<?php $this->beginBody() ?>

<!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <?= Html::a(Html::img('@web/images/logo-nli.png', ['alt' => 'NeuroLeadership Indonesia']), Yii::$app->homeUrl, ['class' => 'standard-logo', 'data-dark-logo' => '@web/images/logo-dark-nli.png']) ?>
                        <?= Html::a(Html::img('@web/images/logo-nli@2x.png', ['alt' => 'NeuroLeadership Indonesia']), Yii::$app->homeUrl, ['class' => 'retina-logo', 'data-dark-logo' => '@web/images/logo-dark-nli@2x.png']) ?>
                    </div> <!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">
                        <?php 
                        $nav_items = [
                            [
                                'label' => '<div>Home</div>', 
                                'active' => $this->context->route == Yii::$app->homeUrl || $this->context->route == 'site/index',  
                                'url' => Yii::$app->homeUrl, 
                            ],
                            [
                                'label' => '<div>About</div>', 
                                'url' => '#', 
                                'items' => [
                                    [
                                        'label' => '<div>About NeuroLeadership Indonesia</div>',
                                        'url' => ['/site/about'],                                        
                                    ],
                                    [
                                        'label' => '<div>Why NeuroLeadership</div>',
                                        'url' => ['/site/why-neuroleadership'],                                        
                                    ],
                                    [
                                        'label' => '<div>Our Team</div>',
                                        'url' => ['/site/team'],                                        
                                    ],
                                ], 
                            ],  
                            // [
                            //     'label' => '<div>Why NeuroLeadership</div>', 
                            //     'url' => ['/site/why-neuroleadership'], 
                            // ],  
                            [
                                'label' => '<div>Activity</div>', 
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => '<div>Training & Development</div>',
                                        'url' => ['/site/training-and-development'],                                        
                                    ],
                                    [
                                        'label' => '<div>Reseach & Development</div>',
                                        'url' => ['/site/research-and-development'],                                        
                                    ],
                                    [
                                        'label' => '<div>Publication & Forum</div>',
                                        'url' => ['/site/publication-and-forum'],                                        
                                    ],
                                ], 
                            ],
                            [
                                'label' => '<div>Event</div>',
                                'active' => $this->context->route == 'site/event' || $this->context->route == 'site/event-details' || $this->context->route == 'site/event-registration', 
                                'url' => ['/site/event'],                                   
                            ],
                            [
                                'label' => '<div>Blog</div>', 
                                'active' => $this->context->route == 'site/blog' || $this->context->route == 'site/blog-detail', 
                                'url' => ['/site/blog'],                                
                            ],
                            [
                                'label' => '<div>Contact</div>', 
                                'url' => ['/site/contact'], 
                            ],
                                                                           
                        ];

                        echo Menu::widget([
                            'items' => $nav_items,
                            // 'options' => ['class' => 'one-page-menu'],
                            'submenuTemplate' => "<ul class='submenu'>{items}</ul>",
                            'encodeLabels' => false,
                            'activeCssClass' => "current",
                            'activateItems' => true,
                            'activateParents' => true,
                        ]);                            
                        ?>


                        <!-- Top Search
                        ============================================= -->
                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div><!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

        <?php if (isset($this->blocks['indexPage'])): ?>
        <?= $this->blocks['indexPage'] ?>
        <?php else: ?>

        <section id="page-title" class="page-title-parallax bgcolor page-title-dark skrollable skrollable-between" style="background-image: url(<?= (isset($this->params['image_url']) ? $this->params['image_url'] : 'images/blur-bg.jpg') ?>); background-size: cover; padding: 80px 0px; background-position: 0px -37.8099px;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
            <div class="container clearfix">
                <h1 style="font-size: 26px;"><?= Html::encode($this->title) ?></h1>           
                <?= Breadcrumbs::widget([
                    'homeLink' => ['label' => 'Home', 'url' => Yii::$app->homeUrl],
                    'tag' => 'ol',
                    'itemTemplate' => "<li class=\"breadcrumb-item\">{link}</li>\n",
                    'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
        </section>

        <section id="content">
            <div class="content-wrap">
                <?= $content ?>            
            </div>
        </section><!-- #content end -->

        <?php endif; ?>

        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; <?= date('Y') ?> All Rights Reserved by NeuroLeadership Indonesia.<br>
                        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-gplus">
                                <i class="icon-gplus"></i>
                                <i class="icon-gplus"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-youtube">
                                <i class="icon-youtube"></i>
                                <i class="icon-youtube"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-instagram">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                           
                            <a href="#" class="social-icon si-small si-borderless si-linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>

                        <div class="clear"></div>

                        <i class="icon-envelope2"></i> info@leadership.id <span class="middot">&middot;</span> <i class="icon-phone"></i> +6221 736 2639 ext 304
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
