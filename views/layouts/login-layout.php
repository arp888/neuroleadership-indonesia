<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\MintonAsset;

MintonAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="loading auth-fluid-pages pb-0">
<?php $this->beginBody() ?>

<div class="auth-fluid">

    <div class="auth-fluid-right">
        <div class="auth-user-testimonial">           
            
            <h2 class="text-white">NeuroLeadership for Highly Effective Decision</h2>
            <h3 class="text-white">30 days sprint coaching platform</h3>
            
           <!--  <div class="d-lg-block d-none mt-3">
                <span class="logo-lg"><?=  Html::img('@web/images/logo-nlii@2x.png', ['height' => 80, 'alt' => '']) ?></span>
            </div> -->

        </div> <!-- end auth-user-testimonial-->
    </div>
            
    <!--Auth fluid left content -->
    <div class="auth-fluid-form-box">
        <div class="align-items-center d-flex h-100">                    
            <div class="card-body">

                <?= $content ?>

            </div> <!-- end .card-body -->
        </div> <!-- end .align-items-center.d-flex.h-100-->
    </div>
    <!-- end auth-fluid-form-box-->
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php
    $this->registerCss('
        .auth-user-testimonial {
            position: absolute;
            margin: 0 auto;
            padding: 0 2.75rem;
            bottom: 5rem!important;
            left: 0;
            right: 0;
        }
    ');
?>
