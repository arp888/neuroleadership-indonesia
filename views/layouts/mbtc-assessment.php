<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\assets\FrontEndAsset;

FrontEndAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns="http://www.w3.org/1999/xhtml" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="stretched">
<?php $this->beginBody() ?>

<!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
      

        <?php if (isset($this->blocks['indexPage'])): ?>
        <?= $this->blocks['indexPage'] ?>
        <?php else: ?>
        <?php if (isset($this->blocks['blogDetailHeader'])): ?>
        <?= $this->blocks['blogDetailHeader'] ?>
        <?php else: ?>    
        <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url(<?= (isset($this->params['image_url']) ? $this->params['image_url'] : null) ?>); background-size:cover; padding: 50px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -100px;">
            
            <div class="">
               <ul class="nav user-menu">
            
                <!-- Search -->
                <!-- <li class="nav-item">
                    <div class="top-nav-search">
                        <a href="javascript:void(0);" class="responsive-search">
                            <i class="fa fa-search"></i>
                       </a>
                        <form action="search.html">
                            <input class="form-control" type="text" placeholder="Search here">
                            <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </li> -->
                <!-- /Search -->
                
                <!-- Flag -->

                    <?php 
                            $getLang = \Yii::$app->request->get('lang');                           
                            
                            if (isset($getLang)) {
                                Yii::$app->session->set('language', $getLang);
                            }                           

                            if (!Yii::$app->session->has('language')) {
                                Yii::$app->session->set('language', 'id');
                            }

                            $language = Yii::$app->session->get('language');
                            
                            Yii::$app->language = $language; 

                    ?>

                    <!-- <span style="color: #fff;font-size: 15px;margin-top:18px;">Pilih Bahasa:</span> -->
                    <li class="nav-item dropdown has-arrow flag-nav">
                            
                        <?php
                            // var_dump($getLang);
                            // var_dump($language); 
                            // var_dump(Yii::$app->language); 

                            if ($language == "id") {
                                echo '<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">' .
                                    Html::img('@web/images/flags/id.png', ['alt' => '', 'height' => '20']) . ' <span>Indonesia</span></a>';
                            } else {
                                echo '<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">' .
                                    Html::img('@web/images/flags/us.png', ['alt' => '', 'height' => '20']) . ' <span>English</span></a>';
                            }                     
                        ?>

                        <div class="dropdown-menu dropdown-menu-right">
                            <?= Html::a(Html::img('@web/images/flags/us.png', ['alt' => '', 'height' => '16']) . ' English', \yii\helpers\Url::current(['lang' => 'en']), ['class' => 'dropdown-item']) ?>

                            <?= Html::a(Html::img('@web/images/flags/id.png', ['alt' => '', 'height' => '16']) . ' Indonesia', \yii\helpers\Url::current(['lang' => 'id']), ['class' => 'dropdown-item']) ?>

                            <!-- <?= Html::a(Html::img('@web/images/flags/us.png', ['alt' => '', 'height' => '16']) . ' English', \yii\helpers\Url::current(['lang' => 'en']), ['class' => 'dropdown-item']) ?>

                            <?= Html::a(Html::img('@web/images/flags/id.png', ['alt' => '', 'height' => '16']) . ' Indonesia', \yii\helpers\Url::current(['lang' => 'id']), ['class' => 'dropdown-item']) ?> -->
                        </div>
                    </li>
                    <!-- /Flag -->


                </ul>

            </div>
        

            <div class="d-flex justify-content-center">

                <?php // echo Html::img('@web/images/mbtc-logo.png', ['height' => 100]) ?>
                <h1 style="font-size: 26px;display:inline-block;">Metaphorical Brain Thinking Cluster (MBTC)</h1>
            </div>
          
        </section>
        <?php endif; ?>

        <section id="content">
            <div class="content-wrap" style="padding: 40px 0;">
                <?= $content ?>            
            </div>
        </section><!-- #content end -->

        <?php endif; ?>



    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
