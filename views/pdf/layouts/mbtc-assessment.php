<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\assets\FrontEndAsset;

FrontEndAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns="http://www.w3.org/1999/xhtml" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="stretched">
<?php $this->beginBody() ?>

<!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <section id="content">
            <div class="content-wrap" style="padding: 40px 0;">
                <?= $content ?>            
            </div>
        </section><!-- #content end -->




    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
