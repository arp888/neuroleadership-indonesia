<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MailSetting */

$this->title = Yii::t('app', 'Create Mail Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mail Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-setting-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
