<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MailSettingSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="mail-setting-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'layout' => 'inline',        
        // 'enableClientValidation'=>false,
        // 'enableAjaxValidation'=>false,   
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-focus'],            
        ],          
    ]); ?>

    <div class="row filter-row">
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'smtp_host')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('smtp_host'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'smtp_username')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('smtp_username'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'smtp_pass')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('smtp_pass'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'smtp_port')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('smtp_port'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= $form->field($model, 'smtp_encryption')->textInput(['class' => 'form-control floating'])->label($model->getAttributeLabel('smtp_encryption'), ['class' => 'focus-label']) ?>
        </div>
        <div class="col-sm-4 col-md-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>

