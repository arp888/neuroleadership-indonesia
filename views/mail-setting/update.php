<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MailSetting */

$this->title = Yii::t('app', 'Perbarui Mail Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mail Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mail-setting-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
