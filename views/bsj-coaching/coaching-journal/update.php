<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */

$this->title = Yii::t('app', 'Perbarui Coaching Participant Journal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Participant Journal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coaching-participant-journal-update">    
    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>            
</div>
