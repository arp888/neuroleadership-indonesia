<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use cornernote\returnurl\ReturnUrl; 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use app\modules\coaching_implementation\models\CoachingParticipant;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="coaching-participant-journal-form">   
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?=  Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>                        

            <?= $form->field($model, 'date', [
                'selectors' => ['input' => '#coachingparticipantjournal-date-disp']
            ])->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'options' => ['id' => 'coachingparticipantjournal-date'],
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Select Date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        // 'calendarWeeks' => true,
                        'daysOfWeekDisabled' => [0, 6],
                        'endDate' => date('d/m/Y'),
                        // 'daysOfWeekHighlighted' => [0, 6],
                    ]
                ],
            ])->hint(Yii::t('app', 'Select') . ' ' . $model->getAttributeLabel('date')) ?>

            <?= $form->field($model, 'problem_and_situation')->widget(\dosamigos\tinymce\TinyMce::classname(), [
                'options' => ['rows' => 3],

                // 'language' => 'id',
                'clientOptions' => [
                    // 'height' => 200,
                    'branding' => false,
                    'menubar' => false,
                    'relative_urls' => false,
                    'remove_script_host' => false,
                    'convert_urls' => true,
                    'plugins' => [
                    'advlist autolink lists link charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste image'
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
                ] 
            ])->hint(Yii::t('app', 'Describe' . ' ' . $model->getAttributeLabel('problem_and_situation'))) ?>

            <?= $form->field($model, 'problem_and_situation_scale')->radioList([0 => 'Highly Destructive', 10 => 'Destructive', 20 => 'Constructive', 30 => 'Highly Constructive'])->hint(Yii::t('app', 'Select one of the 4 options above.')) ?>

            <?= $form->field($model, 'reappraise')->widget(\dosamigos\tinymce\TinyMce::classname(), [
                'options' => ['rows' => 3],
                // 'language' => 'id',
                'clientOptions' => [
                    // 'height' => 200,
                    'branding' => false,
                    'menubar' => false,
                    'relative_urls' => false,
                    'remove_script_host' => false,
                    'convert_urls' => true,
                    'plugins' => [
                    'advlist autolink lists link charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste image'
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
                ] 
            ])->hint(Yii::t('app', 'Describe' . ' ' . $model->getAttributeLabel('reappraise'))) ?>

            <?= $form->field($model, 'visioning')->widget(\dosamigos\tinymce\TinyMce::classname(), [
                'options' => ['rows' => 3],
                // 'language' => 'id',
                'clientOptions' => [
                    // 'height' => 200,
                    'branding' => false,
                    'menubar' => false,
                    'relative_urls' => false,
                    'remove_script_host' => false,
                    'convert_urls' => true,
                    'plugins' => [
                    'advlist autolink lists link charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste image'
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code'
                ] 
            ])->hint(Yii::t('app', 'Describe' . ' ' . $model->getAttributeLabel('visioning'))) ?>



            <?= $form->field($model, 'label_bias')->checkboxlist($model->labelBiasOptions, [
                'item'=>function ($index, $label, $name, $checked, $value){          
                    return '<div class="custom-control custom-checkbox">
                        <input type="checkbox" id="' . $index . '" class="single-checkbox custom-control-input" name="' . $name . '" value="' . $value . '"' . ($checked ? 'checked' : null) . '>
                        <label class="custom-control-label" for="' . $index . '">' . $value . '<p class="small">' . $label. '<p></label>
                    </div>';
                }

            ])->hint(Yii::t('app', 'Choose' . ' ' . $model->getAttributeLabel('label_bias')) . '. You can check up to 3 options.') ?>


            <?= $form->field($model, 'decide_and_action')->widget(\dosamigos\tinymce\TinyMce::classname(), [
                'options' => ['rows' => 3],
                // 'language' => 'id',
                'clientOptions' => [
                    // 'height' => 200,
                    'branding' => false,
                    'menubar' => false,
                    'relative_urls' => false,
                    'remove_script_host' => false,
                    'convert_urls' => true,
                    'plugins' => [
                        'advlist autolink lists link charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste image'
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table code',
                    // 'toolbar_location' => 'bottom',
                    // 'skin' => 'outside'
                ] 
            ])->hint(Yii::t('app', 'Describe' . ' ' . $model->getAttributeLabel('decide_and_action'))) ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>    
</div>

<?php 
    $this->registerCss('
        .custom-control-label {
            font-weight: 400;
        }
        .invalid-feedback {
            display: block;
            width: 100%;
            margin-top: 0.25rem;
            font-size: 0.75rem;
            color: #f1556c;
        }
    ');

$errorMsgTitle = \yii\helpers\Json::htmlEncode(Yii::t('app', 'More than 3!'));
$errorMsgBody = \yii\helpers\Json::htmlEncode(Yii::t('app', 'You may not select more than 3 options.'));

$js = <<<JS
// $(":checkbox").change(function() {
//     var totalCheck = 0;
//     $(":checkbox:checked").each(function() {
//         totalCheck = totalCheck + 1;
        
//     });
//     console.log(totalCheck)

//     if (totalCheck > 3) {
//         console.log($(this).val);
//         swal($errorMsgTitle, $errorMsgBody);
//     }

// })

$(":checkbox").change(function() {
    if ($(":checkbox:checked").length > 3) {
        this.checked = false; 
        swal($errorMsgTitle, $errorMsgBody);
    } 
})

// var limit = 3;
// $('input.single-checkbox').on('change', function(evt) {
//    if($(this).siblings(':checked').length >= limit) {
//        this.checked = false;

//    }
// });
JS;
$this->registerJs($js);
$this->registerCss('
    .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
      background: none;
      color: #d5d5d5;
      cursor: default;
    }
');
?>