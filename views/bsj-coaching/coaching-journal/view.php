<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */

$this->title = Yii::t('app', 'Detail Coaching Journal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Journal'), 'url' => ['coaching-journal']];
$this->params['breadcrumbs'][] = $model->id;
\yii\web\YiiAsset::register($this);
?>

<div class="coaching-participant-journal-view detail-view">         
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> <span class="d-md-inline-block d-none">' . Yii::t('app', 'Index') . '</span>', ['coaching-journal'], ['class' => 'btn btn-light']) ?>    

                    
                    <?= Html::a(Yii::t('app', 'Create'), ['create-coaching-journal', 'ru' => ReturnUrl::getToken()], [
                        'class' => 'btn btn-primary ml-1'
                    ]) ?>
                    
            
                    <div class="btn-group" role="group" aria-label="...">
                       
                        <?= Html::a(Yii::t('app', 'Update'), ['update-coaching-journal', 'id' => $model->id, 'ru' => ReturnUrl::getToken()], [
                            'class' => 'btn btn-success'
                        ]) ?>
                        
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete-coaching-journal', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                       
                    </div> 
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['tag' => false],
                'template' => '<div class="mb-3"><h5>{label}</h5>{value}</div>',                
                'attributes' => [
                    // [
                    //     'attribute' => 'coaching_participant_id',
                    //     'value' => function ($model) {
                    //         return $model->coachingParticipant->name;
                    //     },
                    // ],
                    [
                        'attribute' => 'date',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'problem_and_situation',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'problem_and_situation_scale',                        
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->problem_and_situation_scale == 30) {
                                $label = 'Highly Constructive'; 
                                $percentage = 100; 
                                $class = 'bg-success';
                            } elseif ($model->problem_and_situation_scale == 20) {
                                $label = 'Constructive';
                                $percentage = 75; 
                                $class = 'bg-info';
                            } elseif ($model->problem_and_situation_scale == 10) {
                                $label = 'Destructive';
                                $percentage = 50; 
                                $class = 'bg-warning';                                
                            } elseif ($model->problem_and_situation_scale == 0) {
                                $label = 'Highly Destructive';
                                $percentage = 25;                                 
                                $class = 'bg-danger';
                            } else {
                                $label = '';
                            }

                            $labelVal = '
                                <div style="height:20px; width:300px" class="progress rounded">
                                    <div role="progressbar" aria-valuenow="44" aria-valuemin="0" aria-valuemax="100" style="width: ' . $percentage . '%" class="progress-bar rounded ' . $class .'"></div>

                                </div>
                                <span>' . $label . '</span>
                            ';

                            return $labelVal; 
                        }
                    ],
                    [
                        'attribute' => 'reappraise',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'visioning',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'label_bias',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                        'value' => function ($model) {
                            // $label = '';
                            // $values = json_decode($model->label_bias); 

                            // $label .= '<ul>';
                            // foreach ($values as $key => $val) {
                            //     $label .= '<li>' . $val . '</li>';
                            // }
                            // $label .= '</ul>'; 
                            return $model->labelBiasValues;


                        }
                    ],
                    [
                        'attribute' => 'decide_and_action',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => 'raw',
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>        

<?php 
    $this->registerCss('
        ul {
            padding-left: 15px!important;
        }
    ');
?>