<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use cornernote\returnurl\ReturnUrl; 
use yii\helpers\ArrayHelper;
use app\modules\coaching_implementation\models\CoachingParticipant;
use yii\helpers\StringHelper; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\coaching_implementation\models\CoachingParticipantJournalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Coaching Participant Journal');
$this->params['breadcrumbs'][] = $this->title;

// var_dump($dataProvider->getModels()); die(); 

?>

<div class="coaching-participant-journal-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]) ?>           
    
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?php                        
                        echo Html::a(Yii::t('app', 'Create Coaching Journal'), ['create-coaching-journal', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-primary']);
                    ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProviderCoachingJournal,
                'filterModel' => $searchModelCoachingJournal,
                'pjax' => true,          
                'pjaxSettings'=>[
                    // 'neverTimeout' => false,
                    'loadingCssClass' => false,
                    'options' => [
                        'id' => 'coaching-participant-journal-grid-pjax',                    
                    ],            
                ],
                'layout' => "{items}\n<div class='row d-flex align-items-center mt-2'><div class='col'>{pager}</div><div class='col-auto'>{summary}</div></div>",
                'striped' => false,
                'hover' => false,
                'bordered' => false,
                'responsiveWrap' => false,
                'perfectScrollbar' => false,
                // 'emptyText' => 'Data tidak ditemukan.',
                'krajeeDialogSettings' => ['useNative' => true, 'overrideYiiConfirm' => false],
                // 'summary' => 'Menampilkan <strong>{begin}-{end}</strong> dari <strong>{totalCount}</strong> item.',
                'pager' => [
                    'options' => ['class' => 'pagination pagination-rounded'],
                ],
                'columns' => [           
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        // 'vAlign' => 'middle',
                    ],

                    [
                        'attribute' => 'date',
                        'width' => '130px',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        'mergeHeader' => true, 
                        'format' => ['date', 'long'],
                    ],
                    [
                        'attribute' => 'problem_and_situation',
                        'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                        'vAlign' => 'middle',
                        // 'mergeHeader' => true, 
                        'format' => 'raw',
                        'value' => function ($model) {
                            return HtmlPurifier::process(StringHelper::truncateWords($model->problem_and_situation, 25));
                        }
                    ],
                    [
                        'attribute' => 'problem_and_situation_scale',
                        'width' => '180px',
                        'filter' => [0 => 'Highly Destructive', 10 => 'Destructive', 20 => 'Constructive', 30 => 'Highly Constructive'],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'All')],
                            'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'vAlign' => 'middle',
                        // 'mergeHeader' => true, 
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->problem_and_situation_scale == 30) {
                                $label = 'Highly Constructive'; 
                                $percentage = 100; 
                                $class = 'bg-success';
                            } elseif ($model->problem_and_situation_scale == 20) {
                                $label = 'Constructive';
                                $percentage = 75; 
                                $class = 'bg-info';
                            } elseif ($model->problem_and_situation_scale == 10) {
                                $label = 'Destructive';
                                $percentage = 50; 
                                $class = 'bg-warning';                                
                            } elseif ($model->problem_and_situation_scale == 0) {
                                $label = 'Highly Destructive';
                                $percentage = 25;                                 
                                $class = 'bg-danger';
                            } else {
                                $label = '';
                            }

                            $labelVal = '
                                <div style="height:15px" class="progress rounded">
                                    <div role="progressbar" aria-valuenow="44" aria-valuemin="0" aria-valuemax="100" style="width: ' . $percentage . '%" class="progress-bar rounded ' . $class .'"></div>

                                </div>
                                <span class="small">' . $label . '</span>
                            ';

                            return $labelVal; 
                        }
                    ],
                    // [
                    //     'attribute' => 'reappraise',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                    //     'vAlign' => 'middle',
                    //     'mergeHeader' => true, 
                    //     'format' => 'raw',
                    //     'value' => function ($model) {
                    //         return StringHelper::truncateWords($model->reappraise, 15);
                    //     }
                    // ],
                    // [
                    //     'attribute' => 'visioning',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                    //     'vAlign' => 'middle',
                    //     'mergeHeader' => true, 
                    //     'format' => 'raw',
                    //     'value' => function ($model) {
                    //         return StringHelper::truncateWords($model->visioning, 15);
                    //     }
                    // ],
                    // [
                    //     'attribute' => 'label_bias',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                    //     'vAlign' => 'middle',
                    //     'mergeHeader' => true, 
                    //     'format' => 'raw',
                    //     'value' => function ($model) {
                    //         $label = '';
                    //         $values = json_decode($model->label_bias); 

                    //         $label .= '<ul>';
                    //         foreach ($values as $key => $val) {
                    //             $label .= '<li>' . $val . '</li>';
                    //         }
                    //         $label .= '</ul>'; 
                    //         return $label; 


                    //     }
                    // ],
                    // [
                    //     'attribute' => 'decide_and_action',
                    //     'filterInputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Search')],
                    //     'vAlign' => 'middle',
                    //     'mergeHeader' => true, 
                    //     'format' => 'raw',
                    //     'value' => function ($model) {
                    //         return StringHelper::truncateWords($model->decide_and_action, 15);
                    //     }
                    // ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'width' => '110px',
                        'vAlign' => 'middle',
                        'template' => '<div class="btn-group" role="group">{view}{update}{delete}</div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fe-align-left"></i>', ['view-coaching-journal', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fe-edit"></i>', ['update-coaching-journal', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',                            
                                ]);
                            },                    
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fe-trash-2"></i>', ['delete-coaching-journal', 'id' => $model->id, 'ru' => ReturnUrl::getToken()],
                                [
                                    'class' => 'action-icon',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                        'method' => 'post'
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
        
</div>

<?php 
    $this->registerCss('
        .w0 > p {
            margin-bottom: 0; 
        }

        .bg-gradient {
            background-color: #00d2ff;
            background-image: linear-gradient(to right, #00d2ff 0%, #3a7bd5 100%);
            box-shadow: 2px 2px 10px rgba(57, 123, 213, 0.36)
        }
        ul {
            padding-left: 15px!important;
        }
    ');
?>