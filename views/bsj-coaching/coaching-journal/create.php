<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coaching_implementation\models\CoachingParticipantJournal */

$this->title = Yii::t('app', 'Create Coaching Journal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coaching Journal'), 'url' => ['index']];
$this->params['breadcrumbs'][] =  Yii::t('app', 'Create');
?>
<div class="coaching-participant-journal-create">    
    <?= $this->render('_form', [
        'model' => $model,         
    ]) ?>    
</div>
