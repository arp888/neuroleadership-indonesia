<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ResetPassword */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="auth-brand text-center text-lg-left">
    <div class="auth-logo">           
        <?= Html::a('
            <span class="logo-lg">' .  Html::img('@web/images/logo-bsj@2x.png', ['height' => 100, 'alt' => '']) . '</span>',
            '#',
            [
                'class' => 'logo logo-dark text-center'
            ]
        ) ?>           
    </div>
</div>

<!-- title-->
<h4 class="mt-lg-5 mt-3">Reset Password</h4>
<p class="text-muted mb-3">Enter your new password.</p>


<?php $form = ActiveForm::begin([
    'id' => 'reset-password-form',
    'options' => ['class' => ''],
]); ?>   

<?= $form->field($model, 'password')->textInput([
        'class' => 'form-control',
        'placeholder' => 'Password',
        'autocomplete' => 'off',
    ]) 
?>

<div class="form-group mb-0 text-center">
    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary btn-block mb-3', 'name' => 'reset-password-button']) ?>
</div>

<?php ActiveForm::end(); ?>

<div class="d-block mt-5 text-md-left text-center m-auto">
        <?=  Html::img('@web/images/logo-nlii@2x.png', ['height' => 70, 'alt' => '']) ?>
    </div>
    
<!-- Footer-->
<footer class="footer footer-alt">
    <p class="text-muted">Back to <?= Html::a('<b>Login</a>', ['login'], ['class' => 'text-primary']) ?></p>
</footer>

<?php 
    $this->registerCss('
        .auth-fluid {
            position: relative;
            display: flex;
            align-items: center;
            min-height: 100vh;
            flex-direction: row;
            align-items: stretch;
            background: url(' . Yii::getAlias("@web/images/bsj-coaching-bg.jpg") . ') center;
            background-size: cover;
        }

        .auth-user-testimonial {
            position: absolute;
            margin: 0 auto;
            padding: 0 2.75rem;
            bottom: 5rem!important;
            left: 0;
            right: 0;
        }

        @media (min-width: 992px)
            .auth-brand-2 {
                position: absolute;
                bottom: 1rem;
            }
    ');
?>


