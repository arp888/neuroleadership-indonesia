<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use cornernote\returnurl\ReturnUrl; 

/* @var $this yii\web\View */

$this->title = '30 Days Sprint Coaching Platform';

?>

<div class="bsj-coaching-index">    	
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Coaching Case Progress</h4>
            <div class="progress mb-0" style="height: 50px;">
                <?php 
                $i = 0; 
                foreach ($caseClosed as $key => $case) {
                    if ($case['status'] == 20) {
                        echo '<div class="progress-bar bg-success small-font" role="progressbar" style="width: ' . $progressBarWidth . '%" aria-valuenow="' . $progressBarWidth .'" aria-valuemin="0" aria-valuemax="100">' . Html::a(Yii::$app->formatter->asDate($key, 'short'), ['view-coaching-journal', 'id' => $case['journalId'], 'ru' => ReturnUrl::getToken()], ['class' => 'btn-link text-white', 'target' => 'blank']) . '</div>'; 
                    } elseif ($case['status'] == 10) {
                        echo '<div class="progress-bar bg-warning small-font" role="progressbar" style="width: ' . $progressBarWidth . '%" aria-valuenow="' . $progressBarWidth .'" aria-valuemin="0" aria-valuemax="100">' . Html::a(Yii::$app->formatter->asDate($key, 'short'), ['view-coaching-journal', 'id' => $case['journalId'], 'ru' => ReturnUrl::getToken()], ['class' => 'btn-link text-white', 'target' => 'blank']) . '</div>'; 
                    } else {
                        echo '<div class="progress-bar bg-secondary small-font" role="progressbar" style="width: ' . $progressBarWidth . '%" aria-valuenow="' . $progressBarWidth .'" aria-valuemin="0" aria-valuemax="100">' . Yii::$app->formatter->asDate($key, 'short') . '</div>'; 
                    }
                }

                ?>
            </div>     

            <div class="mt-2">
                <strong><span class="mr-3">Total Case: <?= $totalCase ?></span><span class="bg-success px-1 py-0 mr-1">&nbsp;</span>Total Closed Case: <?= $totalCaseClosed ?><span class="bg-warning px-1 py-0 ml-3 mr-1">&nbsp;</span>Total Unclosed Case: <?= ($totalCase - $totalCaseClosed) ?></strong>
            </div>
        </div>        
    </div>

	<div class="row mb-1">
        <div class="col-lg-6 col-md-6 d-flex">
            <div class="card mb-2 flex-fill">
                <div class="card-body py-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="knob-chart" dir="ltr">
                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#1abc9c"
                                data-bgColor="#d1f2eb" value="<?= Yii::$app->formatter->asDecimal($knob_val['highly_constructive']['value'], 0) ?>"
                                data-skin="tron" data-angleOffset="0" data-readOnly=true
                                data-thickness=".15"/>
                        </div>
                        <div class="text-right">
                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['highly_constructive']['total'] ?></span> </h3>
                            <p class="text-muted mb-0">Highly Constructive</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-6 col-md-6 d-flex">
            <div class="card mb-2 flex-fill">
                <div class="card-body py-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="knob-chart" dir="ltr">
                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#3bafda"
                                data-bgColor="#d8eff8" value="<?= Yii::$app->formatter->asDecimal($knob_val['constructive']['value'], 0) ?>"
                                data-skin="tron" data-angleOffset="0" data-readOnly=true
                                data-thickness=".15"/>
                        </div>
                        <div class="text-right">
                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['constructive']['total'] ?></span> </h3>
                            <p class="text-muted mb-0">Constructive</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 d-flex">
            <div class="card mb-2 flex-fill">
                <div class="card-body py-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="knob-chart" dir="ltr">
                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#ffbd4a"
                                data-bgColor="#FFE6BA" value="<?= Yii::$app->formatter->asDecimal($knob_val['destructive']['value'], 0) ?>"
                                data-skin="tron" data-angleOffset="0" data-readOnly=true
                                data-thickness=".15"/>
                        </div>
                        <div class="text-right">
                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['destructive']['total'] ?></span> </h3>
                            <p class="text-muted mb-0">Destructive</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-6 col-md-6 d-flex">
            <div class="card mb-2 flex-fill">
                <div class="card-body py-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="knob-chart" dir="ltr">
                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#f05050"
                                data-bgColor="#F9B9B9" value="<?= Yii::$app->formatter->asDecimal($knob_val['highly_destructive']['value'], 0) ?>"
                                data-skin="tron" data-angleOffset="0" data-readOnly=true
                                data-thickness=".15"/>
                        </div>
                        <div class="text-right">
                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['highly_destructive']['total'] ?></span> </h3>
                            <p class="text-muted">Highly Destructive</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
	</div>
</div>

<?php 
	$this->registerJsFile('@web/themes/minton-5/libs/jquery-knob/jquery.knob.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCss('
    .small-font{
        font-size: 8px;
    } 
    '); 
?>