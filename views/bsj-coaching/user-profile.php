<?php

use yii\helpers\Html;
use app\models\User;
use yii\widgets\DetailView;

$this->title = 'Profil Pengguna';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class='user-profile'>
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">          
                    <?= Html::a('Update', ['user-profile-update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>                               
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row align-items-center">
                
            
                <div class="col-md-3">
                    <?= Html::img($model->getImageUrl(), ['class' => 'img-thumbnail circle', 'width' => 200, 'height' => 200]) ?>
                </div>

                <div class="col-md-9">
                    <?= DetailView::widget([
                        'model' => $model,
                        'options' => ['tag' => false],
                        'template' => '<div class="mb-3"><h5>{label}</h5>{value}</div>',                
                        'attributes' => [
                            'username',
                            'fullname',
                            'email',                    
                        ],
                    ]) ?>
                </div>
            </div>            
        </div>
    </div>   
</div>