<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */

$this->title = '30 Days Sprint Coaching Platform';
?>

<div class="bsj-coaching-index">    
	<div class="row">

		<div class="col-lg-5">
			<div class="row mb-1">
		        <div class="col-lg-12 col-md-6">
		            <div class="card mb-2">
		                <div class="card-body py-2">
		                    <div class="d-flex justify-content-between align-items-center">
		                        <div class="knob-chart" dir="ltr">
		                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#1abc9c"
		                                data-bgColor="#d1f2eb" value="<?= Yii::$app->formatter->asDecimal($knob_val['highly_constructive']['value'], 0) ?>"
		                                data-skin="tron" data-angleOffset="0" data-readOnly=true
		                                data-thickness=".15"/>
		                        </div>
		                        <div class="text-right">
		                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['highly_constructive']['total'] ?></span> </h3>
		                            <p class="text-muted mb-0">Highly Constructive</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div><!-- end col -->

		        <div class="col-lg-12 col-md-6">
		            <div class="card mb-2">
		                <div class="card-body py-2">
		                    <div class="d-flex justify-content-between align-items-center">
		                        <div class="knob-chart" dir="ltr">
		                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#3bafda"
		                                data-bgColor="#d8eff8" value="<?= Yii::$app->formatter->asDecimal($knob_val['constructive']['value'], 0) ?>"
		                                data-skin="tron" data-angleOffset="0" data-readOnly=true
		                                data-thickness=".15"/>
		                        </div>
		                        <div class="text-right">
		                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['constructive']['total'] ?></span> </h3>
		                            <p class="text-muted mb-0">Constructive</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>

		        <div class="col-lg-12 col-md-6">
		            <div class="card mb-2">
		                <div class="card-body py-2">
		                    <div class="d-flex justify-content-between align-items-center">
		                        <div class="knob-chart" dir="ltr">
		                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#ffbd4a"
		                                data-bgColor="#FFE6BA" value="<?= Yii::$app->formatter->asDecimal($knob_val['destructive']['value'], 0) ?>"
		                                data-skin="tron" data-angleOffset="0" data-readOnly=true
		                                data-thickness=".15"/>
		                        </div>
		                        <div class="text-right">
		                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['destructive']['total'] ?></span> </h3>
		                            <p class="text-muted mb-0">Destructive</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div><!-- end col -->

		        <div class="col-lg-12 col-md-6">
		            <div class="card mb-2">
		                <div class="card-body py-2">
		                    <div class="d-flex justify-content-between align-items-center">
		                        <div class="knob-chart" dir="ltr">
		                            <input data-plugin="knob" data-width="70" data-height="70" data-fgColor="#f05050"
		                                data-bgColor="#F9B9B9" value="<?= Yii::$app->formatter->asDecimal($knob_val['highly_destructive']['value'], 0) ?>"
		                                data-skin="tron" data-angleOffset="0" data-readOnly=true
		                                data-thickness=".15"/>
		                        </div>
		                        <div class="text-right">
		                            <h3 class="mb-1 mt-0"> <span data-plugin="counterup"><?= $knob_val['highly_destructive']['total'] ?></span> </h3>
		                            <p class="text-muted">Highly Destructive</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div><!-- end col -->
			</div>
		</div>

        <div class="col-lg-7">
    		<div class="card">
    			<div class="card-body p-1">
    				<div class="row d-flex justify-content-center align-items-center">
                    <div class="col-12 text-center">                       
                        <?php    
                            echo Highcharts::widget([            
                                'id' => 'renderTo',
                                'scripts' => [
                                    'highcharts-more',
                                    'modules/no-data-to-display',
                                    'modules/accessibility',
                                    'modules/exporting',
                                    'modules/export-data',
                                    'modules/offline-exporting',
                                    'modules/parallel-coordinates'
                                    // 'themes/grid-light',
                                ],
                                'options' => [
                                    'chart'  => [
                                        'polar' => true,                            
                                        // 'marginBottom' => 5,
                                        // 'height' => 480,
                                        'margin' => 0,
                                        'padding' => 0,
                                        'events' => [
                                            'load' => new JsExpression('function(){
                                                var chart = this,
                                                parts = this.xAxis[0].max
                                                customColors = [                                       
                                                    "#007bff", // BLUE                                        
                                                    "#ffc107", // YELLOW
                                                    "#ff5722", // RED
                                                    "#009688", // GREEN
                                                ],
                                                series = chart.series,
                                                renderer = chart.renderer,
                                                cellLeft = tableLeft;

                                                // console.log("parts",parts);

                                                var colorsLength = customColors.length - 1;

                                                var count = 0;

                                                for(var i = 0; i < parts; i++) {
                                                    //setting count back to 0 if colours exceed 20.
                                                    if(count > colorsLength) {
                                                        count = 0;
                                                    }
                                                    var centerX = chart.plotLeft + chart.yAxis[0].center[0];
                                                    var centerY = chart.plotTop + chart.yAxis[0].center[1];
                                                    var outerRradius = chart.yAxis[0].height;
                                                    var innerRadius = 0;
                                                    var start = -Math.PI/2 + (Math.PI/(parts/2) * i);
                                                    var end = -Math.PI/2 + (Math.PI/(parts/2) * (i+1));
                                                    var color = new Highcharts.Color(customColors[count]).setOpacity(0.7).get();
                                                    count++;

                                                    chart.renderer.arc(centerX,centerY,outerRradius,innerRadius,start,end).attr({
                                                        id: "renderedArc",
                                                        fill: color,
                                                        "stroke-width": 1
                                                    }).add();
                                                };

                                                // user options
                                                var tableTop = 10,
                                                    colWidth = 100,
                                                    tableLeft = 20,
                                                    rowHeight = 20,
                                                    cellPadding = 2.5,
                                                    valueDecimals = 1,
                                                    valueSuffix = " °C";                                                
                                            }')
                                        ],
                                    ],
                                    
                                    'title' => [
                                        'text' => '',
                                        'x' => 0,
                                        'enabled' => false,
                                    ],

                                    'plotOptions' => [
                                        'line' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'allowOverlap' => true,
                                                'inside' => false,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '15px',
                                                    'fontWeight' => 'bold',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'enableMouseTracking' => false
                                        ],
                                        // 'series' => [                        
                                        //     'enableMouseTracking' => false,                        
                                        //     'shadow' => false, 
                                        //     'animation' => false,    
                                        //     'color' => '#1a7da1',      
                                        //     'lineWidth' => 3,
                                        //     // 'marker' => false,
                                        //     'marker' => [
                                        //         "radius" => 4,
                                        //         "symbol" => "circle"
                                        //     ],
                                        //     '_colorIndex' => 0,
                                        //     '_symbolIndex' => 0
                                        // ],
                                        'series' => [                        
                                            'enableMouseTracking' => false,                        
                                            'shadow' => false, 
                                            'animation' => false,                        
                                            'lineWidth' => 3,
                                            'marker' => [
                                                "radius" => 6,
                                                "symbol" => "circle"
                                            ],
                                            '_colorIndex' => 0,
                                            '_symbolIndex' => 0
                                        ],
                                        'column' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'allowOverlap' => true,
                                                'inside' => true,
                                                'style' => [
                                                    "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                    'fontSize' => '16px',
                                                    'fontWeight' => 'normal',
                                                    'color' => "#414042",
                                                    'borderWidth' => 0,
                                                    'textShadow' => false,
                                                    'textOutline' => false  
                                                ]
                                            ],
                                            'grouping' => false,
                                            'shadow'  => false,
                                            'borderWidth' => 0
                                        ]
                                    ],   

                                    'pane' => [
                                        'startAngle' => 45,
                                        'endAngle' => 405,
                                        // 'size' => 330,
                                    ],

                                    'xAxis' => [                    
                                        // 'categories' => $score_categories,
                                        'tickmarkPlacement' => 'on',
                                        'lineWidth' => 0,
                                        'gridLineColor' => "transparent",
                                        'tickInterval' => 1,
                                        'labels' => [
                                            'rotation' => 0,                                            
                                            // 'useHTML' =>  true,
                                            'enabled' => false,
                                            'overflow' => 'justified',  
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '14px',
                                                'fontWeight' => '500',
                                                'textOverflow' => 'center',
                                                'color' => '#414042',
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false,
                                                // 'paddingLeft' => '10px',
                                                // 'paddingRight' => '10px',        
                                            ],
                                            'formatter' => new JsExpression('function(){
                                                let label;
                                                switch (this.value) {                                                    
                                                    case 0:
                                                        label = "Constructive";
                                                        break;                              
                                                    case 1:
                                                        label = "Destructive";
                                                        break;                                                    
                                                    case 2:
                                                        label = "Highly Destructive";
                                                        break;                                                    
                                                    case 3:
                                                        label = "Highly Constructive";
                                                        break;                                                                                              
                                                }                                                
                                                return label;
                                            }')
                                        ],
                                        // 'labels' => [
                                        //     'rotation' => 0,
                                        //     'style' => [
                                        //         "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                        //         'fontSize' => '16px',
                                        //         'fontWeight' => 'normal',
                                        //         'color' => "#414042",
                                        //         'borderWidth' => 0,
                                        //         'textShadow' => false,
                                        //         'textOutline' => false  
                                        //     ]
                                        // ],
                                    ],

                                    'yAxis' => [
                                        // 'gridLineInterpolation' => 'polygon',
                                        "gridLineColor" => "#e5e7e9",
                                        "lineWidth" => 0,
                                        "tickmarkPlacement" => "between",
                                        "angle" => -45,
                                        // "tickPixelInterval" => 100,
                                        "tickPosition" => "center",
                                        // 'tickPositions' => [0, 40, 80, 120, 160],
                                        'min' => 0,
                                        'max' => count($journals) + 1,

                                        'color' => '#fdebea',
                                        'showFirstLabel' => false,
                                        'showLastLabel' => false,
                                        'gridLineWidth' => 0,
                                        'tickInterval' => 0,
                                        'endOfTick' => true,
                                        // 'minPadding' => 0,
                                        // 'maxPadding' => 0,
                                        'labels' => [
                                            'align' => 'center',
                                            'x' => 0,
                                            'y' => -4,
                                            'style' => [
                                                "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                                'fontSize' => '10px',
                                                'fontWeight' => 'normal',
                                                'color' => "#414042",
                                                'borderWidth' => 0,
                                                'textShadow' => false,
                                                'textOutline' => false  
                                            ],
                                        ],                            
                                    ],
                                    'legend' => [
                                        'enabled' => false,
                                        'align' => 'right',
                                        'verticalAlign' => 'middle',
                                        'layout' => 'vertical',
                                        'itemMarginTop' => 5,
                                        'itemMarginBottom' => 10,
                                        'backgroundColor' => '#f1f2f2',
                                        'borderRadius' => 5,
                                        'padding' => 10,
                                        'itemDistance' => 40,
                                        'reversed' => true, 
                                        'itemStyle' => [
                                            "fontFamily" => "poppins, roboto, Arial, Helvetica, sans-serif",
                                            'fontSize' => '12pt',
                                            'fontWeight' => 'normal',
                                            'color' => "#414042",
                                            'borderWidth' => 0,
                                            'textShadow' => false,
                                            'textOutline' => false  
                                        ]
                                    ],

                                    'series' => [
							            [
							            	// 'type' => 'area',
							                'name' => 'Value',
							                // 'data' => [1, 2, 3, 4],
							                'data' => [
							                	$knob_val['constructive']['total'],
							                	$knob_val['destructive']['total'],
							                	$knob_val['highly_destructive']['total'],
							                	$knob_val['highly_constructive']['total'],
							                ],
							            ],
							        ],

                                    'lang' => [
                                        'noData' => 'No Data Available.'
                                    ],
                                    'credits' => ['enabled' => false],
                                    'exporting' => [
                                        'enabled' => false,
                                        'sourceWidth' => 960,
                                        'sourceHeight' => 540,
                                        'scale' => 2,
                                        'buttons' => [
                                            'contextButton' => [
                                                'menuItems' => ['downloadPNG', 'downloadJPEG'],
                                            ],
                                        ],
                                        'showTable' => false,
                                        // 'chartOptions' => [
                                        //     'title' => [
                                        //         'style' => [
                                        //             'fontSize' => '12px',
                                        //         ]
                                        //     ]
                                        // ]
                                    ],
                                    'navigation' => [
                                        'buttonOptions' => [
                                            'verticalAlign' => 'middle',
                                            // 'horizontalAlign' => 'bottom',
                                            'x' => -5,
                                            // 'y' => -30,
                                        ]
                                    ],
                                    'colors' => [
                                        "#90ed7d",
                                        "#f15a29",
                                        "#f7a35c",
                                        "#8085e9",
                                        "#f15c80",
                                        "#e4d354",
                                        "#2b908f",
                                        "#f45b5b",
                                        "#91e8e1"
                                    ],                
                                ]
                            ]);                             
                        ?>
                    </div>                
                </div>
    			</div>
    		</div>
    	</div>



    </div>

    

</div>

<?php 
	$this->registerJsFile('@web/themes/minton-5/libs/jquery-knob/jquery.knob.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); 
?>