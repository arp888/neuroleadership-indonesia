<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

$this->title = Yii::t('rbac-admin', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-form">
    <!-- Logo -->    

    <div class="auth-brand text-center text-lg-left">
        <div class="auth-logo">           
            <?= Html::a('
                <span class="logo-lg">' .  Html::img('@web/images/logo-bsj@2x.png', ['height' => 100, 'alt' => '']) . '</span>',
                '#',
                [
                    'class' => 'logo logo-dark text-center'
                ]
            ) ?>           
        </div>
    </div>

    <!-- title-->
    <h4 class="mt-lg-5 mt-3">Sign In</h4>
    <p class="text-muted mb-3">Please enter your username and password.</p>

<!--     <div class="text-md-left text-center">
        <h4 class="mt-3">Sign In</h4>
        <p class="text-muted mb-3">Please enter your username and password.</p>
    </div> -->

    
   
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => ''],
    ]); ?>   

    <?= $form->field($model, 'username')->textInput([
            'class' => 'form-control',
            'placeholder' => 'Username',
            'autocomplete' => 'off',
        ]) 
    ?>
    
    <?= $form->field($model, 'password', [
            'template' => 
                '{beginWrapper}
                    ' . Html::a(Yii::t('app', 'Forgot your password?'), ['request-password-reset'], ['class' => 'text-muted float-right']) . '
                    {label}
                    <div class="input-group input-group-merge">
                        {input}
                        <div class="input-group-append" data-password="false">
                            <div class="input-group-text">
                                <span class="password-eye"></span>
                            </div>
                        </div>
                    </div>
                    
                    {hint}
                    {error}
                {endWrapper}', 
        ])->passwordInput([
            'class' => 'form-control',
            'placeholder' => 'Password',
        ])
    ?>
    
    </div>    

    <div class="form-group mb-0 text-center">
        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary btn-block mb-3', 'name' => 'login-button']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

    <div class="d-block mt-5 text-md-left text-center m-auto">
        <?=  Html::img('@web/images/logo-nlii@2x.png', ['height' => 70, 'alt' => '']) ?>
    </div>

    <!-- div class="auth-brand text-center text-lg-left" style="position: absolute; bottom:1rem;">
        <div class="auth-logo">           
            <?= Html::a('
                <span class="logo-lg">' .  Html::img('@web/images/logo-nlii@2x.png', ['height' => 80, 'alt' => '']) . '</span>',
                '#',
                [
                    'class' => 'logo logo-dark text-center'
                ]
            ) ?>           
        </div>
    </div> -->

</div>

<?php 
    $this->registerCss('
        .auth-fluid {
            position: relative;
            display: flex;
            align-items: center;
            min-height: 100vh;
            flex-direction: row;
            align-items: stretch;
            background: url(' . Yii::getAlias("@web/images/bsj-coaching-bg.jpg") . ') center;
            background-size: cover;
        }

        .auth-user-testimonial {
            position: absolute;
            margin: 0 auto;
            padding: 0 2.75rem;
            bottom: 5rem!important;
            left: 0;
            right: 0;
        }
    ');
?>