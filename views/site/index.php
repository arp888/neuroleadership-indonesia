<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'NeuroLeadership Indonesia';
?>

<div class="site-index">    
	<div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body my-3">
                    <div class="d-flex justify-content-between align-items-center">                       
                        <div>
                            <h3 class="mb-1 mt-0"><?= Html::a('Assessment', ['/assessment'], ['target' => '_blank']) ?></h3>
                            <p class="text-muted mb-0">Manage Assessment</p>
                        </div>
                        <div class="avatar-sm">
                            <span class="avatar-title bg-soft-primary rounded">
                                <i class="ri-list-check-2 font-20 text-primary"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-md-4">
            <div class="card">
                <div class="card-body my-3">
                    <div class="d-flex justify-content-between align-items-center">                       
                        <div>
                            <h3 class="mb-1 mt-0"><?= Html::a('Execution System', ['/scoreboard'], ['target' => '_blank']) ?></h3>
                            <p class="text-muted mb-0">Manage Scoreboard</p>
                        </div>
                        <div class="avatar-sm">
                            <span class="avatar-title bg-soft-success rounded">
                                <i class="ri-bar-chart-2-line font-20 text-success"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-md-4">
            <div class="card">
                <div class="card-body my-3">
                    <div class="d-flex justify-content-between align-items-center">                       
                        <div>
                            <h3 class="mb-1 mt-0"><?= Html::a('User Management', ['/user-management/user'], ['target' => '_blank']) ?></h3>
                            <p class="text-muted mb-0">Manage User</p>
                        </div>
                        <div class="avatar-sm">
                            <span class="avatar-title bg-soft-warning rounded">
                                <i class="ri-user-3-line font-20 text-warning"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-md-4">
            <div class="card">
                <div class="card-body my-3">
                    <div class="d-flex justify-content-between align-items-center">                       
                        <div>
                            <h3 class="mb-1 mt-0"><?= Html::a('Coaching', ['/coaching-implementation'], ['target' => '_blank']) ?></h3>
                            <p class="text-muted mb-0">Coaching Platform</p>
                        </div>
                        <div class="avatar-sm">
                            <span class="avatar-title bg-soft-info rounded">
                                <i class="ri-message-3-line font-20 text-info"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->


    </div>
</div>
