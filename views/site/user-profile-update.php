<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = ['label' => 'Profil Pengguna', 'url' => ['user-profile', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class='user-update'>	
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],  
        'layout' => 'horizontal',
        'fieldClass' => '\app\components\CustomField',
        'fieldConfig' => [
            'options' => ['class' => 'form-group row'],
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            // 'labelOptions' => ['class' => 'col-form-label'],
            'horizontalCssClasses' => [
                'label' => 'col-md-3 col-form-label text-md-right text-left',
                'offset' => 'col-md-3',
                'wrapper' => 'col-md-7',
                'error' => '',
                'hint' => '',
            ],
        ],        
    ]); ?>

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-between">
                <div class="ml-auto">
                    <?= Html::a('<i class="mdi mdi-arrow-left"></i> ' . Yii::t('app', 'Index'), ['user-profile', 'id' => Yii::$app->user->id], ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>

        <div class="card-body">

            <?= $form->field($model, 'username')->textInput() ?>
    
            <?= $form->field($model, 'email')->textInput() ?>
    
            <?= $form->field($model, 'fullname')->textInput() ?>
      
            <?php echo $form->field($model, 'fileImage')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions'=> [
                        'initialPreview'=> [
                            $model->avatar ? Html::img(Yii::getAlias('@web/images/') . $model->avatar, ['class' => 'file-preview-image', 'style' => 'width:auto;height:auto;max-width:100%;max-height:100%;']) : null,
                        ],
                        'initialCaption'=> $model->avatar ? $model->avatar : 'Pilih file...',
                        'overwriteInitial' => true,
                        'showUpload' => false,
                        'showRemove' => false,
                        'allowedFileExtensions' => ['jpg','gif','png'],
                        'maxFileSize' => 1024,
                    ],
                ]);
            ?>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="mdi mdi-cancel"></i> ' . Yii::t('app', 'Cancel'), ['user-profile', 'id' => Yii::$app->user->id], ['class' => 'btn btn-light']) ?>
                </div>    
            </div> 
        </div>     
    </div>
    <?php ActiveForm::end(); ?>


    
</div>