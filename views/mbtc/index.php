<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Metaphorical Brain Thinking Cluster (MBTC)';
$this->params['breadcrumbs'][] = $this->title;
$this->params['image_url'] = Yii::getAlias('@web/images/header-img/3661950.jpg');

?>
<div class="container clearfix">  
    <div class="form-widget">

        <div class="form-result"></div>

        <div class="row shadow bg-light border">

            <div class="col-lg-4 dark" style="background: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,.1)); min-height: 400px;">
                <div class="calories-wrap center mt-4 w-100 px-2">

                	<?= Html::img('@web/images/mbtc-logo.png', ['height' => 70]) ?>                                    
                    <div class="mt-4 mb-2 ls1 text-dark"><?= Yii::t('app', 'Asesmen MBTC (Metaphorical Brain Thinking Cluster) ini dapat membantu Anda mengukur preferensi mental, menginterpretasi dan menerjemahkan preferensi mental ke dalam outcome perilaku yang dapat diprediksi di tempat kerja, serta meningkatkan dan mengembangkan kemampuan Anda menghasilkan insight dari preferensi Anda dan dampaknya pada dunia Anda.') ?> </div>

                    <?= Html::img('@web/images/logo-nlii.png', ['height' => 80]) ?>
                </div>
            </div>

            <div class="col-lg-8 p-5">
                <?php if ($model->mbtcAssessmentGroup->is_active == 10): ?>
                    
                    <?php $form = ActiveForm::begin([
                        'options' => ['class' => 'row mb-0'],  
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'options' => ['class' => 'form-group row'],
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            // 'labelOptions' => ['class' => 'col-form-label'],
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-3 col-form-label',
                                'offset' => 'col-sm-3',
                                'wrapper' => 'col-sm-9',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],        
                    ]); ?>                    
                        
                    <div class="col-12 ">
                        <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app', 'Masukkan Nama Lengkap')])->label(Yii::t('app', 'Nama')) ?>
                    </div>

                    <?php if ($model->mbtcAssessmentGroup->phone_required == 1): ?>

                    <div class="col-12 ">
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => Yii::t('app', 'Masukkan No Telepon/WA')])->label(Yii::t('app', 'No Telepon/WA')) ?>
                    </div>

                    <?php endif; ?>

                    <?php if ($model->mbtcAssessmentGroup->email_required == 1): ?>

                    <div class="col-12">
                        <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'Masukkan Alamat Email')])->label(Yii::t('app', 'Email')) ?>
                    </div>

                    <?php endif; ?>

                    <div class="col-12">

                    <hr>
                    </div>

                    <div class="col-12 ">
                        <?= $form->field($model, 'organization')->textInput(['placeholder' => Yii::t('app', 'Masukkan Nama Perusahaan')])->label(Yii::t('app', 'Perusahaan')) ?>
                    </div>

                    <div class="col-12 ">
                        <?= $form->field($model, 'work_unit')->textInput(['placeholder' => Yii::t('app', 'Masukkan Unit Kerja')])->label(Yii::t('app', 'Unit Kerja')) ?>
                    </div>

                    <div class="col-12 ">
                        <?= $form->field($model, 'position')->textInput(['placeholder' => Yii::t('app', 'Masukkan Jabatan')])->label(Yii::t('app', 'Jabatan')) ?>
                    </div>                    

                    <div class="col-12">
                        <?= $form->field($model, 'location')->textInput(['placeholder' => Yii::t('app', 'Masukkan Kota sebagai Lokasi')])->label(Yii::t('app', 'Lokasi')) ?>
                    </div>

                    <div class="col-12 mt-3 d-flex justify-content-end align-items-center">
                        <?= Html::submitButton(Yii::t('app', 'Lanjut'), ['class' => 'btn btn-success']) ?>                    
                    </div>

                    <?php ActiveForm::end(); ?>
                <?php else: ?>

                <h4 class="lead">There is no active assessment at this time.</h4>

                <?php endif; ?>
            </div>
        </div>
    </div>          
</div>