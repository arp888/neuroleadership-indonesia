<?php
namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

class CustomTab extends \kartik\tabs\TabsX
{
    public $headerWrapperOptions = [];
    public $headerToolbarOptions = [];
    public $contentWrapperOptions = [];

    // /**
    //  * Initializes the widget settings.
    //  * @throws InvalidConfigException
    //  */
    // public function initWidget()
    // {
    //     $this->pluginName = 'tabsX';
    //     $isBs4 = $this->isBs4();
    //     if (!isset($this->dropdownClass)) {
    //         $this->dropdownClass = $isBs4 ? 'kartik\bs4dropdown\Dropdown' : 'yii\bootstrap\Dropdown';
    //     }
    //     Html::addCssClass($this->containerOptions, 'tabs-x');
    //     if (empty($this->containerOptions['id'])) {
    //         $this->containerOptions['id'] = $this->options['id'] . '-container';
    //     }
    //     if (ArrayHelper::getValue($this->containerOptions, 'data-enable-cache', true) === false) {
    //         $this->containerOptions['data-enable-cache'] = "false";
    //     }
    //     $this->registerAssets();
    //     Html::addCssClass($this->options, ['nav', $this->navType]);
    //     Html::addCssClass($this->tabContentOptions, 'tab-content');
    //     if ($this->printable) {
    //         $this->addCssClass($this->options, self::BS_HIDDEN_PRINT);
    //     }
    //     $this->options['role'] = 'tablist';
    //     $css = static::getCss("tabs-{$this->position}", $this->position != null) .
    //         static::getCss("tab-align-{$this->align}", $this->align != null) .
    //         static::getCss("tab-bordered", $this->bordered) .
    //         static::getCss(
    //             "tab-sideways",
    //             $this->sideways && ($this->position == self::POS_LEFT || $this->position == self::POS_RIGHT)
    //         ) .
    //         static::getCss(
    //             "tab-height-{$this->height}",
    //             $this->height != null && ($this->position == self::POS_ABOVE || $this->position == self::POS_BELOW)
    //         ) .
    //         ' ' . ArrayHelper::getValue($this->pluginOptions, 'addCss', 'tabs-krajee');
    //     Html::addCssClass($this->containerOptions, $css);
    //     $this->addCssClass($this->printHeaderOptions, self::BS_VISIBLE_PRINT);
    // }

    /**
     * Renders tab items as specified in [[items]].
     *
     * @return string the rendering result.
     * @throws InvalidConfigException
     * @throws \Exception
     */
    protected function renderItems()
    {
        $headers = $panes = $labels = [];
        $isBs4 = $this->isBs4();
        if (!$this->hasActiveTab()) {
            $this->activateFirstVisibleTab();
        }

        foreach ($this->items as $n => $item) {
            if (!ArrayHelper::remove($item, 'visible', true)) {
                continue;
            }
            $label = $this->getLabel($item);
            $headerOptions = array_merge($this->headerOptions, ArrayHelper::getValue($item, 'headerOptions', []));
            $linkOptions = array_merge($this->linkOptions, ArrayHelper::getValue($item, 'linkOptions', []));
            $this->addCssClass($linkOptions, self::BS_NAV_LINK);

            if (isset($item['items'])) {
                foreach ($item['items'] as $subItem) {
                    $subLabel = $this->getLabel($subItem);
                    $labels[] = $this->printHeaderCrumbs ? $label . $this->printCrumbSeparator . $subLabel : $subLabel;
                }
                if (!$isBs4) {
                    $label .= ' <b class="caret"></b>';
                }
                Html::addCssClass($headerOptions, 'dropdown');
                if ($this->renderDropdown($n, $item['items'], $panes)) {
                    if ($isBs4) {
                        Html::addCssClass($linkOptions, 'active');
                    } else {
                        Html::addCssClass($headerOptions, 'active');
                    }
                }
                Html::addCssClass($linkOptions, 'dropdown-toggle');

                $linkOptions['data-toggle'] = 'dropdown';

                /**
                 * @var \yii\bootstrap\Dropdown $dropdownClass
                 */
                $dropdownClass = $this->dropdownClass;
                $header = Html::a($label, "#", $linkOptions) . "\n"
                    . $dropdownClass::widget([
                        'items' => $item['items'],
                        'clientOptions' => false,
                        'view' => $this->getView(),
                    ]);
            } else {
                $labels[] = $label;
                $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
                $options['id'] = ArrayHelper::getValue($options, 'id', $this->options['id'] . '-tab' . $n);
                $css = 'tab-pane';
                $isActive = ArrayHelper::remove($item, 'active');
                if ($this->fade) {
                    $css = $isActive ? [$css, 'fade', $this->getCssClass(self::BS_SHOW)] : [$css, 'fade'];
                }
                Html::addCssClass($options, $css);
                if ($isActive) {
                    if ($isBs4) {
                        Html::addCssClass($linkOptions, 'active');
                        $css = ['active', 'show'];
                    } else {
                        Html::addCssClass($headerOptions, 'active');
                        $css = 'active';
                    }
                    Html::addCssClass($options, $css);
                }
                if (isset($item['url'])) {
                    $header = Html::a($label, $item['url'], $linkOptions);
                } else {
                    $linkOptions['data-toggle'] = 'tab';
                    $linkOptions['role'] = 'tab';
                    if (!isset($linkOptions['aria-selected'])) {
                        $linkOptions['aria-selected'] = 'false';
                    }
                    $linkOptions['aria-controls'] = $options['id'];
                    $header = Html::a($label, '#' . $options['id'], $linkOptions);
                }
                if ($this->renderTabContent) {
                    $tag = ArrayHelper::remove($options, 'tag', 'div');
                    $panes[] = Html::tag($tag, isset($item['content']) ? $item['content'] : '', $options);
                }
            }
            $this->addCssClass($headerOptions, self::BS_NAV_ITEM);
            $headers[] = Html::tag('li', $header, $headerOptions);
        }

        $outHeader = Html::beginTag('div', $this->headerWrapperOptions);
        $outHeader .= Html::beginTag('div', $this->headerToolbarOptions);

        $outHeader .= Html::tag('ul', implode("\n", $headers), $this->options);
        $outHeader .= Html::endTag('div');
        $outHeader .= Html::endTag('div');

        if ($this->renderTabContent) {
            Html::addCssClass($this->tabContentOptions, static::getCss('printable', $this->printable));
            $outPane = Html::beginTag('div', $this->contentWrapperOptions);
            $outPane .= Html::beginTag('div', $this->tabContentOptions);
            foreach ($panes as $i => $pane) {
                if ($this->printable) {
                    $outPane .= Html::tag('div', ArrayHelper::getValue($labels, $i), $this->printHeaderOptions) . "\n";
                }
                $outPane .= "$pane\n";
            }
            $outPane .= Html::endTag('div');
            $outPane .= Html::endTag('div');
            $tabs = $this->position == self::POS_BELOW ? $outPane . "\n" . $outHeader : $outHeader . "\n" . $outPane;
        } else {
            $tabs = $outHeader;
        }
        return Html::tag('div', $tabs, $this->containerOptions);
    }

    /**
     * Registers the assets for [[TabsX]] widget.
     */
    public function registerAssets()
    {
        $view = $this->getView();
        \kartik\tabs\TabsXAsset::registerBundle($view, $this->bsVersion);
        if ($this->printable) {
            $view->registerCss('@media print{.tab-content.printable > .tab-pane{display:block;opacity:1;}}');
        }
        $id = 'jQuery("#' . $this->containerOptions['id'] . '")';
        $this->registerPlugin($this->pluginName, $id);
        if ($this->enableStickyTabs) {
            \kartik\tabs\StickyTabsAsset::register($view);
            $opts = Json::encode($this->stickyTabsOptions);
            $id = 'jQuery("#' . $this->containerOptions['id'] . '")';
            $view->registerJs("{$id}.find('.nav').stickyTabs({$opts});");
        }
    }
}
