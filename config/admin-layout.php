<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\assets\BackEndAsset;

BackEndAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed-left">
<?php $this->beginBody() ?>

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <?= Html::tag('div', Html::a(Html::img('@web/images/logo-nli-img-white.png', ['height' => 70, 'class' => 'logo-sm']) . '<span>' . Html::img('@web/images/logo-nli-txt-white.png', ['height' => 70, 'class' => 'logo-sm']) . '</span>', ['/administrator/default'],['class' => 'logo']), ['class' => 'logo']) ?>
                <!-- <a href="index.html" class="logo"><i class="mdi mdi-radar"></i> <span>Minton</span></a> -->
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <nav class="navbar-custom">

            <ul class="list-inline float-right mb-0">

                <li class="list-inline-item notification-list hide-phone">
                    <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                        <i class="mdi mdi-crop-free noti-icon"></i>
                    </a>
                </li>

                <?php if (!Yii::$app->user->isGuest): ?>

                <?php 
                    $user = \app\modules\administrator\models\UserProfile::find()->where(['id' => Yii::$app->user->identity->id])->one();
                    $avatar = $user->getImageUrl();
                    $name = $user->fullname;                         
                ?>

                <li class="list-inline-item dropdown notification-list">
                    <?= Html::a(Html::img($avatar, ['class' => 'rounded-circle', 'alt' => $name]), '#', [
                           'class' => 'nav-link dropdown-toggle waves-effect nav-user', 'data-toggle' => 'dropdown', 'role' => 'button',
                           'aria-haspopup' => false, 'aria-expanded' => false, 
                        ]) 
                    ?>    

                    <div class="dropdown-menu dropdown-menu-right profile-dropdown" aria-labelledby="Preview" style="width:220px">
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Welcome! <?= $name ?></small> </h5>
                        </div>
                        <?= Html::a('<i class="mdi mdi-account"></i> <span> Profil Pengguna</span>', ['/administrator/user/user-profile', 'id' => Yii::$app->user->identity->id], ['class' => 'dropdown-item notify-item', 'data' => ['method' => 'post']]) ?>
                        <?= Html::a('<i class="mdi mdi-logout"></i> <span> Logout</span>', ['/administrator/user/logout'], ['class' => 'dropdown-item notify-item', 'data' => ['method' => 'post']]) ?>
                    </div>
                </li>

                <?php endif; ?>           
            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-light waves-effect">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
                <li class="hide-phone app-search">
                    <form role="search" class="">
                        <input type="text" placeholder="Search..." class="form-control">
                        <a href=""><i class="fa fa-search"></i></a>
                    </form>
                </li>
            </ul>

        </nav>

    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <div id="sidebar-menu">
               
                 <?php 
                    $nav_items = [
                        [
                            'label' => 'Menu Utama', 
                            'url' => null, 
                            'options' => ['class' => 'menu-title'],
                        ],
                        [
                            'label' => '<i class="ti-dashboard"></i> Dashboard', 
                            'url' => ['/administrator/default/index'], 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/default/index' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],

                        [
                            'label' => '<i class="ti-desktop"></i> Home Slider', 
                            'url' => ['/administrator/slider/index'], 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/slider/index' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],                        

                        // [
                        //     'label' => '<i class="ti-folder"></i>Blogs', 
                        //     'options' => ['class' => 'has-submenu'],
                        //     'url' => ['/administrator/blog/index'], 
                        // ],

                        // [
                        //     'label' => '<i class="ti-star"></i>Team', 
                        //     'options' => ['class' => 'has-submenu'],
                        //     'url' => ['/administrator/team/index'], 
                        // ],             

                        [
                            'label' => '<i class="ti-world"></i> About NLI Pages <span class="menu-arrow">', 
                            'options' => ['class' => 'has-sub'],
                            'url' => 'javascript:void(0);', 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/why/index' || $this->context->route == 'administrator/team/index' || $this->context->route == 'administrator/pages/index' ? ' active' : null) . '" href="{url}">{label}</a>',                            // 'template' => '<a class="waves-effect waves-primary" href="{url}">{label}</a>',
                            'submenuTemplate' => "<ul class='list-unstyled' style=" . ($this->context->route == 'administrator/why/index' || $this->context->route == 'administrator/team/index' || $this->context->route == 'administrator/pages/index' ? 'display:block' : null) . ">{items}</ul>", 
                            'items' => [
                                [
                                    'label' => 'Why Page',
                                    'url' =>  ['/administrator/why/index'],
                                ],
                                [
                                    'label' => 'Team',
                                    'url' =>  ['/administrator/team/index'],
                                ],
                                [
                                    'label' => 'General Info',
                                    'url' =>  ['/administrator/pages/index'],
                                ],
                            ]
                        ],           

                        [
                            'label' => '<i class="ti-dashboard"></i> NLI Activity <span class="menu-arrow">', 
                            'options' => ['class' => 'has-sub'],
                            'url' => 'javascript:void(0);', 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/training-and-development/index' || $this->context->route == 'administrator/research-and-development/index' || $this->context->route == 'administrator/pages/index' ? ' active' : null) . '" href="{url}">{label}</a>',                            // 'template' => '<a class="waves-effect waves-primary" href="{url}">{label}</a>',
                            'submenuTemplate' => "<ul class='list-unstyled' style=" . ($this->context->route == 'administrator/training-and-development/index' || $this->context->route == 'administrator/research-and-development/index' || $this->context->route == 'administrator/pages/index' ? 'display:block' : null) . ">{items}</ul>", 
                            'items' => [
                                [
                                    'label' => 'Training & Development',
                                    'url' =>  ['/administrator/training-and-development/index'],
                                ],
                                [
                                    'label' => 'Research & Development',
                                    'url' =>  ['/administrator/research-and-development/index'],
                                ],
                                [
                                    'label' => 'General Info',
                                    'url' =>  ['/administrator/pages/index'],
                                ],
                            ]
                        ],

                        [
                            'label' => '<i class="ti-book"></i> Blog & Category <span class="menu-arrow">', 
                            'options' => ['class' => 'has-sub'],
                            'url' => 'javascript:void(0);', 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/blog/index' || $this->context->route == 'administrator/category/index'  ? ' active' : null) . '" href="{url}">{label}</a>',
                            'submenuTemplate' => "<ul class='list-unstyled' style=" . ($this->context->route == 'administrator/blog/index' || $this->context->route == 'administrator/category/index' ? 'display:block' : null) . ">{items}</ul>", 
                            'items' => [
                                [
                                    'label' => 'Blog',
                                    'url' =>  ['/administrator/blog/index'],
                                ],    
                                [
                                    'label' => 'Category', 
                                    'url' => ['/administrator/category/index']
                                ],
                            ]
                        ],

                        [
                            'label' => '<i class="ti-layers-alt"></i> Event & Registration <span class="menu-arrow"></span>', 
                            'options' => ['class' => 'has-sub'],
                            'url' => 'javascript:void(0);', 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/event/index' || $this->context->route == 'administrator/event-registration/index'  ? ' active' : null) . '" href="{url}">{label}</a>',
                            'submenuTemplate' => "<ul class='list-unstyled' style=" . ($this->context->route == 'administrator/event/index' || $this->context->route == 'administrator/event-registration/index' ? 'display:block' : null) . ">{items}</ul>", 
                            'items' => [
                                [
                                    'label' => 'Event',
                                    'url' =>  ['/administrator/event/index'],
                                ],
                                [
                                    'label' => 'Registration',
                                    'url' =>  ['/administrator/event-registration/index'],
                                ],
                            ]
                        ],

                        [
                            'label' => '<i class="ti-book"></i> Book Product <span class="menu-arrow">', 
                            'options' => ['class' => 'has-sub'],
                            'url' => 'javascript:void(0);', 
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/book/index' || $this->context->route == 'administrator/book-order/index'  ? ' active' : null) . '" href="{url}">{label}</a>',
                            'submenuTemplate' => "<ul class='list-unstyled' style=" . ($this->context->route == 'administrator/book/index' || $this->context->route == 'administrator/book-order/index' ? 'display:block' : null) . ">{items}</ul>", 
                            'items' => [
                                [
                                    'label' => 'Book List',
                                    'url' =>  ['/administrator/book/index'],
                                ],    
                                [
                                    'label' => 'Book Order List', 
                                    'url' => ['/administrator/book-order/index']
                                ],
                            ]
                        ],

                        [
                            'label' => '<i class="ti-image"></i> Image Manager', 
                            // 'options' => ['class' => 'has-submenu'],
                            'url' => ['/imagemanager/manager/index'],
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'imagemanager/manager/index' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],                        

                        [
                            'label' => '<i class="ti-user"></i> User Management', 
                            // 'options' => ['class' => 'has-submenu'],
                            'url' => ['/administrator/user/user-index'],
                            'template' => '<a class="waves-effect waves-primary'. ($this->context->route == 'administrator/user/user-index' ? ' active' : null) . '" href="{url}">{label}</a>',
                        ],                                
                    ];

                    echo Menu::widget([
                        'items' => $nav_items,
                        // 'options' => ['class' => 'navigation-menu'],
                        'submenuTemplate' => "<ul class='list-unstyled'>{items}</ul>",
                        'encodeLabels' => false,
                        'activateParents' => true,
                    ]);                    
                ?>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->                      
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title"><?= $this->title ?></h4>                                              
                            <?= Breadcrumbs::widget([
                                'options' => ['class' => 'breadcrumb float-right'],
                                'tag' => 'ol',
                                'itemTemplate' => "<li class=\"breadcrumb-item\">{link}</li>\n",
                                'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                                'homeLink' => ['label' => 'Home', 'url' => ['/administrator/']],
                                'encodeLabels' => false,
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]) ?>

                            <div class="clearfix"></div>                            
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-12">
                        <?= $content ?>
                    </div>
                </div>
            </div> <!-- end container -->
            <!-- end container -->
        </div>
        <!-- end content -->

        <footer class="footer">
            <?= date('Y') ?> © NeuroLeadership Indonesia
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <div class="">
            <ul class="nav nav-tabs tabs-bordered nav-justified">
                <li class="nav-item">
                    <a href="#home-2" class="nav-link active" data-toggle="tab" aria-expanded="false">
                        Activity
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
                        Settings
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="home-2">
                    <div class="timeline-2">
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">5 minutes ago</small>
                                <p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong></p>
                            </div>
                        </div>

                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">30 minutes ago</small>
                                <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                                <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                            </div>
                        </div>

                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">59 minutes ago</small>
                                <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                                <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                            </div>
                        </div>

                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">1 hour ago</small>
                                <p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
                            </div>
                        </div>

                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">3 hours ago</small>
                                <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                                <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                            </div>
                        </div>

                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">5 hours ago</small>
                                <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                                <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane" id="messages-2">

                    <div class="row m-t-20">
                        <div class="col-8">
                            <h5 class="m-0 font-15">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-8">
                            <h5 class="m-0 font-15">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-8">
                            <h5 class="m-0 font-15">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-8">
                            <h5 class="m-0 font-15">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->
<script>
    var resizefunc = [];
</script>

<?php 
$this->registerJsFile('@web/admin-assets/js/modernizr.min.js', ['depends'=> [], 'position' => \yii\web\View::POS_HEAD]);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
