<?php
use kartik\datecontrol\Module;

return [
    'adminEmail' => 'admin@leadership.id',
    'infoEmail' => 'info@leadership.id',
    'storeEmail' => 'store@leadership.id',
    'bsVersion' => '4.x',
    // format settings for displaying each date attribute (ICU format example)
    'dateControlDisplay' => [
        Module::FORMAT_DATE => 'dd-MM-yyyy',
        Module::FORMAT_TIME => 'hh:mm:ss a',
        Module::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a', 
    ],    
    // format settings for saving each date attribute (PHP format example)
    'dateControlSave' => [
        Module::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
        Module::FORMAT_TIME => 'php:H:i:s',
        Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
    ],
    'statusInactive' => 0,
    'statusActive' => 10,
    'languages' => [
        'id' => 'Indonesia',
        'en' => 'English',
    ], 
];
