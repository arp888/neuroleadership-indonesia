<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=neuroleadership_ind',
    'username' => 'root',
    'password' => '',

    // 'dsn' => 'mysql:host=localhost;dbname=nli_app',
    // 'username' => 'nli_admin',
    // 'password' => 'N3uroL34dersh!P',

    // 'dsn' => 'mysql:host=localhost;dbname=leaa6554_neuroleadership',
    // 'username' => 'leaa6554_root',
    // 'password' => 'n3urol3ad3rship',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    // 'enableSchemaCache' => true,
    // 'schemaCacheDuration' => 60,
    // 'schemaCache' => 'cache',
];
