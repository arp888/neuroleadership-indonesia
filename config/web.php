<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'NeuroLeadership Indonesia',
    'timeZone' => 'Asia/Jakarta',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    // 'on beforeRequest' => function ($event) {
    //     if(!Yii::$app->request->isSecureConnection){
    //         $url = Yii::$app->request->getAbsoluteUrl();
    //         $url = str_replace('http:', 'https:', $url);
    //         Yii::$app->getResponse()->redirect($url);
    //         Yii::$app->end();
    //     }
    // },
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@mdm/admin' => '@app/modules/user-management',
    ],    
    'modules' => [
        'user-management' => [
            'class' => 'mdm\admin\Module',
            // 'layout' => '@app/modules/scoreboard/views/layouts/scoreboard-layout.php',
            'layout' => 'user-management-layout',
        ],        
        'assessment' => [
            'class' => 'app\modules\assessment\Module',
            'layout' => 'assessment-layout',
        ],
        'scoreboard' => [
            'class' => 'app\modules\scoreboard\Module',
            'layout' => 'scoreboard-layout',
        ],
        'coaching-implementation' => [
            'class' => 'app\modules\coaching_implementation\Module',
            'layout' => 'coaching-implementation-layout',
        ],        
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],        
        'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',
     
            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'dd-MM-yyyy',
                \kartik\datecontrol\Module::FORMAT_TIME => 'hh:mm:ss a',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a', 
            ],
            
            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
                \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
     
            // set your display timezone
            'displayTimezone' => 'Asia/Jakarta',
     
            // set your timezone for date saved to db
            'saveTimezone' => 'UTC',
            
            // automatically use kartik\widgets for each of the above formats
            'autoWidget' => true,
     
            // default settings for each widget from kartik\widgets used when autoWidget is true
            'autoWidgetSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true]], // example
                \kartik\datecontrol\Module::FORMAT_DATETIME => [], // setup if needed
                \kartik\datecontrol\Module::FORMAT_TIME => [], // setup if needed
            ],
            
            // custom widget settings that will be used to render the date input instead of kartik\widgets,
            // this will be used when autoWidget is set to false at module or widget level.
            'widgetSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => [
                    'class' => 'yii\jui\DatePicker', // example
                    'options' => [
                        'dateFormat' => 'php:d-M-Y',
                        'options' => ['class'=>'form-control'],
                    ]
                ]
            ]
            // other settings
        ],        
    ],
    'controllerMap' => [
        'sort' => [
            'class' => 'arogachev\sortable\controllers\SortController',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'F0iT0FUrfmNXlCiI0lPmAi6mpTY8tovi',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            // 'identityClass' => 'app\modules\administrator\models\User',
            'enableAutoLogin' => false,
            'identityClass' => 'mdm\admin\models\User',
            'loginUrl' => ['/user-management/user/login'],
            // 'enableAutoLogin' => true,
        ],

        'externalUser'=> [
            'class'=>'yii\web\User',
            'identityClass' => 'app\models\ExternalUser',
            'enableAutoLogin' => false,
            'authTimeout' => 60*30,
            'loginUrl' => ['bsj-coaching/login'],
            'identityCookie' => ['name' => 'module_external_user', 'httpOnly' => true],
            'idParam' => 'external_user_id', //this is important !
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'i18n' => [
            'translations' => [
                '*' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    // 'class' => 'yii\i18n\DbMessageSource',
                    'basePath'       => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en',
                    // 'fileMap'        => [
                        //'main' => 'main.php',
                    // ],
                ],
            ],
        ],
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',           
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [                                
                'mbtc/step-one/<id:\d+>' => 'mbtc/mbtc-assessment-step-one',
                'mbtc/step-two/<id:\d+>' => 'mbtc/mbtc-assessment-step-two',
                'mbtc/step-three/<id:\d+>' => 'mbtc/mbtc-assessment-step-three',
                'mbtc/step-four/<id:\d+>' => 'mbtc/mbtc-assessment-step-four',
                'mbtc/result/<id:\d+>' => 'mbtc/mbtc-assessment-result',
                'mbtc/<id:\d+>' => 'mbtc/assessment',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'defaultRoute' => '/site/index',
            ],
        ],
    
        'html2pdf' => [
            'class' => 'yii2tech\html2pdf\Manager',
            'viewPath' => '@app/views/pdf',
            'converter' => 'dompdf',
        ],

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'locale' => 'id',
            'thousandSeparator' => ".",
            'decimalSeparator' => ",",
            'currencyCode' => 'Rp ',
            'nullDisplay' => '-',
        ],        
        'appHelper' => [
            'class' => 'app\components\AppHelper',
        ],
    ],
    // 'on beforeRequest' => function ($event) {
    //     Yii::$app->language = Yii::$app->session->get('language', 'id');
    // },
    'params' => $params,
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            // '*',
            // 'admin/*',
            // 'site/*',
            'bsj-coaching/*',
            'mbtc-bi/*',
            'mbtc/*',
            'user/request-password-reset',
            'user/reset-password',
            'datecontrol/parse/convert',
            'assessment/mbtc-assessment/view',
            // // 'debug/*',
            // 'gii/*',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '37.44.244.220'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class' => 'app\templates\templates\crud\Generator',
                'templates' => [
                    'templates' => '@app/templates/templates/crud/my-template',
                ]
            ],
            'model' => [
                'class' => 'app\templates\templates\model\Generator',
                'templates' => [
                    'templates' => '@app/templates/templates/model/my-template'
                ]
            ],            

        ],
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
