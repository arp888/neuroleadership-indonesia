<?php
 
namespace app\commands;
 
use Yii;
use yii\console\Controller; 
use app\components\helpers\SendReportToEmail;
// use app\components\helpers\ParticipantThankYouEmail;
// use app\components\helpers\InstructorNotificationEmail;
// use app\components\helpers\ApprovalRequestNotificationEmail;
// use app\components\helpers\TrainingSubmissionRequestApprovalNotificationEmail;
// use app\components\helpers\TrainingSubmissionRequestRejectionNotificationEmail;
// use app\components\helpers\InstructorTrainingRescheduleNotificationEmail;
// use app\components\helpers\ParticipantTrainingRescheduleNotificationEmail;
// use app\components\helpers\InstructorTrainingCancelNotificationEmail;
// use app\components\helpers\ParticipantTrainingCancelNotificationEmail;
// use app\components\helpers\InstructorThankYouEmail;
// use app\components\helpers\TrainingApprovalRequestNotificationEmail;
// use app\components\helpers\TrainingApprovalResponseNotificationEmail;
 
class MailQueueController extends Controller
{
    public function actionSendReportToEmail($groupId, $respondentId, $userId){
        $jobId = Yii::$app->queue->priority(20)->push(new SendReportToEmail([
            'groupId' => $groupId,
            'respondentId' => $respondentId,
            'userId' => $userId,
        ]));
    }  
    
}