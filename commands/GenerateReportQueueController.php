<?php
 
namespace app\commands;
 
use Yii;
use yii\console\Controller; 
use app\components\helpers\GenerateReport;
// use app\components\helpers\GenerateReportInd;
 
class GenerateReportQueueController extends Controller
{
    public function actionGenerateReport($id, $userId, $lang){
        $jobId = Yii::$app->queue->priority(10)->push(new GenerateReport([
            'respondentId' => $id,
            'userId' => $userId,
            'lang' => $lang,
        ]));
    }

    // public function actionGenerateReportInd($id, $userId){
    //     $jobId = Yii::$app->queue->priority(10)->delay(1 * 60)->push(new GenerateReportInd([
    //         'respondentId' => $id,
    //         'userId' => $userId,
    //     ]));
    // }
}