<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'NeuroLeadership Indonesia',
    'timeZone' => 'Asia/Jakarta',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],    
    'modules' => [
        'administrator' => [
            'class' => 'app\modules\administrator\Administrator',
            'layout' => 'admin-layout',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'imagemanager' => [
            'class' => 'noam148\imagemanager\Module',
            //set accces rules ()
            'canUploadImage' => true,
            'canRemoveImage' => function(){
                return true;
            },
            'layout' => '@app/modules/administrator/views/layouts/admin-layout.php',
            'deleteOriginalAfterEdit' => false, // false: keep original image after edit. true: delete original image after edit
            // Set if blameable behavior is used, if it is, callable function can also be used
            'setBlameableBehavior' => false,
            //add css files (to use in media manage selector iframe)
            'cssFiles' => [
                'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
            ],
        ],
        'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',
     
            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'dd-MM-yyyy',
                \kartik\datecontrol\Module::FORMAT_TIME => 'hh:mm:ss a',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a', 
            ],
            
            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
                \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
     
            // set your display timezone
            'displayTimezone' => 'Asia/Jakarta',
     
            // set your timezone for date saved to db
            'saveTimezone' => 'UTC',
            
            // automatically use kartik\widgets for each of the above formats
            'autoWidget' => true,
     
            // default settings for each widget from kartik\widgets used when autoWidget is true
            'autoWidgetSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true]], // example
                \kartik\datecontrol\Module::FORMAT_DATETIME => [], // setup if needed
                \kartik\datecontrol\Module::FORMAT_TIME => [], // setup if needed
            ],
            
            // custom widget settings that will be used to render the date input instead of kartik\widgets,
            // this will be used when autoWidget is set to false at module or widget level.
            'widgetSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => [
                    'class' => 'yii\jui\DatePicker', // example
                    'options' => [
                        'dateFormat' => 'php:d-M-Y',
                        'options' => ['class'=>'form-control'],
                    ]
                ]
            ]
            // other settings
        ],
        'social' => [
            // the module class
            'class' => 'kartik\social\Module',

            // the global settings for the disqus widget
            'disqus' => [
                'settings' => ['shortname' => 'DISQUS_SHORTNAME'] // default settings
            ],

            // the global settings for the facebook plugins widget
            'facebook' => [
                'app_id' => 'FACEBOOK_APP_ID',
                'app_secret' => 'FACEBOOK_APP_SECRET',
            ],

            // the global settings for the google plugins widget
            'google' => [
                'clientId' => 'GOOGLE_API_CLIENT_ID',
                'pageId' => 'GOOGLE_PLUS_PAGE_ID',
                'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
            ],

            // the global settings for the google analytic plugin widget
            'googleAnalytics' => [
                'id' => '',
                'domain' => '',
            ],
            
            // the global settings for the twitter plugins widget
            'twitter' => [
                'screenName' => 'TWITTER_SCREEN_NAME'
            ],
        ],
    ],
    'controllerMap' => [
        'sort' => [
            'class' => 'arogachev\sortable\controllers\SortController',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'F0iT0FUrfmNXlCiI0lPmAi6mpTY8tovi',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\administrator\models\User',
            // 'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            // 'enablePrettyUrl' => true,
            // 'showScriptName' => false,
            // 'rules' => [
            // ],
            'class' => 'yii\web\UrlManager',           
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [                
                'home' => 'site/index',
                'about'=>'site/about',
                'why-neuroleadership'=>'site/why-neuroleadership',
                'team' => 'site/team',
                'training-and-development' => 'site/training-and-development',
                'research-and-development' => 'site/research-and-development',                
                'publication-and-forum' => 'site/publication-and-forum',
                'book-order/<id:\d+>/<slug:[\w\-]+>' => 'site/book-order',
                'event' => 'site/event',
                'event-details/<id:\d+>' => 'site/event-details',
                'event-registration/<id:\d+>' => 'site/event-registration',
                'blog' => 'site/blog',
                'blog-detail/<slug:[\w\-]+>' => 'site/blog-detail',
                'contact' => 'site/contact',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'defaultRoute' => '/site/index',
                // '<action:(.*)>' => 'site/<action>',              
            ],
        ],

        'view' => [
            'class' => 'daxslab\taggedview\View',
            //configure some default values that will be shared by all the pages of the website
            //if they are not overwritten by the page itself
            'image' => 'https://leadership.id/images/brain-synapses-firing.jpg',
            'generator' => 'NeuroLeadership merupakan perpaduan interdisiplin bidang ilmu sifat-sifat neuron otak berkaitan dengan praktek-praktek pada Kepemimpinan dan Manajemen.',
        ],

        
        // 'assetManager' => [
        //     'bundles' => [
        //         'yii2mod\alert\AlertAsset' => [
        //             'css' => [
        //                 'dist/sweetalert.css',
        //                 // 'themes/google/google.css',
        //             ]
        //         ],
        //     ],
        // ]
        'imagemanager' => [
            'class' => 'noam148\imagemanager\components\ImageManagerGetPath',
            'mediaPath' => 'images/imagemanager',
            'cachePath' => 'assets/images',
            'useFilename' => true,
            'absoluteUrl' => true,
            'databaseComponent' => 'db'
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'locale' => 'id',
            'thousandSeparator' => ".",
            'decimalSeparator' => ",",
            'currencyCode' => 'Rp ',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
