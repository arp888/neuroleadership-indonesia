<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hbdi_assessment".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $position
 * @property string $organization
 * @property string $location
 * @property int $date
 * @property string $filename
 * @property int $is_completed
 *
 * @property HbdiAssessmentDetail[] $hbdiAssessmentDetails
 */
class MbtcAssessment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hbdi_assessment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'organization', 'location'], 'required'],
            [['date', 'is_completed'], 'integer'],
            [['name', 'position', 'organization'], 'string', 'max' => 150],
            ['email', 'email', 'message' => 'Format email salah.'],
            [['phone'], 'string', 'max' => 50],

            [['is_completed'], 'default', 'value' => 0],

            [['location', 'filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'position' => Yii::t('app', 'Position'),
            'organization' => Yii::t('app', 'Organization'),
            'location' => Yii::t('app', 'Location'),
            'date' => Yii::t('app', 'Date'),
            'filename' => Yii::t('app', 'File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcAssessmentDetails()
    {
        return $this->hasMany(\app\modules\administrator\models\MbtcAssessmentDetail::className(), ['hbdi_assessment_id' => 'id']);
    }
}
