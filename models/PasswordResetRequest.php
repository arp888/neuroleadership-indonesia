<?php
namespace app\models;

use Yii;
use app\models\ExternalUser;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequest extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $class = Yii::$app->externalUser->identityClass ? : 'app\models\ExternalUser';
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => 'app\models\ExternalUser',
                'filter' => ['status' => 10],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {        
        $mail_setting = \app\models\MailSetting::find()->one();
                
        Yii::$app->mailer->setTransport([
            'class' => 'Swift_SmtpTransport',
            'host' => $mail_setting->smtp_host, 
            'username' => $mail_setting->smtp_username,
            'password' => $mail_setting->smtp_pass,
            'port' => $mail_setting->smtp_port,
            'encryption' => $mail_setting->smtp_encryption,
            'streamOptions' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ],
        ]);

        /* @var $user User */
        $class = Yii::$app->externalUser->identityClass ? : 'app\models\ExternalUser';
        $user = $class::findOne([
            'status' => 10,
            'email' => $this->email,
        ]);

        // var_dump($user); die(); 

        if ($user) {
            if (!ResetPassword::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
            }

            if ($user->save()) {

                $participant = \app\modules\coaching_implementation\models\CoachingParticipant::findOne(['user_id' => $user->id]);

                $mail_setting = \app\models\MailSetting::find()->one();
                

                $message = Yii::$app->mailer->compose(['html' => 'passwordResetToken-html'], [
                    'participant_name' => $participant->name, 'user' => $user                        
                ]);

                $message->setFrom([$mail_setting->smtp_username => 'NeuroLeadership Indonesia'])
                    ->setTo($this->email)
                    ->setSubject('Password Reset for ' . $participant->name);


                return $message->send();
            }
        }

        return false;
    }
}
