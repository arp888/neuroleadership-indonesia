<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ec_provinces".
 *
 * @property int $prov_id
 * @property string $prov_name
 * @property int $locationid
 * @property int $status
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ec_provinces';
    }
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['locationid', 'status'], 'integer'],
            [['prov_name'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prov_id' => Yii::t('app', 'Prov'),
            'prov_name' => Yii::t('app', 'Prov Name'),
            'locationid' => Yii::t('app', 'Locationid'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
