<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ec_cities".
 *
 * @property int $city_id
 * @property string $city_name
 * @property int $prov_id
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ec_cities';
    }
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prov_id'], 'integer'],
            [['city_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'city_id' => Yii::t('app', 'City'),
            'city_name' => Yii::t('app', 'City Name'),
            'prov_id' => Yii::t('app', 'Prov'),
        ];
    }
}
