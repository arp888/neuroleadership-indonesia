<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ec_postalcode".
 *
 * @property int $postal_id
 * @property int $subdis_id
 * @property int $dis_id
 * @property int $city_id
 * @property int $prov_id
 * @property int $postal_code
 */
class Postalcode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ec_postalcode';
    }
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdis_id', 'dis_id', 'city_id', 'prov_id', 'postal_code'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'postal_id' => Yii::t('app', 'Postal'),
            'subdis_id' => Yii::t('app', 'Subdis'),
            'dis_id' => Yii::t('app', 'Dis'),
            'city_id' => Yii::t('app', 'City'),
            'prov_id' => Yii::t('app', 'Prov'),
            'postal_code' => Yii::t('app', 'Postal Code'),
        ];
    }
}
