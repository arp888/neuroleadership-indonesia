<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ec_subdistricts".
 *
 * @property int $subdis_id
 * @property string $subdis_name
 * @property int $dis_id
 */
class Subdistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ec_subdistricts';
    }
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dis_id'], 'integer'],
            [['subdis_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subdis_id' => Yii::t('app', 'Subdis'),
            'subdis_name' => Yii::t('app', 'Subdis Name'),
            'dis_id' => Yii::t('app', 'Dis'),
        ];
    }
}
