<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hbdi_assessment_detail".
 *
 * @property int $id
 * @property int $hbdi_assessment_id
 * @property int $hbdi_item_id
 * @property double $score
 *
 * @property HbdiAssessment $hbdiAssessment
 * @property HbdiItem $hbdiItem
 */
class MbtcAssessmentDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hbdi_assessment_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hbdi_assessment_id', 'hbdi_item_id'], 'required'],
            [['hbdi_assessment_id', 'hbdi_item_id'], 'integer'],
            [['score'], 'number'],
            [['hbdi_assessment_id'], 'exist', 'skipOnError' => true, 'targetClass' => MbtcAssessment::className(), 'targetAttribute' => ['hbdi_assessment_id' => 'id']],
            [['hbdi_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\modules\administrator\models\HbdiItem::className(), 'targetAttribute' => ['hbdi_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hbdi_assessment_id' => Yii::t('app', 'Hbdi Assessment ID'),
            'hbdi_item_id' => Yii::t('app', 'Hbdi Item ID'),
            'score' => Yii::t('app', 'Score'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMbtcAssessment()
    {
        return $this->hasOne(MbtcAssessment::className(), ['id' => 'hbdi_assessment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHbdiItem()
    {
        return $this->hasOne(\app\modules\administrator\models\HbdiItem::className(), ['id' => 'hbdi_item_id']);
    }
}
