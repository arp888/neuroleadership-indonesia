<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mail_setting".
 *
 * @property int $id
 * @property string $smtp_host
 * @property string $smtp_username
 * @property string $smtp_pass
 * @property string $smtp_port
 * @property string $smtp_encryption
 * @property int $updated_at
 * @property int $updated_by
 */
class MailSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mail_setting';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => false,
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['smtp_host', 'smtp_username', 'smtp_pass', 'smtp_port', 'smtp_encryption'], 'required'],
            [['updated_at', 'updated_by'], 'integer'],
            [['smtp_host', 'smtp_username', 'smtp_pass'], 'string', 'max' => 64],
            [['smtp_port', 'smtp_encryption'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'smtp_host' => Yii::t('app', 'Smtp Host'),
            'smtp_username' => Yii::t('app', 'Smtp Username'),
            'smtp_pass' => Yii::t('app', 'Smtp Pass'),
            'smtp_port' => Yii::t('app', 'Smtp Port'),
            'smtp_encryption' => Yii::t('app', 'Smtp Encryption'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
