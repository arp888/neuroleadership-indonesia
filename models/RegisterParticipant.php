<?php
namespace app\modules\lms\models;

use Yii;
use app\models\ExternalUser;
use yii\base\Model;

/**
 * Signup form
 */
class RegisterParticipant extends Model
{
    public $username;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            [['username'], 'unique', 'targetClass' => 'app\models\ExternalUser', 'message' => Yii::t('app', 'This username has already been taken by other user.')],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [['email'], 'unique', 'targetClass' => 'app\models\ExternalUser', 'message' => Yii::t('app', 'This email has already been taken by other user.')],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * register participant.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function registerParticipant()
    {
        if ($this->validate()) {
            $user = new ExternalUser();
            $user->username = $this->username;
            $user->email = $this->email;   
            $user->setPassword($this->password);     
            $user->generateAuthKey();
            $user->generatePasswordResetToken();
            if ($user->save()) {
                return $user;                
            }
        }
        return null;
    }
}
