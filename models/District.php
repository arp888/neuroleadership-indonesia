<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ec_districts".
 *
 * @property int $dis_id
 * @property string $dis_name
 * @property int $city_id
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ec_districts';
    }
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['dis_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dis_id' => Yii::t('app', 'Dis'),
            'dis_name' => Yii::t('app', 'Dis Name'),
            'city_id' => Yii::t('app', 'City'),
        ];
    }
}
